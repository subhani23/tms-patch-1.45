<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>FCMS__Changed_Password_Email_Alert</fullName>
        <description>Changed Password Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FCMS__CMS/FCMS__Changed_Password</template>
    </alerts>
    <alerts>
        <fullName>FCMS__Forgot_password_email_alert</fullName>
        <description>Forgot password email alert</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FCMS__CMS/FCMS__Forgot_Username_or_Password</template>
    </alerts>
    <alerts>
        <fullName>FCMS__Notify_Profile_Manager_for_approval_on_creation_of_a_portal_user</fullName>
        <description>Notify Profile Manager for approval on creation of a portal user</description>
        <protected>false</protected>
        <recipients>
            <field>FCMS__Profile_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FCMS__CMS/FCMS__Notify_Profile_Manager</template>
    </alerts>
    <alerts>
        <fullName>FCMS__Pass_Login_Email_Alert</fullName>
        <description>Pass &amp; Login Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FCMS__CMS/FCMS__Email_Pass_Login</template>
    </alerts>
    <alerts>
        <fullName>FCMS__Portal_User_creation_mail_notification</fullName>
        <description>Portal User creation mail notification</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FCMS__CMS/FCMS__User_Created</template>
    </alerts>
    <fieldUpdates>
        <fullName>Add_Employee_in_the_Payroll</fullName>
        <field>Add_Employee_in_the_Payroll__c</field>
        <literalValue>1</literalValue>
        <name>Add Employee in the Payroll</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FCMS__Reset_PortalEmailAlert</fullName>
        <field>FCMS__PortalEmailAlert__c</field>
        <literalValue>No action</literalValue>
        <name>Reset PortalEmailAlert</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VendorName</fullName>
        <field>Vendor_Name__c</field>
        <formula>FirstName  &amp; &quot; &quot; &amp; LastName</formula>
        <name>VendorName</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Add Employee in the Payroll</fullName>
        <actions>
            <name>Add_Employee_in_the_Payroll</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BillToVendor</fullName>
        <actions>
            <name>VendorName</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Bill_to_Vendor__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>When Bill To Vendor box is checked, update VendorName field</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FCMS__Changed Password</fullName>
        <actions>
            <name>FCMS__Changed_Password_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FCMS__Reset_PortalEmailAlert</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.FCMS__UserName__c</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.FCMS__PortalEmailAlert__c</field>
            <operation>equals</operation>
            <value>Password Changed</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FCMS__Email with existing login details1</fullName>
        <actions>
            <name>FCMS__Pass_Login_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>if( ISNEW(), false, IF((PRIORVALUE(FCMS__Password__c) == NULL &amp;&amp; PRIORVALUE(FCMS__UserName__c) == NULL ) &amp;&amp;  ( NOT(ISPICKVAL( FCMS__PortalEmailAlert__c , &apos;User Created&apos;))) &amp;&amp; (FCMS__Password__c != NULL) &amp;&amp; (FCMS__UserName__c != NULL), true, false ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FCMS__Forgot Password</fullName>
        <actions>
            <name>FCMS__Forgot_password_email_alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FCMS__Reset_PortalEmailAlert</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.FCMS__UserName__c</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.FCMS__PortalEmailAlert__c</field>
            <operation>equals</operation>
            <value>Forgot Password</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FCMS__Notify Profile Manager</fullName>
        <actions>
            <name>FCMS__Notify_Profile_Manager_for_approval_on_creation_of_a_portal_user</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.FCMS__Registration_Approved__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>This workflow is used to send an email to the profile manager of the profile which has been assigned to the newly created  contact./candidate.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FCMS__Portal User Created</fullName>
        <actions>
            <name>FCMS__Portal_User_creation_mail_notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FCMS__Reset_PortalEmailAlert</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.FCMS__UserName__c</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.FCMS__PortalEmailAlert__c</field>
            <operation>equals</operation>
            <value>User Created</value>
        </criteriaItems>
        <description>This workflow is used to send an email to the contact./candidate created under a portal user account.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
