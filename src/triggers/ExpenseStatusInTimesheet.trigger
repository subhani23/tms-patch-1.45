/*
* Description : Trigger to update Timesheet Expense Status field based on Expense
*
*/

trigger ExpenseStatusInTimesheet on TMS__Expense__c (after update,after insert,after delete) {
	
	Boolean disabletrigger = (Boolean) CustomSettingsUtility.disableExpenseStatus();
	
	if(disabletrigger !=null && disabletrigger) {
		return;
	}
	
	private static Logger logger = new Logger('ExpenseStatusInTimesheet');
	
	String expenseMultipleStatus = CustomSettingsUtility.getExpenseMultipleStatus();
	
	Set<ID> timesheetID = new Set<Id>();
	
	if(trigger.isDelete) {
		for(TMS__Expense__c expense : trigger.old) {
			if(expense.TMS__status__c != null ) {
				timesheetID.add(expense.TMS__Timesheet__c);
			}
		}
	}else {
		for(TMS__Expense__c expense : trigger.new) {
			if(expense.TMS__status__c != null ) {
				timesheetID.add(expense.TMS__Timesheet__c);
			}
		}
	}
	
	List<TMS__Timesheet__c> timeSheetList = [
		SELECT TMS__Expense_Status__c,TMS__Status__c,TMS__Candidate__c,
			(SELECT TMS__status__c FROM TMS__Expenses__r)
		FROM TMS__Timesheet__c
		WHERE ID IN:timesheetID];
	
	logger.log('ExpenseStatusInTimesheet', 'timeSheetList', timeSheetList);
		
	for(TMS__Timesheet__c timeSheetOBj : timeSheetList) {
		List<TMS__Expense__c> expenseList = timeSheetOBj.TMS__Expenses__r;
		
		if(expenseList.size() >0) {
			
			Set<String> expenseStatuses = 
				GroupByHelper.getFieldValuesSetStrings(expenseList, 'TMS__Status__c');
				
			logger.log('ExpenseStatusInTimesheet', 'expenseStatuses', expenseStatuses);
				
			if (expenseStatuses.size() > 1) {
				timeSheetOBj.TMS__Expense_Status__c = expenseMultipleStatus != null 
											? expenseMultipleStatus 
											: 'Partial';
			} else {
				timeSheetOBj.TMS__Expense_Status__c = expenseList[0].TMS__Status__c;
			}
		}
	}
	
	if(timeSheetList != null && timeSheetList.size() >0) {
		update timeSheetList;
	}
		
}