/** 
 **************************************************************************************
 * File Type.......: Trigger
 * File Name.......: TaskAccountingIdUpdate.trigger
 * Authored by.....: Shashish Kumar
 * Created Date....: 03-Dec-2012
 * Description.....: Trigger is used to populate the Accounting Id field value from the
 *                   existing Task which as same Name as newly created task.
 **************************************************************************************
 * Version History
 ************************************************************************************** 
 * Authored by.....: Shashish Kumar
 * Revision Date...: 06-Dec-2012
 * Revision Details: Updated         
 **************************************************************************************
 */
trigger TaskAccountingIdUpdate on TMS__Task__c (after insert) {
	set<string> taskNameSet = new set<String>();
	set<string> taskProjectSet = new set<String>();
	set<string> taskProAccSet = new set<String>();
	set<string> taskIdSet = new set<String>();
	/*
	 * Check the Custom Setting to execute the Trigger(if Trigger is disabled, it should not execute the trigger).
	 */
	List<TMS__TimesheetCustomSettings__c> settingValList = [select Trigger_Disabled_TaskAccountingIdUpdate__c
		from TMS__TimesheetCustomSettings__c where Trigger_Disabled_TaskAccountingIdUpdate__c = false limit 1];
	System.debug('>>>>settingValList.size()'+settingValList.size());
	
	if(settingValList.size() > 0){
		/*
		 * Get the Task Id from newly created task and store in Set(taskIdSet).
		 */
		for(TMS__Task__c task : trigger.new){
			if(task.TMS__Project__c != null )
				taskIdSet.add(task.id);
		}
	}
	
	/*
	 * Get the Task Name, Project and Account(related to Project) from newly created task and store in related Set(taskIdSet).
	 */
	List<TMS__Task__c> taskNewList = new List<TMS__Task__c>();
	if(taskIdSet.size() > 0)
		taskNewList = [select id ,Name ,TMS__Project__r.TMS__Client__c, TMS__Project__c from TMS__Task__c where id =:taskIdSet];
	
	/*
	 * If taskNewList is not null then add the Task Name, Project and 
	 * Account(related to Project)in related Set(taskNameSet,taskProjectSet,taskProAccSet).
	 */
	if(taskNewList.size() > 0){
		for(TMS__Task__c t : taskNewList){
			taskNameSet.add(t.Name);
			taskProjectSet.add(t.TMS__Project__c);
			taskProAccSet.add(t.TMS__Project__r.TMS__Client__c);
		}
	}
	
	/*
	 * Get the Accounting Id value from the Existing task where Name, Project 
	 * and Account(related to Project) are same like newly created task.
	 */
	TMS__Task__c taskObj = new TMS__Task__c();
	List<TMS__Task__c> taskList1 = new List<TMS__Task__c>();
	if(taskNameSet.size() > 0){
		taskList1 =[select id, Name, TMS__Accounting_Id__c from TMS__Task__c 
			where Name =: taskNameSet and TMS__Project__c =:taskProjectSet and 
			TMS__Project__r.TMS__Client__c =:taskProAccSet  and TMS__Accounting_Id__c != null limit 1];
		if(taskList1.size() > 0)
			taskObj = taskList1[0];
	}
	
	/*
	 * If Accounting Id found, store in the Map(NameMap) and get the all the list of task 
	 * which has accounting Id as null and Name ,Project 
	 * Account(related to Project) are same like newly created task.
	 */
	map<string, string> nameMap = new map<string, string>();
	List<TMS__Task__c> taskList = new List<TMS__Task__c>();
	System.debug('>>>taskObj'+taskObj);
	if(taskObj != null){
		nameMap.put(taskObj.Name, taskObj.TMS__Accounting_Id__c);
		taskList = [select id, Name, TMS__Accounting_Id__c from TMS__Task__c 
			where Name =: taskNameSet and TMS__Project__c =:taskProjectSet and 
			TMS__Project__r.TMS__Client__c =:taskProAccSet  and TMS__Accounting_Id__c = null];
	}
	
	/*
	 * Assign the Accounting Id from map to all the Task accounting Id field where Accounting id value is null.
	 */
	List<TMS__Task__c> taskUpdateList = new List<TMS__Task__c>();
	if(taskList.size() > 0){
		for(TMS__Task__c task : taskList){
			task.TMS__Accounting_Id__c = nameMap.get(task.Name);
			taskUpdateList.add(task);
		}
	}
	
	/*
     * If list(taskUpdateList) has some value then update it.
     */
	if(taskUpdateList.size() > 0)
		update taskUpdateList;
}