/* Writted by Aliaksandr Satskou, 11/08/2012 */
trigger ProcessPayrollTrigger on TMS__Process_Payroll__c (before delete) {
	TMS__TimesheetCustomSettingsHierarchy__c v_timeSheetCustomSettingsHierarchy =
				TMS__TimesheetCustomSettingsHierarchy__c.getInstance();

	String v_payrollSendStatus = (v_timeSheetCustomSettingsHierarchy != null)
			? v_timeSheetCustomSettingsHierarchy.TMS__Payroll_send_status__c
			: null;

	for (TMS__Process_Payroll__c v_processPayroll : Trigger.old) {
		if (v_processPayroll.TMS__Status__c == v_payrollSendStatus) {
			v_processPayroll.addError(
					System.Label.This_payroll_has_been_sent_to_paychex_and_it_cannot_be_deleted);
		}
	}
}