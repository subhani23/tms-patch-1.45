trigger TimeTrigger on TMS__Time__c (before insert, before update,
		after insert, after update, after delete) {
	
	Logger logger = new Logger('TimeTrigger');

	Boolean updateTimesheetHours;
	Boolean disableUpdCaseTotalTimeSpent;

	/* Added by Aliaksandr Satskou, 05/23/2014 (case #00020083) */
	Boolean isNotifyPayroll;
	String tSubmittedStatus = CustomSettingsUtility.getTimesheetSubmittedStatus();
	
	TMS__TimesheetCustomSettingsHierarchy__c csHierarchy = 
			TMS__TimesheetCustomSettingsHierarchy__c.getInstance();
	
	if (Trigger.isInsert || Trigger.isUpdate) {
		if (csHierarchy != null
				&& (csHierarchy.TMS__Minimum_Hours_Enabled__c == true 
						|| csHierarchy.TMS__Rounding_Enabled__c == true 
						|| csHierarchy.TMS__Start_Week_Enabled__c == true 
						|| csHierarchy.TMS__Weekend_Differential_Enabled__c == true))
		if (trigger.isBefore) {
			RoundTimeHelper.before(Trigger.new);
		} else {
			RoundTimeHelper.after(Trigger.new);
		}
	}

	Map<String, TMS__TimesheetCustomSettings__c> tmsSettingMap =
			TMS__TimesheetCustomSettings__c.getAll();

	if (tmsSettingMap != null) {
		TMS__TimesheetCustomSettings__c tmsSetting = tmsSettingMap.get('ProjectStatusToShow');

		if (tmsSetting != null) {
			disableUpdCaseTotalTimeSpent = tmsSetting.TMS__Disable_Update_Case_Total_Time_Spent__c;

			/* Added by Aliaksandr Satskou, 05/23/2014 (case #00020083) */
			isNotifyPayroll = tmsSetting.TMS__Notify_Payroll_on_additional_Time__c;
		}
	}


	if (Trigger.isAfter) {
		List<TMS__Time__c> triggerList = Trigger.new != null ? Trigger.new : Trigger.old;

		if (TimesheetHelper.isObjectEnabled()) {
			TimesheetTriggerHelper.updateTimesheet(triggerList);
		}

		if ((disableUpdCaseTotalTimeSpent != null) && (!disableUpdCaseTotalTimeSpent)) {
			CaseHelper.updateTotalTimeSpent(triggerList);
		}


		if ((Trigger.isInsert) || (Trigger.isUpdate)) {
			/* Added by Aliaksandr Satskou, 05/23/2014 (case #00020083) */
			if ((isNotifyPayroll != null) && (isNotifyPayroll)) {
				List<TMS__Time__c> notifyTimeList = new List<TMS__Time__c>();

				for (Id timeNewId : Trigger.newMap.keySet()) {
					TMS__Time__c timeNew = Trigger.newMap.get(timeNewId);

					if (timeNew.TMS__Status__c == tSubmittedStatus) {
						notifyTimeList.add(timeNew);
					}
				}


				PayrollNotify.notifyOnAdditionalTime(notifyTimeList);
			}
		}
	}


	/* Added by Aliaksandr Satskou, 06/04/2013 (case #00018244, point 4) */
	Boolean v_isDisabledWeekSync = (Boolean)CustomSettingsUtility.getCustomSettingListTypeValue(
			null, 'TMS__Disable_week_management_sync_from_Date__c');

	if (v_isDisabledWeekSync == null) { v_isDisabledWeekSync = false; }


	/*
	* @author Aliaksandr Satskou
	* @date 06/04/2013
	* @description Check the corresponding week from the WeekManagement Object according
	*       to the date entered in the time.
	*/
	if ((trigger.isBefore) && (v_isDisabledWeekSync == false)) {
		Set<Id> taskIds = GroupByHelper.getFieldValuesIds(Trigger.new, 'TMS__Task__c');
		List<TMS__Task__c> tasks = [
				SELECT TMS__Project_Resource__r.TMS__Contact__c
				FROM TMS__Task__c
				WHERE Id IN :taskIds];
		
		Set<Id> contactIds = GroupByHelper.getFieldValuesIds(
				tasks, 'TMS__Project_Resource__r.TMS__Contact__c');
		
		List<TMS__Project_Resource__c> projectResources = [
				SELECT TMS__Contact__c, TMS__Project__r.Client__r.Week_Management_Adjustment__c,
					TMS__Project__r.Client__r.Week_Management_Adjustment_Days__c
				FROM TMS__Project_Resource__c
				WHERE TMS__Contact__c IN :contactIds];
		
		Map<Object, List<TMS__Project_Resource__c>> contactIdToResources = 
				(Map<Object, List<TMS__Project_Resource__c>>)GroupByHelper.groupByField(
						projectResources, 'TMS__Contact__c');
		
		Map<Id, List<TMS__Project__c>> contactIdToProjects = new Map<Id, List<TMS__Project__c>>();
		for (Object contactId : contactIdToResources.keySet()) {
			List<TMS__Project_Resource__c> resources = contactIdToResources.get(contactId);
			
			List<TMS__Project__c> projects = new List<TMS__Project__c>();
			for (TMS__Project_Resource__c resource : resources) {
				if (resource.Project__r != null) {
					projects.add(resource.Project__r);
				}
			}
			
			contactIdToProjects.put((Id)contactId, projects);
		}
		
		/* Creating query and getting list of TMS__Week_Management__c */
		String v_query = 'SELECT TMS__Start_Date__c, TMS__End_Date__c FROM TMS__Week_Management__c';
		String v_whereClause = '';

		/* Creating WHERE condition for specified dates */
		Map<Id, TMS__Task__c> tasksMap = new Map<Id, TMS__Task__c>(tasks);
		
		for (TMS__Time__c v_time : Trigger.new) {
			if (v_time.TMS__Date__c != null && v_time.TMS__Task__c != null) {
				Id contactId = 
						tasksMap.get(v_time.TMS__Task__c).TMS__Project_Resource__r.TMS__Contact__c;
				
				Integer adjustment = TimesheetTableHelper.getWeekManagementAdjustment(
						contactIdToProjects.get(contactId));
				
				logger.log('', 'v_time.TMS__Date__c 1', v_time.TMS__Date__c);
				
				String dtString = String.valueOf(v_time.TMS__Date__c.addDays(adjustment * -1));
				
				logger.log('', 'v_time.TMS__Date__c 2', v_time.TMS__Date__c);
				
				v_whereClause += '((TMS__Start_Date__c <= ' + dtString +
						') AND (TMS__End_Date__c >= ' + dtString + ')) OR ';
			}
		}
		v_whereClause = (v_whereClause != '')
				? ' WHERE (' + v_whereClause.substring(0, v_whereClause.length() - 4) + ')'
				: '';

		/* Getting list of TMS__Week_Management__c based created query */
		List<TMS__Week_Management__c> v_weekManagementList = DataBase.query(v_query + v_whereClause);

		/* Updating TMS__Week_Management__c for Time Object */
		for (TMS__Time__c v_time : Trigger.new) {
			Boolean v_isTimeUpdated = false;
			
			if (v_time.TMS__Date__c != null && v_time.TMS__Task__c != null) {
				Id contactId = 
						tasksMap.get(v_time.TMS__Task__c).TMS__Project_Resource__r.TMS__Contact__c;
				
				Integer adjustment = TimesheetTableHelper.getWeekManagementAdjustment(
						contactIdToProjects.get(contactId));
				for (TMS__Week_Management__c v_weekManagement : v_weekManagementList) {
					if ((v_weekManagement.TMS__Start_Date__c <= v_time.TMS__Date__c.addDays(adjustment * -1)) &&
							(v_weekManagement.TMS__End_Date__c >= v_time.TMS__Date__c.addDays(adjustment * -1))) {
						v_time.TMS__Week_Management__c = v_weekManagement.Id;
						v_isTimeUpdated = true;
	
						break;
					}
				}
			}

			/* if no Week Management Object for specified date */
			if (!v_isTimeUpdated) {
				v_time.TMS__Week_Management__c = null;
			}
		}
	}


	/*
	* @author Aliaksandr Satskou; edited by Aliaksandr Satskou, 09/04/2013 (case #00021274, point 1,3)
	* @date 06/04/2013
	* @description Updating Bill Rate and Pay Rate for Time object only when it creating.
			(case #00018244, point 3);
	*/
	Map<Id, List<TMS__Time__c>> taskIdTimeListMap = new Map<Id, List<TMS__Time__c>>();
	List<TMS__Time__c> targetList = (Trigger.new != null) ? Trigger.new : Trigger.old;

	/* Added by Aliaksandr Satskou, 09/04/2013 (case #00021274, point 1,3) */
	List<TMS__Time__c> timeWithoutTaskList = new List<TMS__Time__c>();

	/* Edited by Aliaksandr Satskou, 09/04/2013 (case #00021274, point 1,3) */
	for (TMS__Time__c v_time : targetList) {
		if (v_time.TMS__Task__c != null) {
			List<TMS__Time__c> timeList = taskIdTimeListMap.get(v_time.TMS__Task__c);

			if (timeList != null) {
				timeList.add(v_time);
			} else {
				taskIdTimeListMap.put(v_time.TMS__Task__c, new List<TMS__Time__c> { v_time });
			}
		} else {
			timeWithoutTaskList.add(v_time);
		}
	}

	/* Edited by Aliaksandr Satskou, 09/04/2013 (case #00021274, point 1,3) */
	Map<Id, TMS__Task__c> taskMap = new Map<Id, TMS__Task__c>([
			SELECT TMS__Bill_Rate__c, TMS__Pay_Rate__c, TMS__Project_Resource__c,
				TMS__Project_Resource__r.TMS__Contact__c, TMS__Project_Resource__r.TMS__Project__c
			FROM TMS__Task__c
			WHERE Id IN :taskIdTimeListMap.keySet()]);


	/* Edited by Aliaksandr Satskou, 09/04/2013 (case #00021274, point 1,3) */
	if (Trigger.isBefore) {
		/* Added by Aliaksandr Satskou, 09/04/2013 (case #00021274, point 1,3) */
		for (Id taskId : taskIdTimeListMap.keySet()) {
			TMS__Task__c task = taskMap.get(taskId);

			if (task.TMS__Project_Resource__c != null) {
				for (TMS__Time__c v_time : taskIdTimeListMap.get(taskId)) {
					v_time.TMS__Candidate__c = task.TMS__Project_Resource__r.TMS__Contact__c;
					v_time.TMS__Project__c = task.TMS__Project_Resource__r.TMS__Project__c;
				}
			}
		}

		/* Added by Aliaksandr Satskou, 09/04/2013 (case #00021274, point 1,3) */
		for (TMS__Time__c timeObj : timeWithoutTaskList) {
			timeObj.TMS__Candidate__c = null;
			timeObj.TMS__Project__c = null;
		}


		if (Trigger.isInsert) {
			for (Id taskId : taskIdTimeListMap.keySet()) {
				TMS__Task__c task = taskMap.get(taskId);

				for (TMS__Time__c v_time : taskIdTimeListMap.get(taskId)) {
					v_time.TMS__Bill_Rate_On_Create__c = task.TMS__Bill_Rate__c;
					v_time.TMS__Pay_Rate_On_Create__c = task.TMS__Pay_Rate__c;
				}
			}
		}
	}

	/* Added by Aliaksandr Satskou, 05/08/2013 */
	Boolean v_isDisabled = (Boolean)CustomSettingsUtility.getCustomSettingListTypeValue(
			null, 'TMS__Trigger_Disable_Time_Accounting_Sync__c');
	if (v_isDisabled == null || v_isDisabled == false) {

		/*
		* @author Aliaksandr Satskou
		* @date 01/06/2013
		* @description Counting Consumed Hours for Task Object.
		*/
		if (Trigger.isAfter) {
			/* Getting needed Time objects related Trigger condition */
			List<TMS__Time__c> v_triggerTimeList = (Trigger.isDelete)
					? Trigger.old
					: Trigger.new;
	
			Set<Id> v_taskIdSet = new Set<Id>();
	
			for (TMS__Time__c v_time : v_triggerTimeList) {
				if (v_time.TMS__Task__c != null) {
					v_taskIdSet.add(v_time.TMS__Task__c);
				}
			}
	
			/* Getting list of Task Object */
			List<TMS__Task__c> v_taskList = [
					SELECT TMS__Consumed_Hours__c
					FROM TMS__Task__c
					WHERE Id IN :v_taskIdSet];
	
			/* Getting list of Time Object for Task Objects */
			List<TMS__Time__c> v_relatedTimeList = [
					SELECT TMS__Task__c, TMS__Time_Spent__c
					FROM TMS__Time__c
					WHERE TMS__Task__c IN :v_taskIdSet];
	
			/* Counting Consumed Hours for Task Object */
			for (TMS__Task__c v_task : v_taskList) {
				v_task.TMS__Consumed_Hours__c = 0;
	
				for (TMS__Time__c v_time : v_relatedTimeList) {
					if (v_time.TMS__Task__c == v_task.Id) {
						v_task.TMS__Consumed_Hours__c += (v_time.TMS__Time_Spent__c != null)
								? v_time.TMS__Time_Spent__c
								: 0;
					}
				}
			}
	
			update v_taskList;
	
			if ((Trigger.isInsert) || (Trigger.isUpdate)) {
				Map<TMS__Week_Management__c, List<TMS__Time__c>> v_WeekToTimesMap =
						new Map<TMS__Week_Management__c, List<TMS__Time__c>>();
				List<String> v_TimeIds = new List<String>();
				List<TMS__Week_Management__c> v_weekManagmentUpdateNULLList = new List<TMS__Week_Management__c>();
				List<TMS__Week_Management__c> v_weekManagmentUpdateTMSList = new List<TMS__Week_Management__c>();
	
				for (TMS__Time__c v_time : Trigger.new) {
					v_TimeIds.add(v_time.TMS__Week_Management__c);
				}
	
				List<TMS__Week_Management__c> v_weekManagmentList = [
							SELECT TMS__Accounting_Id__c, TMS__Generate__c
							FROM TMS__Week_Management__c
							WHERE Id = :v_TimeIds LIMIT 1000];
	
				if (!v_weekManagmentList.isEmpty()) {
					List<TMS__Time__c> v_timeList = [
							SELECT TMS__Accounting_Id__c
							FROM TMS__Time__c
							WHERE TMS__Week_Management__c IN :v_weekManagmentList LIMIT 30000];
	
					if (!v_timeList.isEmpty()) {
						for (TMS__Week_Management__c v_week : v_weekManagmentList) {
							v_WeekToTimesMap.put(v_week, new List<TMS__Time__c>());
	
							for (TMS__Time__c v_time : v_timeList) {
								v_WeekToTimesMap.get(v_week).add(v_time);
							}
						}
	
						for (TMS__Week_Management__c v_week : v_WeekToTimesMap.keySet()) {
							Boolean v_flag = true;
	
							for (TMS__Time__c v_time : v_WeekToTimesMap.get(v_week)) {
								if (v_time.TMS__Accounting_Id__c == null) {
									v_flag = false;
									break;
								}
							}
	
							if (v_flag) {
								v_weekManagmentUpdateNULLList.add(v_week); // Accounting Id not NULL
							} else {
								v_weekManagmentUpdateTMSList.add(v_week); // Accounting Id IS NULL
							}
						}
						
						for (TMS__Week_Management__c v_week : v_weekManagmentUpdateNULLList) {
							v_week.TMS__Generate__c = System.Label.Timesheet_Updated; // Accounting Id not NULL
						}
	
						for (TMS__Week_Management__c v_week : v_weekManagmentUpdateTMSList) {
							v_week.TMS__Generate__c = System.Label.Timesheet; // Accounting Id IS NULL
						}
	
						update v_weekManagmentUpdateNULLList;
						update v_weekManagmentUpdateTMSList;
					}
				}
			}
		}
	}
}