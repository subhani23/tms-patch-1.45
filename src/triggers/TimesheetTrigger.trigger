/**
* @author Aliaksandr Satskou
* @date 12/03/2013
* @description Updating fields from Placement based on Project.
*/
trigger TimesheetTrigger on TMS__Timesheet__c (before insert, before update) {

	Boolean isPopulateAdditionalFields;


	Map<String, TMS__TimesheetCustomSettings__c> tmsSettingMap = TMS__TimesheetCustomSettings__c.getAll();

	if (tmsSettingMap != null) {
		TMS__TimesheetCustomSettings__c tmsSetting = tmsSettingMap.get('ProjectStatusToShow');

		if (tmsSetting != null) {
			isPopulateAdditionalFields = tmsSetting.TMS__Populate_Additional_Timesheet_Fields__c;
		}
	}
	System.debug(LoggingLevel.ERROR, '::::::isPopulateAdditionalFields=' + isPopulateAdditionalFields);


	if ((isPopulateAdditionalFields != null) && (isPopulateAdditionalFields)) {
		/* Edited by Aliaksandr Satskou, 10/20/2014 (case #00032938) */
		Boolean isUseBatch = false;
		
		
		TMS__TimesheetCustomSettingsHierarchy__c tmsSetting = 
				TMS__TimesheetCustomSettingsHierarchy__c.getInstance();
		
		if (tmsSetting != null) {
			isUseBatch = tmsSetting.TMS__Use_Apex_Batch_for_Timesheet_Trigger__c;
		}
		System.debug(LoggingLevel.ERROR, '::::isUseBatch=' + isUseBatch);
		
		
		if (isUseBatch) {
			UpdateTimesheetsBatch updateTimesheetsBatch = new UpdateTimesheetsBatch(Trigger.new);
			Database.executeBatch(updateTimesheetsBatch, 1);
		} else {
			// TimesheetTriggerHelper.updateTimesheetFieldsFromPlacement(Trigger.new);	uncomment before packaging
		}
	}
}