@isTest
private class PayrollGeneratorTest {

	private static TMS__TimesheetCustomSettingsHierarchy__c timesheetSetting;
	private static List<TMS__Payroll_Provider_Settings__c> payrollProviderSettingList;
	private static List<TMS__Payroll_Settings__c> payrollSettingList;
	private static TMS__Payroll_Filenames__c filenameSetting;

	private static List<TMS__Process_Payroll__c> processPayrollList;
	private static List<TMS__Payroll_Line_items__c> payrollLineItemsList;

	private static List<Contact> employeeList;


	/**
	* @name initData
	* @author Aliaksandr Satskou
	* @date 10/31/2012
	* @description Initialize all needed data.
	* @return void
	*/
	private static void initData() {
		timesheetSetting = TMS__TimesheetCustomSettingsHierarchy__c.getInstance();
		timesheetSetting.TMS__Payroll_send_status__c = 'Sent to Payroll';

		upsert timesheetSetting;


		employeeList = new List<Contact> {
			new Contact(
				LastName = 'LastName #1',
				Phone = '111223456789'
			),
			new Contact(
				LastName = 'LastName #2',
				Phone = '111223456789'
			)
		};
		insert employeeList;


		filenameSetting = new TMS__Payroll_Filenames__c(
			Name = 'Default',

			TMS__ADP_Data_File_Name__c = 'epidecaa.csv',

			TMS__Paychex_Data_File_Name__c = 'R320_TA.TXT',
			TMS__Paychex_Employee_Bank_Data_File_Name__c = 'CVT1B.TXT',
			TMS__Paychex_Employee_Data_File_Name__c = 'CVT1.TXT'
		);
		insert filenameSetting;


		payrollProviderSettingList = new List<TMS__Payroll_Provider_Settings__c> {
			new TMS__Payroll_Provider_Settings__c(
				Name = 'Paychex',
				TMS__Email__c = 'test@test.com',
				TMS__Payroll_Provider_Name__c = 'Paychex',
				TMS__Use_CSV_File_Format__c = false
			),
			new TMS__Payroll_Provider_Settings__c(
				Name = 'ADP',
				TMS__Email__c = 'test@test.com',
				TMS__Payroll_Provider_Name__c = 'ADP',
				TMS__Use_CSV_File_Format__c = true,
				TMS__Field_Delimiter__c = ',',
				TMS__Include_Column_Header_in_Payroll_File__c = true
			)
		};
		insert payrollProviderSettingList;


		payrollSettingList = new List<TMS__Payroll_Settings__c> {
			new TMS__Payroll_Settings__c(
				Name = 'Paychex_Name',
				TMS__Payroll_Provider_Name__c = 'Paychex',
				TMS__Object_API_Name__c = 'TMS__Payroll_Line_items__c',
				TMS__Field_API_Name__c = 'TMS__Name__c',
				TMS__Characters__c = '1-12',
				TMS__File_Name__c = 'R320_TA.TXT'
			),
			new TMS__Payroll_Settings__c(
				Name = 'Paychex_Hours',
				TMS__Payroll_Provider_Name__c = 'Paychex',
				TMS__Object_API_Name__c = 'TMS__Payroll_Line_items__c',
				TMS__Field_API_Name__c = 'TMS__Hours__c',
				TMS__Characters__c = '13-20',
				TMS__File_Name__c = 'R320_TA.TXT'
			),
			new TMS__Payroll_Settings__c(
				Name = 'Paychex_Employee_Number',
				TMS__Payroll_Provider_Name__c = 'Paychex',
				TMS__Object_API_Name__c = 'TMS__Payroll_Line_items__c',
				TMS__Field_API_Name__c = 'TMS__Employee_Number__c',
				TMS__Characters__c = '25-35',
				TMS__File_Name__c = 'R320_TA.TXT'
			),
			new TMS__Payroll_Settings__c(
				Name = 'Paychex_SSN',
				TMS__Payroll_Provider_Name__c = 'Paychex',
				TMS__Object_API_Name__c = 'TMS__Payroll_Line_items__c',
				TMS__Field_API_Name__c = 'TMS__Social_Security_Number__c',
				TMS__Characters__c = '36-45',
				TMS__File_Name__c = 'R320_TA.TXT'
			),
			new TMS__Payroll_Settings__c(
				Name = 'Name',
				TMS__Payroll_Provider_Name__c = 'ADP',
				TMS__Object_API_Name__c = 'TMS__Payroll_Line_items__c',
				TMS__Field_API_Name__c = 'TMS__Name__c',
				TMS__File_Name__c = 'epidecaa.csv',
				TMS__Field_Order__c = '1',
				TMS__Column_Name__c = 'Name',
				TMS__Line__c = '1'
			),
			new TMS__Payroll_Settings__c(
				Name = 'Gender',
				TMS__Payroll_Provider_Name__c = 'ADP',
				TMS__Object_API_Name__c = 'Contact',
				TMS__Field_API_Name__c = '"M"',
				TMS__File_Name__c = 'epidecaa.csv',
				TMS__Field_Order__c = '1',
				TMS__Column_Name__c = 'Gender',
				TMS__Line__c = '1',
				TMS__Formula__c = 'ADDBEFORE("Gender: ")'
			),
			new TMS__Payroll_Settings__c(
				Name = 'LastName',
				TMS__Payroll_Provider_Name__c = 'ADP',
				TMS__Object_API_Name__c = 'Contact',
				TMS__Field_API_Name__c = 'LastName',
				TMS__File_Name__c = 'epidecaa.csv',
				TMS__Field_Order__c = '1',
				TMS__Column_Name__c = 'LastName',
				TMS__Line__c = '1',
				TMS__Formula__c = 'ADDBEFORE("Hi, ")::UPPER()::SUBSTRING(0,2)'
			),
			new TMS__Payroll_Settings__c(
				Name = 'Phone',
				TMS__Payroll_Provider_Name__c = 'ADP',
				TMS__Object_API_Name__c = 'Contact',
				TMS__Field_API_Name__c = 'Phone',
				TMS__File_Name__c = 'epidecaa.csv',
				TMS__Field_Order__c = '2',
				TMS__Column_Name__c = 'Phone',
				TMS__Line__c = '1',
				TMS__Formula__c = 'FORMAT("+000(00)000-00-00")'
			),
			new TMS__Payroll_Settings__c(
				Name = 'CreatedDate',
				TMS__Payroll_Provider_Name__c = 'ADP',
				TMS__Object_API_Name__c = 'Contact',
				TMS__Field_API_Name__c = 'CreatedDate',
				TMS__File_Name__c = 'epidecaa.csv',
				TMS__Field_Order__c = '3',
				TMS__Column_Name__c = 'CreatedDate',
				TMS__Line__c = '1',
				TMS__Formula__c = 'DATEFMT("M/dd/yy")'
			)
		};
		insert payrollSettingList;


		processPayrollList = new List<TMS__Process_Payroll__c> {
			new TMS__Process_Payroll__c(
				TMS__Start_Date__c = Date.today(),
				TMS__End_Date__c = Date.today(),
				TMS__Payroll_Process_Date__c = Date.today(),
				TMS__Payroll_Provider_Name__c = 'Paychex',
				TMS__Comments__c = 'Comments #1'
			),
			new TMS__Process_Payroll__c(
				TMS__Start_Date__c = Date.today(),
				TMS__End_Date__c = Date.today(),
				TMS__Payroll_Process_Date__c = Date.today(),
				TMS__Payroll_Provider_Name__c = 'ADP',
				TMS__Comments__c = 'Comments #2'
			)
		};
		insert processPayrollList;

		payrollLineItemsList = new List<TMS__Payroll_Line_items__c> {
			new TMS__Payroll_Line_items__c(
				TMS__Payroll__c = processPayrollList[0].Id,
				TMS__Name__c = 'Name #1',
				TMS__Hours__c = 10.0,
				TMS__Employee_Number__c = 'Employee Number #1',
				TMS__Social_Security_Number__c = 'S.S. Number #1',
				TMS__Payroll_File_Name__c = 'R320_TA.TXT'
			),
			new TMS__Payroll_Line_items__c(
				TMS__Payroll__c = processPayrollList[0].Id,
				TMS__Name__c = 'Name #2',
				TMS__Hours__c = 20.0,
				TMS__Employee_Number__c = 'Employee Number #2',
				TMS__Social_Security_Number__c = 'S.S. Number #2',
				TMS__Payroll_File_Name__c = 'R320_TA.TXT'
			),
			new TMS__Payroll_Line_items__c(
				TMS__Payroll__c = processPayrollList[1].Id,
				TMS__Name__c = 'Name #3',
				TMS__Hours__c = 30.0,
				TMS__Employee_Number__c = 'Employee Number #3',
				TMS__Social_Security_Number__c = 'S.S. Number #3',
				TMS__Payroll_File_Name__c = 'epidecaa.csv'
			),
			new TMS__Payroll_Line_items__c(
				TMS__Payroll__c = processPayrollList[1].Id,
				TMS__Name__c = 'Name #4',
				TMS__Hours__c = 40.0,
				TMS__Employee_Number__c = 'Employee Number #4',
				TMS__Social_Security_Number__c = 'S.S. Number #4',
				TMS__Payroll_File_Name__c = 'epidecaa.csv'
			)
		};
		insert payrollLineItemsList;
	}


	/**
	* @name testPayrollGenerator
	* @author Aliaksandr Satskou
	* @date 11/08/2012
	* @description Testing PayrollGenerator class.
	* @return void
	*/
	private static testMethod void testPayrollGenerator() {
		initData();


		PayrollGenerator.doSendPayroll(processPayrollList[0].Id);
		PayrollGenerator.doSendPayroll(processPayrollList[1].Id);


		List<String> paychexLineItemList = new List<String> {
				payrollLineItemsList[0].Id, payrollLineItemsList[1].Id };
		List<String> adpLineItemList = new List<String> {
				payrollLineItemsList[2].Id, payrollLineItemsList[3].Id };

		PayrollGenerator.doSendPayrollForSpecificLineItems(processPayrollList[0].Id, paychexLineItemList);
		PayrollGenerator.doSendPayrollForSpecificLineItems(processPayrollList[1].Id, adpLineItemList);


		employeeList = [
				SELECT Phone, LastName, CreatedDate
				FROM Contact
				WHERE Id IN :employeeList];

		PayrollGenerator.getCSVPayrollFileContent('ADP', 'epidecaa.csv', 'Contact', employeeList);

	}
}