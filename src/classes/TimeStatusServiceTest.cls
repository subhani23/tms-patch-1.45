/**
* @author Aliaksandr Satskou
* @date 10/21/2013
* @description Test class for TimeStatusService class.
*/
@isTest
private class TimeStatusServiceTest {

	private static List<TMS__Timesheet__c> timesheetList;
	private static List<TMS__Time__c> timeList;
	private static List<TMS__Expense__c> expenseList;
	private static TMS__TimesheetCustomSettings__c tmsSetting;


	/**
	* @author Aliaksandr Satskou
	* @name initData
	* @date 10/21/2013
	* @description Init data.
	* @return void
	*/
	private static void initData() {
		tmsSetting = new TMS__TimesheetCustomSettings__c(
			Name = 'ProjectStatusToShow',
			TMS__Timesheet_Submitted_Status__c = 'Submitted',
			TMS__Timesheet_Approved_Status__c = 'Approved',
			TMS__Timesheet_Rejected_Status__c = 'Rejected',
			TMS__Expense_Approved_Status__c = 'Approved',
			TMS__Expense_Rejected_Status__c = 'Rejected',
			TMS__Expense_Submitted_Status__c = 'Submitted'
		);
		upsert tmsSetting;


		timesheetList = new List<TMS__Timesheet__c> {
			new TMS__Timesheet__c(
				TMS__Status__c = 'Submitted'
			)
		};
		insert timesheetList;


		timeList = new List<TMS__Time__c> {
			new TMS__Time__c(
				TMS__Timesheet__c = timesheetList[0].Id,
				TMS__Status__c = 'Submitted'
			),
			new TMS__Time__c(
				TMS__Timesheet__c = timesheetList[0].Id,
				TMS__Status__c = 'Submitted'
			)
		};
		insert timeList;


		/* Needed for Expense object */
		List<Contact> contactList = new List<Contact> {
			new Contact(
				LastName = 'Contact #1'
			)
		};
		insert contactList;


		/* Needed for Expense object */
		List<TMS__Week_Management__c> weekManagementList = new List<TMS__Week_Management__c> {
			new TMS__Week_Management__c(
				TMS__Start_Date__c = Date.today(),
				TMS__End_Date__c = Date.today() + 6
			)
		};
		insert weekManagementList;


		expenseList = new List<TMS__Expense__c> {
			new TMS__Expense__c(
				TMS__Timesheet__c = timesheetList[0].Id,
				TMS__Contact__c = contactList[0].Id,
				TMS__Week_Management__c = weekManagementList[0].Id,
				TMS__Status__c = 'Submitted'
			),
			new TMS__Expense__c(
				TMS__Timesheet__c = timesheetList[0].Id,
				TMS__Contact__c = contactList[0].Id,
				TMS__Week_Management__c = weekManagementList[0].Id,
				TMS__Status__c = 'Submitted'
			)
		};
		insert expenseList;
	}


	/**
	* @author Aliaksandr Satskou
	* @name testTimeStatusService
	* @date 10/21/2013
	* @description Testing TimeStatusService class.
	* @return void
	*/
	static testMethod void testTimeStatusService1() {
		initData();


		/* Updating Time List with "Approved" status */
		TimeStatusService.updateTimeListStatusNew(new List<Id> { timeList[0].Id, timeList[1].Id }, 1);

		timeList = [SELECT TMS__Status__c FROM TMS__Time__c WHERE Id IN :timeList];
		System.assertEquals(tmsSetting.TMS__Timesheet_Approved_Status__c, timeList[0].TMS__Status__c);
		System.assertEquals(tmsSetting.TMS__Timesheet_Approved_Status__c, timeList[1].TMS__Status__c);

		/* Updating Expense List with "Approved" status */
		TimeStatusService.updateExpenseListStatusNew(new List<Id> { expenseList[0].Id, expenseList[1].Id }, 1);

		expenseList = [SELECT TMS__Status__c FROM TMS__Expense__c WHERE Id IN :expenseList];
		System.assertEquals(tmsSetting.TMS__Expense_Approved_Status__c, expenseList[0].TMS__Status__c);
		System.assertEquals(tmsSetting.TMS__Expense_Approved_Status__c, expenseList[1].TMS__Status__c);


		/* Added by Aliaaksandr Satskou, 10/17/2014 (case #00032969) */
		TimeStatusService.updateTimesheetRejectionReason(timesheetList[0].Id, 'Some Reason');
	}


	static testMethod void testTimeStatusService2() {
		initData();


		/* Updating Time List with "Rejected" status */
		TimeStatusService.updateTimeListStatusNew(new List<Id> { timeList[0].Id, timeList[1].Id }, 2);

		timeList = [SELECT TMS__Status__c FROM TMS__Time__c WHERE Id IN :timeList];
		System.assertEquals(tmsSetting.TMS__Timesheet_Rejected_Status__c, timeList[0].TMS__Status__c);
		System.assertEquals(tmsSetting.TMS__Timesheet_Rejected_Status__c, timeList[1].TMS__Status__c);

		/* Updating Expense List with "Rejected" status */
		TimeStatusService.updateExpenseListStatusNew(new List<Id> { expenseList[0].Id, expenseList[1].Id }, 2);

		expenseList = [SELECT TMS__Status__c FROM TMS__Expense__c WHERE Id IN :expenseList];
		System.assertEquals(tmsSetting.TMS__Expense_Rejected_Status__c, expenseList[0].TMS__Status__c);
		System.assertEquals(tmsSetting.TMS__Expense_Rejected_Status__c, expenseList[1].TMS__Status__c);
	}


	static testMethod void testTimeStatusService3() {
		initData();


		/* Updating Time List and Expense List for Timesheet with "Approved" status */
		TimeStatusService.updateTimesheetTimeListStatusNew(new List<Id> { timesheetList[0].Id }, 1);

		timeList = [SELECT TMS__Status__c FROM TMS__Time__c WHERE Id IN :timeList];
		System.assertEquals(tmsSetting.TMS__Timesheet_Approved_Status__c, timeList[0].TMS__Status__c);
		System.assertEquals(tmsSetting.TMS__Timesheet_Approved_Status__c, timeList[1].TMS__Status__c);

		expenseList = [SELECT TMS__Status__c FROM TMS__Expense__c WHERE Id IN :expenseList];
		System.assertEquals(tmsSetting.TMS__Expense_Approved_Status__c, expenseList[0].TMS__Status__c);
		System.assertEquals(tmsSetting.TMS__Expense_Approved_Status__c, expenseList[1].TMS__Status__c);
	}


	static testMethod void testTimeStatusService4() {
		initData();


		/* Updating Time List and Expense List for Timesheet with "Rejected" status */
		TimeStatusService.updateTimesheetTimeListStatusNew(new List<Id> { timesheetList[0].Id }, 2);

		timeList = [SELECT TMS__Status__c FROM TMS__Time__c WHERE Id IN :timeList];
		System.assertEquals(tmsSetting.TMS__Timesheet_Rejected_Status__c, timeList[0].TMS__Status__c);
		System.assertEquals(tmsSetting.TMS__Timesheet_Rejected_Status__c, timeList[1].TMS__Status__c);

		expenseList = [SELECT TMS__Status__c FROM TMS__Expense__c WHERE Id IN :expenseList];
		System.assertEquals(tmsSetting.TMS__Expense_Rejected_Status__c, expenseList[0].TMS__Status__c);
		System.assertEquals(tmsSetting.TMS__Expense_Rejected_Status__c, expenseList[1].TMS__Status__c);
	}
}