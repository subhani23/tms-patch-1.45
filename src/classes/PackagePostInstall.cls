/**
* @author Aliaksandr Satskou
* @date 04/10/2014
* @description Package post install script.
*/
global class PackagePostInstall implements InstallHandler {

	/**
	* @author Aliaksandr Satskou
	* @name onInstall
	* @date 04/10/2014
	* @param context Install Context
	* @description Package post install script.
	* @return void
	*/
	global void onInstall(InstallContext context) {

		/* Creating Payroll Settings */
		PackagePostInstallHelper.createPayrollSettings();

		/* Creating Payroll Filenames Settings */
		PackagePostInstallHelper.createPayrollFilenamesSettings();
	}
}