/**
* @author Aliaksandr Satskou
* @date 12/13/2013
* @description Test class for ProjectManagementTabController.
*/
@isTest
private class ProjectManagementTabControllerTest {
	
	/**
	* @author Aliaksandr Satskou
	* @date 12/13/2013
	* @name testProjectManagementTabController
	* @description Testing ProjectManagementTabController class.
	* @return void
	*/
    static testMethod void testProjectManagementTabController() {
    	ProjectManagementTabController controller = new ProjectManagementTabController();
    	
    	controller.getWeekManagementTab();
    	controller.getProjectTab();
    	controller.getProjectResourceTab();
    	controller.getTaskTab();
    	controller.getTimesheetTab();
    	controller.getTimeTab();
    	controller.getExpenseTab();
    	controller.getPayrollTab();
    }
}