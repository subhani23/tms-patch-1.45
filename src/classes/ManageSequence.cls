// Deprecated Class
public class ManageSequence {

	/* Added by Aliaksandr Satskou, 04/18/2014 (case #00023506) */
	public String locale {
		get { return UserInfo.getLocale(); }
		private set;
	}


	String projectKeyPrefix = Schema.SObjectType.TMS__Project__c.getKeyPrefix();
	String projectTemplateKeyPrefix = Schema.SObjectType.TMS__Project_Template__c.getKeyPrefix();
	String taskId;
	String sfId;
	List<TMS__Task__c> taskList = new List<TMS__Task__c>();
	public List<TMS__Task__c> getTaskList(){
		return taskList;
	}
	public void setTaskList(List<TMS__Task__c> taskList){
		this.taskList = taskList;
	}
	public ManageSequence(){
		taskId = ApexPages.currentPage().getParameters().get('tId');
		sfId = ApexPages.currentPage().getParameters().get('sfId');
		if(taskId != null && taskId.length() > 0){
			taskList = [Select Id, Name, TMS__Sequence__c, TMS__Parent_Task__c from TMS__Task__c where Id = :taskId];
		}
	}

	public PageReference updateSequence(){
		update taskList;
		PageReference pg;
		if(projectKeyPrefix == sfId.substring(0,3))
			pg = new PageReference('/apex/TaskListInProject?id='+sfId);
		else
			pg = new PageReference('/apex/TaskListInProjectTemplate?id='+sfId);
		return pg;
	}

	public static testMethod void tstManageSequence() {
		PageReference pg = Page.ManageSequence;
		pg.getParameters().put('tId','323233343434234');
		pg.getParameters().put('sfId','323233343434234');
		Test.setCurrentPageReference(pg);
		ManageSequence obj = new ManageSequence();
		TMS__Task__c task = new TMS__Task__c();
		task.Name = 'Test Task';
		List<TMS__Task__c> taskList = new List<TMS__Task__c>();
		taskList.add(task);
		insert taskList;
		obj.getTaskList();
		obj.setTaskList(taskList);
		obj.updateSequence();
	}
}