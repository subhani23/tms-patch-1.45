@isTest
private class TimeSheetApprovalControllerTest {
	private static TestDataGenerator gen = new TestDataGenerator();
	
	private static Logger logger = new Logger('TimeSheetApprovalControllerTest');
	
	@isTest
	private static void setApproveStatus() {
		initData();
		
		TimeSheetApprovalController controller = new TimeSheetApprovalController();
		
		controller.init();
		
		Test.startTest();
		
		controller.wrapperClassList[0].setCheckBox(true);
		controller.approveSelected();
		
		Test.stopTest();
		
		TMS__Timesheet__c timesheet = [
				SELECT TMS__Status__c
				FROM TMS__Timesheet__c];
		
		String timesheetApprovedStatus = CustomSettingsUtility.getTimesheetApprovedStatus();
		//String timesheetRejectedStatus = CustomSettingsUtility.getTimesheetRejectedStatus();
	}
	
	@isTest
	private static void codeCoverage() {
		initData();
		
		Test.startTest();
		
		TimeSheetApprovalController obj = new TimeSheetApprovalController();
		obj.init();
		obj.getProIdsList();
		obj.setProIdsList(new List<String>{'a','b'});
		obj.getTempInteger();
		obj.setTempInteger(3);
		obj.getWeek();
		obj.getWeekItems();
		obj.previousWeek();
		obj.nextWeek();
		obj.changeWeek();
		obj.changeTask();
		obj.getTimeIdsList();
		obj.setTimeIdsList(new List<String>{'a','b'});
		obj.timeIdsList();
		
		obj.approveSelected();
		obj.rejectSelected();
		obj.initExpenses();
		obj.approveExpenses();
		obj.rejectExpenses();

		TimeSheetApprovalController.WrapperClass wrapObj = new TimeSheetApprovalController.WrapperClass();
		wrapObj.getCheckBox();
		wrapObj.setCheckBox(true);
		wrapObj.getMonday();
		wrapObj.setMonday(gen.times[0]);
		wrapObj.getTuesday();
		wrapObj.setTuesday(gen.times[1]);
		wrapObj.getWednesday();
		wrapObj.setWednesday(gen.times[2]);
		wrapObj.getThursday();
		wrapObj.setThursday(gen.times[3]);
		wrapObj.getFriday();
		wrapObj.setFriday(gen.times[4]);
		wrapObj.getSaturday();
		wrapObj.setSaturday(gen.times[5]);
		wrapObj.getSunday();
		wrapObj.setSunday(gen.times[6]);
		List<TimeSheetApprovalController.WrapperClass> wrapObjList = new List<TimeSheetApprovalController.WrapperClass>();
		wrapObjList.add(wrapObj);
		obj.approveSelected();
		obj.rejectSelected();

		obj.checkForTaskList(gen.taskRegular.Id);

		TimeSheetApprovalController.WrapperClass1 wrapper1 = new TimeSheetApprovalController.WrapperClass1();
		wrapper1.getProId();
		wrapper1.setProId(gen.project.Id);
		
		Test.stopTest();
	}
	
	private static void initData() {
		TestDataGenerator.InitOptions initOptions = new TestDataGenerator.InitOptions();
		initOptions.loginAsManager = true;
		initOptions.insertTimesheetRecord = true;
		gen.initData(initOptions);
	}
}