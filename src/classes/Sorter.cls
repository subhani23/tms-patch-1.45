public abstract class Sorter 
{
	public List<Object> Result;
	
	public Sorter(List<Object> objects)
	{
		Map<string, List<Object>> forSort = new Map<string, List<Object>>();
		for (Object obj : objects)
		{
		    String key = getKey(obj);
		    if(forSort.get(key) == null) 
		        forSort.put(key, new List<Object>());  
		        
		    forSort.get(key).add(obj);
		}
		
		List<String> keyList = new List<String>(forSort.keySet());
		keyList.sort();
		
		objects.clear();
		for (String key : keyList)
			for (Object obj : forSort.get(key))
		    	objects.add(obj);
		    
		Result = objects;
	 }
	 
	 public abstract String getKey(Object obj);
}