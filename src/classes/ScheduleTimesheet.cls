global class ScheduleTimesheet implements Schedulable{
    global void execute(SchedulableContext sc){
        RemindSubmitTimesheet obj = new RemindSubmitTimesheet();
    }
    
    private static testmethod void ScheduleTimesheetTest() {
    	String CRON_EXP = '0 0 0 3 9 ? 2022';
    	Test.startTest();
    		System.schedule('test', CRON_EXP, new ScheduleTimesheet());
    	Test.stopTest();
    } 
}