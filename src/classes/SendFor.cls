public inherited sharing abstract class SendFor {
	protected TMS__Week_Management__c weekManagement;
	protected Set<Id> recepientIdSet;

	public void send(Id p_weekId) {
		weekManagement = [
				SELECT TMS__Week__c, TMS__Start_Date__c, TMS__End_Date__c
				FROM TMS__Week_Management__c
				WHERE Id = :p_weekId];
		
		System.debug(LoggingLevel.ERROR, ':::::::weekManagement=' + weekManagement);
		
		getRecepientIdSet();
		
		System.debug(LoggingLevel.ERROR, ':::::::recepientIdSet=' + recepientIdSet);
		
		sendEmailWithApprovalPageLinkToRecepients();
	}
	
	
	/**
	* @author Aliaksandr Satskou
	* @name sendBasedParticularResource
	* @date 05/04/2013
	* @description Sending email-s based particular resource.
	* @param p_weekId Id of week
	* @param p_resourceId Id of resource
	* @return void
	*/
	public void sendBasedParticularResource(Id p_weekId, Id p_resourceId) {
		weekManagement = [
				SELECT TMS__Week__c, TMS__Start_Date__c, TMS__End_Date__c
				FROM TMS__Week_Management__c
				WHERE Id = :p_weekId];
				
		getRecepientIdSet(p_resourceId);
		
		sendEmailWithApprovalPageLinkToRecepients();
	}
	
	
	protected abstract void getRecepientIdSet();
	protected abstract void getRecepientIdSet(Id p_resourceId);
	
	private void sendEmailWithApprovalPageLinkToRecepients() {
		
		Set<Id> v_recepientIdWithoutSessionSet = getAllRecepientIdSetWithoutSession();
				
		System.debug(LoggingLevel.ERROR, 
				':::::::v_recepientIdWithoutSessionSet=' + v_recepientIdWithoutSessionSet);
		
		generateSessionsForContacts(v_recepientIdWithoutSessionSet);
		
		try {
			sendEmails();
		} catch (Exception ex) {
			
		}
	}
	
	public Set<Id> getAllRecepientIdSetWithoutSession() {
		List<Contact> v_recepientWithoutSessionList = [
				SELECT Id
				FROM Contact
				WHERE Id IN :recepientIdSet 
				  AND Id NOT IN (
				  		SELECT FCMS__Session_For__c 
				  		FROM FCMS__Session__c
				  		WHERE FCMS__Session_For__c IN :recepientIdSet)];
		
		Set<Id> v_recepientIdWithoutSessionSet = new Set<Id>();
		
		for (Contact v_recepientWithoutSession : v_recepientWithoutSessionList) {
			v_recepientIdWithoutSessionSet.add(v_recepientWithoutSession.Id);
		}
		
		return v_recepientIdWithoutSessionSet;
	}
	
	public void generateSessionsForContacts(Set<Id> v_contactIdSet) {
		List<FCMS__Session__c> v_insertSessionList = new List<FCMS__Session__c>();
		
		for (Id v_contactId : v_contactIdSet) {
			v_insertSessionList.add(new FCMS__Session__c(
					FCMS__Session_For__c = v_contactId,
					FCMS__SessionId__c = String.valueOf(Math.random()).substring(2,16))
			);
		}
		
		insert v_insertSessionList;
	}
	
	private void sendEmails() {
		List<Contact> v_recepientList = getAllRecepients();
		
		System.debug(LoggingLevel.ERROR, ':::::::v_recepientList=' + v_recepientList);
		
		List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
		for (Contact v_recepient : v_recepientList) {
			mails.addAll(generateEmails(v_recepient));
		}
		Messaging.sendEmail(mails); 
	}
	
	private List<Contact> getAllRecepients() {
		return [
				SELECT Name, Email,TMS__CMS_profile_Name__c,
					(SELECT FCMS__SessionId__c FROM FCMS__Sessions__r LIMIT 1)
				FROM Contact
				WHERE Id IN :recepientIdSet];
	}
	
	protected abstract List<Messaging.SingleEmailMessage> generateEmails(Contact p_recepient);
}