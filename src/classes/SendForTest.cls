/**
* @author Aliaksandr Satskou
* @date 03/25/2013
* @description Test class for SendFor, SendForAccounting, SendForApprove classes.
*/
@isTest
private class SendForTest {
	
	private static List<TMS__Project__c> projectList;
	private static List<Contact> candidateList;
	private static List<TMS__Project_Resource__c> projectResourceList;
	private static List<TMS__Task__c> taskList;
	private static List<TMS__Time__c> timeList;
	private static List<TMS__Week_Management__c> weekManagementList;
	
	
	/**
	* @name initData
	* @author Aliaksandr Satskou
	* @date 08/06/2013
	* @description Initialize all needed test data.
	* @return void
	*/
	private static void initData() {
		TMS__TimesheetCustomSettingsHierarchy__c customSettings = 
				TMS__TimesheetCustomSettingsHierarchy__c.getInstance();
		customSettings.TMS__Statuses_for_Accounting_Manager__c = 'Submitted;Approved';
		customSettings.TMS__Statuses_for_Manager_Approval__c = 'Submitted;Approved';
		customSettings.TMS__Site_URL__c = 'http://site.com/';
		
		upsert customSettings;
		
		
		candidateList = new List<Contact> {
			new Contact(
				LastName = 'Contact #1',
				Email = 'test1@test.com'
			),
			new Contact(
				LastName = 'Contact #2',
				Email = 'test2@test.com'
			),
			new Contact(
				LastName = 'Contact #3',
				Email = 'test3@test.com'
			)
		};
		insert candidateList;

		projectList = new List<TMS__Project__c> {
			new TMS__Project__c(
				RecordTypeId = RecordTypeUtility.getProjectRecordTypeId(),
				Name = 'Project #1',
				TMS__Project_Status__c = 'Open',
				Internal_Approver__c = candidateList[1].Id,
				Accounting_Contact__c = candidateList[2].Id
			)
		};
		insert projectList;

		projectResourceList = new List<TMS__Project_Resource__c> {
			new TMS__Project_Resource__c(
				TMS__Project__c = projectList[0].Id,
				TMS__Contact__c = candidateList[0].Id
			)
		};
		insert projectResourceList;
		
		weekManagementList = new List<TMS__Week_Management__c> {
			new TMS__Week_Management__c(
				TMS__Start_Date__c = Date.today(),
				TMS__End_Date__c = Date.today() + 6,
				TMS__Active__c = true
			)
		};
		insert weekManagementList;
		
		taskList = new List<TMS__Task__c> {
			new TMS__Task__c(
				Name = 'Task #1',
				TMS__Project__c = projectList[0].Id,
				TMS__Project_Resource__c = projectResourceList[0].Id,
				TMS__Task_data_type__c = 'Number',
				TMS__Billable__c = 'Yes'
			),
			new TMS__Task__c(
				Name = 'Task #2',
				TMS__Project__c = projectList[0].Id,
				TMS__Project_Resource__c = projectResourceList[0].Id,
				TMS__Task_data_type__c = 'Time',
				TMS__Billable__c = 'Yes'
			)
		};
		insert taskList;
		
		timeList = new List<TMS__Time__c> {
			new TMS__Time__c(
				TMS__Task__c = taskList[0].Id,
				TMS__Date__c = Date.today(),
				TMS__Time_Spent__c = 10.0,
				TMS__Week_Management__c = weekManagementList[0].Id,
				Status__c = 'Submitted'
			),
			new TMS__Time__c(
				TMS__Task__c = taskList[0].Id,
				TMS__Date__c = Date.today() + 1,
				TMS__Time_Spent__c = 10.0,
				TMS__Week_Management__c = weekManagementList[0].Id,
				Status__c = 'Submitted'
			),
			new TMS__Time__c(
				TMS__Task__c = taskList[0].Id,
				TMS__Date__c = Date.today() + 2,
				TMS__Time_Spent__c = 8.0,
				TMS__Week_Management__c = weekManagementList[0].Id,
				Status__c = 'Submitted'
			),
			new TMS__Time__c(
				TMS__Task__c = taskList[0].Id,
				TMS__Date__c = Date.today() + 3,
				TMS__Time_Spent__c = 0.0,
				TMS__Week_Management__c = weekManagementList[0].Id,
				Status__c = 'Submitted'
			),
			new TMS__Time__c(
				TMS__Task__c = taskList[0].Id,
				TMS__Date__c = Date.today() + 4,
				TMS__Time_Spent__c = 8.0,
				TMS__Week_Management__c = weekManagementList[0].Id,
				Status__c = 'Submitted'
			),
			new TMS__Time__c(
				TMS__Task__c = taskList[1].Id,
				TMS__Date__c = Date.today(),
				TMS__Time_Spent_Time_Format__c = '20:00',
				TMS__Week_Management__c = weekManagementList[0].Id,
				Status__c = 'Submitted'
			),
			new TMS__Time__c(
				TMS__Task__c = taskList[1].Id,
				TMS__Date__c = Date.today() + 1,
				TMS__Time_Spent_Time_Format__c = '20:00',
				TMS__Week_Management__c = weekManagementList[0].Id,
				Status__c = 'Submitted'
			),
			new TMS__Time__c(
				TMS__Task__c = taskList[1].Id,
				TMS__Date__c = Date.today() + 2,
				TMS__Time_Spent_Time_Format__c = '08:00',
				TMS__Week_Management__c = weekManagementList[0].Id,
				Status__c = 'Submitted'
			),
			new TMS__Time__c(
				TMS__Task__c = taskList[1].Id,
				TMS__Date__c = Date.today() + 3,
				TMS__Time_Spent_Time_Format__c = '01:00',
				TMS__Week_Management__c = weekManagementList[0].Id,
				Status__c = 'Submitted'
			),
			new TMS__Time__c(
				TMS__Task__c = taskList[1].Id,
				TMS__Date__c = Date.today() + 4,
				TMS__Time_Spent_Time_Format__c = '08:00',
				TMS__Week_Management__c = weekManagementList[0].Id,
				Status__c = 'Submitted'
			)
		};
		insert timeList;
	}
	
	
	/**
	* @name testSendFor
	* @author Aliaksandr Satskou
	* @date 08/06/2013
	* @description Testing SendFor, SendForAccounting, SendForApprove classes.
	* @return void
	*/
    static testMethod void testSendFor() {
    	initData();
    	
    	
    	Test.startTest();
    	
        SendForAccountingWebservice.send(weekManagementList[0].Id);
        
        new SendForAccounting().send(weekManagementList[0].Id);
        new SendForAccounting().sendBasedParticularResource(weekManagementList[0].Id, candidateList[0].Id);
        
        new SendForApprove().send(weekManagementList[0].Id);
        new SendForApprove().sendBasedParticularResource(weekManagementList[0].Id, candidateList[0].Id);
        
        Test.stopTest();
    }
}