/**
* @author Aliaksandr Satskou
* @date 04/17/2014
* @description SOQL Helper.
*/
public with sharing class QueryHelper {


    /**
    * @author Aliaksandr Satskou
    * @name getSObjectListWithAllFields
    * @date 04/17/2014
    * @param objectName SObject Name
    * @param addFieldSet Additional fields of related SObjects
    * @param whereClause Where clause of query
    * @description Getting list of records with all selected fields.
    * @return List<sObject>
    */
    public static List<sObject> getSObjectListWithAllFields(
        String objectName, Set<String> addFieldSet, String whereClause) {

        Map<String, Schema.SObjectField> sObjectFieldMap =
                Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();


        String whatClause = '';

        for (String fieldName : sObjectFieldMap.keySet()) {
            whatClause += fieldName + ', ';
        }

        if (addFieldSet != null) {
            for (String addField : addFieldSet) {
                whatClause += addField + ', ';
            }
        }

        whatClause = (whatClause != '') ? whatClause.subString(0, whatClause.length() - 2) : 'Id';


        String query = 'SELECT ' + whatClause + ' FROM ' + objectName +
                (((whereClause != null) && (whereClause != ''))
                        ? ' WHERE ' + whereClause
                        : '');
        List<sObject> sObjectList = new List<sObject>();

        try {
            sObjectList = DataBase.query(query);
        } catch (QueryException e) { }


        return sObjectList;
    }
}