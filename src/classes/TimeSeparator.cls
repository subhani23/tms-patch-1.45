/**
* @author Aliaksandr Satskou
* @date 10/31/2013
* @description Separate Time objects for same Task to different lists.
*/
public with sharing class TimeSeparator {
	
	private List<TMS__Time__c> timeList;
	
	private Integer listNumber;
	private List<List<TMS__Time__c>> listOfSeparatedTimeList;
	
	public Boolean isSeparated;
	
	public Boolean isNext {
		get {
			return (listNumber + 2 <= listOfSeparatedTimeList.size());
		}
		private set;
	}
	
	public Boolean isPrevious {
		get {
			return (listNumber + 1 > 1);
		}
		private set;
	}
	
	
	/**
	* @author Aliaksandr Satskou
	* @name TimeSeparator
	* @date 10/31/2013
	* @description Init data.
	* @param timeList List of Time objects
	*/
	public TimeSeparator(List<TMS__Time__c> timeList) {
		this.timeList = timeList;
		
		listNumber = -1;
		
		listOfSeparatedTimeList = new List<List<TMS__Time__c>>();
		listOfSeparatedTimeList.add(new List<TMS__Time__c>());
		
		isSeparated = separateTimes();
		
		isNext = (isSeparated) ? true : false;
		isPrevious = (isSeparated) ? true : false;
	}
	
	
	/**
	* @author Aliaksandr Satskou
	* @name separateTimes
	* @date 10/31/2013
	* @description Separating Time objects.
	* @param task Task object
	* @return Boolean
	*/
	private Boolean separateTimes() {
		Boolean isAdded = false;
		
		for (TMS__Time__c timeObj : timeList) {
			System.debug(LoggingLevel.ERROR, '::::::timeObj=' + timeObj);
			isAdded = false;
			
			for (List<TMS__Time__c> timeList : listOfSeparatedTimeList) {
				if (isPlaceFree(timeList, timeObj)) {
					System.debug(LoggingLevel.ERROR, '::::::isPlaceFree=true');
					timeList.add(timeObj);
					isAdded = true;
					
					break;
				}
			}
			
			if (!isAdded) {
				System.debug(LoggingLevel.ERROR, '::::::isPlaceFree=false');
				listOfSeparatedTimeList.add(new List<TMS__Time__c> { timeObj });
			}
			
			System.debug(LoggingLevel.ERROR, '::::::listOfSeparatedTimeList=' + listOfSeparatedTimeList);
		}
		
		
		if (listOfSeparatedTimeList.size() > 1) {
			return true;
		} else {
			return false;
		}
	}
	
	
	/**
	* @author Aliaksandr Satskou
	* @name isPlaceFree
	* @date 10/31/2013
	* @description Checking, if Time object with Date already there.
	* @param timeList List of Time objects
	* @param timeObj Time object
	* @return Boolean
	*/
	private Boolean isPlaceFree(List<TMS__Time__c> timeList, TMS__Time__c timeObj) {
		for (TMS__Time__c tObj : timeList) {
			if (tObj.TMS__Date__c == timeObj.TMS__Date__c) {
				return false;
			}
		}
		
		return true;
	}
	
	
	/**
	* @author Aliaksandr Satskou
	* @name getNextTimeList
	* @date 10/31/2013
	* @description Getting next Time list.
	* @return List<TMS__Time__c>
	*/
	public List<TMS__Time__c> getNextTimeList() {
		if (isSeparated) {
			if (isNext) {
				listNumber++;
				
				return listOfSeparatedTimeList[listNumber];
			}
		}
		
		return null;
	}
	
	
	/**
	* @author Aliaksandr Satskou
	* @name getPreviousTimeList
	* @date 10/31/2013
	* @description Getting previous Time list.
	* @return List<TMS__Time__c>
	*/
	public List<TMS__Time__c> getPreviousTimeList() {
		if (isSeparated) {
			if (isPrevious) {
				listNumber--;
				
				return listOfSeparatedTimeList[listNumber];
			}
		}
		
		return null;
	}
}