public inherited sharing class SendForApprove extends SendFor {
	
	/* Added by Aliaksandr Satskou, 02/13/2013 */
	private String SiteURL = (TMS__TimesheetCustomSettingsHierarchy__c.getInstance() != null)
			? TMS__TimesheetCustomSettingsHierarchy__c.getInstance().TMS__Site_URL__c
			: null;
			
	
	/**
	* @author Aliaksandr Satskou
	* @name getManagerApprovalStatusList
	* @date 05/04/2013
	* @description Getting manager approval statuses.
	* @return List of manager approval statuses
	*/
	private static List<String> getManagerApprovalStatusList() {
		TMS__TimesheetCustomSettingsHierarchy__c v_customSettingHierarchy = 
				TMS__TimesheetCustomSettingsHierarchy__c.getInstance();
		
		String v_managerApprovalStatuses = (v_customSettingHierarchy != null)
				? v_customSettingHierarchy.TMS__Statuses_for_Manager_Approval__c
				: null;
		
		List<String> v_managerApprovalStatusList = (v_managerApprovalStatuses != null)
				? v_managerApprovalStatuses.split(';')
				: new List<String>();
				
		return v_managerApprovalStatusList;
	}
	
	
	protected override void getRecepientIdSet() {
		List<Time__c> submittedTimes = getSubmittedTimes();
		recepientIdSet = getApproverIdSetFromTimes(submittedTimes);
	}
	
	
	/**
	* @author Aliaksandr Satskou
	* @name getRecepientIdSet
	* @date 05/04/2013
	* @description Getting set of recepients.
	* @param p_resourceId Id of resource
	* @return void
	*/
	protected override void getRecepientIdSet(Id p_resourceId) {
		List<String> v_managerApprovalStatusList = getManagerApprovalStatusList();
		
		List<TMS__Time__c> v_timeList = [
				SELECT Task__r.Project__r.Internal_Approver__c
				FROM Time__c
				WHERE Week_Management__c = :weekManagement.Id 
				  AND Status__c IN :v_managerApprovalStatusList
				  AND Task__r.Project_Resource__r.TMS__Contact__c = :p_resourceId];
		
		System.debug(LoggingLevel.ERROR, '::::::::::::::::::::;v_timeList=' + v_timeList);
		
		recepientIdSet = getApproverIdSetFromTimes(v_timeList);
	}
	
	
	private List<Time__c> getSubmittedTimes() {
		/* Added by Aliaksandr Satskou, 03/18/2013 (case #00015548, point 1) */
		List<String> v_managerApprovalStatusList = getManagerApprovalStatusList();
		
		/* Edited by Aliaksandr Satskou, 03/18/2013 (case #00015548, point 1) */
		return [
				SELECT Task__r.Project__r.Internal_Approver__c
				FROM Time__c
				WHERE Week_Management__c = :weekManagement.Id 
				  AND Status__c IN :v_managerApprovalStatusList];
	}
	
	private static Set<Id> getApproverIdSetFromTimes(List<Time__c> p_timeList) {
		Set<Id> v_internalApproverIdSet = new Set<Id>();
		for (Time__c v_time : p_timeList) {
			v_internalApproverIdSet.add(v_time.Task__r.Project__r.Internal_Approver__c);
		}
		return v_internalApproverIdSet;
	}
	
	protected override List<Messaging.SingleEmailMessage> generateEmails(Contact p_recepient) {
		Messaging.SingleEmailMessage v_email = new Messaging.SingleEmailMessage();
		
		
		/* Added by Aliaksandr Satskou, 11/19/2014 (case #00033908) */
		Boolean isNoActivity = (Boolean)CustomSettingsUtility.getCustomSettingListTypeValue(
				null, 'TMS__Don_t_create_Activity_History__c');
		
		if (isNoActivity) {
			v_email.setToAddresses(new List<String> { p_recepient.Email });
		} else {
			v_email.setTargetObjectId(p_recepient.Id);
		}
		
		
		v_email.setSubject(System.Label.Approval_Email_Subject);
		v_email.setPlainTextBody(String.format(System.Label.Approval_Email_Body, 
				new String[] { 
						getApprovalPageURL(p_recepient),
						p_recepient.Name,
						UserInfo.getName()}));
		
		return new List<Messaging.SingleEmailMessage> {v_email};
	}
	
	private String getApprovalPageURL(Contact p_recepient) {
		/* Edited by Aliaksandr Satskou, 03/20/2013, (case #00015810) */
		/* Commented by Aliaksandr Satskou, 02/13/2013 */
		return /*TMS__TimesheetCustomSettingsHierarchy__c.getInstance().TMS__Site_URL__c*/ SiteURL +
				'TMS__TimesheetApproval?sessionId=' + 
				p_recepient.FCMS__Sessions__r[0].FCMS__SessionId__c +
				'&approval=manager' + 
				'&p='+ EncodingUtil.urlEncode(p_recepient.TMS__CMS_profile_Name__c, 'UTF-8')  + 
				'&week=' + EncodingUtil.urlEncode(weekManagement.TMS__Week__c, 'UTF-8');
				
	}
}