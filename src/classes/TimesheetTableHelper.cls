public without sharing class TimesheetTableHelper {
	
	private static Logger logger = new Logger('TimesheetTableHelper');
	
	public static TMS__Week_Management__c getDefaultWeekManagement(
			List<TMS__Project__c> projectList) {
		
		TMS__Week_Management__c defaultWeekManagement;
		
		String weekParameter = ApexPages.currentPage().getParameters().get('week');
		if (weekParameter != null) {
			defaultWeekManagement = getWeekManagement(
					'AND TMS__Week__c = \'' + weekParameter + '\'',
					projectList);
		}
		
		if (defaultWeekManagement == null) {
			defaultWeekManagement = getWeekManagement(
					'AND TMS__Start_Date__c = THIS_WEEK',
					projectList);
		}
		
		if (defaultWeekManagement == null) {
			defaultWeekManagement = getWeekManagement(
					'',
					projectList);
		}
		
		return defaultWeekManagement;
	}
	
	public static Integer getWeekManagementAdjustment(List<TMS__Project__c> projectList) {
		
		Boolean enableFlexibleStartDates = TMS__TimesheetCustomSettingsHierarchy__c.getOrgDefaults()
					.Enable_Flexible_Start_Dates__c == true;
		
		if (!enableFlexibleStartDates) {
			return 0;
		}
		
		Set<Object> isTheSameOperation = 
				GroupByHelper.getFieldValues(
						projectList, 
						'Client__r.Week_Management_Adjustment__c');
		
		logger.log('getWeekManagementAdjustment', 'isTheSameOperation', isTheSameOperation);
		
		if (isTheSameOperation.size() != 1) {
			return 0;
		}
		
		Set<Object> isTheSameNumberOfDays = 
				GroupByHelper.getFieldValues(
						projectList, 
						'Client__r.Week_Management_Adjustment_Days__c');
		
		logger.log('getWeekManagementAdjustment', 'isTheSameNumberOfDays', isTheSameNumberOfDays);
		
		if (isTheSameNumberOfDays.size() != 1) {
			return 0;
		}
		
		Account resourceAccount = projectList[0].Client__r;
		
		Decimal adjumentDays = resourceAccount.Week_Management_Adjustment_Days__c != null 
									? resourceAccount.Week_Management_Adjustment_Days__c
									: 0;
		if (resourceAccount.Week_Management_Adjustment__c == Label.Week_Management_Adjustment_Add) {
			return Integer.valueOf(
					adjumentDays);
		} else if (resourceAccount.Week_Management_Adjustment__c == Label.Week_Management_Adjustment_Subtract) {
			return Integer.valueOf(adjumentDays) * -1;
		}
		
		return 0;
	}
	
	private static TMS__Week_Management__c getWeekManagement(
			String additionalWherePart, List<TMS__Project__c> projectList) {
		
		List<TMS__Week_Management__c> weekManagementList = new List<TMS__Week_Management__c>();
		
		String basicQuery = 
				' SELECT TMS__Week__c, TMS__Start_Date__c, TMS__End_Date__c ' +
				' FROM TMS__Week_Management__c ' + 
				' WHERE TMS__Active__c = true {0} ' +
				' ORDER BY TMS__Week__c DESC ' +
				' LIMIT 1 ';
				
		weekManagementList = (List<TMS__Week_Management__c>)Database.query(
				String.format(basicQuery, new String[] {additionalWherePart}));
		
		if (weekManagementList.size() > 0) {
			TMS__Week_Management__c week = weekManagementList[0];
			
			Integer adjustment = getWeekManagementAdjustment(projectList);
			
			logger.log('getWeekManagement', 'adjustment', adjustment);
			
			week.TMS__Start_Date__c = week.TMS__Start_Date__c.addDays(adjustment);
			week.TMS__End_Date__c = week.TMS__End_Date__c.addDays(adjustment);
			
			return week;
		}
		
		return null;
	}
}