/**
* @author Aliaksandr Satskou
* @date 12/24/2012
* @description Unit Test for CustomSettingsUtility class.
*/
@isTest
private class CustomSettingsUtilityTest {
	private static List<TMS__TimesheetCustomSettings__c> timesheetCustomSettingListTypeList;
	private static List<TMS__TimesheetCustomSettingsHierarchy__c> timesheetCustomSettingHierarchyTypeList;
	private static List<TMS__Payroll_Provider_Settings__c> payrollProviderSettingList;
	private static List<TMS__Payroll_Settings__c> payrollSettingList;

	/**
	* @author Aliaksandr Satskou
	* @date 12/24/2012
	* @description Creating all needed test data.
	* @return void
	*/
	private static void initData() {
		timesheetCustomSettingListTypeList = new List<TMS__TimesheetCustomSettings__c> {
			new TMS__TimesheetCustomSettings__c(
				Name = 'Custom_Setting_List_Type #1',
				TMS__Disable_sync_placement_to_task__c = true,
				TMS__Timesheet_Save_Status__c = 'Open',
				TMS__Timesheet_Submitted_Status__c = 'Submitted',
				TMS__Timesheet_Approved_Status__c = 'Approved',
				TMS__Timesheet_Rejected_Status__c = 'Rejected',
				TMS__Expense_Save_Status__c = 'Open',
				TMS__Expense_Submitted_Status__c = 'Submitted',
				TMS__Expense_Approved_Status__c = 'Approved',
				TMS__Expense_Rejected_Status__c = 'Rejected',
				TMS__Notify_Resource_on_Rejection__c = true,
				TMS__Notify_Manager_on_Rejection__c = true
			),
			new TMS__TimesheetCustomSettings__c(
				Name = 'Custom_Setting_List_Type #2',
				TMS__Disable_sync_placement_to_task__c = false,
				TMS__Timesheet_Save_Status__c = 'Open',
				TMS__Timesheet_Submitted_Status__c = 'Submitted',
				TMS__Timesheet_Approved_Status__c = 'Approved',
				TMS__Timesheet_Rejected_Status__c = 'Rejected',
				TMS__Expense_Save_Status__c = 'Open',
				TMS__Expense_Submitted_Status__c = 'Submitted',
				TMS__Expense_Approved_Status__c = 'Approved',
				TMS__Expense_Rejected_Status__c = 'Rejected',
				TMS__Notify_Resource_on_Rejection__c = true,
				TMS__Notify_Manager_on_Rejection__c = true
			)
		};
		insert timesheetCustomSettingListTypeList;

		timesheetCustomSettingHierarchyTypeList = new List<TMS__TimesheetCustomSettingsHierarchy__c> {
			new TMS__TimesheetCustomSettingsHierarchy__c(
				SetupOwnerId = UserInfo.getUserId(),
				TMS__Site_URL__c = 'Site_URL #1'
			),
			new TMS__TimesheetCustomSettingsHierarchy__c(
				SetupOwnerId = UserInfo.getProfileId(),
				TMS__Site_URL__c = 'Site_URL #2'
			)
		};
		insert timesheetCustomSettingHierarchyTypeList;

		payrollProviderSettingList = new List<TMS__Payroll_Provider_Settings__c> {
			new TMS__Payroll_Provider_Settings__c(
				Name = 'Payroll Provider #1',
				TMS__Payroll_Provider_Name__c = 'Payroll Provider #1',
				TMS__Email__c = 'test_1@test.com',
				TMS__Payroll_items_sort_by__c = 'TMS__API_Field_Name__c'
			),
			new TMS__Payroll_Provider_Settings__c(
				Name = 'Payroll Provider #2',
				TMS__Payroll_Provider_Name__c = 'Payroll Provider #2',
				TMS__Email__c = 'test_2@test.com',
				TMS__Payroll_items_sort_by__c = 'TMS__API_Field_Name__c'
			)
		};
		insert payrollProviderSettingList;

		payrollSettingList = new List<TMS__Payroll_Settings__c> {
			new TMS__Payroll_Settings__c(
				Name = 'Payroll Field #1',
				TMS__Payroll_Provider_Name__c = 'Payroll Provider #1',
				TMS__Object_API_Name__c = 'TMS__API_Object_Name__c',
				TMS__File_Name__c = 'File Name #1',
				TMS__Field_API_Name__c = 'TMS__API_Field_Name__c',
				TMS__Characters__c = '1-10'
			),
			new TMS__Payroll_Settings__c(
				Name = 'Payroll Field #2',
				TMS__Payroll_Provider_Name__c = 'Payroll Provider #2',
				TMS__Object_API_Name__c = 'TMS__API_Object_Name__c',
				TMS__File_Name__c = 'File Name #2',
				TMS__Field_API_Name__c = 'TMS__API_Field_Name__c',
				TMS__Characters__c = '11-20'
			)
		};
		insert payrollSettingList;
	}

	/**
	* @author Aliaksandr Satskou
	* @date 12/24/2012
	* @description Testing CustomSettingsUtility class.
	* @return void
	*/
    static testMethod void testCustomSettingsUtility() {
		initData();

		/* Testing CustomSettingsUtility.getCustomSettingListTypeList */
		System.assertEquals(2, CustomSettingsUtility.getCustomSettingListTypeList().size());

		/* Testing CustomSettingsUtility.getCustomSettingListTypeByDataSetName*/
		TMS__TimesheetCustomSettings__c v_timesheetCustomSettingListType =
				CustomSettingsUtility.getCustomSettingListTypeByDataSetName('Custom_Setting_List_Type #1');
		System.assertEquals(true, v_timesheetCustomSettingListType.TMS__Disable_sync_placement_to_task__c);

		/* Testing CustomSettingsUtility.getCustomSettingListTypeValue, case #1 */
		Object v_customSettingListTypeValue = CustomSettingsUtility.getCustomSettingListTypeValue(
				'Custom_Setting_List_Type #2', 'TMS__Disable_sync_placement_to_task__c');
		System.assertEquals(false, (Boolean)v_customSettingListTypeValue);
		/* Testing CustomSettingsUtility.getCustomSettingListTypeValue, case #2 */
		v_customSettingListTypeValue = CustomSettingsUtility.getCustomSettingListTypeValue(
				null, 'TMS__Disable_sync_placement_to_task__c');

		/* Testing CustomSettingsUtility.getCustomSettingHierarchyType */
		TMS__TimesheetCustomSettingsHierarchy__c v_timesheetCustomSettingHierarchyType =
				CustomSettingsUtility.getCustomSettingHierarchyType();
		System.assertEquals('Site_URL #1', v_timesheetCustomSettingHierarchyType.TMS__Site_URL__c);

		/* Testing CustomSettingsUtility.getCustomSettingHierarchyTypeByUserOrProfileId */
		v_timesheetCustomSettingHierarchyType =
				CustomSettingsUtility.getCustomSettingHierarchyTypeByUserOrProfileId(UserInfo.getProfileId());
		System.assertEquals('Site_URL #2', v_timesheetCustomSettingHierarchyType.TMS__Site_URL__c);

		/* Testing CustomSettingsUtility.getCustomSettingHierarchyTypeOrgDefaults */
		v_timesheetCustomSettingHierarchyType =
				CustomSettingsUtility.getCustomSettingHierarchyTypeOrgDefaults();

		/* Testing CustomSettingsUtility.getPayrollProviderSettingList */
		System.assertEquals(2, CustomSettingsUtility.getPayrollProviderSettingList().size());

		/* Testing CustomSettingsUtility.getPayrollProviderSettingByDataSetName */
		TMS__Payroll_Provider_Settings__c v_payrollProviderSetting =
				CustomSettingsUtility.getPayrollProviderSettingByDataSetName('Payroll Provider #1');
		System.assertEquals('test_1@test.com', v_payrollProviderSetting.TMS__Email__c);

		/* Testing CustomSettingsUtility.getPayrollProviderSettingValue, case #1 */
		Object v_payrollProviderSettingValue = CustomSettingsUtility.getPayrollProviderSettingValue(
				'Payroll Provider #2', 'TMS__Email__c');
		System.assertEquals('test_2@test.com', v_payrollProviderSettingValue);
		/* Testing CustomSettingsUtility.getPayrollProviderSettingValue, case #2 */
		v_payrollProviderSettingValue = CustomSettingsUtility.getPayrollProviderSettingValue(
				null, 'TMS__Email__c');

		/* Testing CustomSettingsUtility.getPayrollProviderNameList */
		System.assertEquals(2, CustomSettingsUtility.getPayrollProviderNameList().size());

		/* Testing CustomSettingsUtility.getPayrollSettingList */
		System.assertEquals(2, CustomSettingsUtility.getPayrollSettingList().size());

		/* Testing CustomSettingsUtility.getPayrollSettingByDataSetName */
		TMS__Payroll_Settings__c v_payrollSetting =
				CustomSettingsUtility.getPayrollSettingByDataSetName('Payroll Field #1');
		System.assertEquals('Payroll Provider #1', v_payrollSetting.TMS__Payroll_Provider_Name__c);

		/* Testing CustomSettingsUtility.getPayrollSettingValue, case #1 */
		Object v_payrollSettingValue = CustomSettingsUtility.getPayrollSettingValue(
				'Payroll Field #2', 'TMS__File_Name__c');
		System.assertEquals('File Name #2', v_payrollSettingValue);
		/* Testing CustomSettingsUtility.getPayrollSettingValue, case #2 */
		v_payrollSettingValue = CustomSettingsUtility.getPayrollSettingValue(null, 'TMS__File_Name__c');

		/* Testing CustomSettingsUtility.getPayrollFileNameSet */
		System.assertEquals(2, CustomSettingsUtility.getPayrollFileNameSet().size());
		
		
		String status = '';
		
		/* Testing CustomSettingsUtility.getTimesheetSaveStatus */
		status = CustomSettingsUtility.getTimesheetSaveStatus();
		System.assertEquals('Open', status);
		
		/* Testing CustomSettingsUtility.getTimesheetSubmittedStatus */
		status = CustomSettingsUtility.getTimesheetSubmittedStatus();
		System.assertEquals('Submitted', status);
		
		/* Testing CustomSettingsUtility.getTimesheetApprovedStatus */
		status = CustomSettingsUtility.getTimesheetApprovedStatus();
		System.assertEquals('Approved', status);
		
		/* Testing CustomSettingsUtility.getTimesheetRejectedStatus */
		status = CustomSettingsUtility.getTimesheetRejectedStatus();
		System.assertEquals('Rejected', status);
		
		/* Testing CustomSettingsUtility.getExpenseSaveStatus */
		status = CustomSettingsUtility.getExpenseSaveStatus();
		System.assertEquals('Open', status);
		
		/* Testing CustomSettingsUtility.getExpenseSubmittedStatus */
		status = CustomSettingsUtility.getExpenseSubmittedStatus();
		System.assertEquals('Submitted', status);
		
		/* Testing CustomSettingsUtility.getExpenseApprovedStatus */
		status = CustomSettingsUtility.getExpenseApprovedStatus();
		System.assertEquals('Approved', status);
		
		/* Testing CustomSettingsUtility.getExpenseRejectedStatus */
		status = CustomSettingsUtility.getExpenseRejectedStatus();
		System.assertEquals('Rejected', status);
		
		
		/* Testing CustomSettingsUtility.isNotifyManagerOnRejection */
		Boolean isNotifyManager = CustomSettingsUtility.isNotifyManagerOnRejection();
		System.assertEquals(isNotifyManager, true);
		
		/* Testing CustomSettingsUtility.isNotifyResourceOnRejection */
		Boolean isNotifyResource = CustomSettingsUtility.isNotifyResourceOnRejection();
		System.assertEquals(isNotifyResource, true);
    }
}