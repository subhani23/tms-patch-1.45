/**
* @author Aliaksandr Satskou
* @date 03/25/2013
* @description Test class for OvertimeCalculationHelper class.
*/
@isTest
private class OvertimeCalculationHelperTest {

	private static List<TMS__Project__c> projectList;
	private static List<Contact> candidateList;
	private static List<TMS__Project_Resource__c> projectResourceList;
	private static List<TMS__Task__c> taskList;
	private static List<TMS__Time__c> timeList;
	private static List<TMS__Week_Management__c> weekManagementList;


	/**
	* @name initData
	* @author Aliaksandr Satskou
	* @data 05/22/2013
	* @description Initialize all needed test data.
	* @return void
	*/
	private static void initData() {
		candidateList = new List<Contact> {
			new Contact(
				LastName = 'Contact #1'
			)
		};
		insert candidateList;

		projectList = new List<TMS__Project__c> {
			new TMS__Project__c(
				RecordTypeId = RecordTypeUtility.getProjectRecordTypeId(),
				Name = 'Project #1',
				TMS__Project_Status__c = 'Open'
			)
		};
		insert projectList;

		projectResourceList = new List<TMS__Project_Resource__c> {
			new TMS__Project_Resource__c(
				TMS__Project__c = projectList[0].Id,
				TMS__Contact__c = candidateList[0].Id
			)
		};
		insert projectResourceList;

		weekManagementList = new List<TMS__Week_Management__c> {
			new TMS__Week_Management__c(
				TMS__Start_Date__c = Date.today(),
				TMS__End_Date__c = Date.today() + 6,
				TMS__Active__c = true
			)
		};
		insert weekManagementList;

		taskList = new List<TMS__Task__c> {
			new TMS__Task__c(
				Name = 'Regular Task #1',
				TMS__Project__c = projectList[0].Id,
				TMS__Project_Resource__c = projectResourceList[0].Id,
				TMS__Overtime_overflow_task__c = null,
				TMS__Overtime_hours_threshold__c = 40,
				TMS__Overtime_calculation_schedule__c = 'Daily',
				TMS__Task_data_type__c = 'Number'
			),
			new TMS__Task__c(
				Name = 'Regular Task #2',
				TMS__Project__c = projectList[0].Id,
				TMS__Project_Resource__c = projectResourceList[0].Id,
				TMS__Overtime_overflow_task__c = null,
				TMS__Overtime_hours_threshold__c = 40,
				TMS__Overtime_calculation_schedule__c = 'Weekly',
				TMS__Task_data_type__c = 'Time'
			),
			new TMS__Task__c(
				Name = 'Overtime Task #1',
				TMS__Project__c = projectList[0].Id,
				TMS__Project_Resource__c = projectResourceList[0].Id,
				TMS__Overtime_overflow_task__c = null,
				TMS__Overtime_hours_threshold__c = null,
				TMS__Overtime_calculation_schedule__c = null,
				TMS__Task_data_type__c = 'Number'
			),
			new TMS__Task__c(
				Name = 'Overtime Task #2',
				TMS__Project__c = projectList[0].Id,
				TMS__Project_Resource__c = projectResourceList[0].Id,
				TMS__Overtime_overflow_task__c = null,
				TMS__Overtime_hours_threshold__c = null,
				TMS__Overtime_calculation_schedule__c = null,
				TMS__Task_data_type__c = 'Time'
			)
		};
		insert taskList;

		timeList = new List<TMS__Time__c> {
			new TMS__Time__c(
				TMS__Task__c = taskList[0].Id,
				TMS__Date__c = Date.today(),
				TMS__Time_Spent__c = 10.0,
				TMS__Week_Management__c = weekManagementList[0].Id
			),
			new TMS__Time__c(
				TMS__Task__c = taskList[0].Id,
				TMS__Date__c = Date.today() + 1,
				TMS__Time_Spent__c = 10.0,
				TMS__Week_Management__c = weekManagementList[0].Id
			),
			new TMS__Time__c(
				TMS__Task__c = taskList[0].Id,
				TMS__Date__c = Date.today() + 2,
				TMS__Time_Spent__c = 8.0,
				TMS__Week_Management__c = weekManagementList[0].Id
			),
			new TMS__Time__c(
				TMS__Task__c = taskList[0].Id,
				TMS__Date__c = Date.today() + 3,
				TMS__Time_Spent__c = 0.0,
				TMS__Week_Management__c = weekManagementList[0].Id
			),
			new TMS__Time__c(
				TMS__Task__c = taskList[0].Id,
				TMS__Date__c = Date.today() + 4,
				TMS__Time_Spent__c = 8.0,
				TMS__Week_Management__c = weekManagementList[0].Id
			),
			new TMS__Time__c(
				TMS__Task__c = taskList[1].Id,
				TMS__Date__c = Date.today(),
				TMS__Time_Spent_Time_Format__c = '20:00',
				TMS__Week_Management__c = weekManagementList[0].Id
			),
			new TMS__Time__c(
				TMS__Task__c = taskList[1].Id,
				TMS__Date__c = Date.today() + 1,
				TMS__Time_Spent_Time_Format__c = '20:00',
				TMS__Week_Management__c = weekManagementList[0].Id
			),
			new TMS__Time__c(
				TMS__Task__c = taskList[1].Id,
				TMS__Date__c = Date.today() + 2,
				TMS__Time_Spent_Time_Format__c = '08:00',
				TMS__Week_Management__c = weekManagementList[0].Id
			),
			new TMS__Time__c(
				TMS__Task__c = taskList[1].Id,
				TMS__Date__c = Date.today() + 3,
				TMS__Time_Spent_Time_Format__c = '01:00',
				TMS__Week_Management__c = weekManagementList[0].Id
			),
			new TMS__Time__c(
				TMS__Task__c = taskList[1].Id,
				TMS__Date__c = Date.today() + 4,
				TMS__Time_Spent_Time_Format__c = '08:00',
				TMS__Week_Management__c = weekManagementList[0].Id
			)
		};
		insert timeList;
	}


	/**
	* @name testOvertimeHelper
	* @author Aliaksandr Satskou
	* @data 05/22/2013
	* @description Testing OvertimeHelper class.
	* @return void
	*/
    static testMethod void testOvertimeHelper() {
        initData();

        /* Creating relationships */
        taskList[0].TMS__Overtime_overflow_task__c = taskList[2].Id;
        taskList[1].TMS__Overtime_overflow_task__c = taskList[3].Id;

        update taskList;


        /* Creating set of Id of Task objects */
        Set<Id> v_taskIdSet = new Set<Id>();

        for (TMS__Task__c v_task : taskList) {
        	v_taskIdSet.add(v_task.Id);
        }

		TimeSheetController controller = new TimeSheetController();
		
       	/* Updating Time objects for Overtime Task object */
       	OvertimeCalculationHelper.calculateOvertime(v_taskIdSet, weekManagementList[0], 'Open',controller);


       	Map<Id, TMS__Task__c> v_overtimeTaskMap = new Map<Id, TMS__Task__c> {
       		taskList[2].Id => taskList[2],
       		taskList[3].Id => taskList[3]
       	};

       	List<TMS__Time__c> v_timeList = [
       			SELECT TMS__Time_Spent__c, TMS__Time_Spent_Time_Format__c, TMS__Date__c, TMS__Task__c
       			FROM TMS__Time__c
       			WHERE TMS__Task__c IN :v_overtimeTaskMap.keySet()];

       	System.assertEquals(3, v_timeList.size());
    }
}