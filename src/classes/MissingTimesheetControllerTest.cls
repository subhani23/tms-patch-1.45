/**
* @author Aliaksandr Satskou
* @date 14/01/2013
* @description Test class for MissingTimesheetController class.
*/
@isTest
private class MissingTimesheetControllerTest {

	private static List<Account> accountList;
	private static List<TMS__Project__c> projectList;
	private static List<Contact> candidateList;
	private static List<TMS__Project_Resource__c> projectResourceList;
	private static List<TMS__Week_Management__c> weekManagementList;
	private static List<TMS__Task__c> taskList;
	private static List<TMS__Time__c> timeList;
	private static TMS__Timesheet__c timesheet ;

	/**
	* @name initData
	* @author Aliaksandr Satskou
	* @data 14/01/2013
	* @description Init all needed data.
	* @return void
	*/
	private static void initData() {
		TMS__TimesheetCustomSettings__c v_timesheetCustomSetting = new TMS__TimesheetCustomSettings__c(
			Name = 'Default',
			TMS__Open_Project_Criteria__c = '(TMS__Project_Status__c = \'Open\' OR ' +
					'TMS__Project_Status__c = \'Completed\')',
			TMS__Missing_Timesheet_Status_to_Consider__c ='Open;Partial;Submitted'
					
		);
		insert v_timesheetCustomSetting;

		candidateList = new List<Contact> {
			new Contact(
				LastName = 'Contact #1'
			),
			new Contact(
				LastName = 'Contact #2'
			)
		};
		insert candidateList;

		accountList = new List<Account> {
			new Account(
				Name = 'Account #1'
			)
		};
		insert accountList;

		projectList = new List<TMS__Project__c> {
			new TMS__Project__c(
				RecordTypeId = RecordTypeUtility.getProjectRecordTypeId(),
				Name = 'Project #1',
				TMS__Project_Status__c = 'Open',
				TMS__End_Date__c = Date.today() + 15,
				TMS__Client__c = accountList[0].Id
			)
		};
		insert projectList;

		projectResourceList = new List<TMS__Project_Resource__c> {
			new TMS__Project_Resource__c(
				TMS__Project__c = projectList[0].Id,
				TMS__Contact__c = candidateList[0].Id
			),
			new TMS__Project_Resource__c(
				TMS__Project__c = projectList[0].Id,
				TMS__Contact__c = candidateList[1].Id
			)
		};
		insert projectResourceList;

		taskList = new List<TMS__Task__c> {
			new TMS__Task__c(
				Name = 'Regular Task #1',
				TMS__Project__c = projectList[0].Id,
				TMS__Project_Resource__c = projectResourceList[0].Id
			),
			new TMS__Task__c(
				Name = 'Overtime Task #1',
				TMS__Project__c = projectList[0].Id,
				TMS__Project_Resource__c = projectResourceList[1].Id
			)
		};
		insert taskList;

		weekManagementList = new List<TMS__Week_Management__c> {
			new TMS__Week_Management__c(
				TMS__Start_Date__c = Date.today(),
				TMS__End_Date__c = Date.today() + 7,
				TMS__Active__c = true
			),
			new TMS__Week_Management__c(
				TMS__Start_Date__c = Date.today() + 8,
				TMS__End_Date__c = Date.today() + 15,
				TMS__Active__c = true
			)
		};
		insert weekManagementList;
		
		timesheet = new TMS__Timesheet__c(
					TMS__Week_Management__c = weekManagementList[0].Id,
					TMS__Candidate__c = candidateList[0].Id,
					TMS__Status__c = 'Open');
		insert timesheet;
			
		timeList = new List<TMS__Time__c> {
			new TMS__Time__c(
				TMS__Accounting_Id__c = 'Accounting Id #1',
				TMS__Time_Spent__c = 0,
				TMS__Candidate__c = candidateList[0].id,
				TMS__Date__c = Date.today() + 3,
				TMS__Task__c = taskList[0].Id,
				TMS__Timesheet__c = timesheet.id,
				TMS__Week_Management__c = weekManagementList[0].Id,
				TMS__Status__c = 'Open'
			),
			new TMS__Time__c(
				TMS__Accounting_Id__c = 'Accounting Id #2',
				TMS__Time_Spent__c = 8,
				TMS__Date__c = Date.today() + 6,
				TMS__Task__c = taskList[0].Id,
				TMS__Timesheet__c = timesheet.id,
				TMS__Week_Management__c = weekManagementList[0].Id,
				TMS__Status__c = 'Submitted'
			),
			new TMS__Time__c(
				TMS__Accounting_Id__c = 'Accounting Id #3',
				TMS__Time_Spent__c = 0,
				TMS__Date__c = Date.today() + 9,
				TMS__Task__c = taskList[1].Id,
				TMS__Week_Management__c = weekManagementList[1].Id,
				TMS__Status__c = 'Submitted'
			),
			new TMS__Time__c(
				TMS__Accounting_Id__c = 'Accounting Id #4',
				TMS__Time_Spent__c = 0,
				TMS__Date__c = Date.today() + 12,
				TMS__Task__c = taskList[1].Id,
				TMS__Week_Management__c = weekManagementList[1].Id,
				TMS__Status__c = 'Submitted'
			)
		};
		insert timeList;
	}

	/**
	* @name testMissingTimesheets_case1
	* @author Aliaksandr Satskou
	* @data 14/01/2013
	* @description Testing MissingTimesheetController class.
	* @return void
	*/
    static testMethod void testMissingTimesheets_case1() {
		initData();

		ApexPages.currentPage().getParameters().put('WeekManagementId', weekManagementList[0].Id);
		MissingTimesheetController v_missingTimesheetController = new MissingTimesheetController();

		System.assertEquals(1, v_missingTimesheetController.accountProjectListMap.size());
		System.assertEquals(1, v_missingTimesheetController.projectProjectResourceSetMap.size());
		
		MissingTimesheetNewController timeSheetNew = new MissingTimesheetNewController();
		MissingTimesheetNewController.ContactTimesheet contTime = new MissingTimesheetNewController.ContactTimesheet();
		List<MissingTimesheetNewController.ContactTimesheet> contactTimesheetList = new List<MissingTimesheetNewController.ContactTimesheet>();
		contTime.timesheet = timesheet;
		contTime.cont = candidateList[0];
		contTime.selected = true;
		contactTimesheetList.add(contTime);
		timeSheetNew.contactTimesheetList = contactTimesheetList;
		timeSheetNew.init();
		timeSheetNew.sendFirstReminder();
		timeSheetNew.sendFinalReminder();
		timeSheetNew.sendEmailReminder('Gentle');
    }

    /**
	* @name testMissingTimesheets_case2
	* @author Aliaksandr Satskou
	* @data 14/01/2013
	* @description Testing MissingTimesheetController class.
	* @return void
	*/
    static testMethod void testMissingTimesheets_case2() {
		initData();

		ApexPages.currentPage().getParameters().put('WeekManagementId', weekManagementList[1].Id);
		MissingTimesheetController v_missingTimesheetController = new MissingTimesheetController();

		System.assertEquals(1, v_missingTimesheetController.accountProjectListMap.size());
		System.assertEquals(1, v_missingTimesheetController.projectProjectResourceSetMap.size());
    }
}