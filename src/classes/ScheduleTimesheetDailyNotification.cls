global class ScheduleTimesheetDailyNotification implements Schedulable{
    global void execute(SchedulableContext sc){
        RemaindFillTimesheet obj = new RemaindFillTimesheet();
    }
    
    private static testmethod void ScheduleTimesheetDailyNotificationTest() {
    	String CRON_EXP = '0 0 0 3 9 ? 2022';
    	Test.startTest();
    		System.schedule('test', CRON_EXP, new ScheduleTimesheetDailyNotification());
    	Test.stopTest();
    } 
}