public class TestHelper {
	
	public static TestHelperClasses.ProjectWrapper createProjectWith2Resources2TasksAndInternalApprover() {
		TestHelperClasses.ProjectWrapper v_projectWrapper = createProjectWithInternalApprover();
		
		createProjectResourceWithTasksForProject(v_projectWrapper);
		createProjectResourceWithTasksForProject(v_projectWrapper);
		
		return v_projectWrapper;
	}
	
	private static TestHelperClasses.ProjectWrapper createProjectWithInternalApprover() {
		Contact v_internalApprover = new Contact(LastName = 'Test');
		insert v_internalApprover;
		
		TMS__Project__c v_project = new TMS__Project__c(Name = 'Test');
		v_project.TMS__Internal_Approver__c = v_internalApprover.Id;
		insert v_project;
		
		return new TestHelperClasses.ProjectWrapper(v_project);
	}
	
	private static void createProjectResourceWithTasksForProject(
			TestHelperClasses.ProjectWrapper p_projectWrapper) {
				
		Contact v_employee = new Contact(LastName = 'Test');
		insert v_employee;
		
		TMS__Project_Resource__c v_projectResource = new Project_Resource__c();
		v_projectResource.TMS__Project__c = p_projectWrapper.project.Id;
		v_projectResource.TMS__Contact__c = v_employee.Id;
		insert v_projectResource;
		
		TestHelperClasses.ProjectResourceWrapper v_projectResourceWrapper = 
				new TestHelperClasses.ProjectResourceWrapper(v_projectResource);
		p_projectWrapper.projectResourceList.add(v_projectResourceWrapper);
		
		create2TasksForProjectResource(v_projectResourceWrapper);
	}
	
	private static void create2TasksForProjectResource(
			TestHelperClasses.ProjectResourceWrapper p_projectResourceWrapper) {
		
		List<TMS__Task__c> v_taskList = new List<TMS__Task__c>();
		for (Integer i = 0; i < 2; i++) {
			TMS__Task__c v_task = new TMS__Task__c(Name = 'Test');
			v_task.TMS__Project_Resource__c = p_projectResourceWrapper.projectResource.Id;
			v_taskList.add(v_task);
			
			TestHelperClasses.TaskWrapper v_taskWrapper = 
					new TestHelperClasses.TaskWrapper(v_task);
			p_projectResourceWrapper.taskList.add(v_taskWrapper);
		}
		insert v_taskList;
	}
	
	public static TestHelperClasses.WeekManagementWrapper createWeekManagementWithTimesForProjectResource(
			TestHelperClasses.ProjectResourceWrapper p_projectResourceWrapper) {
		
		TestHelperClasses.WeekManagementWrapper v_weekManagementWrapper = createWeekManagement();
		
		createTimesForWeekManagementAndProjectResource(v_weekManagementWrapper, p_projectResourceWrapper);
		
		return v_weekManagementWrapper;
	}
	
	public static TestHelperClasses.WeekManagementWrapper createWeekManagement() {
		TMS__Week_Management__c v_weekManagement = new TMS__Week_Management__c();
		v_weekManagement.TMS__Active__c = true;
		insert v_weekManagement;
		
		return new TestHelperClasses.WeekManagementWrapper(v_weekManagement);
	}
	
	public static void createTimesForWeekManagementAndProjectResource(
			TestHelperClasses.WeekManagementWrapper p_weekManagementWrapper,
			TestHelperClasses.ProjectResourceWrapper p_projectResourceWrapper) {
	
		for (TestHelperClasses.TaskWrapper v_taskWrapper : p_projectResourceWrapper.taskList) {
			createTimesForWeekManagementAndTask(p_weekManagementWrapper, v_taskWrapper);
		}
	}
	
	private static void createTimesForWeekManagementAndTask(
			TestHelperClasses.WeekManagementWrapper p_weekManagementWrapper,
			TestHelperClasses.TaskWrapper p_taskWrapper) {
		
		List<TMS__Time__c> v_timeList = new List<TMS__Time__c>();
		for (Integer i = 0; i < 7; i++) {
			TMS__Time__c v_time = new TMS__Time__c();
			v_time.TMS__Week_Management__c = p_weekManagementWrapper.weekManagement.Id;
			v_time.TMS__Task__c = p_taskWrapper.task.Id;
			v_timeList.add(v_time);
			
			p_weekManagementWrapper.timeList.add(v_time);
			p_taskWrapper.timeList.add(v_time);
		}
		insert v_timeList;
	}
}