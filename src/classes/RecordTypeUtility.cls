/**
* @author Aliaksandr Satskou
* @date 10/01/2013
* @description Working with Record Type.
*/
public with sharing class RecordTypeUtility {

	/**
	* @name getProjectRecordTypeId
	* @author Aliaksandr Satskou
	* @data 10/01/2013
	* @description Getting Id of Project record type.
	* @return Id
	*/
	public static Id getProjectRecordTypeId() {
		List<Schema.RecordTypeInfo> v_projectRecordTypeInfoList =
				TMS__Project__c.SObjectType.getDescribe().getRecordTypeInfos();

		for (Schema.RecordTypeInfo v_recordTypeInfo : v_projectRecordTypeInfoList) {
			String v_recordTypeName = v_recordTypeInfo.getName().toLowerCase();

			if (v_recordTypeName == 'project') {
				return v_recordTypeInfo.getRecordTypeId();
			}
		}

		return null;
	}
}