/**
* @author Pandeiswari
* @date 02/13/2018
* @description Display Missing Timesheet candidate in Week Management.
*/

public class MissingTimesheetNewController {

	public TMS__Week_Management__c weekManagement { get; set;}
	public List<ContactTimesheet> contactTimesheetList { get; set; }
	public List<String> columns { get; set;}
	private Map<Id, TMS__Timesheet__c> contTimesheetMap ;
	private static String displayColumns = CustomSettingsUtility.getMissingTimesheetDisplayColumns();
	private static String missingCriteria = CustomSettingsUtility.getMissingTimesheetCriteria();
	private static String missingTimesheetStatus = CustomSettingsUtility.getMissingTimesheetStatus();
	private static String missingTimesheetFinalTemplate = CustomSettingsUtility.getFinalEmailTemplate();
	private static String missingTimesheetFirstTemplate = CustomSettingsUtility.getFirstReminderTemplate();
	
	/**
	* @name init
	* @author Pandeiswari
	* @data 14/02/2018
	* @description Get list of timesheet and candidates
	* @return void
	*/
	
	public void init() {
		
		contTimesheetMap = new Map<Id, TMS__Timesheet__c>();
		
		contactTimesheetList = new List<ContactTimesheet>();
		
		ID weekManagementId = ApexPages.currentPage().getParameters().get('WeekManagementId');
		
		columns = displayColumns != null 
					? displayColumns.split(';')
					: new List<String>();
		
		weekManagement = [
			SELECT Name,TMS__Week__c,TMS__Start_Date__c,TMS__End_Date__c 
			FROM TMS__Week_Management__c 
			WHERE Id = :WeekManagementId limit 1];
		
		List<String> missingTimesheetList = missingTimesheetStatus != null 
								?  missingTimesheetStatus.split(';')
								: new List<String>();
		
		MetadataUtilNew mdTimeSheet =
			new MetadataUtilNew(new TMS__Timesheet__c());
			
		MetadataUtilNew mdContact =
			new MetadataUtilNew(new Contact());
			
		List<TMS__Timesheet__c> timeSheetList = Database.query(
			'SELECT '+ mdTimeSheet.getAllFieldsString() + 
			' FROM TMS__Timesheet__c '+
			' WHERE TMS__Week_Management__c =: WeekManagementId' +
			' AND TMS__Status__c IN : missingTimesheetList');
		
		List<Id> candidateIdList = new List<ID>();
		
		for(TMS__Timesheet__c tSheet : timeSheetList) {
			candidateIdList.add(tSheet.TMS__Candidate__c);
			contTimesheetMap.put(tSheet.TMS__Candidate__c, tSheet);
		} 
		//This will return all the candidates who are not there in the timesheet
		
		missingCriteria = missingCriteria != null ? ' AND ' + missingCriteria : '';
		
		List<Contact> candidateList = Database.query(
			'SELECT Name'+ 
			' FROM Contact '+
			' WHERE ID IN : candidateIdList' +
			 missingCriteria);
			
		for(Contact candidate : candidateList) {
			ContactTimesheet contTime = new ContactTimesheet();
			contTime.cont = candidate;
			contTime.timesheet = contTimesheetMap.get(candidate.id);
			contactTimesheetList.add(contTime);
		}
	}
	
	/**
	* @name sendFirstReminder
	* @author Pandeiswari
	* @data 14/02/2018
	* @description Send First Email Remainder
	* @return PageReference
	*/
	
	public void sendFirstReminder() {
		sendEmailReminder(System.Label.Gentle);
	}
	
	/**
	* @name sendFinalReminder
	* @author Pandeiswari
	* @data 14/02/2018
	* @description Send Final Email Remainder
	* @return PageReference
	*/
	
	public void sendFinalReminder() {
		sendEmailReminder(System.Label.Final);
	}
	
	/**
	* @name sendEmailReminder
	* @author Pandeiswari
	* @data 14/02/2018
	* @description Send Email Remainder
	* @param Remainder Type
	* @return Void
	*/
	
	public void sendEmailReminder(String remainderType) {
		try{
			
			List<Id> emailsToSent = new List<ID>();
			
			Boolean selectedCandidate = false;
			
			for(ContactTimesheet contTime : contactTimesheetList) {
				if(contTime.selected == true) {
					selectedCandidate = true;
					emailsToSent.add(contTime.cont.id);
				}
			}
			
			if(selectedCandidate == false) {
				ApexPages.addmessage(new ApexPages.message(
					ApexPages.severity.WARNING,System.Label.Please_select_atleast_one_Candidate));
				return;
			}
			List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
			
			for(Id contactId : emailsToSent) {
				
				Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
				mail.setTargetObjectId(contactId);
				mail.setWhatID(contTimesheetMap.get(contactId).id);
				if(remainderType.equalsIgnoreCase(System.Label.Gentle)) {
					mail.setTemplateId(getEmailTemplateByName(missingTimesheetFirstTemplate));
				} else if(remainderType.equalsIgnoreCase(System.Label.Final)) {
					mail.setTemplateId(getEmailTemplateByName(missingTimesheetFinalTemplate));
				}
				
				mail.setBccSender(false);
				mail.setUseSignature(false);
				mails.add(mail);
			}
			Messaging.sendEmail(mails);
			ApexPages.addmessage(new ApexPages.message(
				ApexPages.severity.CONFIRM,System.Label.Email_Sent_Successfully));
		
		} catch(Exception e) {
			System.debug('Exception : ' + e.getMessage()); 
			ApexPages.addmessage(new ApexPages.message(
				ApexPages.severity.ERROR,System.Label.Issue_in_sending_Email));
		}
	}
	
	/**
	* @name ContactTimesheet
	* @author Pandeiswari
	* @data 14/02/2018
	* @description Wrapper Class to display Candidate and Timesheet.
	*/
	
	public class ContactTimesheet implements Comparable {
		
		public TMS__Timesheet__c timesheet {get; set;}
		public Contact cont {get; set;}
		public Boolean selected {get; set;}
		
		public ContactTimesheet() {
			selected = false;
		}
		public Integer compareTo(Object compareTo) {
			if(cont == null) {
				return 1;
			}
			ContactTimesheet contTimesheet = (ContactTimesheet)compareTo;
			if(contTimesheet == null || contTimesheet.cont == null) {
				return -1;
			}
		
			return cont.Name.compareTo(contTimesheet.cont.Name);
		}
	}

    /**
	* @name getEmailTemplateByName
	* @author Pandeiswari
	* @data 14/02/2018
	* @description Getting email template Id by name.
	* @param name Name of email template
	* @return Id
	*/
	private static Id getEmailTemplateByName(String name)
	{
		List<EmailTemplate> templateList = [
			SELECT Name, Subject 
			FROM EmailTemplate 
			WHERE Name = :name];

		if (templateList.size() > 0)
		{
			return templateList[0].Id;
		}

		return null;
	}
}