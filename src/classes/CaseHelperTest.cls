/**
* @author Aliaksandr Satskou
* @date 12/17/2013
* @description Test class for CaseHelper class.
*/
@isTest
private class CaseHelperTest {
	
	private static List<Case> caseList;
	private static List<TMS__Time__c> timeList;
	
	
	/**
	* @author Aliaksandr Satskou
	* @date 12/17/2013
	* @name initData
	* @description Init data.
	* @return void
	*/
	private static void initData() {
		insert new TMS__TimesheetCustomSettings__c(
			Name = 'ProjectStatusToShow',
			TMS__Disable_Update_Case_Total_Time_Spent__c = false
		); 
		
		
		caseList = new List<Case> {
			new Case()
		};
		insert caseList;
		
		
		timeList = new List<TMS__Time__c> {
			new TMS__Time__c(
				TMS__Time_Spent__c = 5,
				TMS__Case__c = caseList[0].Id
			),
			new TMS__Time__c(
				TMS__Time_Spent__c = 7,
				TMS__Case__c = caseList[0].Id
			)
		};
		insert timeList;
	}

	
	/**
	* @author Aliaksandr Satskou
	* @date 12/17/2013
	* @name testCaseHelper
	* @description Testing CaseHelper class.
	* @return void
	*/
    static testMethod void testCaseHelper() {
        initData();
        
        List<Case> checkCaseList = [SELECT TMS__Total_Time_Spent__c FROM Case WHERE Id IN :caseList];
        System.assertEquals(12.0, checkCaseList[0].TMS__Total_Time_Spent__c);
    }
}