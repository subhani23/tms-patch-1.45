/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TaskAccountingIdUpdateTest {

    static testMethod void TestTaskAccountingIdUpdate() {
        Account acc = new Account(Name = 'TestAcc');
    	insert acc;
    	TMS__Project__c pro = new TMS__Project__c(Name ='TestProject', TMS__Client__c =  acc.id);
    	insert pro;
    	
    	TMS__Project__c pro1 = new TMS__Project__c(Name ='TestProject2');
    	insert pro1;
    	    	
    	TMS__Task__c task = new TMS__Task__c(TMS__Project__c =  pro.id, Name = 'Regular Hours', TMS__Accounting_Id__c = 'testAcc1');
    	insert task;
    	 
    	TMS__Task__c task2 = new TMS__Task__c(TMS__Project__c =  pro.id, Name = 'Regular Hours');
    	insert task2;
    }
}