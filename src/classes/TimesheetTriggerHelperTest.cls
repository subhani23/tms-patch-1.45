/**
* @author Aliaksandr Satskou
* @date 12/04/2013
* @description Test class for TimesheetTriggerHelper class.
*/
@isTest
private class TimesheetTriggerHelperTest {
	
	private static List<TMS__Project__c> projectList;
	private static List<TMS__Timesheet__c> timesheetList;
	private static List<TMS__Time__c> timeList;
	
	private static TMS__TimesheetCustomSettings__c tmsSetting; 
	
	
	/**
	* @author Aliaksandr Satskou
	* @date 12/04/2013
	* @name initData
	* @description Init data.
	* @return void
	*/
	private static void initData() {
		tmsSetting = new TMS__TimesheetCustomSettings__c(
			Name = 'ProjectStatusToShow',
			TMS__Populate_Additional_Timesheet_Fields__c = true
		);
		insert tmsSetting;
		
		
		/* Added by Aliaksandr Satskou, 10/20/2014 (case #00032938) */
		TMS__TimesheetCustomSettingsHierarchy__c hierarchySetting = 
				TMS__TimesheetCustomSettingsHierarchy__c.getInstance();
		
		hierarchySetting.TMS__Use_Apex_Batch_for_Timesheet_Trigger__c = false;
		
		upsert hierarchySetting;
		
		
		projectList = new List<TMS__Project__c> {
			new TMS__Project__c(
				Name = 'Project #1'
			),
			new TMS__Project__c(
				Name = 'Project #2'
			)
		};
		insert projectList;
		
		
		timesheetList = new List<TMS__Timesheet__c> {
			new TMS__Timesheet__c(
				TMS__Project__c = projectList[0].Id
			),
			new TMS__Timesheet__c(
				TMS__Project__c = projectList[1].Id
			)
		};
		insert timesheetList;
		
		
		timeList = new List<TMS__Time__c> {
			new TMS__Time__c(
				TMS__Timesheet__c = timesheetList[0].Id,
				TMS__Time_Spent__c = 3.1
			),
			new TMS__Time__c(
				TMS__Timesheet__c = timesheetList[0].Id,
				TMS__Time_Spent__c = 3.1
			),
			new TMS__Time__c(
				TMS__Timesheet__c = timesheetList[0].Id,
				TMS__Time_Spent__c = 3.1
			),
			new TMS__Time__c(
				TMS__Timesheet__c = timesheetList[1].Id,
				TMS__Time_Spent__c = 4.2
			),
			new TMS__Time__c(
				TMS__Timesheet__c = timesheetList[1].Id,
				TMS__Time_Spent__c = 4.2
			),
			new TMS__Time__c(
				TMS__Timesheet__c = timesheetList[1].Id,
				TMS__Time_Spent__c = 4.2
			)
		};
		insert timeList;
	}
	
	
	/**
	* @author Aliaksandr Satskou
	* @date 12/04/2013
	* @name testTimesheetTriggerHelper
	* @description Testing TimesheetTriggerHelper class.
	* @return void
	*/
	static testMethod void testTimesheetTriggerHelper() {
		initData();
		
		TimesheetTriggerHelper.updateTimesheet(new List<TMS__Time__c> { timeList[0], timeList[3] });
		
		List<TMS__Timesheet__c> timesheetCheckList = [
			SELECT TMS__Timesheet_Hours__c 
			FROM TMS__Timesheet__c 
			WHERE Id IN :timesheetList];
		
		TimesheetTriggerHelper.updateTimesheetFieldsFromPlacement(timesheetList);
	}
}