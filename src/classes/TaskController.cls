public class TaskController {
	public String locale { get; set; }
	
	public Boolean showAddFromTemplateButton {get; set;}
	public String baseURL { get; private set;}
	public List<TaskWrapper> taskWrappers {get; set;}
	public List<Field> fieldList {get; set;}
	
	private Id projectId = ApexPages.currentPage().getParameters().get('id');

	public TaskController(ApexPages.StandardController controller) {
		locale = UserInfo.getLocale();
		
		baseURL = URL.getSalesforceBaseUrl().toExternalForm();
		
		showAddFromTemplateButton = !currentRecordIsProjectTemplate();
		
		taskWrappers = getTaskWrappers();
		
		fieldList = getFieldList();
	}
	
	private Boolean currentRecordIsProjectTemplate() {
		TMS__Project__c projectType = [
				SELECT RecordType.Name
				FROM TMS__Project__c
				WHERE Id = :projectId];

		return projectType.RecordType.Name != 'Project';
	}
	
	private List<TaskWrapper> getTaskWrappers() {
		String fields = '';
		for (Schema.SObjectField field : Schema.SObjectType.TMS__Task__c.fields.getMap().values()) {
			fields += ', ' + field.getDescribe().Name;
		}
		fields = (fields != '') ? fields.substring(2) : 'Name';

		String query = 
				' SELECT ' + fields + 
				' FROM TMS__Task__c ' + 
				' WHERE TMS__Project__c = \'' + projectId + '\'';
		List<TMS__Task__c> tasks = (List<TMS__Task__c>)DataBase.query(query);
		
		List<TaskWrapper> taskWrappers = new List<TaskWrapper>();
		for (TMS__Task__c task : tasks) {
			taskWrappers.add(new TaskWrapper(task));
		}
		return taskWrappers;
	}
	
	private List<Field> getFieldList() {
		List<TMS__Task_Fields__c> taskFieldList = [
				SELECT Name, TMS__Field_Order__c, TMS__CSS_Style__c, TMS__Is_Required__c
				FROM TMS__Task_Fields__c
				ORDER BY TMS__Field_Order__c ASC];

		List<Field> fieldList = new List<Field>();

		for (TMS__Task_Fields__c taskField : taskFieldList) {
			Field field = new Field();

			field.apiName = taskField.Name;
			field.order = Integer.valueOf(taskField.TMS__Field_Order__c);
			field.css = taskField.TMS__CSS_Style__c;
			field.isRequired = taskField.TMS__Is_Required__c;
			field.isCustomOptions = false;
			field.optionList = new List<SelectOption>();

			if (taskField.Name.toLowerCase().contains('project_resource__c')) {
				field.isCustomOptions = true;
				field.optionList = getProjectResourceItems();
			}

			fieldList.add(field);
		}

		return fieldList;
	}
	
	private List<SelectOption> getProjectResourceItems() {
		List<TMS__Project_Resource__c> projectResources = [
				SELECT TMS__Contact__c, TMS__Contact__r.Name
				FROM TMS__Project_Resource__c
				WHERE TMS__Project__c = :projectId];

		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('', 'None'));
		
		for (TMS__Project_Resource__c projectResource : projectResources) {
			if (projectResource.Contact__r.Name != null) {
				options.add(new SelectOption(projectResource.Id, projectResource.Contact__r.Name));
			}
		}

		return options;
	}
	
	public void addTask() {
		taskWrappers.add(new TaskWrapper(new TMS__Task__c()));
	}
	
	public void saveTasks() {
		List<TMS__Task__c> tasks = new List<TMS__Task__c>();
		for (TaskWrapper taskWrapper : taskWrappers) {
			taskWrapper.task.TMS__Project__c = projectId;
			tasks.add(taskWrapper.task);
		}
		upsert tasks;
	}
	
	public void deleteTask() {
		List<TMS__Task__c> tasks = new List<TMS__Task__c>();
		for (TaskWrapper taskWrapper : taskWrappers) {
			if (taskWrapper.isSelected && taskWrapper.task.Id != null) {
				tasks.add(taskWrapper.task);
			}
		}
		
		delete tasks;
			
		taskWrappers = getTaskWrappers();
	}
	
	private class TaskWrapper {
		public Boolean isSelected { get; set; }
		public TMS__Task__c task { get; set; }
		
		public TaskWrapper(TMS__Task__c task) {
			this.task = task;
			this.isSelected = false;
		}
	}
	
	private class Field {
		public String apiName {get; set;}
		public Integer order {get; set;}
		public String css {get; set;}
		public Boolean isRequired {get; set;}

		public Boolean isCustomOptions {get; set;}
		public List<SelectOption> optionList {get; set;}
	}

	public static testMethod void tstTaskController() {
		insert new List<TMS__Task_Fields__c> {
			new TMS__Task_Fields__c(
				Name = 'Name',
				TMS__Field_Order__c = 1,
				TMS__CSS_Style__c = '',
				TMS__Is_Required__c = true
			),
			new TMS__Task_Fields__c(
				Name = 'TMS__Project_Resource__c',
				TMS__Field_Order__c = 2,
				TMS__CSS_Style__c = 'width: 50px',
				TMS__Is_Required__c = true
			)
		};

		TMS__Project__c project = new TMS__Project__c();
		project.Name = 'Test Project';
		insert project;
		
		List<TMS__Task__c> taskList = new List<TMS__Task__c>();
		TMS__Task__c task = new TMS__Task__c();
		task.Name = 'Test Task';
		task.TMS__Project__c = project.Id;
		insert task;
		taskList.add(task);

		task = new TMS__Task__c();
		task.Name = 'Test Task2';
		task.TMS__Project__c = project.Id;
		task.TMS__Parent_Task__c = task.Id;
		insert task;
		taskList.add(task);

		task = new TMS__Task__c();
		task.Name = 'Test Task2';
		task.TMS__Project__c = project.Id;
		task.TMS__Parent_Task__c = task.Id;
		insert task;
		taskList.add(task);
		
		Test.startTest();

		ApexPages.currentPage().getParameters().put('id', project.Id);
		TaskController obj = new TaskController(new ApexPages.StandardController(project));
		
		obj.addTask();
		obj.saveTasks();
		obj.deleteTask();
		
		Test.stopTest();
	}
}