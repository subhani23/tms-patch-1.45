/**
* @author Aliaksandr Satskou
* @date 09/04/2013
* @description Generating Payroll Line Items.
*/
public without sharing class PayrollHelper {

    private static Map<String, TMS__Payroll_Provider_Settings__c> payrollProviderSettingMap;
    private static TMS__TimesheetCustomSettings__c timesheetCustomSetting;
    private static TMS__Payroll_Filenames__c filenamesSetting;
    private static TMS__TimesheetCustomSettingsHierarchy__c tmsHierarchySettings;

    private static List<TMS__Time__c> timeList;
    private static Map<Id, TMS__Task__c> tMap;
    private static Map<Id, TMS__Project__c> projectMap;
    private static List<String> considerTimeStatusList;

    private static List<TMS__Payroll_Line_items__c> currentLineList;
    private static List<Contact> employeeList;
	private static List<String> contactIds;
	private static List<String> taskIds;
    private static List<TMS__Payroll_Line_items__c> outputLineList = new List<TMS__Payroll_Line_items__c>();
	private static Map<List<Object>, List<sObject>> timeGrouping = new Map<List<Object>, List<sObject>>();
	private static Map<TMS__Time__c,TMS__Payroll_Line_items__c> timeMapToupdate = new Map<TMS__Time__c,TMS__Payroll_Line_items__c>();

    /**
    * @name initPayrollProviderSettingMap
    * @author Aliaksandr Satskou
    * @date 09/04/2013
    * @description Initialize Payroll Provider Custom Settings.
    * @return void
    */
    private static void initPayrollProviderSettingMap() {
        if (payrollProviderSettingMap == null) {
            payrollProviderSettingMap = TMS__Payroll_Provider_Settings__c.getAll();
        }
    }


    /**
    * @name initData
    * @author Aliaksandr Satskou
    * @date 09/04/2013
    * @description Initialize data.
    * @return void
    */
    public static void initData(TMS__Process_Payroll__c payroll,Boolean selectionFlag,List<String> contactTaskIds) {
        initPayrollProviderSettingMap();

        if (filenamesSetting == null) {
            filenamesSetting = TMS__Payroll_Filenames__c.getAll().get('Default');
        }


        if (timesheetCustomSetting == null) {
            timesheetCustomSetting = TMS__TimesheetCustomSettings__c.getAll().get('ProjectStatusToShow');
        }


        considerTimeStatusList = new List<String>();

        if (tmsHierarchySettings == null) {
            tmsHierarchySettings = TMS__TimesheetCustomSettingsHierarchy__c.getOrgDefaults();

            if ((tmsHierarchySettings.TMS__Consider_Payroll_Time_Statuses__c != null) && (
                    tmsHierarchySettings.TMS__Consider_Payroll_Time_Statuses__c.trim() != '')) {

                considerTimeStatusList =
                        tmsHierarchySettings.TMS__Consider_Payroll_Time_Statuses__c.split(';');
            }
        }

        
        if (timeList == null) {
            
            Date startDate = payroll.TMS__Start_Date__c;
            Date endDate = payroll.TMS__End_Date__c;
            
            String timeQuery = 'SELECT Id, TMS__Task__c,TMS__Payroll_Line_item__c,' +
		                    		'TMS__Task__r.TMS__D_E_Code__c,TMS__Time_Spent_Formula__c' +
				                    ' FROM TMS__Time__c' +
				                    ' WHERE (TMS__Date__c >= :startDate)' +
				                    ' AND (TMS__Date__c <= :endDate)' +
				                    ' AND (TMS__Task__c != null)' +
				                    ' AND (TMS__Status__c IN :considerTimeStatusList)';
            if(selectionFlag){
            	
            	contactIds = new List<String>();
            	taskIds = new List<String>();
            	
            	for(String ids : contactTaskIds){
            		List<String> splittedIds = ids.split('-');
            		contactIds.add(splittedIds[0]);
            		taskIds.add(splittedIds[1]);
            		system.debug('contactIds>>'+contactIds);
            		system.debug('taskIds>>'+taskIds);
            		
            	}
            	
            	timeQuery = timeQuery + ' AND TMS__Task__c IN :taskIds'+
            						    ' AND TMS__Candidate__c IN : contactIds';
                    timeList = Database.query(timeQuery);
                   
            }else{
            	 timeList = Database.query(timeQuery);
            	
            }
			
			timeGrouping =
				GroupByHelper.groupByFields(timeList,
						new List<String> { 'TMS__Task__c', 'TMS__Task__r.TMS__D_E_Code__c' });	
  
 		                
			system.debug('timeList>>>'+timeList);
			
			system.debug('timeGrouping>>>'+timeGrouping);
			
            Set<Id> taskIdSet = new Set<Id>();

            for (TMS__Time__c t : timeList) {
                Id taskId = t.TMS__Task__c;

                if (taskId != null) { taskIdSet.add(taskId); }
            }

            tMap = new Map<Id, TMS__Task__c>((List<TMS__Task__c>)DataBase.query(
                    'SELECT Name, TMS__D_E__c, TMS__D_E_Code__c, TMS__Shift__c, TMS__Sequence_Number__c, ' +
                        'TMS__Project_Resource__r.TMS__Contact__r.Name, ' +
                        'TMS__Project_Resource__r.TMS__Contact__r.FirstName, ' +
                        'TMS__Project_Resource__r.TMS__Contact__r.LastName, ' +
                        'TMS__Project_Resource__r.TMS__Contact__r.SS__c, ' +
                        'TMS__Project_Resource__r.TMS__Contact__r.Employee_ID__c, ' +
                    //  'TMS__Project_Resource__r.TMS__Contact__r.AVTRRT__Candidate_Id__c, ' +   // uncommnet before packaging
                        'TMS__Project_Resource__r.TMS__Project__r.TMS__Client__c, ' +
                        'TMS__Project_Resource__r.TMS__Project__r.TMS__Client__r.ShippingState, ' +
                        'TMS__Project_Resource__r.TMS__Project__r.TMS__Client__r' +
                            '.Paychex_Local_Tax_Code__c, ' +
                        'TMS__Project_Resource__r.TMS__Project__r.TMS__Client__r.TMS__Division__c, ' +
                        'TMS__Project_Resource__r.TMS__Project__r.TMS__Client__r' +
                            '.TMS__Financial_Category__c ' +
                    'FROM TMS__Task__c ' +
                    'WHERE Id IN :taskIdSet'));
			
			system.debug('tMap>>>'+tMap);

            Set<Id> projectIdSet = new Set<Id>();

            for (TMS__Task__c task : tMap.values()) {
                if ((task.TMS__Project_Resource__c != null) &&
                        (task.TMS__Project_Resource__r.TMS__Project__c != null)) {
                    projectIdSet.add(task.TMS__Project_Resource__r.TMS__Project__c);
                }
            }
			projectMap = new Map<Id, TMS__Project__c>();
           /* projectMap = new Map<Id, TMS__Project__c>((List<TMS__Project__c>)DataBase.query(      // uncommnet before packaging
                    'SELECT TMS__Client__c, ' + 
                    '(SELECT Project_TMS__c, Shift_Name__c, Pay_Rate__c, AVTRRT__Pay_Type__c ' +
                        'FROM Placements__r ' +
                        'ORDER BY CreatedDate DESC ' +
                        'LIMIT 1) ' +
                    'FROM TMS__Project__c ' +
                    'WHERE Id IN :projectIdSet'));*/
        }
    }


    /**
    * @name initCurrentLineListWithEmployeeList
    * @author Aliaksandr Satskou
    * @date 09/04/2013
    * @description Initialize current Line Item list and Contact list.
    * @return void
    */
    private static void initCurrentLineListWithEmployeeList(TMS__Process_Payroll__c payroll) {
        if (currentLineList == null) {
            currentLineList = [
                    SELECT TMS__Employee__c
                    FROM TMS__Payroll_Line_items__c
                    WHERE TMS__Payroll__c = :payroll.Id];


            Set<Id> employeeIdSet = new Set<Id>();

            for (TMS__Payroll_Line_items__c lineItem : currentLineList) {
                employeeIdSet.add(lineItem.TMS__Employee__c);
            }

            /*employeeList = DataBase.query(
                    'SELECT Routing__c, Accountnum__c, Amount__c, Bank_Account_Type__c, ' +     // uncomment before packaging
                        'Bank_Account_Type_other__c, Employee_ID__c, AVTRRT__Candidate_Id__c, ' +
                        'Bank_Name_other__c, Account_othernum__c, Routing_other__c, ' +
                        'FirstName, LastName, Phone, Birthdate, MailingCity, MailingState, ' +
                        'MailingPostalCode, SS__c, Federal_Exemptions__c, State_Exemptions__c, ' +
                        'Federal_Filing_Status__c, State_Filing_Status__c, AVTRRT__Gender__c, ' +
                        'AVTRRT__Ethnicity__c, AVTRRT__State__c, Federal_Additional_Withholding__c, ' +
                        'State_Additional_Withholding__c, Is_W2__c, MailingStreet, ' +
                        'Account.TMS__Division__c, Account.TMS__Financial_Category__c, ' +
                        '(SELECT ShiftName__c, AVTRRT__Start_Date__c, AVTRRT__Job_Title__c, ' +
                                'AVTRRT__End_Date__c, AVTRRT__Terminated__c, Workers_Comp_Code__c, ' +
                                'Shift_Name__c, AVTRRT__Pay_Type__c ' +
                        'FROM AVTRRT__Placements1__r ' +
                        'ORDER BY CreatedDate DESC) ' +
                    'FROM Contact ' +
                    'WHERE Id IN :employeeIdSet');*/
                    
             employeeList = DataBase.query(
                    'SELECT Routing__c, Accountnum__c, Amount__c, Bank_Account_Type__c, ' +
                        'Bank_Account_Type_other__c, Employee_ID__c, ' +
                        'Bank_Name_other__c, Account_othernum__c, Routing_other__c, ' +
                        'FirstName, LastName, Phone, Birthdate, MailingCity, MailingState, ' +
                        'MailingPostalCode, SS__c, Federal_Exemptions__c, State_Exemptions__c, ' +
                        'Federal_Filing_Status__c, State_Filing_Status__c, ' +
                        'Federal_Additional_Withholding__c, ' +
                        'State_Additional_Withholding__c, Is_W2__c, MailingStreet, ' +
                        'Account.TMS__Division__c, Account.TMS__Financial_Category__c '+
                    ' FROM Contact ' +
                    ' WHERE Id IN :employeeIdSet');       
        }
    }


    private static String getPlacementOwner() {
        return tmsHierarchySettings.TMS__Payroll_Line_Items_Placement_Owner_API__c;
    }


    /**
    * @name getProviderNameList
    * @author Aliaksandr Satskou
    * @date 09/04/2013
    * @description Getting list of Providers.
    * @return List<SelectOption>
    */
    public static List<SelectOption> getProviderNameList() {
        initPayrollProviderSettingMap();
        List<SelectOption> providerNameList = new List<SelectOption>();

        for (TMS__Payroll_Provider_Settings__c providerSetting : payrollProviderSettingMap.values()) {
            providerNameList.add(new SelectOption(
                    providerSetting.TMS__Payroll_Provider_Name__c,
                    providerSetting.TMS__Payroll_Provider_Name__c));
        }


        return providerNameList;
    }


    /**
    * @name extractShiftName
    * @author Aliaksandr Satskou
    * @date 05/14/2014
    * @description Extract ShiftName from Placement.
    * @param shiftName Placement ShiftName
    * @return String
    */
    private static String extractShiftName(String shiftName) {
        if (shiftName != null) {
            Integer fromInd = shiftName.lastIndexOf(':');
            if (fromInd != -1) { fromInd += 1; } else { return shiftName; }

            Integer toInd = shiftName.indexOf('=');
            if (toInd == -1) { return shiftName; }


            return shiftName.substring(fromInd, toInd);
        }


        return '';
    }


    /**
    * @name paychex_createData
    * @author Aliaksandr Satskou
    * @date 09/04/2013
    * @description Creating Data Line Items for Paychex Provider.
    * @return void
    */
    public static void paychex_createData(TMS__Process_Payroll__c payroll) {
        List<TMS__Payroll_Line_items__c> lineList = new List<TMS__Payroll_Line_items__c>();

		
		for(List<Object> keys : timeGrouping.keyset()){
            
            List<TMS__Time__c>	timeListData = (List<TMS__Time__c>)timeGrouping.get(keys);
			
			Decimal totalTime = 0;
			
			TMS__Time__c timeObj = timeListData[0];
			
            Id tId = timeObj.TMS__Task__c;
            TMS__Task__c task = tMap.get(tId);

	
            TMS__Payroll_Line_items__c lineItem = new TMS__Payroll_Line_items__c(
                TMS__Payroll__c = payroll.Id,
                TMS__Payroll_Provider__c = payroll.TMS__Payroll_Provider_Name__c,
                TMS__Task__c = task.Id,
                TMS__Payroll_File_Name__c = (filenamesSetting != null)
                        ? filenamesSetting.Paychex_Data_File_Name__c
                        : '', //'R320_TA',
                TMS__Shift__c = task.TMS__Shift__c,
                TMS__D_E__c = task.TMS__D_E__c,
                TMS__D_E_Code__c = task.TMS__D_E_Code__c,
                TMS__Sequence_Number__c = task.TMS__Sequence_Number__c
            );
			
			for(TMS__Time__c sb:timeListData){
				totalTime = totalTime + sb.TMS__Time_Spent_Formula__c;
				timeMapToupdate.put(sb,lineItem);
			}
			
			lineItem.TMS__Hours__c = totalTime;

			
	
            Contact employee = task.TMS__Project_Resource__r.TMS__Contact__r;

            if (employee != null) {
                lineItem.TMS__Employee__c = employee.Id;
                lineItem.TMS__Name__c = employee.Name;

                lineItem.TMS__Employee_Number__c = (employee.Employee_ID__c != null)
                        ? String.valueOf(employee.Employee_ID__c)
                       // : String.valueOf(employee.get('AVTRRT__Candidate_Id__c'));
                       :'';


                lineItem.TMS__Social_Security_Number__c = employee.SS__c;
            }

	
            Account client = task.TMS__Project_Resource__r.TMS__Project__r.TMS__Client__r;

            if (client != null) {
                lineItem.Account__c = client.Id;

                /* Added by Aliaksandr Satskou, 04/11/2014 */
                lineItem.TMS__Account_Lookup__c = client.Id;

                lineItem.TMS__Override_State__c = client.ShippingState;
                lineItem.TMS__Override_Local__c = client.Paychex_Local_Tax_Code__c;


                /* Added by Aliaksandr Satskou, 04/22/2014 (case #00027041)*/
            lineItem.TMS__Division__c = client.TMS__Division__c;
            lineItem.TMS__Financial_Category__c = client.TMS__Financial_Category__c;
        }

            /* Edited by Aliaksandr Satskou, 10/21/2014 (case #00032923) */
            if (task.TMS__Project_Resource__r.TMS__Project__c != null) {
                TMS__Project__c project = projectMap.get(task.TMS__Project_Resource__r.TMS__Project__c);

                if (project != null) {
                    List<sObject> placements = project.getSObjects('Placements__r');


                    if ((placements != null) && (placements.size() > 0)) {
                        lineItem.put(getPlacementOwner(), (Id)placements[0].get('Id'));

                        String placementShiftName = (String)placements[0].get('Shift_Name__c');

                        lineItem.TMS__Override_Department__c = extractShiftName(placementShiftName);
                        lineItem.TMS__Override_Rate__c = (Decimal)placements[0].get('Pay_Rate__c');

                        /* Added by Aliaksandr Satskou, 04/11/2014 */
	                        //lineItem.TMS__Pay_Type__c = (String)placements[0].get('AVTRRT__Pay_Type__c');
	                    }
	                }
	            }

            lineList.add(lineItem);
            
		}
			

        insert lineList;
    }


    /**
    * @name paychex_createEmployeeBankData
    * @author Aliaksandr Satskou
    * @date 09/04/2013
    * @description Creating Employee Bank Data Line Items for Paychex Provider.
    * @return void
    */
    public static void paychex_createEmployeeBankData(TMS__Process_Payroll__c payroll, String provider) {
        TMS__Payroll_Provider_Settings__c providerSetting = payrollProviderSettingMap.get(provider);

        initCurrentLineListWithEmployeeList(payroll);

        for (Contact employee : employeeList) {
            TMS__Payroll_Line_items__c lineItem = new TMS__Payroll_Line_items__c(
                TMS__Payroll__c = payroll.Id,
                TMS__Payroll_Provider__c = provider,
                TMS__Employee__c = employee.Id,
                TMS__Payroll_File_Name__c = (filenamesSetting != null)
                        ? filenamesSetting.Paychex_Employee_Bank_Data_File_Name__c
                        : '', //'CVT1B.TXT'
                TMS__Company_ID__c = (providerSetting != null)
                        ? providerSetting.TMS__Payroll_Company_ID__c
                        : '', //'R320'
                TMS__Employee_Number__c = (employee.Employee_ID__c != null)
                        ? String.valueOf(employee.Employee_ID__c)
                        //: String.valueOf(employee.get('AVTRRT__Candidate_Id__c')),
                        :'',
                TMS__Sequence__c = '99',
                TMS__Bank__c = employee.Routing__c,
                TMS__Account__c = employee.Accountnum__c,
                TMS__DeCode__c = 'C1',
                TMS__Amount_or_Percent__c = 'A',
                TMS__Amount__c = employee.Amount__c,

                /* Added by Aliaksandr Satskou, 05/13/2014 (case #00027041)*/
                TMS__Division__c = employee.Account.TMS__Division__c,
                TMS__Financial_Category__c = employee.Account.TMS__Financial_Category__c
            );

            if (employee.Bank_Account_Type__c == 'Checking') {
                lineItem.TMS__DeCode__c = 'C1';
            } else if (employee.Bank_Account_Type__c == 'Savings') {
                lineItem.TMS__DeCode__c = 'S1';
            }


            /* Edited by Aliaksandr Satskou, 10/21/2014 (case #00032923) 
            List<sObject> placements = employee.getSObjects('AVTRRT__Placements1__r');

            if ((placements != null) && (placements.size() > 0)) {
                lineItem.put(getPlacementOwner(), (Id)placements[0].get('Id'));

                lineItem.TMS__Pay_Type__c = (String)placements[0].get('AVTRRT__Pay_Type__c');
            }*/


            outputLineList.add(lineItem);

            if ((employee.Bank_Account_Type_other__c != null) || (employee.Bank_Name_other__c != null) ||
                    (employee.Account_othernum__c != null) || (employee.Routing_other__c != null)) {
                TMS__Payroll_Line_items__c additionalLineItem = lineItem.clone(false, false);

                if (employee.Bank_Account_Type_other__c == 'Checking') {
                    additionalLineItem.TMS__DeCode__c = 'C1';
                } else if (employee.Bank_Account_Type_other__c == 'Savings') {
                    additionalLineItem.TMS__DeCode__c = 'S1';
                }

                additionalLineItem.TMS__Bank__c = employee.Routing_other__c;
                additionalLineItem.TMS__Account__c = employee.Account_othernum__c;

                outputLineList.add(additionalLineItem);
            }
        }
    }


    /**
    * @name paychex_createEmployeeData
    * @author Aliaksandr Satskou
    * @date 09/04/2013
    * @description Creating Employee Data Line Items for Paychex Provider.
    * @return void
    */
    public static void paychex_createEmployeeData(TMS__Process_Payroll__c payroll, String provider) {
        TMS__Payroll_Provider_Settings__c providerSetting = payrollProviderSettingMap.get(provider);

        initCurrentLineListWithEmployeeList(payroll);

        for (Contact employee : employeeList) {
            TMS__Payroll_Line_items__c lineItem = new TMS__Payroll_Line_items__c(
                TMS__Payroll__c = payroll.Id,
                TMS__Payroll_Provider__c = provider,
                TMS__Employee__c = employee.Id,
                TMS__Payroll_File_Name__c = (filenamesSetting != null)
                        ? filenamesSetting.Paychex_Employee_Data_File_Name__c
                        : '', // CVT1.TXT
                TMS__Company_ID__c = (providerSetting != null)
                        ? providerSetting.TMS__Payroll_Company_ID__c
                        : '',
                TMS__Employee_Number__c = (employee.Employee_ID__c != null)
                        ? String.valueOf(employee.Employee_ID__c)
                        //: String.valueOf(employee.get('AVTRRT__Candidate_Id__c')),
                        :'',

                TMS__First_Name__c = employee.FirstName,
                TMS__Last_Name__c = employee.LastName,
                TMS__City__c = employee.MailingCity,
                TMS__State__c = employee.MailingState,
                TMS__Zip__c = employee.MailingPostalCode,
                TMS__Phone__c = employee.Phone,
                TMS__Social_Security_Number__c = employee.SS__c,
                TMS__Birth_Date__c = String.valueOf(employee.Birthdate),

                //TMS__Gender__c = (String)employee.get('AVTRRT__Gender__c'),

                TMS__Fed_Dependents__c = employee.Federal_Exemptions__c,
                TMS__State_Dependents__c = employee.State_Exemptions__c,
                TMS__Fed_Marital_Status__c = employee.Federal_Filing_Status__c,
                TMS__State_Marital_Status__c = employee.State_Filing_Status__c,

                //TMS__EEO_Code__c = (String)employee.get('AVTRRT__Ethnicity__c'),

                TMS__X1099_Employee_Code__c = (employee.Is_W2__c  == true) ? 'Y' : 'N',

                //TMS__SIT_State_tax_code__c = (String)employee.get('AVTRRT__State__c'),
                //TMS__SDI_State_tax_code__c = (String)employee.get('AVTRRT__State__c'),
                //TMS__SUI_State_tax_code__c = (String)employee.get('AVTRRT__State__c'),

                TMS__Additional_Fed_Tax_Amt__c = String.valueOf(employee.Federal_Additional_Withholding__c),
                TMS__Additional_State_Tax_Amt__c = String.valueOf(employee.State_Additional_Withholding__c),
                TMS__Address_1__c = employee.MailingStreet,

                /* Added by Aliaksandr Satskou, 05/13/2014 (case #00027041)*/
                TMS__Division__c = employee.Account.TMS__Division__c,
                TMS__Financial_Category__c = employee.Account.TMS__Financial_Category__c
            );

            /* Edited by Aliaksandr Satskou, 10/21/2014 (case #00032923) 
            List<sObject> placements = employee.getSObjects('AVTRRT__Placements1__r');

            if ((placements != null) && (placements.size() > 0)) {
                lineItem.put(getPlacementOwner(), (Id)placements[0].get('Id'));

                lineItem.TMS__Override_Department__c = (String)placements[0].get('Shift_Name__c');
                lineItem.TMS__Hire_Date__c = String.valueOf(placements[0].get('AVTRRT__Start_Date__c'));
                lineItem.TMS__Position_or_Job_Title__c = (String)placements[0].get('AVTRRT__Job_Title__c');
                lineItem.TMS__Term_Code__c = String.valueOf(
                        placements[0].get('AVTRRT__Terminated__c')).toUpperCase().substring(0, 1);
                lineItem.TMS__Termination_Date__c = String.valueOf(placements[0].get('AVTRRT__End_Date__c'));
                lineItem.TMS__Workers_Comp_Code__c = (String)placements[0].get('Workers_Comp_Code__c');

                lineItem.TMS__Pay_Type__c = (String)placements[0].get('AVTRRT__Pay_Type__c');
            }*/

            outputLineList.add(lineItem);
        }
    }


    /**
    * @name adp_createData
    * @author Aliaksandr Satskou
    * @date 09/04/2013
    * @description Creating Data Line Items for ADP Provider.
    * @return void
    */
    public static void adp_createData(TMS__Process_Payroll__c payroll, String provider) {
        TMS__Payroll_Provider_Settings__c providerSetting = payrollProviderSettingMap.get(provider);

        String batchId = [SELECT Name FROM TMS__Process_Payroll__c WHERE Id = :payroll.Id LIMIT 1][0].Name;


        List<String> regularAndOvertimeTaskNameList = new List<String> {
                timesheetCustomSetting.Regular_Hours_Task_Name__c,
                timesheetCustomSetting.Overtime_Hours_Task_Name__c
        };
		
		Set<Id> regularandOvertimeTaskIdSet = new Set<Id>();
		
		Date startDate = payroll.TMS__Start_Date__c;
        Date endDate = payroll.TMS__End_Date__c;
            
		String timeQuery = 'SELECT Id, TMS__Task__c,TMS__Payroll_Line_item__c,' +
		                    		'TMS__Task__r.TMS__D_E_Code__c,TMS__Time_Spent_Formula__c' +
				                    ' FROM TMS__Time__c' +
				                    ' WHERE (TMS__Date__c >= :startDate)' +
				                    ' AND (TMS__Date__c <= :endDate)' +
				                    ' AND (TMS__Task__c != null)' +
				                    ' AND (TMS__Status__c IN :considerTimeStatusList)';
		
		List<TMS__Time__c> regularAndOvertimeTimeList = new List<TMS__Time__c> ();
		
		system.debug('contactIds>>>'+contactIds+'taskIds>>>'+taskIds);
		
		if(contactIds != null  && taskIds != null){
			 
			timeQuery = timeQuery + ' AND TMS__Task__c IN :taskIds' +
									' AND TMS__Candidate__c IN : contactIds'+
									' AND (TMS__Task__r.Name IN :regularAndOvertimeTaskNameList)';
			
			regularAndOvertimeTimeList = Database.query(timeQuery);
									
			
		}else{
				
				timeQuery = timeQuery + ' AND (TMS__Task__r.Name IN :regularAndOvertimeTaskNameList)';
				
				regularAndOvertimeTimeList = Database.query(timeQuery);
		}	
		
		system.debug('regularAndOvertimeTimeList>>>'+regularAndOvertimeTimeList);
		                    
		/*List<TMS__Time__c> regularAndOvertimeTimeList = [
                   		SELECT Id, TMS__Task__c,TMS__Payroll_Line_item__c,TMS__Time_Spent_Formula__c
		                    FROM TMS__Time__c
		                    WHERE (TMS__Date__c >= :payroll.TMS__Start_Date__c)
			                AND (TMS__Date__c <= :payroll.TMS__End_Date__c)
			                AND TMS__Task__c IN :taskIds
					        AND TMS__Candidate__c IN : contactIds
			                AND (TMS__Task__r.Name IN :regularAndOvertimeTaskNameList)
			                AND (TMS__Status__c IN :considerTimeStatusList)];
		*/
		if(regularAndOvertimeTimeList.size()>0){
			
			 Map<Object, List<sObject>> timeGrouping =
					GroupByHelper.groupByField(regularAndOvertimeTimeList,
							'TMS__Task__c');	
			
			for(Object key : timeGrouping.keyset())	{
				
				List<TMS__Time__c> timeListData = timeGrouping.get(key);
				
				Decimal totalTime = 0;
				
				TMS__Time__c timeObj = timeListData[0];
			
				Id tId = (Id)timeObj.TMS__Task__c;
				
				regularandOvertimeTaskIdSet.add(tId);
				
	            TMS__Task__c task = tMap.get(tId);
	
	            TMS__Payroll_Line_items__c lineItem = createADPLineItem(payroll, task, providerSetting, batchId);
				
				for(TMS__Time__c sb:timeListData){
					totalTime = totalTime + sb.TMS__Time_Spent_Formula__c;
					timeMapToupdate.put(sb,lineItem);
				}
				
	            if (task.Name == timesheetCustomSetting.Regular_Hours_Task_Name__c) {
	                lineItem.Regular_Hours__c =  String.valueOf(totalTime);
	            }
	
	            if (task.Name == timesheetCustomSetting.Overtime_Hours_Task_Name__c) {
	                lineItem.Overtime_Hours__c = String.valueOf(totalTime);
	            }
	
	            outputLineList.add(lineItem);
				
			}				                    
		}
		
		String timeQueryNotRegualr = 'SELECT Id, TMS__Task__c,TMS__Payroll_Line_item__c,' +
		                    		'TMS__Task__r.TMS__D_E_Code__c,TMS__Time_Spent_Formula__c' +
				                    ' FROM TMS__Time__c' +
				                    ' WHERE (TMS__Date__c >= :startDate)' +
				                    ' AND (TMS__Date__c <= :endDate)' +
				                    ' AND (TMS__Task__c != null)' +
				                    ' AND (TMS__Status__c IN :considerTimeStatusList)';
		
		List<TMS__Time__c> notRegularAndOvertimeTimeList = new List<TMS__Time__c> ();
		
		if(contactIds !=null  && taskIds !=null){
			
			timeQueryNotRegualr = timeQueryNotRegualr + ' AND TMS__Task__c IN :taskIds' +
									' AND TMS__Candidate__c IN : contactIds'+
									' AND (TMS__Task__r.Name NOT IN :regularandOvertimeTaskIdSet)';
			
			notRegularAndOvertimeTimeList = Database.query(timeQueryNotRegualr);
									
			
		}else{
			
				timeQuery = timeQuery + ' AND (TMS__Task__r.Name IN :regularAndOvertimeTaskNameList)';
				
				notRegularAndOvertimeTimeList = Database.query(timeQueryNotRegualr);
		}
		
		/*List<TMS__Time__c> notRegularAndOvertimeTimeList = [
                   		SELECT Id, TMS__Task__c,TMS__Payroll_Line_item__c,TMS__Time_Spent_Formula__c
		                    FROM TMS__Time__c
		                    WHERE (TMS__Date__c >= :payroll.TMS__Start_Date__c)
			                AND (TMS__Date__c <= :payroll.TMS__End_Date__c)
			                AND TMS__Task__c IN :taskIds
					        AND TMS__Candidate__c IN : contactIds
			                AND (TMS__Task__c NOT IN :regularandOvertimeTaskIdSet)
			                AND (TMS__Status__c IN :considerTimeStatusList)];*/
			                
		if(notRegularAndOvertimeTimeList.size()>0){		                
		
			 Map<Object, List<sObject>> timeGroupingNotregular =
					GroupByHelper.groupByField(notRegularAndOvertimeTimeList,
							'TMS__Task__c');	
			
			for(Object key : timeGroupingNotregular.keyset())	{
				
				List<TMS__Time__c> timeListData = timeGroupingNotregular.get(key);
				
				Decimal totalTime = 0;
				
				TMS__Time__c timeObj = timeListData[0];
			
					
				Id tId = (Id)timeObj.TMS__Task__c;
					
				regularandOvertimeTaskIdSet.add(tId);
		        
		        TMS__Task__c task = tMap.get(tId);
		
		        TMS__Payroll_Line_items__c lineItem = createADPLineItem(payroll, task, providerSetting, batchId);
	
		            
		        String h3Code = (task.TMS__D_E_Code__c != null)
		                    ? task.TMS__D_E_Code__c
		                    : '';
		        lineItem.Hours_3_code__c = h3Code;
				
				for(TMS__Time__c sb:timeListData){
					totalTime = totalTime + sb.TMS__Time_Spent_Formula__c;
					timeMapToupdate.put(sb,lineItem);
				}
				
				lineItem.TMS__Hours__c = totalTime;
				
		        outputLineList.add(lineItem);
		            
			}	
		}			                    
     
        system.debug('total outputLineList>>'+outputLineList);
        
        system.debug('timeMapToupdate>>'+timeMapToupdate);
         
    }


    /**
    * @name createADPLineItem
    * @author Aliaksandr Satskou
    * @date 09/04/2013
    * @description Creating Data Line Item for ADP Provider.
    * @param payroll Payroll object
    * @param task Task object
    * @param providerSetting Payroll Provider Custom Setting
    * @param batchId Batch Id
    * @return TMS__Payroll_Line_items__c
    */
    private static TMS__Payroll_Line_items__c createADPLineItem(TMS__Process_Payroll__c payroll,
            TMS__Task__c task, TMS__Payroll_Provider_Settings__c providerSetting, String batchId) {
		
		system.debug('task>>>>'+task);
		
        TMS__Payroll_Line_items__c lineItem = new TMS__Payroll_Line_items__c(
            TMS__Payroll__c = payroll.Id,
            TMS__Payroll_Provider__c = payroll.TMS__Payroll_Provider_Name__c,
            TMS__Company_ID__c = providerSetting.TMS__Payroll_Company_ID__c,
            Task__c = task.Id,
            Account__c = task.TMS__Project_Resource__r.TMS__Project__r.TMS__Client__c,

            /* Added by Aliaksandr Satskou, 04/11/2014 */
            TMS__Account_Lookup__c = task.TMS__Project_Resource__r.TMS__Project__r.TMS__Client__c,

            /* Added by Aliaksandr Satskou, 05/13/2014 (case #00027041)*/
            TMS__Division__c = task.TMS__Project_Resource__r.TMS__Project__r.TMS__Client__r.TMS__Division__c,
            TMS__Financial_Category__c =
                    task.TMS__Project_Resource__r.TMS__Project__r.TMS__Client__r.TMS__Financial_Category__c,

            TMS__Payroll_File_Name__c = (filenamesSetting != null)
                    ? filenamesSetting.ADP_Data_File_Name__c
                    : '',
            TMS__Batch_Id__c = batchId
        );


        Contact employee = task.TMS__Project_Resource__r.TMS__Contact__r;

        if (employee != null) {
            lineItem.TMS__Employee__c = employee.Id;
            lineItem.TMS__Name__c = employee.Name;
            lineItem.TMS__First_Name__c  = employee.FirstName;
            lineItem.TMS__Last_Name__c = employee.LastName;

            lineItem.TMS__Employee_Number__c = (employee.Employee_ID__c != null)
                    ? String.valueOf(employee.Employee_ID__c)
                    //: String.valueOf(employee.get('AVTRRT__Candidate_Id__c'));
                    :'';


        }


        /* Added by Aliaksandr Satskou, 04/11/2014 */
        /* Edited by Aliaksandr Satskou, 10/21/2014 (case #00032923) */
        if (task.TMS__Project_Resource__r.TMS__Project__c != null) {
            TMS__Project__c project = projectMap.get(task.TMS__Project_Resource__r.TMS__Project__c);

            if (project != null) {
                List<sObject> placements = project.getSObjects('Placements__r');

                if ((placements != null) && (placements.size() > 0)) {
                    lineItem.put(getPlacementOwner(), (Id)placements[0].get('Id'));

                    //lineItem.TMS__Pay_Type__c = (String)placements[0].get('AVTRRT__Pay_Type__c');
                }
            }
        }
		system.debug('lineItem>>>>'+lineItem);

        return lineItem;
    }


    /**
    * @name adp_createEmployeeFile
    * @author Aliaksandr Satskou
    * @date 04/11/2014
    * @description Creating Employee File Line Items for ADP Provider.
    * @return void
    */
    public static void adp_createEmployeeFile(TMS__Process_Payroll__c payroll, String provider) {
        TMS__Payroll_Provider_Settings__c providerSetting = payrollProviderSettingMap.get(provider);

        if (providerSetting.TMS__Process_Employee_File__c) {
            List<Contact> candidateList = (List<Contact>)QueryHelper.getSObjectListWithAllFields(
                    'Contact',
                    new Set<String> { 'Account.ShippingState' },
                    'TMS__Add_Employee_in_the_Payroll__c = true');

			Map<Id, sObject> candidateIdPlacementMap = new Map<Id, sObject>();
			
            /*List<sObject> placementList = DataBase.query(
                    'SELECT AVTRRT__Contact_Candidate__c, AVTRRT__Start_Date__c, Pay_Rate__c, ' +
                        'Shift_Name__c, Workers_Comp_Code__c ' +
                    'FROM AVTRRT__Placement__c ' +
                    'WHERE AVTRRT__Contact_Candidate__c IN :candidateList ' +
                    'ORDER BY CreatedDate DESC');


            Map<Id, sObject> candidateIdPlacementMap = new Map<Id, sObject>();

            for (Contact candidate : candidateList) {
                for (sObject placement : placementList) {
                    if ((Id)placement.get('AVTRRT__Contact_Candidate__c') == candidate.Id) {
                        if (candidateIdPlacementMap.get(candidate.Id) == null) {
                            candidateIdPlacementMap.put(candidate.Id, placement);
                        }
                    }
                }
            }*/


            for (Contact candidate : candidateList) {
                /* Edited by Aliaksandr Satskou, 06/02/2014 (case #00028497) */
                candidate.TMS__Co_Code__c = 'DEC';
                candidate.TMS__Sui_Sdi_Tax_Jurisdiction_Code__c = candidate.Account.ShippingState;
                candidate.TMS__Change_Effective_On__c = payroll.TMS__Payroll_Process_Date__c;


                sObject placement = candidateIdPlacementMap.get(candidate.Id);

                if (placement != null) {
                    //candidate.TMS__Hire_Date__c = (Date)placement.get('AVTRRT__Start_Date__c');
                    candidate.TMS__Rate_1_Amount__c = (Decimal)placement.get('Pay_Rate__c');
                    candidate.TMS__Home_Department__c = (String)placement.get('Shift_Name__c');
                    candidate.TMS__Naics_Workers_Comp_Code__c = (String)placement.get('Workers_Comp_Code__c');
                }


                candidate.TMS__Add_Employee_in_the_Payroll__c = false;
            }

            update candidateList;


            List<Attachment> attachmentList = new List<Attachment>();
            PayrollGenerator.initData(provider);

            for (String filename : PayrollGenerator.objectNameFilenameSetMap.get('Contact')) {
                attachmentList.add(new Attachment(
                    Name = filename,
                    Body = Blob.valueOf(
                            PayrollGenerator.getCSVPayrollFileContent(
                                    provider, filename, 'Contact', candidateList)),
                    ParentId = payroll.Id
                ));
            }


            insert attachmentList;
        }
    }


    /**
    * @name insertData
    * @author Aliaksandr Satskou
    * @date 09/04/2013
    * @description Inserting Line Items.
    * @return void
    */
    public static void insertData() {
       // upsert outputLineList;
       
       Set<TMS__Payroll_Line_items__c> lineItemSet = new Set<TMS__Payroll_Line_items__c>();
       
       //to remove duplicate line items while inserting
       
       lineItemSet.addAll(timeMapToupdate.values());
       
       List<TMS__Payroll_Line_items__c> lineItemsToInsert = new List<TMS__Payroll_Line_items__c>();
       
       lineItemsToInsert.addall(lineItemSet);
       
       upsert lineItemsToInsert;
       
       system.debug('lineItemsToInsert>>>'+lineItemsToInsert);
        
        List<TMS__Time__c>  timeToUpdate = new List<TMS__Time__c>();
        
        system.debug('timeMapToupdate>>>'+timeMapToupdate);
        
        for(TMS__Time__c timeToRecord : timeMapToupdate.keyset()){
        	
        	TMS__Payroll_Line_items__c  payLine = timeMapToupdate.get(timeToRecord);
        	
        	system.debug('payLine>>>'+payLine);
        	
        	
        	system.debug('timeToRecord>>>'+timeToRecord);
        	
    		timeToRecord.TMS__Payroll_Line_item__c  = payLine.id;
    		timeToUpdate.add(timeToRecord);
        	
        }
        update timeToUpdate;
        
    }
}