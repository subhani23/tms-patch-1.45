/**
* @author Aliaksandr Satskou
* @date 12/17/2013
* @description Case helper class.
*/
public class CaseHelper {
	
	
	/**
	* @author Aliaksandr Satskou
	* @date 12/17/2013
	* @name updateTotalTimeSpent
	* @description Updating Total Time Spent field for Case object.
	* @param timeList List of Time objects
	* @return void
	*/
	public static void updateTotalTimeSpent(List<TMS__Time__c> timeList) {
		Set<Id> caseIdSet = new Set<Id>();
		
		for (TMS__Time__c timeObj : timeList) {
			if (timeObj.TMS__Case__c != null) {
				caseIdSet.add(timeObj.TMS__Case__c);
			}
		}
		
		
		List<Case> caseList = [
				SELECT TMS__Total_Time_Spent__c,
					(SELECT TMS__Time_Spent_Formula__c
					 FROM TMS__Time__r)
				FROM Case
				WHERE Id IN :caseIdSet];
		
		
		for (Case caseObj : caseList) {
			Decimal timeSpent = 0;
			
			for (TMS__Time__c timeObj : caseObj.TMS__Time__r) {
				timeSpent += timeObj.TMS__Time_Spent_Formula__c;
			}
			
			caseObj.TMS__Total_Time_Spent__c = timeSpent;
		}
		
		
		update caseList;
	}
}