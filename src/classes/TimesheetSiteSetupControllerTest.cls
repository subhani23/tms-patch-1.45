@isTest
private class TimesheetSiteSetupControllerTest {

	private static testMethod void test() {
		TimesheetSiteSetupController siteSetup= new TimesheetSiteSetupController();

		siteSetup.setSiteSetupForTimesheet();
		/*pageList = [select id,Name,FCMS__Name__c from FCMS__Page__c ];

		for(FCMS__Page__c page:pageList){
			page.FCMS__Active__c = false;
			page.FCMS__Authentication_Required__c=false;
		}
		if(pageList.size()>0){
			update pageList;
		}

		menuList = [Select m.FCMS__Visiblity_Authentication__c ,m.FCMS__Page__r.FCMS__Name__c, m.FCMS__Page__c, m.FCMS__Order__c,m.FCMS__Name__c,m.Id,
											m.FCMS__Active__c From FCMS__Menu__c m];
		for(FCMS__Menu__c menu:menuList){
			menu.FCMS__Page__c = null;
			menu.FCMS__Active__c = false;
			menu.FCMS__Visiblity_Authentication__c = false;
		}
		if(menuList.size()>0){
			update menuList;
		}

		blockList = [Select f.Name,FCMS__Page__c, f.FCMS__Weight__c, f.FCMS__Type__c, f.FCMS__Section__c, f.FCMS__Name__c,
											f.FCMS__Menu_Component_Name__c, f.FCMS__Active__c From FCMS__Block__c f];


		for(FCMS__Block__c block:blockList){
			block.FCMS__Active__c=false;
			block.FCMS__Section__c ='footer';
			block.FCMS__Weight__c =7;
			block.FCMS__IFrame_URL__c ='JobAionPage';
			block.FCMS__Add_Parameters_From_Parent_Window__c='Ids=,';
			block.FCMS__Width__c='10%';
		}
		if(blockList.size()>0){
			update blockList;
		}*/

		siteSetup.setSiteSetupForTimesheet();
	}


	/**
	* @author Aliaksandr Satskou
	* @name testGenerateWeekManagements
	* @date 04/24/2014
	* @description Testing generating Week Managements based on Start Date and End Date.
	* @return void
	*/
	private static testMethod void testGenerateWeekManagements() {
		TimesheetSiteSetupController controller = new TimesheetSiteSetupController();
		controller.week.TMS__Start_Date__c = Date.today() + 1;
		controller.week.TMS__End_Date__c = Date.today();

		controller.generateWeekManagements();
		System.assertEquals(true, ApexPages.hasMessages(ApexPages.Severity.ERROR));


		controller.week.TMS__Start_Date__c = Date.today();
		controller.week.TMS__End_Date__c = Date.today() + 15;
		controller.week.TMS__Active__c = true;
		controller.week.TMS__Generate__c = 'Timesheet';

		controller.generateWeekManagements();

		List<TMS__Week_Management__c> weekList = [SELECT Id FROM TMS__Week_Management__c];
		System.assertEquals(3, weekList.size());
	}
}