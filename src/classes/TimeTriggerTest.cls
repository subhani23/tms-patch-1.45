/**
* @author Aliaksandr Satskou
* @date 06/01/2013
* @description Test class for TimeTrigger.trigger.
*/
@isTest
private class TimeTriggerTest {

	private static List<TMS__Project__c> projectList;
	private static List<Contact> candidateList;
	private static List<TMS__Project_Resource__c> projectResourceList;
	private static List<TMS__Week_Management__c> weekManagementList;
	private static List<TMS__Task__c> taskList;
	private static List<TMS__Time__c> timeList;

	/**
	* @name initData
	* @author Aliaksandr Satskou
	* @data 06/01/2013
	* @description Init all needed data.
	* @return void
	*/
	private static void initData() {
		candidateList = new List<Contact> {
			new Contact(
				LastName = 'Contact #1'
			),
			new Contact(
				LastName = 'Contact #2'
			)
		};
		insert candidateList;

		projectList = new List<TMS__Project__c> {
			new TMS__Project__c(
				RecordTypeId = RecordTypeUtility.getProjectRecordTypeId(),
				Name = 'Project #1',
				TMS__Project_Status__c = 'Open'
			)
		};
		insert projectList;

		projectResourceList = new List<TMS__Project_Resource__c> {
			new TMS__Project_Resource__c(
				TMS__Project__c = projectList[0].Id,
				TMS__Contact__c = candidateList[0].Id
			),
			new TMS__Project_Resource__c(
				TMS__Project__c = projectList[0].Id,
				TMS__Contact__c = candidateList[1].Id
			)
		};
		insert projectResourceList;

		taskList = new List<TMS__Task__c> {
			new TMS__Task__c(
				Name = 'Regular Task #1',
				TMS__Project__c = projectList[0].Id,
				TMS__Project_Resource__c = projectResourceList[0].Id
			),
			new TMS__Task__c(
				Name = 'Overtime Task #1',
				TMS__Project__c = projectList[0].Id,
				TMS__Project_Resource__c = projectResourceList[1].Id
			)
		};
		insert taskList;

		weekManagementList = new List<TMS__Week_Management__c> {
			new TMS__Week_Management__c(
				TMS__Start_Date__c = Date.today(),
				TMS__End_Date__c = Date.today() + 7,
				TMS__Active__c = true
			),
			new TMS__Week_Management__c(
				TMS__Start_Date__c = Date.today() + 8,
				TMS__End_Date__c = Date.today() + 15,
				TMS__Active__c = true
			)
		};
		insert weekManagementList;

		timeList = new List<TMS__Time__c> {
			new TMS__Time__c(
				TMS__Accounting_Id__c = 'Accounting Id #1',
				TMS__Time_Spent__c = 1,
				TMS__Date__c = Date.today() + 3,
				TMS__Task__c = taskList[0].Id,
				TMS__Week_Management__c = weekManagementList[0].Id
			),
			new TMS__Time__c(
				TMS__Accounting_Id__c = 'Accounting Id #2',
				TMS__Time_Spent__c = 2,
				TMS__Date__c = Date.today() + 6,
				TMS__Task__c = taskList[0].Id,
				TMS__Week_Management__c = weekManagementList[0].Id
			),
			new TMS__Time__c(
				TMS__Accounting_Id__c = 'Accounting Id #3',
				TMS__Time_Spent__c = 3,
				TMS__Date__c = Date.today() + 9,
				TMS__Task__c = taskList[1].Id,
				TMS__Week_Management__c = weekManagementList[1].Id
			),
			new TMS__Time__c(
				TMS__Accounting_Id__c = 'Accounting Id #4',
				TMS__Time_Spent__c = 4,
				TMS__Date__c = Date.today() + 12,
				TMS__Task__c = taskList[1].Id,
				TMS__Week_Management__c = weekManagementList[1].Id
			)
		};
		insert timeList;
	}

	/**
	* @name testTimeTrigger
	* @author Aliaksandr Satskou
	* @data 06/01/2013
	* @description Testing TimeTrigger.
	* @return void
	*/
	private static testMethod void testTimeTrigger() {
		/* Init all needed test data */
		initData();

		/* Changing date for Time Object to auto change Week Management Object */
		timeList[0].TMS__Date__c = Date.today() + 9;
		timeList[3].TMS__Date__c = Date.today() + 100500;

		/* Changing Time Spent for Time Object to auto change Consumed Hours for Task Object */
		timeList[1].TMS__Time_Spent__c = 5;
		/* Changing Accounting Id for Time Object to auto change Generate for Week Management Object */
		timeList[1].TMS__Accounting_Id__c = null;
		update timeList;

		/* Re-Select list of Time Objects to update Week Management field */
		timeList = [
				SELECT TMS__Week_Management__c
				FROM TMS__Time__c
				WHERE Id IN :timeList];

		/* Re-Select list of Task Objects to update Consumed Hours field */
		taskList = [
				SELECT TMS__Consumed_Hours__c
				FROM TMS__Task__c
				WHERE Id IN :taskList];

		System.assertEquals(weekManagementList[1].Id, timeList[0].TMS__Week_Management__c);
		System.assertEquals(null, timeList[3].TMS__Week_Management__c);

		System.assertEquals(6, taskList[0].TMS__Consumed_Hours__c);
		System.assertEquals(7, taskList[1].TMS__Consumed_Hours__c);

		/* Changing Accounting Id for Time Object to auto change Generate for Week Management Object */
		timeList[1].TMS__Accounting_Id__c = null;
		update timeList;

		/* Re-Select list of Week Management Objects to update Generate field */
		weekManagementList = [
				SELECT TMS__Generate__c
				FROM TMS__Week_Management__c
				WHERE Id IN :weekManagementList];

		System.assertEquals('Timesheet', weekManagementList[0].TMS__Generate__c);
		System.assertEquals('Timesheet', weekManagementList[1].TMS__Generate__c);
	}
}