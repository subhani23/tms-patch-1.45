/**
* @author Aliaksandr Satskou
* @date 04/10/2014
* @description Test class for PackagePostInstall and PackagePostInstallHelperTest classes.
*/
@isTest
private class PackagePostInstallHelperTest {

	/**
	* @author Aliaksandr Satskou
	* @name testPackagePostInstallHelper
	* @date 04/10/2014
	* @description Testing PackagePostInstall and PackagePostInstallHelperTest classes.
	* @return void
	*/
	static testMethod void testPackagePostInstallHelper() {
		PackagePostInstall postInstall = new PackagePostInstall();
		Test.testInstall(postInstall, null);

		List<TMS__Payroll_Settings__c> payrollSettingList = [SELECT Id FROM TMS__Payroll_Settings__c];
		System.assertEquals(130, payrollSettingList.size());

		TMS__Payroll_Filenames__c payrollFilenamesSetting = [SELECT Id FROM TMS__Payroll_Filenames__c];
		System.assertNotEquals(null, payrollFilenamesSetting);
	}
}