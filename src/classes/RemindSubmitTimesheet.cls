public class RemindSubmitTimesheet {
    public RemindSubmitTimesheet () {
        //List<List<String>> employeesIdListOfList = new List<List<String>>();
        List<TMS__Time__c> timeList = new List<TMS__Time__c>();
        List<String> employeesIdList = new List<String>();
        Map<String,String> employeesIdMap = new Map<String,String>();
        List<TMS__Time__c> timeListNew = new List<TMS__Time__c>();
        List<String> weekMgmtIdList = new List<String>();
        Map<String,String> weekMgmtIdMap = new Map<String,String>();
        //Map<Id, String> employeesIdMap = new Map<Id, String>();
        List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
        Date dt = Date.today().addDays(-7);
        System.debug('********amrender date = '+dt);
        timeList = [Select TMS__Time_Spent__c, TMS__Task__c, TMS__Task__r.Project_Resource__r.Contact__r.Id, TMS__Task__r.Project_Resource__r.Contact__r.Email, TMS__Status__c, Id, TMS__Week_Management__r.Id, TMS__Week_Management__r.Week__c, TMS__Comments__c, TMS__Billable__c From TMS__Time__c where (TMS__Status__c = 'Open' OR TMS__Status__c = 'Rejected') AND TMS__Date__c < :dt];
        System.debug('********amrender timeList size = '+timeList.size()+'****'+timeList);
        if(timeList != null && timeList.size() > 0) {
            employeesIdList.clear();
            for(Integer i=0; i<timeList.size(); i++) {
                if(!employeesIdMap.containsKey(timeList[i].TMS__Task__r.Project_Resource__r.Contact__r.Id)) {
                    employeesIdMap.put(timeList[i].TMS__Task__r.Project_Resource__r.Contact__r.Id, timeList[i].TMS__Task__r.Project_Resource__r.Contact__r.Id);
                }
            }
            employeesIdList = employeesIdMap.values();
            System.debug('********* Amrender employeesIdList = '+employeesIdList);
            //employeesIdList = removeDuplicate(employeesIdList);
            for(Integer i=0; i<employeesIdList.size(); i++) {
                String employeeId = employeesIdList[i];
                timeListNew.clear();
                String emailBody = '';
                timeListNew = [Select TMS__Time_Spent__c, TMS__Task__c, TMS__Task__r.Project_Resource__r.Contact__r.Id, TMS__Task__r.Project_Resource__r.Contact__r.Email, TMS__Status__c, Id, TMS__Week_Management__r.Id, TMS__Week_Management__r.Week__c, TMS__Comments__c, TMS__Billable__c From TMS__Time__c where (TMS__Status__c = 'Open' OR TMS__Status__c = 'Rejected') AND TMS__Date__c < :dt AND TMS__Task__r.Project_Resource__r.Contact__r.Id =:employeesIdList[i]];
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                List<String> toAddress = new List<String>();
                if(timeListNew != null && timeListNew.size() > 0) {
                    weekMgmtIdList.clear();
                    weekMgmtIdMap.clear();
                    //for(Integer k=0; k<timeListNew.size(); k++) {
                    if(timeListNew[0].TMS__Task__r.Project_Resource__r.Contact__r.Email != null) {
                        toAddress.add(timeListNew[0].TMS__Task__r.Project_Resource__r.Contact__r.Email);
                        mail.setToAddresses(toAddress);
                        for(Integer k=0; k<timeListNew.size(); k++) {
                            if(timeListNew[k].TMS__Week_Management__r.Week__c != null) {
                                if(!weekMgmtIdMap.containsKey(timeListNew[k].TMS__Week_Management__r.Week__c))
                                    weekMgmtIdMap.put(timeListNew[k].TMS__Week_Management__r.Week__c, timeListNew[k].TMS__Week_Management__r.Week__c);
                            }
                        }
                        weekMgmtIdList = weekMgmtIdMap.values();
                        //weekMgmtIdList = removeDuplicate(weekMgmtIdList);
                        for(Integer k=0; k<weekMgmtIdList.size(); k++) {
                            emailBody = emailBody+timeListNew[k].TMS__Week_Management__r.Week__c+'\n';
                        }
                        mail.setSubject('Your TimeSheet is due');
                        mail.setPlainTextBody('Dear '+(string.valueOf(timeListNew[0].TMS__Task__r.Project_Resource__r.Contact__r.Email)).toUpperCase()+' \nYou have not submitted your timesheet for the period \n'+emailBody);
                        System.debug('*********Amrender mail ='+mail);
                        System.debug('********amrender employeesIdList size = '+employeesIdList);
                        mailList.add(mail);
                        //Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mailList });
                    }
                }
                //}
            }
            if(mailList.size() > 0) {
                try {
                    Messaging.SendEmailResult [] r = Messaging.sendEmail(mailList);
                } catch(Exception e) {
                    System.debug('*********Error = '+e);
                }
            }
        }
    }
    /*private List<String> removeDuplicate(List<String> proIdsList1) {
        //List<String> projectIdsList = new List<String>();
        if(proIdsList1 != null && proIdsList1.size() > 0) {
            for(Integer i=0; i<proIdsList1.size(); i++) {
                for(Integer j=i+1; j<proIdsList1.size(); j++) {
                    if(proIdsList1[i] == proIdsList1[j]) {
                        proIdsList1.remove(i);
                        //break;
                    }
                }
            }
        }
        return proIdsList1;
    }*/

    public testMethod static void RemindSubmitTimesheet() {
        Contact con = new Contact();
        con.LastName = 'Test Contact';
        con.Email = 'amrender.g@avankia.com';
        insert con;
        TMS__Project_Resource__c projectResource = new TMS__Project_Resource__c();
        projectResource.TMS__Contact__c = con.Id;
        insert projectResource;
        TMS__Task__c task = new TMS__Task__c();
        task.Name = 'Test Task';
        task.TMS__Project_Resource__c = projectResource.Id;
        insert task;
        TMS__Week_Management__c weekMgmt = new TMS__Week_Management__c();
        weekMgmt.TMS__Start_Date__c = date.today();
        weekMgmt.TMS__End_Date__c = date.today().addDays(7);
        weekMgmt.TMS__Active__c = true;
        insert weekMgmt;
        TMS__Time__c tm = new TMS__Time__c();
        tm.TMS__Task__c = task.Id;
        tm.TMS__Week_Management__c = weekMgmt.Id;
        tm.TMS__Date__c = date.today().addDays(-9);
        tm.TMS__Status__c = 'Open';
        insert tm;
        RemindSubmitTimesheet obj = new RemindSubmitTimesheet();
    }
}