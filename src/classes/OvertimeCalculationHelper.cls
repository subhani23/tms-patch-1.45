/**
* @author Aliaksandr Satskou
* @date 05/22/2013
* @description Update Time objects, related to Overtime Task, based on Time objects, related to Regular Task.
*/
public without sharing class OvertimeCalculationHelper {

	private static final Integer SCHEDULER_DAILY = 1;
	private static final Integer SCHEDULER_WEEKLY = 2;
	private static final Integer SCHEDULER_DAILY_AND_WEEKLY = 3;

	private static final Integer DATATYPE_NUMBER = 1;
	private static final Integer DATATYPE_TIME = 2;

	private static final String NUMBER_API_NAME = 'TMS__Time_Spent__c';
	private static final String TIME_API_NAME = 'TMS__Time_Spent_Time_Format__c';

	private static Logger logger = new Logger('OvertimeCalculationHelper');


	/**
	* @author Aliaksandr Satskou
	* @date 05/22/2013
	* @description Wrapper class, which contains data about time for each day.
	*/
	private class Day {
		public TMS__Time__c timeObject;
		public Boolean isCanUpdate {
			get {
				if (timeObject != null) { return true; }

				return false;
			}
		}
		public Boolean isAdded;

		public Decimal daySpent;


		public Day() {
			daySpent = 0;
		}

		public Day(TMS__Time__c timeObject, Integer tDataType) {
			logger.log('Day', 'timeObject', timeObject);
			logger.log('Day', 'tDataType', tDataType);

			Decimal v_daySpent;

			if (tDataType == DATATYPE_NUMBER) {
				v_daySpent = (Decimal)timeObject.get(NUMBER_API_NAME);
			}

			if (tDataType == DATATYPE_TIME) {
				logger.log('Day', 'TIME_API_NAME', TIME_API_NAME);
				logger.log('Day', 'timeObject.get(TIME_API_NAME)', timeObject.get(TIME_API_NAME));

				v_daySpent = convertTimeToNumber(timeObject.get(TIME_API_NAME));
			}

			logger.log('Day', 'v_daySpent', v_daySpent);

			daySpent = (v_daySpent != null) ? v_daySpent : 0;
			this.timeObject = timeObject;

			isAdded = false;
		}


		/**
		* @name updateTime
		* @author Aliaksandr Satskou
		* @data 05/22/2013
		* @description Updating Time object, based calculated hours.
		* @param entity CalculationEntity
		* @param dayOfWeek Day of Week
		* @param status Status of Time object
		* @return Boolean
		*/
		public Boolean updateTime(CalculationEntity entity, Integer dayOfWeek, String status) {
			logger.log('updateTime', 'entity', entity);
			logger.log('updateTime', 'dayOfWeek', dayOfWeek);
			logger.log('updateTime', 'status', status);
			logger.log('updateTime', 'timeObject1', timeObject);

			if ((timeObject == null) && (daySpent == 0)) {
				return false;
			}

			/* Edited by Aliaksandr Satskou, 09/17/2014 (case # 00031941) */
			if (timeObject == null)  {
				logger.log('updateTime', '(timeObject == null)');

				TMS__Timesheet__c tms = tmsMap.get(entity.task.TMS__Project_Resource__r.TMS__Contact__c);

				timeObject = new TMS__Time__c(
					TMS__Week_Management__c = entity.week.Id,
					TMS__Date__c = entity.week.TMS__Start_Date__c + dayOfWeek,
					TMS__Project__c = entity.task.TMS__Project__c,
					TMS__Candidate__c = entity.task.TMS__Project_Resource__r.TMS__Contact__c,
					TMS__Timesheet__c = (tms != null) ? tms.Id : null
				);
			}


			logger.log('updateTime', 'timeObject != null');
			logger.log('updateTime', 'timeObject2', timeObject);

			timeObject.TMS__Task__c = entity.taskId;


			/* Edited by Aliaksandr Satskou, 02/19/2015 (case #00037511) */
			CustomSettingsUtility.StatusContainer sc = new CustomSettingsUtility.StatusContainer();

			if (timeObject.TMS__Status__c == null) {
				timeObject.TMS__Status__c = status;
			} else {
				if (status == sc.tmsOpen) {
					if ((timeObject.TMS__Status__c != sc.tmsSubmitted) &&
							(timeObject.TMS__Status__c != sc.tmsApproved) &&
							(timeObject.TMS__Status__c != sc.tmsRejected)) {

						timeObject.TMS__Status__c = status;
					}
				}

				if (status == sc.tmsSubmitted) {
					if (timeObject.TMS__Status__c != sc.tmsApproved) {
						timeObject.TMS__Status__c = status;
					}
				}
			}


			if (entity.tDataType == DATATYPE_NUMBER) {
				timeObject.put(NUMBER_API_NAME, daySpent);

				return true;
			}

			if (entity.tDataType == DATATYPE_TIME) {
				timeObject.put(TIME_API_NAME, convertNumberToTime(daySpent));

				return true;
			}


			return false;
		}
	}


	/**
	* @author Aliaksandr Satskou
	* @date 05/22/2013
	* @description Wrapper class, which contains all needed data for calculation overtime hours.
	*/
	private class CalculationEntity {
		public Decimal otTreshold;
		public Decimal otDailyTreshold;
		public Integer otScheduler;

		public Id taskId;

		/* Added by Aliaksandr Satskou, 09/17/2014 (case # 00031941) */
		public TMS__Task__c task;

		public Id otId;
		public Integer tDataType;

		/* Added by Aliaksandr Satskou, 09/17/2014 (case # 00031941) */
		public TMS__Week_Management__c week;

		public List<Day> dayList;

		public Boolean hasOvertime;
		public Boolean isRegular;
		public Boolean isOvertimeOverflow;
		public Boolean isBillable;

		public CalculationEntity(
				TMS__Task__c task, TMS__Week_Management__c wObject, 
				TimeSheetController controller, Map<Id, TMS__Task__c> tMap) {

			logger.log('CalculationEntity', 'task', task);

			taskId = task.Id;

			/* Added by Aliaksandr Satskou, 09/17/2014 (case # 00031941) */
			this.task = task;

			otId = task.TMS__Overtime_overflow_task__c;

			tDataType = (task.TMS__Task_data_type__c == 'Time') ? DATATYPE_TIME : DATATYPE_NUMBER;
			hasOvertime = (otId != null) ? true : false;
			isOvertimeOverflow = isOvertimeOverflowTask();
			isRegular = !isOvertimeOverflow;
			
			if (isOvertimeOverflow) {
				TMS__Task__c regularTask = getRegularTask(tMap);
				
				logger.log('CalculationEntity', 'regularTask.Name', regularTask.Name);
				
				List<TMS__Time__c> regularTimes = regularTask.TMS__Time__r;
				List<TMS__Time__c> overtimeTimes = task.TMS__Time__r;
				
				logger.log('CalculationEntity', 'regularTimes Number', 
						GroupByHelper.getFieldValuesList(
								regularTimes, 'TMS__Time_Spent__c'));
				
				logger.log('CalculationEntity', 'regularTimes Time', 
						GroupByHelper.getFieldValuesList(
								regularTimes, 'TMS__Time_Spent_Time_Format__c'));
				
				for (TMS__Time__c overtimeTime : overtimeTimes) {
					TMS__Time__c relatedRegularTime;
					for (TMS__Time__c regularTime : regularTimes) {
						if (overtimeTime.TMS__Date__c == regularTime.TMS__Date__c) {
							relatedRegularTime = regularTime;
						}
					}
					
					logger.log('CalculationEntity', 'relatedRegularTime', relatedRegularTime);
					
					logger.log('CalculationEntity', 'task', task);
					logger.log('CalculationEntity', 'tDataType', tDataType);
					
					// relatedRegularTime = null if regular hours 
					// was removed for current overtime record. 
					if (controller.isTimeChanged(
								relatedRegularTime, overtimeTime.TMS__Date__c, regularTask)) {
						
						if (tDataType == DATATYPE_TIME) {
							overtimeTime.TMS__Time_Spent_Time_Format__c = '00:00';
						} else {
							overtimeTime.TMS__Time_Spent__c = 0;
						}
					}
					
					logger.log('CalculationEntity', 'relatedRegularTime2', relatedRegularTime);
					logger.log('CalculationEntity', 'overtimeTime', overtimeTime);
				}
				
				logger.log('CalculationEntity', 'regularTimes2 Number', 
						GroupByHelper.getFieldValuesList(
								regularTimes, 'TMS__Time_Spent__c'));
				
				logger.log('CalculationEntity', 'regularTimes2 Time', 
						GroupByHelper.getFieldValuesList(
								regularTimes, 'TMS__Time_Spent_Time_Format__c'));
			}
			
			logger.log('CalculationEntity', 'task.Name', task.Name);
			logger.log('CalculationEntity', 'task.TMS__Time__r.size()', task.TMS__Time__r.size());
			logger.log('CalculationEntity', 'taskTimes Number', 
					GroupByHelper.getFieldValuesList(task.TMS__Time__r, 'TMS__Time_Spent__c'));
			logger.log('CalculationEntity', 'taskTimes Time', 
					GroupByHelper.getFieldValuesList(task.TMS__Time__r, 'TMS__Time_Spent_Time_Format__c'));

			/* Added by Aliaksandr Satskou, 09/17/2014 (case # 00031941) */
			week = wObject;

			logger.log('CalculationEntity', 'hasOvertime', hasOvertime);

			if (hasOvertime) {
				otTreshold = task.TMS__Overtime_hours_threshold__c;
				otDailyTreshold = task.TMS__Daily_hours_threshold__c;

				String v_otScheduler = task.TMS__Overtime_calculation_schedule__c;

				if (v_otScheduler == 'Daily') {
					otScheduler = SCHEDULER_DAILY;
				} else if (v_otScheduler == 'Weekly') {
					otScheduler = SCHEDULER_WEEKLY;
				} else if (v_otScheduler == 'Daily & Weekly') {
					otScheduler = SCHEDULER_DAILY_AND_WEEKLY;
				}

				String otBillable = task.TMS__Overtime_Billable__c;

				if (otBillable != null) {
					isBillable = (otBillable == 'Yes') ? true : false;
				} else { isBillable = false; }
			}

			dayList = getDayList(task, wObject, tDataType);
		}
		
		private Boolean isOvertimeOverflowTask() {
			return task.TMS__OvertimeTasks__r.size() > 0;
		}
		
		private TMS__Task__c getRegularTask(Map<Id, TMS__Task__c> tMap) {
			
			TMS__Task__c regularTask = task.TMS__OvertimeTasks__r[0];
			regularTask = tMap.get(regularTask.Id);
			if (regularTask.TMS__OvertimeTasks__r.size() > 0) {
				regularTask = regularTask.TMS__OvertimeTasks__r[0];
				regularTask = tMap.get(regularTask.Id);
			}
			
			return regularTask;
		}

		/**
		* @name getDayList
		* @author Aliaksandr Satskou
		* @data 05/22/2013
		* @description Getting list of Day, based Task object.
		* @param task Task object
		* @param wObject Week Management object
		* @param tDataType Task data type
		* @return List of Day
		*/
		private List<Day> getDayList(
				TMS__Task__c task, TMS__Week_Management__c wObject, Integer tDataType) {

			List<Day> dayList = new List<Day> {
				new Day(), new Day(), new Day(), new Day(), new Day(), new Day(), new Day()
			};


			for (TMS__Time__c timeObject : task.TMS__Time__r) {

				logger.log('getDayList', 'timeObject', timeObject);

				if (timeObject.TMS__Date__c == wObject.TMS__Start_Date__c) {
					dayList[0] = new Day(timeObject, tDataType);
				} else if (timeObject.TMS__Date__c == wObject.TMS__Start_Date__c.addDays(1)) {
					dayList[1] = new Day(timeObject, tDataType);
				} else if (timeObject.TMS__Date__c == wObject.TMS__Start_Date__c.addDays(2)) {
					dayList[2] = new Day(timeObject, tDataType);
				} else if (timeObject.TMS__Date__c == wObject.TMS__Start_Date__c.addDays(3)) {
					dayList[3] = new Day(timeObject, tDataType);
				} else if (timeObject.TMS__Date__c == wObject.TMS__Start_Date__c.addDays(4)) {
					dayList[4] = new Day(timeObject, tDataType);
				} else if (timeObject.TMS__Date__c == wObject.TMS__Start_Date__c.addDays(5)) {
					dayList[5] = new Day(timeObject, tDataType);
				} else if (timeObject.TMS__Date__c == wObject.TMS__Start_Date__c.addDays(6)) {
					dayList[6] = new Day(timeObject, tDataType);
				}
				
				logger.log('getDayList', 'dayList', dayList);
			}

			return dayList;
		}
		
		public String getLog() {
			String result = 'Task: ' + task.Name + '\n';
			
			result += 'Times: ';
			for (Day d : dayList) {
				result += d.daySpent + '\t';
			}
			result += '\n';
			
			return result;
		}
		
		public void moveTimeFromAnotherEntity(CalculationEntity anotherEntity) {
			for (Integer i = 0; i < 7; i++) {
				this.dayList[i].daySpent += anotherEntity.dayList[i].daySpent;
				anotherEntity.dayList[i].daySpent = 0;
			}
		}
	}



	private static Map<Id, TMS__Task__c> tMap;

	/* Added by Aliaksandr Satskou, 09/17/2014 (case # 00031941) */
	private static Map<Id, TMS__Timesheet__c> tmsMap;

	/**
	* @name calculateOvertime
	* @author Aliaksandr Satskou
	* @data 05/22/2013
	* @description Updating Time objects, related to Overtime Task, based on Time objects,
	*       related to Regular Task.
	* @param taskIdSet Set of Id of Task objects
	* @param wObject Week Management object
	* @param timeStatus Status of Time objects before save
	* @return void
	*/
	public static void calculateOvertime(
			Set<Id> taskIdSet, TMS__Week_Management__c wObject, String timeStatus, 
			TimeSheetController controller) {

		updateMap(taskIdSet, wObject);

		Map<Id, CalculationEntity> entityMap = new Map<Id, CalculationEntity>();

		for (Id taskId : tMap.keySet()) {
			TMS__Task__c task = tMap.get(taskId);
			CalculationEntity entity = new CalculationEntity(task, wObject, controller, tMap);

			logger.log('calculateOvertime', 'entity0', entity);
			logger.log('calculateOvertime', 'entity.getLog() 0', entity.getLog());

			entityMap.put(entity.taskId, entity);
		}
		
		List<Id> taskIdList = getOrderedIdList();

		for (Id taskId : taskIdList) {
			CalculationEntity entity = entityMap.get(taskId);
			
			if (entity.isRegular && entity.hasOvertime) {
				CalculationEntity otEntity = entityMap.get(entity.otId);
				
				entity.moveTimeFromAnotherEntity(otEntity);
				
				if (otEntity.hasOvertime) {
					CalculationEntity ofEntity = entityMap.get(otEntity.otId);
					
					entity.moveTimeFromAnotherEntity(ofEntity);
				}
			}
		}
		
		logger.log('calculateOvertime', 'taskIdList', taskIdList);

		for (Id taskId : taskIdList) {
			CalculationEntity entity = entityMap.get(taskId);

			logger.log('calculateOvertime', 'entity', entity);

			if ((entity != null) && (entity.hasOvertime)) {
				CalculationEntity otEntity = entityMap.get(entity.otId);

				logger.log('calculateOvertime', 'otEntity', otEntity);

				if (otEntity != null) {
					calculate(entity, otEntity);
					
					logger.log('calculateOvertime', 'entity.getLog() 1', entity.getLog());
					logger.log('calculateOvertime', 'otEntity.getLog() 1', otEntity.getLog());
				}
			}
		}
		
		Set<TMS__Time__c> timesToSave = new Set<TMS__Time__c>();

		for (Id taskId : taskIdList) {
			CalculationEntity entity = entityMap.get(taskId);

			logger.log('calculateOvertime', 'entity2', entity);

			if (entity != null) {
				if (entity.hasOvertime) {
					CalculationEntity otEntity = entityMap.get(entity.otId);

					logger.log('calculateOvertime', 'otEntity2', otEntity);

					for (Integer i = 0; i < 7; i++) {
						Boolean isUpdatedSameDateList = false;

						logger.log('calculateOvertime',
								'entity.dayList[i].isCanUpdate', entity.dayList[i].isCanUpdate);

						entity.dayList[i].updateTime(entity, i, timeStatus);

						if (entity.dayList[i].isCanUpdate) {
							if (entity.dayList[i].isAdded != true) {
								List<TMS__Time__c> toUpdateList = getSameDateUpdatedTimeList(
										entity.dayList[i].timeObject,
										entity.tDataType, entity.dayList[i].daySpent);
								
								isUpdatedSameDateList = (toUpdateList.size() > 1) ? true : false;

								timesToSave.addAll(toUpdateList);
								entity.dayList[i].isAdded = true;
							}
						}

						Boolean isUpdated = otEntity.dayList[i].updateTime(otEntity, i, timeStatus);

						if ((otEntity.dayList[i].isCanUpdate) && (isUpdated)) {
							otEntity.dayList[i].timeObject.TMS__Billable__c = entity.isBillable;

							if (isUpdatedSameDateList) {
								otEntity.dayList[i].timeObject.TMS__Case__c = null;
							}

							logger.log('calculateOvertime', 
									'otEntity.dayList[i]', otEntity.dayList[i]);
							logger.log('calculateOvertime', 
									'otEntity.dayList[i].isAdded', otEntity.dayList[i].isAdded);

							if (otEntity.dayList[i].isAdded != true) {
								timesToSave.add(otEntity.dayList[i].timeObject);
								otEntity.dayList[i].isAdded = true;
							}
						}
					}
				} else {
					for (Integer i = 0; i < 7; i++) {
						/* Edited by Aliaksandr Satskou, 09/17/2014 (case # 00031941) */
						entity.dayList[i].updateTime(entity, i, timeStatus);

						if ((entity.dayList[i].isCanUpdate) 
								&& (entity.dayList[i].isAdded != true)) {
							
							timesToSave.add(entity.dayList[i].timeObject);
						}
					}
				}
			}
		}

		logger.log('calculateOvertime', 'timesToSave', timesToSave);

		upsert new List<TMS__Time__c>(timesToSave);
		
		List<TMS__Time__c> timesToDelete = [
				SELECT Id FROM TMS__Time__c
				WHERE TMS__Task__c IN :taskIdList
				AND TMS__Time_Spent_Formula__c = 0];
		
		logger.log('calculateOvertime', 'timesToDelete', timesToDelete);

		delete timesToDelete;
	}


	/**
	* @name getSameDateUpdatedTimeList
	* @author Aliaksandr Satskou
	* @data 11/04/2013
	* @description Getting updated list of Time objects with the same date.
	* @param timeObject Time object
	* @param tDataType Task data type
	* @param timeSpent Time spent
	* @return List<TMS__Time__c>
	*/
	private static List<TMS__Time__c> getSameDateUpdatedTimeList(
			TMS__Time__c timeObject, Integer tDataType, Decimal timeSpent) {

		System.debug(LoggingLevel.ERROR, ':::::::::timeSpent=' + timeSpent);
		TMS__Task__c task = tMap.get(timeObject.TMS__Task__c);


		List<TMS__Time__c> timeList = new List<TMS__Time__c>();

		if (task != null) {
			for (TMS__Time__c timeObj : task.TMS__Time__r) {
				if (timeObj.TMS__Date__c == timeObject.TMS__Date__c) {
					if (tDataType == DATATYPE_NUMBER) {
						timeObj.put(NUMBER_API_NAME, 0);
					}

					if (tDataType == DATATYPE_TIME) {
						timeObj.put(TIME_API_NAME, '00:00');
					}

					timeList.add(timeObj);
				}
			}
		}

		System.debug(LoggingLevel.ERROR, ':::::::::timeList1=' + timeList);


		if (timeList.size() > 0) {

			Decimal div = timeSpent / timeList.size();
			System.debug(LoggingLevel.ERROR, '::div=' + div);
			Integer allow = Integer.valueOf(div);
			System.debug(LoggingLevel.ERROR, '::allow=' + allow);
			Integer added = 0;

			for (TMS__Time__c timeObj : timeList) {
				if (tDataType == DATATYPE_NUMBER) {
					timeObj.put(NUMBER_API_NAME, allow);
					added += allow;
				}

				if (tDataType == DATATYPE_TIME) {
					logger.log('getSameDateUpdatedTimeList', 'convertNumberToTime(allow)', convertNumberToTime(allow));

					timeObj.put(TIME_API_NAME, convertNumberToTime(allow));
					added += allow;
				}
			}

			if (added < timeSpent) {
				System.debug(LoggingLevel.ERROR, '::added=' + added);
				if (tDataType == DATATYPE_NUMBER) {
					timeList[0].put(NUMBER_API_NAME, (Decimal)timeList[0].get(NUMBER_API_NAME) +
							(timeSpent - added));
				}

				if (tDataType == DATATYPE_TIME) {
					Decimal tSpent = convertTimeToNumber(timeList[0].get(TIME_API_NAME)) +
							(timeSpent - added);

					logger.log('getSameDateUpdatedTimeList', 'convertNumberToTime(tSpent)', convertNumberToTime(tSpent));

					timeList[0].put(TIME_API_NAME, convertNumberToTime(tSpent));
				}
			}
		}

		System.debug(LoggingLevel.ERROR, ':::::::::timeList2=' + timeList);

		if (timeList.size() <= 1) { return new List<TMS__Time__c> { timeObject }; }

		return timeList;
	}


	/**
	* @name updateMap
	* @author Aliaksandr Satskou
	* @data 05/22/2013
	* @description Updating map of Task objects.
	* @param taskIdSet Set of Id of Task objects
	* @param wObject Week Management object
	* @return void
	*/
	private static void updateMap(Set<Id> taskIdSet, TMS__Week_Management__c wObject) {
		//if (tMap == null) {
			tMap = new Map<Id, TMS__Task__c>([
					SELECT TMS__Overtime_overflow_task__c, TMS__Overtime_hours_threshold__c,
						TMS__Overtime_calculation_schedule__c, TMS__Daily_hours_threshold__c,
						TMS__Task_data_type__c, TMS__Enable_Time_Tracking__c,
						Name, TMS__Overtime_Billable__c,
						TMS__Project__c, TMS__Project_Resource__r.TMS__Contact__c,
						(
							SELECT TMS__Overtime_overflow_task__c FROM TMS__OvertimeTasks__r),
						(
							SELECT TMS__Date__c, TMS__Time_Spent__c, TMS__Time_Spent_Time_Format__c,
									TMS__Start_Time__c, TMS__End_Time__c, TMS__Break__c,
									TMS__Status__c, TMS__Case__c, TMS__Timesheet__c,
									TMS__Week_Management__c
							FROM TMS__Time__r
							WHERE TMS__Date__c >= :wObject.TMS__Start_Date__c
								AND TMS__Date__c <= :wObject.TMS__End_Date__c)
					FROM TMS__Task__c
					WHERE Id IN :taskIdSet]);
		//}

		/* Added by Aliaksandr Satskou, 09/17/2014 (case # 00031941) */
		//if (tmsMap == null) {
			List<TMS__Timesheet__c> tmsList = [
					SELECT TMS__Candidate__c
					FROM TMS__Timesheet__c
					WHERE TMS__Week_Management__c = :wObject.Id];


			tmsMap = new Map<Id, TMS__Timesheet__c>();

			for (TMS__Timesheet__c tms : tmsList) {
				if (tms.TMS__Candidate__c != null) {
					tmsMap.put(tms.TMS__Candidate__c, tms);
				}
			}
		//}
	}


	/**
	* @name getOrderedIdList
	* @author Aliaksandr Satskou
	* @data 05/22/2013
	* @description Sorting Task objects.
	* @return List of Id of Task object
	*/
	private static List<Id> getOrderedIdList() {
		List<Id> taskIdList = new List<Id>();

		for (Id taskId : tMap.keySet()) {
			TMS__Task__c task = tMap.get(taskId);

			if (task.TMS__OvertimeTasks__r.size() != 0) {
				continue;
			} else {
				taskIdList.add(taskId);

				Id otId = task.TMS__Overtime_overflow_task__c;
				TMS__Task__c nextObject = (otId != null) ? tMap.get(otId) : null;

				while (nextObject != null) {
					taskIdList.add(nextObject.Id);

					Id v_otId = nextObject.TMS__Overtime_overflow_task__c;
					nextObject = (v_otId != null) ? tMap.get(v_otId) : null;
				}
			}
		}
		
		logger.log('getOrderedIdList', 'taskIdList', taskIdList);

		return taskIdList;
	}


	/**
	* @name calculate
	* @author Aliaksandr Satskou
	* @data 05/22/2013
	* @description Calculating regular and overtime hours.
	* @param entity CalculationEntity for Regular Task
	* @param otEntity CalculationEntity for Overtime Task
	* @return void
	*/
	private static void calculate(CalculationEntity entity, CalculationEntity otEntity) {
		logger.log('calculate');
		
		Decimal totalRegular = 0;
		Decimal totalOvertime = 0;

		for (Integer i = 0; i < 7; i++) {
			if ((entity.otScheduler == SCHEDULER_DAILY) ||
					(entity.otScheduler == SCHEDULER_DAILY_AND_WEEKLY)) {

				System.debug(LoggingLevel.ERROR, ':::::b1[' + i + ']=' + entity.dayList[i].daySpent);
				System.debug(LoggingLevel.ERROR, ':::::b2[' + i + ']=' + otEntity.dayList[i].daySpent);

				if (entity.dayList[i].daySpent > entity.otDailyTreshold) {
					otEntity.dayList[i].daySpent += entity.dayList[i].daySpent - entity.otDailyTreshold;
					entity.dayList[i].daySpent = entity.otDailyTreshold;
				}

				if (entity.dayList[i].daySpent < entity.otDailyTreshold) {
					Decimal d = entity.otDailyTreshold - entity.dayList[i].daySpent;

					if (otEntity.dayList[i].daySpent > 0) {
						if (otEntity.dayList[i].daySpent >= d) {
							entity.dayList[i].daySpent += d;
							otEntity.dayList[i].daySpent -= d;
						}

						if (otEntity.dayList[i].daySpent < d) {
							entity.dayList[i].daySpent += otEntity.dayList[i].daySpent;
							otEntity.dayList[i].daySpent = 0;
						}
					}
				}
				System.debug(LoggingLevel.ERROR, ':::::a1[' + i + ']=' + entity.dayList[i].daySpent);
				System.debug(LoggingLevel.ERROR, ':::::a2[' + i + ']=' + otEntity.dayList[i].daySpent);
			}


			Decimal dayOvertime = 0;

			if ((entity.otScheduler == SCHEDULER_WEEKLY) ||
					(entity.otScheduler == SCHEDULER_DAILY_AND_WEEKLY)) {

				logger.log('calculate', 'entity.dayList[i].daySpent', entity.dayList[i].daySpent);

				totalRegular += entity.dayList[i].daySpent;

				logger.log('calculate', 'totalRegular', totalRegular);
				logger.log('calculate', 'entity.otTreshold', entity.otTreshold);
				logger.log('calculate', 'totalOvertime', totalOvertime);

				if (totalRegular > entity.otTreshold) {
					dayOvertime = totalRegular - entity.otTreshold - totalOvertime;

					logger.log('calculate', 'dayOvertime', dayOvertime);

					totalOvertime += dayOvertime;

					logger.log('calculate', 'totalOvertime', totalOvertime);

					if (dayOvertime > 0) {
						entity.dayList[i].daySpent -= dayOvertime;
						otEntity.dayList[i].daySpent += dayOvertime;
					}
				}
			}
		}


		if (((entity.otScheduler == SCHEDULER_WEEKLY) 
						|| (entity.otScheduler == SCHEDULER_DAILY_AND_WEEKLY))
				&& (totalRegular < entity.otTreshold)) {
			Decimal d = entity.otTreshold - totalRegular;

			logger.log('calculate', 'd', d);

			for (Integer i = 6; i >= 0; i--) {

				logger.log('calculate', 'i', i);

				if (otEntity.dayList[i].daySpent > 0) {

					logger.log('calculate', 'otEntity.dayList[i].daySpent', otEntity.dayList[i].daySpent);

					if (entity.otScheduler == SCHEDULER_DAILY_AND_WEEKLY) {
						if (d > 0) {
							Decimal allow = entity.otDailyTreshold - entity.dayList[i].daySpent;

							if (otEntity.dayList[i].daySpent >= allow) {
								entity.dayList[i].daySpent += allow;
								otEntity.dayList[i].daySpent -= allow;

								d -= allow;
							}

							if (otEntity.dayList[i].daySpent < allow) {
								entity.dayList[i].daySpent += otEntity.dayList[i].daySpent;
								otEntity.dayList[i].daySpent = 0;

								d -= otEntity.dayList[i].daySpent;
							}
						}
					} else {
						logger.log('calculate', 'd2', d);
						logger.log('calculate', 'otEntity.dayList[i].daySpent 2', otEntity.dayList[i].daySpent);

						if (otEntity.dayList[i].daySpent >= d) {
							entity.dayList[i].daySpent += d;
							otEntity.dayList[i].daySpent -= d;

							logger.log('calculate', 'entity.dayList[i].daySpent 4', entity.dayList[i].daySpent);
							logger.log('calculate', 'otEntity.dayList[i].daySpent 4', otEntity.dayList[i].daySpent);

							break;
						}

						if (otEntity.dayList[i].daySpent < d) {
							entity.dayList[i].daySpent += otEntity.dayList[i].daySpent;

							d -= otEntity.dayList[i].daySpent;

							otEntity.dayList[i].daySpent = 0;

							logger.log('calculate', 'entity.dayList[i].daySpent 3', entity.dayList[i].daySpent);
							logger.log('calculate', 'otEntity.dayList[i].daySpent 3', otEntity.dayList[i].daySpent);
							logger.log('calculate', 'd3', d);
						}
					}
				}
			}
		}
	}


	/**
	* @name convertTimeToNumber
	* @author Aliaksandr Satskou
	* @data 05/22/2013
	* @description Converting time to number.
	* @param p_time time in hh:mm format
	* @return Decimal
	*/
	public static Decimal convertTimeToNumber(Object p_time) {
		logger.log('convertTimeToNumber', 'p_time', p_time);

		if (p_time == null) {
			return null;
		}

		String v_time = String.valueOf(p_time);

		if (v_time.indexOf('.') != -1) {
			return Decimal.valueOf(v_time);
		} else if (v_time.indexOf(':') != -1) {
			Integer v_colonIndex = v_time.indexOf(':');

			System.debug(LoggingLevel.ERROR, ':::::v_time=' + v_time);
			System.debug(LoggingLevel.ERROR, ':::::v_colonIndex=' + v_colonIndex);

			Decimal v_hours = Decimal.valueOf(v_time.substring(0, v_colonIndex));
			Decimal v_minutes = Decimal.valueOf(v_time.substring(v_colonIndex + 1));

			logger.log('convertTimeToNumber', 'v_time', v_time);
			logger.log('convertTimeToNumber', 'v_hours', v_hours);
			logger.log('convertTimeToNumber', 'v_minutes', v_minutes);
			logger.log('convertTimeToNumber', 'v_hours + v_minutes.divide(60, 2)', v_hours + v_minutes.divide(60, 2));

			return v_hours + v_minutes.divide(60, 2);
		}

		return Decimal.valueOf(v_time);
	}


	/**
	* @name convertNumberToTime
	* @author Aliaksandr Satskou
	* @data 05/22/2013
	* @description Converting number to time.
	* @param p_time time in number format
	* @return String
	*/
	public static String convertNumberToTime(Decimal p_time) {
		if (p_time == null) {
			return null;
		}

		String v_time = String.valueOf(p_time);

		logger.log('convertNumberToTime', 'v_time', v_time);

		Integer v_dotIndex = v_time.indexOf('.');

		if (v_dotIndex != -1) {
			String v_hours = v_time.substring(0, v_dotIndex);
			Decimal v_minutes = Decimal.valueOf(v_time.substring(v_dotIndex + 1));

			v_minutes = Math.round((v_minutes * 60).divide(100, 2));

			logger.log('convertNumberToTime', 'v_hours', v_hours);
			logger.log('convertNumberToTime', 'v_minutes', v_minutes);

			return ((Decimal.valueOf(v_hours) <= 9)
					? '0' + v_hours
					: v_hours) + ':' + ((v_minutes <= 9)
							? '0' + String.valueOf(v_minutes)
							: String.valueOf(v_minutes));
		}

		return String.valueOf(p_time);
	}

}