/**
* @description Helper class for Grouping fields based on various criteria like by Id, etc.
*/
public without sharing class GroupByHelper {
	private static Logger logger = new Logger('GroupByHelper');

	/**
* @name groupByField
* @param recordsToGroup List<sObject>
* @param keyField String
* @return Map<Object, List<sObject>>
*/
	// Usage: Map<Object, List<TMS__Time__c>> var = (Map<Object, List<TMS__Time__c>>)GroupByHelper.groupByField(times, 'TMS__Case__c');
	public static Map<Object, List<sObject>> groupByField(
			List<sObject> recordsToGroup, String keyField) {

		Map<Object, List<sObject>> keyToRecordsMap = new Map<Object, List<sObject>>();

		if (recordsToGroup != null) {
			for (sObject record : recordsToGroup) {
				Object key = getValue(record, keyField);

				logger.log('groupByField', 'key', key);

				List<sObject> recordList = keyToRecordsMap.get(key);
				if (recordList == null) {
					recordList = new List<sObject>();
					keyToRecordsMap.put(key, recordList);
				}

				recordList.add(record);
			}
		}

		return keyToRecordsMap;
	}
	
	// Usage: Map<List<Object>, List<sObject>> keysToRecords = GroupByHelper.groupByFields(times, new List<String> { 'Project__c', 'Case__c' });
	public static Map<List<Object>, List<sObject>> groupByFields(
			List<sObject> recordsToGroup, List<String> keyFields) {


		Map<List<Object>, List<sObject>> keysToRecordsMap = new Map<List<Object>, List<sObject>>();

		if (recordsToGroup != null) {
			for (sObject record : recordsToGroup) {
				List<Object> keys = new List<Object>();

				for (String keyField : keyFields) {
					keys.add(MetadataUtilNew.getValue(record, keyField));
				}


				List<sObject> recordList = keysToRecordsMap.get(keys);
				if (recordList == null) {
					recordList = new List<sObject>();
					keysToRecordsMap.put(keys, recordList);
				}

				recordList.add(record);
			}
		}

		return keysToRecordsMap;
	}

	/**
* @name groupByFieldKeyId
* @param recordsToGroup List<sObject>
* @param keyField String
* @return Map<Id, List<sObject>>
*/
	public static Map<Id, List<sObject>> groupByFieldKeyId(
			List<sObject> recordsToGroup, String keyField) {

		Map<Id, List<sObject>> mapKeyId = new Map<Id, List<sObject>>();

		Map<Object, List<sObject>> mapKeyObject = groupByField(recordsToGroup, keyField);
		for (Object keyObj : mapKeyObject.keySet()) {
			Id keyId = (Id)keyObj;
			mapKeyId.put(keyId, mapKeyObject.get(keyObj));
		}

		return mapKeyId;
	}

	/**
* @name groupByFieldReturnSingle
* @param recordsToGroup List<sObject>
* @param keyField String
* @return Map<Object, sObject>
*/
	public static Map<Object, sObject> groupByFieldReturnSingle(
			List<sObject> recordsToGroup, String keyField) {

		Map<Object, sObject> result = new Map<Object, sObject>();

		Map<Object, List<sObject>> mapKeyObject = groupByField(recordsToGroup, keyField);
		for (Object keyObj : mapKeyObject.keySet()) {
			result.put(keyObj, mapKeyObject.get(keyObj)[0]);
		}

		return result;
	}

/**
* @name groupByIdToObject
* @param recordsToGroup List<sObject>
* @param keyField String
* @return Map<Id, sObject>
*/
	public static Map<Id, sObject> groupByIdToObject(
			List<sObject> recordsToGroup, String keyField) {

		Map<Id, sObject> result = new Map<Id, sObject>();

		Map<Object, sObject> mapKeyObject = groupByFieldReturnSingle(recordsToGroup, keyField);
		for (Object keyObj : mapKeyObject.keySet()) {
			result.put((Id)keyObj, mapKeyObject.get(keyObj));
		}

		return result;
	}
	
	public static Set<Object> getFieldValues(List<sObject> records, String keyField) {
		Set<Object> fieldValues = new Set<Object>();

		for (sObject record : records) {
			fieldValues.add(getValue(record, keyField));
		}

		return fieldValues;
	}
	
	public static List<Object> getFieldValuesList(List<sObject> records, String keyField) {
		List<Object> fieldValues = new List<Object>();

		for (sObject record : records) {
			fieldValues.add(getValue(record, keyField));
		}

		return fieldValues;
	}

/**
* @name getFieldValuesIds
* @description Gets Set of field value ID.
* @param records List<sObject>
* @param keyField String
* @return Set<Id>
*/
	public static Set<Id> getFieldValuesIds(List<sObject> records, String keyField) {
		Set<Id> fieldValues = new Set<Id>();

		for (sObject record : records) {
			fieldValues.add((Id)getValue(record, keyField));
		}

		return fieldValues;
	}

	/**
* @name getFieldValuesStrings
* @description Gets Set of field value String.
* @param records List<sObject>
* @param keyField String
* @return Set<String>
*/
	public static List<String> getFieldValuesStrings(List<sObject> records, String keyField) {
		List<String> fieldValues = new List<String>();

		for (sObject record : records) {
			fieldValues.add((String)getValue(record, keyField));
		}

		return fieldValues;
	}
	
	/**
	* @name getFieldValuesSetStrings
	* @description Gets Set of field value String.
	* @param records List<sObject>
	* @param keyField String
	* @return Set<String>
	*/
	public static Set<String> getFieldValuesSetStrings(List<sObject> records, String keyField) {
		Set<String> fieldValues = new Set<String>();

		for (sObject record : records) {
			fieldValues.add((String)getValue(record, keyField));
		}

		return fieldValues;
	}
	
	public static List<Date> getFieldValuesDates(List<sObject> records, String keyField) {
		List<Date> fieldValues = new List<Date>();

		for (sObject record : records) {
			fieldValues.add((Date)getValue(record, keyField));
		}

		return fieldValues;
	}
	
	public static List<String> removeBlankStrings(List<String> withBlank) {
		List<String> result = new List<String>();
		for (String record : withBlank) {
			if (String.isBlank(record)) {
				continue;
			}
			
			result.add(record);
		}
		
		return result;
	}

	private static Object getValue(sObject record, String keyField) {
		Object value;
		
		String[] keyLevels = keyField.split('\\.');
		
		for (Integer i = 0; i < keyLevels.size(); i++) {
			if (record == null) {
				return null;
			}
			
			String keyLevel = keyLevels[i];
			
			if (i < keyLevels.size() - 1) {
				record = (sObject)record.getSObject(keyLevel);
			} else {
				value = record.get(keyLevel);
			}
		}
		
		return value;
	}
}