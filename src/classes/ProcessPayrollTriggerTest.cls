@isTest
private class ProcessPayrollTriggerTest {

	private static List<TMS__Process_Payroll__c> processPayrollList;

	/**
	 Method

	 @name initData
	 @author Aliaksandr Satskou
	 @data 11/08/2012
	 @description Initialize all needed data.
	 @return void.
	*/
	private static void initData() {
		TMS__TimesheetCustomSettingsHierarchy__c v_timeSheetCustomSettingsHierarchy =
				new TMS__TimesheetCustomSettingsHierarchy__c();
		v_timeSheetCustomSettingsHierarchy.TMS__Payroll_send_status__c = 'Sent to Payroll';
		insert v_timeSheetCustomSettingsHierarchy;

		processPayrollList = new List<TMS__Process_Payroll__c> {
			new TMS__Process_Payroll__c(
				TMS__Start_Date__c = Date.today(),
				TMS__End_Date__c = Date.today(),
				TMS__Status__c = 'Sent to Payroll'
			)
		};
		insert processPayrollList;
	}

	/**
	 Method

	 @name testProcessPayrollTrigger
	 @author Aliaksandr Satskou
	 @data 11/08/2012
	 @description Testing ProcessPayrollTrigger trigger.
	 @return void.
	*/
	static testMethod void testProcessPayrollTrigger() {
		initData();

		try {
			delete processPayrollList[0];
			System.assert(false);
		}
		catch (DMLException v_dmlException) { }
	}
}