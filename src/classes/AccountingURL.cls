/**
* @author Aliaksandr Satskou
* @date 06/06/2013
* @description Updating Project Resource object with Timesheet link.
*/
public with sharing class AccountingURL {

	private static Map<Id, TMS__Time__c> timeMap;
	private static Map<Id, TMS__Week_Management__c> weekMap;
	private static Map<Id, Set<TMS__Project_Resource__c>> accountingIdResourceSetMap;
	
	private static String siteURL = (TMS__TimesheetCustomSettingsHierarchy__c.getInstance() != null)
			? TMS__TimesheetCustomSettingsHierarchy__c.getInstance().TMS__Site_URL__c
			: null;
	
	
	/**
	* @name initData
	* @author Aliaksandr Satskou
	* @date 06/06/2013
	* @description Init all needed data.
	* @param wId Id of Week Management
	* @return void
	*/
	private static void initData(Id wId) {
		if (weekMap == null) {
			weekMap = new Map<Id, TMS__Week_Management__c>([
					SELECT TMS__Week__c
					FROM TMS__Week_Management__c]);
		}
		
		if (timeMap == null) {
			TMS__TimesheetCustomSettingsHierarchy__c setting = 
					TMS__TimesheetCustomSettingsHierarchy__c.getInstance();
			
			String accountingManagerStatuses = (setting != null)
					? setting.TMS__Statuses_for_Accounting_Manager__c
					: null;
			
			List<String> statusList = (accountingManagerStatuses != null)
					? accountingManagerStatuses.split(';')
					: new List<String>();
					
				
			timeMap = new Map<Id, TMS__Time__c>([
					SELECT Task__r.Project__r.Accounting_Contact__c,
						Task__r.Project_Resource__r.Contact__c,
						Task__r.Project_Resource__r.Contact__r.Name,
						Task__r.Project_Resource__r.Project__r.Name,
						Task__r.Project_Resource__r.TMS__View_Timesheet__c,
						Task__r.Project_Resource__r.Project__r.Internal_Approver__r.Name
					FROM TMS__Time__c
					WHERE TMS__Week_Management__c = :wId 
						AND TMS__Status__c IN :statusList
						AND Task__r.IsBillable__c = 'true'
						AND Task__r.Project__r.Accounting_Contact__c != null]);
			
			
			accountingIdResourceSetMap = new Map<Id, Set<TMS__Project_Resource__c>>();
			
			for (Id timeId : timeMap.keySet()) {
				TMS__Time__c v_time = timeMap.get(timeId);
				
				Set<TMS__Project_Resource__c> resourceSet = 
						accountingIdResourceSetMap.get(v_time.Task__r.Project__r.Accounting_Contact__c);
				
				if (resourceSet != null) {
					resourceSet.add(v_time.Task__r.Project_Resource__r);
				} else {
					accountingIdResourceSetMap.put(v_time.Task__r.Project__r.Accounting_Contact__c, 
							new Set<TMS__Project_Resource__c> { v_time.Task__r.Project_Resource__r });
				}
			}
		}
	}
	
	
	/**
	* @name generateSession
	* @author Aliaksandr Satskou
	* @date 06/06/2013
	* @description Generating Session object for resources.
	* @param recepientSet Set of Id of Contact objects
	* @return Map of Contact objects
	*/
	private static Map<Id, Contact> generateSession(Set<Id> recepientSet) {
		Map<Id, Contact> recepientWithoutSessionMap = new Map<Id, Contact>([
				SELECT Id
				FROM Contact
				WHERE Id IN :recepientSet 
				  AND Id NOT IN (
				  		SELECT FCMS__Session_For__c
				  		FROM FCMS__Session__c
				  		WHERE FCMS__Session_For__c IN :recepientSet)]);
		
		
		List<FCMS__Session__c> sessionList = new List<FCMS__Session__c>();
		
		for (Id contactId : recepientWithoutSessionMap.keySet()) {
			sessionList.add(new FCMS__Session__c(
					FCMS__Session_For__c = contactId,
					FCMS__SessionId__c = String.valueOf(Math.random()).substring(2, 16))
			);
		}
		
		insert sessionList;
		
		
		Map<Id, Contact> recepientWithSessionMap = new Map<Id, Contact>([
				SELECT Id,
					(SELECT FCMS__Session_For__c, FCMS__SessionId__c
					 FROM FCMS__Sessions__r)
				FROM Contact
				WHERE Id IN :recepientSet]);
		
		
		Map<Id, Contact> recepientSessionMap = new Map<Id, Contact>();
		
		for (Id contactId : recepientWithSessionMap.keySet()) {
			recepientSessionMap.put(contactId, recepientWithSessionMap.get(contactId));
		}
		
		return recepientSessionMap;
	}
	
	
	/**
	* @name getUpdatedResource
	* @author Aliaksandr Satskou
	* @date 06/06/2013
	* @description Updating Project Resource object.
	* @param wId Id of Week Management object
	* @param resourceId Id of Contact object
	* @return Updated List of Project Resource objects
	*/
	public static List<TMS__Project_Resource__c> getUpdatedResource(Id wId, Id resourceId) {
		initData(wId);
		
		Map<Id, Contact> recepientMap = generateSession(accountingIdResourceSetMap.keySet());
		List<TMS__Project_Resource__c> projectResourceList = new List<TMS__Project_Resource__c>();
		
		for (Id recepientId : accountingIdResourceSetMap.keySet()) {
			Set<TMS__Project_Resource__c> resourceSet = accountingIdResourceSetMap.get(recepientId);
			
			for (TMS__Project_Resource__c resource : resourceSet) {
				if (resource.Contact__c == resourceId) {
					resource.TMS__View_Timesheet__c = siteURL + 'TMS__TimesheetApproval?sessionId=' + 
						recepientMap.get(recepientId).FCMS__Sessions__r[0].FCMS__SessionId__c +
						'&approval=accounting&week=' + EncodingUtil.urlEncode(
						weekMap.get(wId).TMS__Week__c, 'UTF-8') + '&resourceId=' + resource.Id;
					
					projectResourceList.add(resource);
				}
			}
		}
		
		return projectResourceList;
	}
}