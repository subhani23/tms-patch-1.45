/**
* @author Aliaksandr Satskou
* @date 09/26/2014
* @description Test for UpdateTimesheetsBatch class.
*/
@isTest
public with sharing class UpdateTimesheetsBatchTest {

	private static List<TMS__Project__c> projectList;
	private static List<TMS__Timesheet__c> timesheetList;

	private static TMS__TimesheetCustomSettings__c tmsSetting;


	/**
	* @author Aliaksandr Satskou
	* @date 09/26/2014
	* @name initData
	* @description Init data.
	* @return void
	*/
	private static void initData() {
		projectList = new List<TMS__Project__c> {
			new TMS__Project__c(
				Name = 'Project #1'
			),
			new TMS__Project__c(
				Name = 'Project #2'
			)
		};
		insert projectList;


		timesheetList = new List<TMS__Timesheet__c> {
			new TMS__Timesheet__c(
				TMS__Project__c = projectList[0].Id
			),
			new TMS__Timesheet__c(
				TMS__Project__c = projectList[1].Id
			)
		};
		insert timesheetList;
	}


	/**
	* @author Aliaksandr Satskou
	* @date 09/26/2014
	* @name testUpdateTimesheetsBatch
	* @description Testing UpdateTimesheetsBatch class.
	* @return void
	*/
	private static testMethod void testUpdateTimesheetsBatch() {
		initData();

		Test.startTest();

			// Can't execute Batch because AVTRRT__Placement__c doesn't exist.
			//UpdateTimesheetsBatch updateTimesheetsBatch = new UpdateTimesheetsBatch(timesheetList);
			//Database.executeBatch(updateTimesheetsBatch, 2);

		Test.stopTest();
	}
}