/**
* @author Aliaksandr Satskou
* @date 10/17/2013
* @description Helper class to work with Timesheet object.
*/
public without sharing class TimesheetHelper {

	private Id candidateId;
	private Id weekId;
	private Id resourceId;


	public Boolean isFound;
	public Boolean isCreated;
	private TMS__Timesheet__c timesheet;


	/**
	* @author Aliaksandr Satskou
	* @name TimesheetHelper
	* @date 10/17/2013
	* @description Getting Timesheet object, init class data.
	* @param candidateId Id of Contact
	* @param weekId Id of Week Management
	*/
	public TimesheetHelper(Id candidateId, Id weekId) {
		this.candidateId = candidateId;
		this.weekId = weekId;


		if ((candidateId != null) && (weekId != null)) {
			List<TMS__Timesheet__c> timesheetList = [
					SELECT TMS__Candidate__c, TMS__Week_Management__c
					FROM TMS__Timesheet__c
					WHERE TMS__Candidate__c = :candidateId
					AND TMS__Week_Management__c = :weekId];


			if (timesheetList.size() > 0) {
				timesheet = timesheetList[0];

				isFound = true;
			} else { isFound = false; }

			isCreated = false;
		} else {
			isFound = false;
			isCreated = false;
		}


		if (candidateId != null) {
			List<TMS__Project_Resource__c> resourceList = [
					SELECT Id
					FROM TMS__Project_Resource__c
					WHERE TMS__Contact__c = :candidateId];

			if (resourceList.size() > 0) {
				resourceId = resourceList[0].Id;
			}
		}
	}


	/**
	* @author Aliaksandr Satskou
	* @name isObjectEnabled
	* @date 10/17/2013
	* @description Checking is enabled Timesheet object.
	* @return Boolean
	*/
	public static Boolean isObjectEnabled() {
		Boolean isObjectEnabled = (Boolean)CustomSettingsUtility.getCustomSettingListTypeValue(
				null, 'TMS__Enable_Timesheet_Object__c');

		if (isObjectEnabled == null) { return false; }


		return isObjectEnabled;
	}


	/**
	* @author Aliaksandr Satskou
	* @name getId
	* @date 10/17/2013
	* @description Getting Id of Timesheet object.
	* @return Id
	*/
	public Id getId() {
		if (timesheet != null) {
			return timesheet.Id;
		}

		return null;
	}


	/**
	* @author Aliaksandr Satskou
	* @name create
	* @date 12/18/2013
	* @description Creating Timesheet object.
	* @return void
	*/
	public void create() {
		if (!isCreated) {
			reCreate();

			isCreated = true;
		}
	}


	/**
	* @author Aliaksandr Satskou
	* @name reCreate
	* @date 12/18/2013
	* @description Adding new Timesheet object.
	* @return void
	*/
	private void reCreate() {
		timesheet = new Timesheet__c(
			TMS__Candidate__c = candidateId,
			TMS__Week_Management__c = weekId,
			TMS__Project_Resource__c = resourceId
		);

		isCreated = true;
	}
	
	/**
	* @author Aliaksandr Satskou
	* @name setStatus
	* @date 12/18/2013
	* @description Setting status to Timesheet object.
	* @param tStatus New status
	* @return void
	*/
	public void setStatus(String tStatus) {
		timesheet.TMS__Status__c = tStatus;
	}
	
	/**
	* @author Aliaksandr Satskou
	* @name setStatus
	* @date 12/18/2013
	* @description Setting status to Timesheet object.
	* @param tStatus New status
	* @return void
	*/
	public void setTimeStatus(List<TMS__Time__c> timeRecords, String tStatus) {
		
		List<String> timeStatuses = 
				GroupByHelper.getFieldValuesStrings(timeRecords, 'TMS__Status__c');
				
		if (timeStatuses.size() > 1) {
			
			String timesheetMultipleStatus = CustomSettingsUtility.getTimesheetMultipleStatus();
			
			timesheet.TMS__Status__c = timesheetMultipleStatus != null 
										? timesheetMultipleStatus 
										: 'Partial';
		} else {
			timesheet.TMS__Status__c = tStatus;
		}
	}
	
	/**
	* @author Aliaksandr Satskou
	* @name setStatus
	* @date 12/18/2013
	* @description Setting status to Timesheet object.
	* @param tStatus New status
	* @return void
	*/
	public void setExpenseStatus(List<TMS__Expense__c> expenseRecords , String tStatus) {
		Set<String> expenseStatuses = 
				GroupByHelper.getFieldValuesSetStrings(expenseRecords, 'TMS__Status__c');
				
		if (expenseStatuses.size() > 1) {
			
			String expenseMultipleStatus = CustomSettingsUtility.getExpenseMultipleStatus();
			
			timesheet.TMS__Expense_Status__c = expenseMultipleStatus != null 
											? expenseMultipleStatus 
											: 'Partial';
		} else {
			timesheet.TMS__Expense_Status__c = tStatus;
		}
	}
	
	/**
	* @author Aliaksandr Satskou
	* @name save
	* @date 12/18/2013
	* @description Saving Timesheet to database.
	* @return void
	*/
	public void save() {
		upsert timesheet;
	}
}