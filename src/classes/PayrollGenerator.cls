/**
* @author Aliaksandr Satskou
* @date 09/12/2013
* @description Generating Payroll.
*/
global class PayrollGenerator {

	private static Map<String, TMS__Payroll_Settings__c> settingMap;
	private static Map<String, TMS__Payroll_Provider_Settings__c> providerMap;

	public static Map<String, Set<String>> objectNameFilenameSetMap;
	private static String payrollSendStatus;


	/**
	* @author Aliaksandr Satskou
	* @name initData
	* @date 09/12/2013
	* @description Init data.
	* @param provider Payroll Provider name
	* @return void
	*/
	public static void initData(String provider) {
		if (settingMap == null) {
			settingMap = TMS__Payroll_Settings__c.getAll();


			objectNameFilenameSetMap = new Map<String, Set<String>>();

			for (TMS__Payroll_Settings__c setting : settingMap.values()) {
				Set<String> filenameSet = objectNameFilenameSetMap.get(setting.TMS__Object_API_Name__c);

				if (filenameSet != null) {
					filenameSet.add(setting.TMS__File_Name__c);
				} else {
					objectNameFilenameSetMap.put(
							setting.TMS__Object_API_Name__c, new Set<String> { setting.TMS__File_Name__c });
				}
			}
		}

		if (providerMap == null) {
			providerMap = TMS__Payroll_Provider_Settings__c.getAll();
		}


		TMS__TimesheetCustomSettingsHierarchy__c timesheetSettings =
				TMS__TimesheetCustomSettingsHierarchy__c.getInstance();

		payrollSendStatus = (timesheetSettings != null)
				? timesheetSettings.TMS__Payroll_send_status__c
				: null;
	}


	/**
	* @author Aliaksandr Satskou
	* @name addWhiteSpaces
	* @date 09/12/2013
	* @description Adding whitespaces to string.
	* @param text Input text
	* @param num Count of whitespaces
	* @return String
	*/
	private static String addWhiteSpaces(String text, Integer num) {
		String txt = (text != null) ? text : '';

		for (Integer i = 0; i < num; i++) {
			txt = ' ' + txt;
		}

		return txt;
	}


	/**
	* @author Aliaksandr Satskou
	* @name getFieldnameCharsMap
	* @date 09/12/2013
	* @description Getting map of fieldname to characters.
	* @param provider Payroll Provider name
	* @param filename File name
	* @param objectName Object name
	* @return Map<String, String>
	*/
	private static Map<String, String> getFieldnameCharsMap(
			String provider, String filename, String objectName) {

		Map<String, String> fieldnameCharsMap = new Map<String, String>();

		for (TMS__Payroll_Settings__c setting : settingMap.values()) {
			if ((setting.TMS__Payroll_Provider_Name__c == provider) &&
					(setting.TMS__File_Name__c == filename) &&
					(setting.TMS__Object_API_Name__c == objectName)) {

				fieldnameCharsMap.put(setting.TMS__Field_API_Name__c, setting.TMS__Characters__c);
			}
		}

		return fieldnameCharsMap;
	}


	/**
	* @author Aliaksandr Satskou
	* @name getItemIdStr
	* @date 09/12/2013
	* @description Getting string of Id's, separated by comma.
	* @param itemIdList List of Id's
	* @return String
	*/
	private static String getItemIdStr(List<Id> itemIdList) {
		if (itemIdList != null) {
			String payrollItemIdStr = '(';

			for (Id itemId : itemIdList) {
				payrollItemIdStr += '\'' + itemId + '\', ';
			}
			payrollItemIdStr = payrollItemIdStr.substring(0, payrollItemIdStr.length() - 2);
			payrollItemIdStr += ')';

			return payrollItemIdStr;
		}

		return null;
	}


	/**
	* @author Aliaksandr Satskou
	* @name getFixedWidthPayrollItemList
	* @date 09/12/2013
	* @description Getting list of lines in fixed width format.
	* @param lineItemList List of Payroll Line Items
	* @param fieldnameCharsMap Map of fieldname to characters
	* @return List<String>
	*/
	private static List<String> getFixedWidthPayrollItemList(
			List<TMS__Payroll_Line_items__c> lineItemList, Map<String, String> fieldnameCharsMap) {

		List<String> payrollItemList = new List<String>();

		for (TMS__Payroll_Line_items__c lineItem : lineItemList) {
			String payrollItem = '';
			Map<Integer, String> indexPartialLineMap = new Map<Integer, String>();

			for (String fieldname : fieldnameCharsMap.keySet()) {
				String chars = fieldnameCharsMap.get(fieldname);

				Integer startIndex = (chars.indexOf('-') != -1)
						? Integer.valueOf(chars.substring(0, chars.indexOf('-')))
						: Integer.valueOf(chars);
				Integer endIndex = (chars.indexOf('-') != -1)
						? Integer.valueOf(chars.substring(chars.indexOf('-') + 1))
						: Integer.valueOf(chars);
				Integer allowSize = endIndex - startIndex + 1;


				String fieldvalue = String.valueOf(lineItem.get(fieldname));

				if ((fieldvalue != null) && (fieldvalue != '')) {
					if (fieldvalue.length() > allowSize) {
						fieldvalue = fieldvalue.substring(0, allowSize);
					} else {
						fieldvalue = addWhiteSpaces(
								fieldvalue, allowSize - fieldvalue.length());
					}
				} else {
					fieldvalue = addWhiteSpaces(fieldvalue, allowSize);
				}

				indexPartialLineMap.put(startIndex, fieldvalue);
			}


			List<Integer> sortedIndexList = new List<Integer>(indexPartialLineMap.keySet());
			sortedIndexList.sort();

			for (Integer i = 0; i < sortedIndexList.size(); i++) {
				Integer orderIndex = sortedIndexList[i];
				Integer prevOrderIndex = (i > 0) ? sortedIndexList[i - 1] : null;

				Integer prevEndIndex = (prevOrderIndex != null)
						? prevOrderIndex +
								indexPartialLineMap.get(prevOrderIndex).length() - 1
						: 0;

				if (orderIndex > (prevEndIndex + 1)) {
					payrollItem += addWhiteSpaces(null, orderIndex - (prevEndIndex + 1));
					payrollItem += indexPartialLineMap.get(orderIndex);
				} else {
					payrollItem += indexPartialLineMap.get(orderIndex);
				}
			}

			payrollItemList.add(payrollItem);
		}

		return payrollItemList;
	}


	/**
	* @author Aliaksandr Satskou
	* @name getOrderColumnMap
	* @date 06/02/2014
	* @description Getting map of order to column name.
	* @param provider Payroll Provider name
	* @param filename File name
	* @param objectName Object Name
	* @return Map<Integer, String>
	*/
	private static Map<Integer, String> getOrderColumnMap(
			String provider, String filename, String objectName) {

		Map<Integer, String> orderColumnMap = new Map<Integer, String>();

		/* Edited by Aliaksandr Satskou, 06/06/2014 (case #00028764) */
		for (TMS__Payroll_Settings__c setting : settingMap.values()) {
			if ((setting.TMS__Payroll_Provider_Name__c == provider) &&
					(setting.TMS__File_Name__c == filename) &&
					(setting.TMS__Object_API_Name__c == objectName) &&
					(setting.TMS__Line__c == '1')) {

				orderColumnMap.put(Integer.valueOf(setting.TMS__Field_Order__c), setting.TMS__Column_Name__c);
			}
		}

		return orderColumnMap;
	}


	/**
	* @author Aliaksandr Satskou
	* @name getFieldnameOrderMap
	* @date 09/16/2013
	* @description Getting map of order to fieldname.
	* @param provider Payroll Provider name
	* @param filename File name
	* @param objectName Object Name
	* @param lineNumber Line Number
	* @return Map<Integer, String>
	*/
	private static Map<Integer, String> getOrderFieldnameMap(
			String provider, String filename, String objectName, Integer lineNumber) {

		Map<Integer, String> orderFieldnameMap = new Map<Integer, String>();

		/* Edited by Aliaksandr Satskou, 06/06/2014 (case #00028764) */
		for (TMS__Payroll_Settings__c setting : settingMap.values()) {
			if ((setting.TMS__Payroll_Provider_Name__c == provider) &&
					(setting.TMS__File_Name__c == filename) &&
					(setting.TMS__Object_API_Name__c == objectName) &&
					(setting.TMS__Line__c == String.valueOf(lineNumber))) {

				if (setting.TMS__Field_Order__c != null) {
					orderFieldnameMap.put(
							Integer.valueOf(setting.TMS__Field_Order__c), setting.TMS__Field_API_Name__c);
				}
			}
		}


		return orderFieldnameMap;
	}


	/**
	* @author Aliaksandr Satskou
	* @name getOrderFormulaMap
	* @date 06/25/2014
	* @description Getting map of order to formula.
	* @param provider Payroll Provider name
	* @param filename File name
	* @param objectName Object Name
	* @param lineNumber Line Number
	* @return Map<Integer, String>
	*/
	private static Map<Integer, String> getOrderFormulaMap(
			String provider, String filename, String objectName, Integer lineNumber) {

		Map<Integer, String> orderFormulaMap = new Map<Integer, String>();

		for (TMS__Payroll_Settings__c setting : settingMap.values()) {
			if ((setting.TMS__Payroll_Provider_Name__c == provider) &&
					(setting.TMS__File_Name__c == filename) &&
					(setting.TMS__Object_API_Name__c == objectName) &&
					(setting.TMS__Line__c == String.valueOf(lineNumber))) {

				if (setting.TMS__Field_Order__c != null) {
					orderFormulaMap.put(
							Integer.valueOf(setting.TMS__Field_Order__c), setting.TMS__Formula__c);
				}
			}
		}


		return orderFormulaMap;
	}


	/**
	* @author Aliaksandr Satskou
	* @name getLineNumberSet
	* @date 06/06/2014
	* @description Getting set of line numbers.
	* @param provider Payroll Provider name
	* @param filename File name
	* @param objectName Object Name
	* @return Set<Integer>
	*/
	private static Set<Integer> getLineNumberSet(String provider, String filename, String objectName) {
		Set<Integer> lineNumberSet = new Set<Integer>();

		for (TMS__Payroll_Settings__c setting : settingMap.values()) {
			if ((setting.TMS__Payroll_Provider_Name__c == provider) &&
					(setting.TMS__File_Name__c == filename) &&
					(setting.TMS__Object_API_Name__c == objectName)) {

				lineNumberSet.add(Integer.valueOf(setting.TMS__Line__c));
			}
		}


		return lineNumberSet;
	}


	/**
	* @author Aliaksandr Satskou
	* @name getCSVPayrollFileContent
	* @date 06/06/2014
	* @description Getting CSV Payroll File Content.
	* @param provider Payroll Provider name
	* @param filename File name
	* @param objectName Object Name
	* @param dataList Data list
	* @return Set<Integer>
	*/
	public static String getCSVPayrollFileContent(
			String provider, String filename, String objectName, List<sObject> dataList) {

		/* Edited by Aliaksandr Satskou, 06/06/2014 (case #00028764) */
		List<String> payrollLineItemList = getCSVPayrollItemList(provider, dataList, filename, objectName);


		String fileContent = '';

		for (String payrollLineItem : payrollLineItemList) {
			fileContent += payrollLineItem + '\n';
		}


		return fileContent;
	}


	/**
	* @author Aliaksandr Satskou
	* @name getValue
	* @date 06/09/2014, edited 06/25/2014
	* @description Getting value.
	* @param nameOrValue API Name or Value
	* @param obj sObject
	* @param formula Formula
	* @return String
	*/
	private static String getValue(String nameOrValue, sObject obj, String formula) {
		if (nameOrValue.startsWith('"')) {
			String value = (nameOrValue.substring(1, nameOrValue.length() - 1));

			return applyFormula(value, formula);
		} else {
			Object objValue = obj.get(nameOrValue);

			if (objValue == null) {
				return null;
			} else {
				return applyFormula(objValue, formula);
			}
		}
	}


	/**
	* @author Aliaksandr Satskou
	* @name applyFormula
	* @date 06/25/2014
	* @description Applying formula for value.
	* @param objValue Value
	* @param formulas Formula(s)
	* @return String
	*/
	private static String applyFormula(Object objValue, String formulas) {
		if (formulas != null) {
			List<String> formulaList = formulas.split('::');
			String result = String.valueOf(objValue);

			for (String formula : formulaList) {
				if (formula.startsWith('ADDBEFORE')) {
					// ADDBEFORE("$")
					String what = formula.substringBetween('("', '")');

					result = what + result;
				}

				if (formula.startsWith('SUBSTRING')) {
					// SUBSTRING(0,1)
					String what = formula.substringBetween('(', ')');

					Integer commaInd = what.indexOf(',');
					Integer firstInd = Integer.valueOf(what.substring(0, commaInd));
					Integer secondInd = Integer.valueOf(what.substring(commaInd + 1));

					if (secondInd <= result.length()) {
						result = result.substring(firstInd, secondInd);
					}
				}

				if (formula.startsWith('FORMAT')) {
					// FORMAT("000-000-0000")
					String fmt = formula.substringBetween('("', '")');

					String clearStr = getDigitAndAlphaStr(result);
					String fmtResult = '';
					Integer ind = 0;

					for (Integer i = 0; i < fmt.length(); i++) {
						String fch = getCh(fmt, i);

						if (clearStr.length() > ind) {
							String rch = getCh(clearStr, ind);

							if (fch == '0') {
								fmtResult += rch;
								ind++;
							} else {
								fmtResult += fch;
							}
						} else {
							if (fch != '0') {
								fmtResult += fch;
							}
						}
					}

					/*
					if (clearStr.length() > ind) {
						fmtResult += clearStr.substring(ind);
					}
					*/

					result = fmtResult;
				}

				if (formula.startsWith('UPPER')) {
					result = result.toUpperCase();
				}

				if (formula.startsWith('DATEFMT')) {
					// DATEFMT("M/dd/yy")
					String fmt = formula.substringBetween('("', '")');

					DateTime dt = (DateTime)objValue;
					DateTime gmtDt = DateTime.newInstance(dt.year(), dt.month(), dt.dayGmt());

					result = gmtDt.format(fmt);
				}
			}


			return result;
		}


		return String.valueOf(objValue);
	}


	/**
	* @author Aliaksandr Satskou
	* @name getCh
	* @date 06/25/2014
	* @description Getting character of string by index.
	* @param value Value
	* @param ind Index
	* @return String
	*/
	private static String getCh(String value, Integer ind) {
		return value.substring(ind, ind + 1);
	}


	/**
	* @author Aliaksandr Satskou
	* @name getDigitAndAlphaStr
	* @date 06/25/2014
	* @description Remove not-digit and not-alpha characters.
	* @param value Value
	* @return String
	*/
	private static String getDigitAndAlphaStr(String value) {
		String result = '';

		for (Integer i = 0; i < value.length(); i++) {
			String ch = getCh(value, i);

			if (ch.isAlphanumeric()) {
				result += ch;
			}
		}

		return result;
	}


	/**
	* @author Aliaksandr Satskou
	* @name getCSVPayrollItemList
	* @date 09/12/2013
	* @description Getting list of lines in CSV format.
	* @param provider Payroll Provider
	* @param lineItemList List of records
	* @param filename File name
	* @param objectName Object name
	* @return List<String>
	*/
	private static List<String> getCSVPayrollItemList(String provider, List<sObject> lineItemList,
			String filename, String objectName) {

		/* Added by Aliaksandr Satskou, 06/06/2014 (case #00028764) */
		Map<Integer, String> orderColumnMap = getOrderColumnMap(provider, filename, objectName);
		Set<Integer> lineNumberSet = getLineNumberSet(provider, filename, objectName);


		String delimiter = providerMap.get(provider).TMS__Field_Delimiter__c;
		delimiter = ((delimiter != null) ? delimiter : ',');

		Boolean isIncludeHeader = providerMap.get(provider).TMS__Include_Column_Header_in_Payroll_File__c;


		/* Edited by Aliaksandr Satskou, 06/06/2014 (case #00028764) */
		List<Integer> indexSortList = new List<Integer>(orderColumnMap.keySet());
		indexSortList.sort();


		List<String> payrollItemList = null;

		if (isIncludeHeader) {
			String header = '';

			for (Integer index : indexSortList) {
				/* Edited by Aliaksandr Satskou, 06/02/2014 (case #00028497) */
				header += orderColumnMap.get(index) + ((delimiter != null) ? delimiter : ',');
			}
			header = (header != '') ? header.substring(0, header.length() - 1) : '';


			/* Edited by Aliaksandr Satskou, 05/23/2014 (case #00028140) */
			if (header != '') {
				payrollItemList = new List<String> { header };
			} else {
				payrollItemList = new List<String>();
			}
		} else {
			payrollItemList = new List<String>();
		}


		for (sObject lineItem : lineItemList) {
			/* Added by Aliaksandr Satskou, 06/06/2014 (case #00028764) */
			for (Integer lineNumber : lineNumberSet) {
				Map<Integer, String> orderFieldnameMap = getOrderFieldnameMap(
						provider, filename, objectName, lineNumber);

				/* Added by Aliaksandr Satskou, 06/25/2014 (case #00022233) */
				Map<Integer, String> orderFormulaMap = getOrderFormulaMap(
						provider, filename, objectName, lineNumber);


				String payrollItem = '';

				/* Edited by Aliaksandr Satskou, 06/02/2014 (case #00028497) */
				/* Edited by Aliaksandr Satskou, 06/09/2014 (case #00028764) */
				/* Edited by Aliaksandr Satskou, 06/25/2014 (case #00022233) */
				for (Integer index : indexSortList) {
					String nameOrValue = orderFieldnameMap.get(index);
					String formula = orderFormulaMap.get(index);


					if (nameOrValue != null) {
						Integer commaInd = nameOrValue.indexOf(',');

						if (commaInd != -1) {
							String firstPart = nameOrValue.substring(0, commaInd);
							String secondPart = nameOrValue.substring(commaInd + 1);

							/* Edited by Aliaksandr Satskou, 06/25/2014 (case #00022233) */
							String objValue = getValue(firstPart, lineItem, formula);

							if (objValue == null) {
								/* Edited by Aliaksandr Satskou, 06/25/2014 (case #00022233) */
								objValue = getValue(secondPart, lineItem, formula);
							}

							payrollItem += ((objValue == null) ? '' : objValue) + delimiter;
						} else {
							/* Edited by Aliaksandr Satskou, 06/25/2014 (case #00022233) */
							String objValue = getValue(nameOrValue, lineItem, formula);

							payrollItem += ((objValue == null) ? '' : objValue) + delimiter;
						}
					} else {
						payrollItem += delimiter;
					}
				}

				payrollItem = (payrollItem != null)
						? payrollItem.substring(0, payrollItem.length() - 1)
						: '';

				payrollItemList.add(payrollItem);
			}
		}

		return payrollItemList;
	}


	/**
	* @author Aliaksandr Satskou
	* @name getPayrollItemList
	* @date 09/12/2013
	* @description Getting list of lines for send the file.
	* @param payrollId Payroll Id
	* @param provider Payroll Provider name
	* @param filename File name
	* @param itemIdList List of Payroll Line Items Id's
	* @param orderBy Field and order of sorting
	* @return List<String>
	*/
	private static List<String> getPayrollItemList(
			String payrollId, String provider, String filename, List<Id> itemIdList, String orderBy) {

		Map<String, String> fieldnameCharsMap = getFieldnameCharsMap(
				provider, filename, 'TMS__Payroll_Line_items__c');


		String what = '';

		for (String fieldname : fieldnameCharsMap.keySet()) {
			if (fieldname.startsWith('"')) { continue; }

			what += fieldname + ', ';
		}
		what = (what != '') ? what.substring(0, what.length() - 2) : 'Id';


		String itemIdStr = getItemIdStr(itemIdList);

		String query = 'SELECT ' + what + ' FROM TMS__Payroll_Line_items__c WHERE TMS__Payroll__c = ' +
				'\'' + payrollId + '\' AND TMS__Payroll_File_Name__c = \'' + filename + '\'' +
				((itemIdStr != null)
						? ' AND Id IN ' + itemIdStr
						: '') +
				((orderBy != null)
						? ' ORDER BY ' + orderBy
						: '');


		List<TMS__Payroll_Line_items__c> lineItemList = DataBase.query(query);
		List<String> payrollItemList = new List<String>();

		if (providerMap.get(provider).TMS__Use_CSV_File_Format__c) {
			/* Edited by Aliaksandr Satskou, 06/06/2014 (case #00028764) */
			payrollItemList = getCSVPayrollItemList(
					provider, lineItemList, filename, 'TMS__Payroll_Line_items__c');
		} else {
			payrollItemList = getFixedWidthPayrollItemList(lineItemList, fieldnameCharsMap);
		}


		return payrollItemList;
	}


	/**
	* @author Aliaksandr Satskou
	* @name doSendPayroll
	* @date 09/12/2013
	* @description Sending Payroll.
	* @param payrollId Payroll Id
	* @return void
	*/
	WebService static void doSendPayroll(String payrollId) {
		doSendPayrollForSpecificLineItems(payrollId, null);
	}


	/**
	* @author Aliaksandr Satskou
	* @name doSendPayrollForSpecificLineItems
	* @date 09/12/2013
	* @description Sending Payroll for specific Payroll Line Items.
	* @param payrollId Payroll Id
	* @param itemIdList List of Payroll Line Items Id's
	* @return void
	*/
	WebService static void doSendPayrollForSpecificLineItems(String payrollId, List<String> itemIdList) {
		TMS__Process_Payroll__c payroll = [
				SELECT TMS__Status__c, TMS__Payroll_Provider_Name__c, TMS__Comments__c
				FROM TMS__Process_Payroll__c
				WHERE Id = :payrollId
				LIMIT 1];


		List<Attachment> payrollAttList = [
				SELECT Name, Body
				FROM Attachment
				WHERE ParentId = :payrollId];


		if (payroll != null) {
			String provider = payroll.TMS__Payroll_Provider_Name__c;

			if (provider != null) {
				initData(provider);


				if (!isValidPayrollSettings()) {
					payroll.TMS__Status__c = 'Error';
					payroll.TMS__Comments__c = Label.Payroll_Settings_specified_incorrect_Payroll_was_not_sent;

					update payroll;

					return;
				}


				String emailAddr = providerMap.get(provider).TMS__Email__c;
				String orderBy = providerMap.get(provider).TMS__Payroll_items_sort_by__c;

				if (emailAddr != null) {
					Map<String, String> filenameBodyMap = new Map<String, String>();

					for (String filename : objectNameFilenameSetMap.get('TMS__Payroll_Line_items__c')) {
						List<String> payrollItemList = getPayrollItemList(
								payrollId, provider, filename, itemIdList, orderBy);


						String payrollString = '';

						for (String payrollItem : payrollItemList) {
							payrollString += payrollItem + '\n';
						}

						if (payrollString != '') {
							filenameBodyMap.put(filename, payrollString);
						}
					}


					try {
						if (filenameBodyMap.size() != 0) {
							Messaging.sendEmail(new List<Messaging.SingleEmailMessage> {
									createEmailMessage(emailAddr, filenameBodyMap, payrollAttList) });


							payroll.TMS__Status__c = payrollSendStatus;
							payroll.TMS__Comments__c =
									System.Label.Payroll_successfully_sent_to + ' ' + emailAddr;
						} else {
							payroll.TMS__Status__c = 'Error';
							payroll.TMS__Comments__c =
									System.Label.No_data_or_incorrect_data_for_generate_Payroll;
						}
					} catch (Exception e) {
						payroll.TMS__Status__c = 'Error';
						payroll.TMS__Comments__c = e.getMessage();
					}
				} else {
					payroll.TMS__Status__c = 'Error';
							payroll.TMS__Comments__c =
									System.Label.Not_specified_email_address_for + ' ' + provider +
									' ' + System.Label.provider;
				}
			} else {
				payroll.TMS__Status__c = 'Error';
				payroll.TMS__Comments__c = System.Label.Provider_not_specified;
			}

			update payroll;
		}
	}


	/**
	* @author Aliaksandr Satskou
	* @name isValidPayrollSettings
	* @date 07/13/2015
	* @description Check Payroll Settings.
	* @return Boolean
	*/
	private static Boolean isValidPayrollSettings() {
		if ((settingMap != null) && (settingMap.size() > 0)) {
			for (TMS__Payroll_Settings__c setting : settingMap.values()) {
				if (setting.TMS__Payroll_Provider_Name__c == 'ADP') {
					if ((setting.TMS__Column_Name__c == null)
							|| (setting.TMS__Field_API_Name__c == null)
							|| (setting.TMS__Field_Order__c == null)
							|| (setting.TMS__File_Name__c == null)
							|| (setting.TMS__Line__c == null)
							|| (setting.TMS__Object_API_Name__c == null)) {

						return false;
					}
				}

				if (setting.TMS__Payroll_Provider_Name__c == 'Paychex') {
					if ((setting.TMS__Field_API_Name__c == null)
							|| (setting.TMS__Characters__c == null)
							|| (setting.TMS__File_Name__c == null)
							|| (setting.TMS__Object_API_Name__c == null)) {

						return false;
					}
				}
			}
		} else {
			return false;
		}


		return true;
	}


	/**
	* @author Aliaksandr Satskou
	* @name createEmailMessages
	* @date 09/12/2013
	* @description Creating email message.
	* @param emailAddr Email address
	* @param filenameBodyMap Map of filename to body of file
	* @return Messaging.SingleEmailMessage
	*/
	private static Messaging.SingleEmailMessage createEmailMessage(
			String emailAddr, Map<String, String> filenameBodyMap, List<Attachment> payrollAttList) {

		Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();

		email.setToAddresses(new List<String> { emailAddr });
		email.setSenderDisplayName(UserInfo.getFirstName() + ' ' + UserInfo.getLastName());
		email.setSubject('Payroll');
		email.setUseSignature(false);
		email.setPlainTextBody('');

		List<Messaging.EmailFileAttachment> attachmentList = new List<Messaging.EmailFileAttachment>();

		for (String attachmentName : filenameBodyMap.keySet()) {
			Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();

			attachment.setBody(Blob.valueOf(filenameBodyMap.get(attachmentName)));
			attachment.setContentType('text/plain');
			attachment.setFileName(attachmentName);

			attachmentList.add(attachment);
		}


		for (Attachment payrollAtt : payrollAttList) {
			Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();

			attachment.setBody(payrollAtt.Body);
			attachment.setContentType('text/plain');
			attachment.setFileName(payrollAtt.Name);

			attachmentList.add(attachment);
		}


		email.setFileAttachments(attachmentList);

		return email;
	}
}