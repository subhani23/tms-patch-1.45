/**
* @author Aliaksandr Satskou
* @date 06/21/2014
* @description Test class for PayrollNotify class.
*/
@isTest
private class PayrollNotifyTest {

	private static List<TMS__Process_Payroll__c> payrollList;
	private static List<Contact> employeeList;
	private static List<TMS__Week_Management__c> weekList;
	private static List<TMS__Time__c> timeList;


	/**
	* @author Aliaksandr Satskou
	* @date 06/21/2014
	* @name initData
	* @description Init data.
	* @return void
	*/
	private static void initData() {
		insert new TMS__Payroll_Provider_Settings__c(
			Name = 'ADP',
			TMS__Email__c = 'test@test.com'
		);


		insert new TMS__TimesheetCustomSettings__c(
			Name = 'Default',
			TMS__Payroll_Notify_Statuses__c = 'Completed;Sent to Payroll'
		);


		employeeList = new List<Contact> {
			new Contact(
				LastName = 'Employee #1'
			),
			new Contact(
				LastName = 'Employee #2'
			)
		};
		insert employeeList;


		payrollList = new List<TMS__Process_Payroll__c> {
			new TMS__Process_Payroll__c(
				TMS__Start_Date__c = Date.today(),
				TMS__End_Date__c = Date.today() + 6,
				TMS__Status__c = 'In Progress'
			),
			new TMS__Process_Payroll__c(
				TMS__Start_Date__c = Date.today() + 7,
				TMS__End_Date__c = Date.today() + 13,
				TMS__Status__c = 'Completed'
			),
			new TMS__Process_Payroll__c(
				TMS__Start_Date__c = Date.today() + 14,
				TMS__End_Date__c = Date.today() + 20,
				TMS__Status__c = 'Sent to Payroll'
			)
		};
		insert payrollList;


		weekList = new List<TMS__Week_Management__c> {
			new TMS__Week_Management__c(
				TMS__Start_Date__c = Date.today(),
				TMS__End_Date__c = Date.today() + 6,
				TMS__Active__c = true
			),
			new TMS__Week_Management__c(
				TMS__Start_Date__c = Date.today() + 7,
				TMS__End_Date__c = Date.today() + 13,
				TMS__Active__c = true
			),
			new TMS__Week_Management__c(
				TMS__Start_Date__c = Date.today() + 14,
				TMS__End_Date__c = Date.today() + 20,
				TMS__Active__c = true
			)
		};
		insert weekList;


		timeList = new List<TMS__Time__c> {
			new TMS__Time__c(
				TMS__Week_Management__c = weekList[0].Id,
				TMS__Candidate__c = employeeList[0].Id,
				TMS__Date__c = Date.today()
			),
			new TMS__Time__c(
				TMS__Week_Management__c = weekList[1].Id,
				TMS__Candidate__c = employeeList[0].Id,
				TMS__Date__c = Date.today() + 7
			),
			new TMS__Time__c(
				TMS__Week_Management__c = weekList[2].Id,
				TMS__Candidate__c = employeeList[0].Id,
				TMS__Date__c = Date.today() + 14
			),
			new TMS__Time__c(
				TMS__Week_Management__c = weekList[0].Id,
				TMS__Candidate__c = employeeList[1].Id,
				TMS__Date__c = Date.today()
			),
			new TMS__Time__c(
				TMS__Week_Management__c = weekList[1].Id,
				TMS__Candidate__c = employeeList[1].Id,
				TMS__Date__c = Date.today() + 7
			),
			new TMS__Time__c(
				TMS__Week_Management__c = weekList[2].Id,
				TMS__Candidate__c = employeeList[1].Id,
				TMS__Date__c = Date.today() + 14
			),
			new TMS__Time__c(
				TMS__Week_Management__c = weekList[2].Id,
				TMS__Candidate__c = employeeList[1].Id,
				TMS__Date__c = Date.today() + 15
			)
		};
		insert timeList;
	}


	/**
	* @author Aliaksandr Satskou
	* @date 06/21/2014
	* @name testPayrollNotify
	* @description Testing PayrollNotify class.
	* @return void
	*/
	static testMethod void testPayrollNotify() {
		initData();


		PayrollNotify.notifyOnAdditionalTime(timeList);

		PayrollNotify.createEmail(
				new List<String> { 'test@test.com' }, 'Create Email', 'Create Email Method');
	}
}