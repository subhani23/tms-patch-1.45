/**
* @author Aliaksandr Satskou
* @date 04/17/2014
* @description Test class for QueryHelper class.
*/
@isTest
private class QueryHelperTest {


    private static List<Contact> contactList;
    private static List<Account> accountList;


    /**
    * @author Aliaksandr Satskou
    * @date 04/17/2014
    * @name initData
    * @description Init data.
    * @return void
    */
    private static void initData() {
        accountList = new List<Account> {
            new Account(
                Name = 'Account #1'
            ),
            new Account(
                Name = 'Account #2'
            )
        };

        insert accountList;


        contactList = new List<Contact> {
            new Contact(
                LastName = 'Contact #1',
                AccountId = accountList[0].Id
            ),
            new Contact(
                LastName = 'Contact #2',
                AccountId = accountList[1].Id
            )
        };

        insert contactList;
    }


    /**
    * @author Aliaksandr Satskou
    * @date 04/17/2014
    * @name testQueryHelper
    * @description Testing QueryHelper class.
    * @return void
    */
    static testMethod void testQueryHelper() {
        Test.startTest();

        initData();


        List<sObject> sObjectList = QueryHelper.getSObjectListWithAllFields(
                'Contact',
                new Set<String> { 'Account.Name' },
                'Id = \'' + contactList[0].Id + '\' OR Id = \'' + contactList[1].Id + '\'');

        System.assertEquals(2, sObjectList.size());

        Test.stopTest();

    }
}