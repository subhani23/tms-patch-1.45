/**
* @author Aliaksandr Satskou
* @date 09/26/2014
* @description Update Timesheets objects.
*/
global inherited sharing class UpdateTimesheetsBatch implements Database.Batchable<sObject> {
	
	private Set<Id> projectIdSet;
	private List<TMS__Timesheet__c> timesheetList;
	
	
	/**
	* @author Aliaksandr Satskou
	* @date 09/26/2014
	* @name UpdateTimesheetsBatch
	* @description Constructor.
	* @param timesheetList Timesheet list
	*/
	global UpdateTimesheetsBatch(List<TMS__Timesheet__c> timesheetList) {
		this.timesheetList = timesheetList;
		
		
		projectIdSet = new Set<Id>();
		
		for (TMS__Timesheet__c timesheet : timesheetList) {
			if (timesheet.TMS__Project__c != null) {
				projectIdSet.add(timesheet.TMS__Project__c);
			}
		}	
	}
	
	
	/**
	* @author Aliaksandr Satskou
	* @date 09/26/2014
	* @name start
	* @description Apex Batch start method.
	* @param BC Batchable Context
	* @return Database.QueryLocator
	*/
	global Database.QueryLocator start(Database.BatchableContext BC) {
		String query = 'SELECT AVTRRT__Account_Manager__c, AVTRRT__Employer__c, Project_TMS__c FROM ' + 
				'AVTRRT__Placement__c WHERE Project_TMS__c IN :projectIdSet ORDER BY CreatedDate DESC';
		
		
		if (!Test.isRunningTest()) {
			return DataBase.getQueryLocator(query);
		} else {
			/* Added to run Unit Tests, because no AVTRRT__Placement__c object. */
			return DataBase.getQueryLocator('SELECT Id FROM Contact');
		} 
	}
	
	
	/**
	* @author Aliaksandr Satskou
	* @date 09/26/2014
	* @name execute
	* @description Apex Batch execute method.
	* @param BC Batchable Context
	* @param placementList Placement list
	* @return void
	*/
	global void execute(Database.BatchableContext BC, List<sObject> placementList) {
		if (placementList.size() > 0) {
			Map<Id, sObject> projectIdPlacementMap = new Map<Id, sObject>();
				
			for (sObject placement : placementList) {
				sObject placementFromMap = projectIdPlacementMap.get((Id)placement.get('Project_TMS__c'));
					
				if (placementFromMap == null) {
					projectIdPlacementMap.put((Id)placement.get('Project_TMS__c'), placement);
				}
			}
				
				
			for (TMS__Timesheet__c timesheet : timesheetList) {
				if (timesheet.TMS__Project__c != null) {
					sObject placement = projectIdPlacementMap.get(timesheet.TMS__Project__c);
						
					timesheet.TMS__Account__c = (Id)placement.get('AVTRRT__Employer__c');
					timesheet.TMS__Account_Manager__c = (Id)placement.get('AVTRRT__Account_Manager__c');
					timesheet.TMS__Placement_ID__c = (String)placement.get('Id');
				}
			}
		}
	
		
		update timesheetList;
	}
	
	
	/**
	* @author Aliaksandr Satskou
	* @date 09/26/2014
	* @name finish
	* @description Apex Batch finish method.
	* @param BC Batchable Context
	* @return void
	*/
	global void finish(Database.BatchableContext BC) { }
}