@isTest
public class TestDataGenerator {
	public Contact employee;
	public Contact manager;
	
	public TMS__Project__c project;
	public TMS__Project_Resource__c projectResource;
	
	public TMS__Task__c taskRegular;
	public TMS__Task__c taskOvertime;
	public TMS__Task__c taskOverflow;
	
	public TMS__Week_Management__c weekManagement;
	
	public TMS__Timesheet__c timesheet;
	public List<TMS__Time__c> times;
	public List<TMS__Expense__c> expenseList;
	public List<Case> cases;
	
	public void initData(InitOptions options) {
		insert new FCMS__CustomSettingsComponent__c(
				Name = 'Default',
				FCMS__SessionTimeout__c = 1,
				FCMS__ErrorsReceiver__c = 'test@test.com');
		
		insert new TMS__TimesheetCustomSettings__c(
				Name = 'ProjectStatusToShow',
				TMS__Associate_Case_with_Time__c = true,
				TMS__Case_Assigned_Field_Name__c = 'ContactId',
				TMS__Case_Subject_Length__c = '10',
				TMS__Consider_Cases_closed_before_days__c = 0,
				TMS__Display_all_open_Cases_by_Project__c = false,
				TMS__Enable_Timesheet_Object__c = true,
				TMS__TMSProjectStatusToShow__c = 'open;',
				TMS__Task_Statuses_To_Hide__c = 'Completed',
				TMS__TMSTimeStatusToShow__c = 'Submitted',
				TMS__Disable_week_management_sync_from_Date__c = true);
		
		TMS__TimesheetCustomSettingsHierarchy__c tmsHierarchy =
				TMS__TimesheetCustomSettingsHierarchy__c.getOrgDefaults();
		tmsHierarchy.TMS__Display_Cases_by_Projects_Only__c = false;
		tmsHierarchy.TMS__Statuses_for_Accounting_Manager__c = 'Submitted';
		tmsHierarchy.TMS__Statuses_for_Manager_Approval__c = 'Submitted';
		upsert tmsHierarchy;
		
		employee = new Contact(
				LastName = 'Employee',
				Email = 'employee@avankia.com');
		insert employee;
		
		manager = new Contact(
				LastName = 'Manager',
				Email = 'manager@avankia.com');
		insert manager;
		
		FCMS__Session__c session = new FCMS__Session__c(
				FCMS__Session_For__c = options.loginAsManager ? manager.Id : employee.Id,
				FCMS__Is_Valid__c = true,
				FCMS__SessionId__c = '86183049801873');
		insert session;
		
		ApexPages.currentPage().getParameters().put('sessionId', session.FCMS__SessionId__c);
		
		if (options.loginAsManager) {
			ApexPages.currentPage().getParameters().put('approval', 'manager');
		}
		
		RecordType projectRecordType = [
				SELECT Id
				FROM RecordType
				WHERE SObjectType = 'TMS__Project__c' AND Name = 'Project'
				LIMIT 1];
		
		project = new TMS__Project__c(
				Name = 'Test Project',
				TMS__Internal_Approver__c = manager.Id,
				RecordTypeId = projectRecordType.Id);
		insert project;
		
		projectResource = new TMS__Project_Resource__c(
				TMS__Project__c = project.Id,
				TMS__Contact__c = employee.Id);
		insert projectResource;
		
		taskRegular = insertTask('Regular Hours');
		
		if (options.enableTimeTracking != null){
			taskRegular.TMS__Enable_Time_Tracking__c = options.enableTimeTracking;
		}
		
		if (options.taskDataType != null){
			taskRegular.TMS__Task_data_type__c = options.taskDataType;
		}
		
		update taskRegular;
		
		weekManagement = new TMS__Week_Management__c(
				TMS__Start_Date__c = Date.today().toStartOfWeek(),
				TMS__End_Date__c = Date.today().toStartOfWeek().addDays(6),
				TMS__Active__c = true);
		insert weekManagement;
		
		if (options.insertTimesheetRecord) {
			timesheet = new TMS__Timesheet__c(
					TMS__Week_Management__c = weekManagement.Id,
					TMS__Candidate__c = employee.Id,
					TMS__Project_Resource__c = projectResource.Id,
					TMS__Status__c = 'Submitted');
			insert timesheet;
		}
		
		if (options.insertTimeRecords) {
			times = new List<TMS__Time__c>();
			
			for (Integer i = 0; i < 7; i++) {
				times.add(new TMS__Time__c(
						TMS__Date__c = Date.today().toStartOfWeek() + i,
						TMS__Time_Spent__c = 3.0,
						TMS__Status__c = 'Submitted',
						TMS__Comments__c = 'Test Comment',
						TMS__Task__c = taskRegular.Id,
						TMS__Week_Management__c = weekManagement.Id,
						TMS__Timesheet__c = options.insertTimesheetRecord ? timesheet.Id : null));
			}
			
			insert times;
		}
		
		if (options.insertOvetimeTask) {
			insertOvertimeTask();
		}
		
		if (options.insertOverflowTask) {
			insertOverflowTask();
		}
		
		expenseList = new List<TMS__Expense__c>();
		
		for (Integer i = 0; i < 7; i++) {
				expenseList.add(new TMS__Expense__c(
						TMS__Status__c = 'Submitted',
						TMS__Comments__c = 'Test Comment',
						TMS__Contact__c = employee.id,
						TMS__Amount__c = 10,
						TMS__Week_Management__c = weekManagement.Id,
						TMS__Timesheet__c = options.insertTimesheetRecord ? timesheet.Id : null));
			}
		insert expenseList;
	}
	
	private void insertOvertimeTask() {
		taskOvertime = insertTask('Overtime Hours');
		
		taskRegular.TMS__Overtime_overflow_task__c = taskOvertime.Id;
		taskRegular.TMS__Overtime_hours_threshold__c = 40;
		taskRegular.TMS__Daily_hours_threshold__c = 8;
		taskRegular.TMS__Overtime_calculation_schedule__c = 'Daily & Weekly';
		update taskRegular;
	}
	
	private void insertOverflowTask() {
		taskOverflow = insertTask('Overflow Hours');
		
		taskOvertime.TMS__Overtime_overflow_task__c = taskOverflow.Id;
		taskOvertime.TMS__Overtime_hours_threshold__c = 20;
		taskOvertime.TMS__Daily_hours_threshold__c = 4;
		taskOvertime.TMS__Overtime_calculation_schedule__c = 'Daily & Weekly';
		update taskOvertime;
	}
	
	private TMS__Task__c insertTask(String taskName) {
		TMS__Task__c task = new TMS__Task__c(
				Name = taskName,
				TMS__Project__c = project.Id,
				TMS__Project_Resource__c = projectResource.Id,
				TMS__Billable__c = 'Yes');
		
		insert task;
		
		return task;
	}
	
	public class InitOptions {
		public Boolean insertTimeRecords = true;
		public Boolean insertTimesheetRecord = false;
		
		public Boolean enableTimeTracking;
		public String taskDataType;
		
		public Boolean insertOvetimeTask = false;
		public Boolean insertOverflowTask = false;
		
		public Boolean loginAsManager = false;
	}
}