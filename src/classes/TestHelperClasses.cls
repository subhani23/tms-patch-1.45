public class TestHelperClasses {
	public class ProjectWrapper {
		public TMS__Project__c project;	
		public List<ProjectResourceWrapper> projectResourceList = new List<ProjectResourceWrapper>();
		
		public ProjectWrapper(TMS__Project__c p_project) {
			project = p_project; 
		}
	}
	
	public class ProjectResourceWrapper {
		public TMS__Project_Resource__c projectResource;
		public List<TaskWrapper> taskList = new List<TaskWrapper>();
		
		public ProjectResourceWrapper(TMS__Project_Resource__c p_projectResource) {
			projectResource = p_projectResource; 
		}
	}
	
	public class TaskWrapper {
		public TMS__Task__c task;
		public List<TMS__Time__c> timeList = new List<TMS__Time__c>();
		
		public TaskWrapper(TMS__Task__c p_task) {
			task = p_task; 
		}
	}
	
	public class WeekManagementWrapper {
		public TMS__Week_Management__c weekManagement;
		public List<TMS__Time__c> timeList = new List<TMS__Time__c>();
		
		public WeekManagementWrapper(TMS__Week_Management__c p_weekManagement) {
			weekManagement = p_weekManagement; 
		}
	}
}