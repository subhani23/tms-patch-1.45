/**
* @author Aliaksandr Satskou
* @date 04/10/2014
* @description Package post install methods.
*/
public class PackagePostInstallHelper {


	/**
	* @author Aliaksandr Satskou
	* @name createPayrollSetting
	* @date 04/10/2014
	* @param name Custom Setting Name
	* @param columnName Column Name
	* @param objectApiName Object Api Name
	* @param fieldApiName Field Api Name
	* @param fieldOrder Field Order
	* @param fileName File Name
	* @param characters Characters
	* @param payrollProviderName Payroll Provider Name
	* @param lineNumber Line Number
	* @description Creating PayrollSetting.
	* @return TMS__Payroll_Settings__c
	*/
	private static TMS__Payroll_Settings__c createPayrollSetting(
			String name, String columnName, String objectApiName, String fieldApiName, String fieldOrder,
			String fileName, String characters, String payrollProviderName, String lineNumber) {

		return new TMS__Payroll_Settings__c(
			Name = name,
			TMS__Column_Name__c = columnName,
			TMS__Object_API_Name__c = objectApiName,
			TMS__Field_API_Name__c = fieldApiName,
			TMS__Field_Order__c = fieldOrder,
			TMS__File_Name__c = fileName,
			TMS__Characters__c = characters,
			TMS__Payroll_Provider_Name__c = payrollProviderName,
			TMS__Line__c = lineNumber
		);
	}


	/**
	* @author Aliaksandr Satskou
	* @name createPayrollSettings
	* @date 04/10/2014
	* @description Creating PayrollSettings.
	* @return void
	*/
	public static void createPayrollSettings() {
		insert new List<TMS__Payroll_Settings__c> {
			createPayrollSetting('Paychex Company_ID', null, 'TMS__Payroll_Line_items__c',
					'TMS__Company_ID__c', null, 'CVT1.TXT', '1-4', 'Paychex', null),

			createPayrollSetting('Paychex Employee_ID', null, 'TMS__Payroll_Line_items__c',
					'TMS__Employee_Number__c', null, 'CVT1.TXT', '5-10', 'Paychex', null),

			createPayrollSetting('Paychex Position OR JobTitle', null, 'TMS__Payroll_Line_items__c',
					'TMS__Position_or_Job_Title__c', null, 'CVT1.TXT', '228-247', 'Paychex', null),

			createPayrollSetting('Paychex Department', null, 'TMS__Payroll_Line_items__c',
					'TMS__Override_Department__c', null, 'CVT1.TXT', '23-28', 'Paychex', null),

			createPayrollSetting('Paychex LastName', null, 'TMS__Payroll_Line_items__c',
					'TMS__Last_Name__c', null, 'CVT1.TXT', '29-58', 'Paychex', null),

			createPayrollSetting('Paychex FirstName', null, 'TMS__Payroll_Line_items__c',
					'TMS__First_Name__c', null, 'CVT1.TXT', '59-78', 'Paychex', null),

			createPayrollSetting('Paychex Address #1', null, 'TMS__Payroll_Line_items__c',
					'TMS__Address_1__c', null, 'CVT1.TXT', '79-108', 'Paychex', null),

			createPayrollSetting('Paychex City', null, 'TMS__Payroll_Line_items__c',
					'TMS__City__c', null, 'CVT1.TXT', '139-158', 'Paychex', null),

			createPayrollSetting('Paychex State', null, 'TMS__Payroll_Line_items__c',
					'TMS__State__c', null, 'CVT1.TXT', '159-160', 'Paychex', null),

			createPayrollSetting('Paychex Zip', null, 'TMS__Payroll_Line_items__c',
					'TMS__Zip__c', null, 'CVT1.TXT', '161-170', 'Paychex', null),

			createPayrollSetting('Paychex Phone', null, 'TMS__Payroll_Line_items__c',
					'TMS__Phone__c', null, 'CVT1.TXT', '171-184', 'Paychex', null),

			createPayrollSetting('Paychex Social Security #', null, 'TMS__Payroll_Line_items__c',
					'TMS__Social_Security_Number__c', null, 'CVT1.TXT', '185-195', 'Paychex', null),

			createPayrollSetting('Paychex BirthDate', null, 'TMS__Payroll_Line_items__c',
					'TMS__Birth_Date__c', null, 'CVT1.TXT', '196-203', 'Paychex', null),

			createPayrollSetting('Paychex HireDate', null, 'TMS__Payroll_Line_items__c',
					'TMS__Hire_Date__c', null, 'CVT1.TXT', '204-211', 'Paychex', null),

			createPayrollSetting('Paychex Term Code', null, 'TMS__Payroll_Line_items__c',
					'TMS__Term_Code__c', null, 'CVT1.TXT', '256', 'Paychex', null),

			createPayrollSetting('Paychex Fed Marital Status', null, 'TMS__Payroll_Line_items__c',
					'TMS__Fed_Marital_Status__c', null, 'CVT1.TXT', '270', 'Paychex', null),

			createPayrollSetting('Paychex Termination Date', null, 'TMS__Payroll_Line_items__c',
					'TMS__Termination_Date__c', null, 'CVT1.TXT', '257-264', 'Paychex', null),

			createPayrollSetting('Paychex Gender', null, 'TMS__Payroll_Line_items__c',
					'TMS__Gender__c', null, 'CVT1.TXT', '265', 'Paychex', null),

			createPayrollSetting('Paychex Fed Dependents', null, 'TMS__Payroll_Line_items__c',
					'TMS__Fed_Dependents__c', null, 'CVT1.TXT', '266-267', 'Paychex', null),

			createPayrollSetting('Paychex State Dependents', null, 'TMS__Payroll_Line_items__c',
					'TMS__State_Dependents__c', null, 'CVT1.TXT', '268-269', 'Paychex', null),

			createPayrollSetting('Paychex State Marital Status', null, 'TMS__Payroll_Line_items__c',
					'TMS__State_Marital_Status__c', null, 'CVT1.TXT', '271', 'Paychex', null),

			createPayrollSetting('Paychex Employee ID', null, 'TMS__Payroll_Line_items__c',
					'TMS__Employee__c', null, 'CVT1B.TXT', '5-10', 'Paychex', null),

			createPayrollSetting('Paychex Amount or Percent', null, 'TMS__Payroll_Line_items__c',
					'TMS__Amount_or_Percent__c', null, 'CVT1B.TXT', '55', 'Paychex', null),

			createPayrollSetting('Paychex Company ID', null, 'TMS__Payroll_Line_items__c',
					'TMS__Company_ID__c', null, 'CVT1B.TXT', '1-4', 'Paychex', null),

			createPayrollSetting('Paychex Amount CVT1B.TXT', null, 'TMS__Payroll_Line_items__c',
					'TMS__Amount__c', null, 'CVT1B.TXT', '56-66', 'Paychex', null),

			createPayrollSetting('Paychex DeCode', null, 'TMS__Payroll_Line_items__c',
					'TMS__DeCode__c', null, 'CVT1B.TXT', '53-54', 'Paychex', null),

			createPayrollSetting('Paychex Account', null, 'TMS__Payroll_Line_items__c',
					'TMS__Account__c', null, 'CVT1B.TXT', '33-52', 'Paychex', null),

			createPayrollSetting('Paychex Bank', null, 'TMS__Payroll_Line_items__c',
					'TMS__Bank__c', null, 'CVT1B.TXT', '13-32', 'Paychex', null),

			createPayrollSetting('Paychex Sequence', null, 'TMS__Payroll_Line_items__c',
					'TMS__Sequence__c', null, 'CVT1B.TXT', '11-12', 'Paychex', null),

			createPayrollSetting('Paychex Amount', null, 'TMS__Payroll_Line_items__c',
					'TMS__Amount__c', null, 'R320_TA.TXT', '81-89', 'Paychex', null),

			createPayrollSetting('Paychex D/E', null, 'TMS__Payroll_Line_items__c',
					'TMS__D_E__c', null, 'R320_TA.TXT', '51', 'Paychex', null),

			createPayrollSetting('Paychex Deduction or Earning Code', null, 'TMS__Payroll_Line_items__c',
					'TMS__D_E_Code__c', null, 'R320_TA.TXT', '52-53', 'Paychex', null),

			createPayrollSetting('Paychex Employee Number', null, 'TMS__Payroll_Line_items__c',
					'TMS__Employee_Number__c', null, 'R320_TA.TXT', '1-6', 'Paychex', null),

			createPayrollSetting('Paychex Hours', null, 'TMS__Payroll_Line_items__c',
					'TMS__Hours__c', null, 'R320_TA.TXT', '63-70', 'Paychex', null),

			createPayrollSetting('Paychex Name', null, 'TMS__Payroll_Line_items__c',
					'TMS__Name__c', null, 'R320_TA.TXT', '7-31', 'Paychex', null),

			createPayrollSetting('Paychex Override Department', null, 'TMS__Payroll_Line_items__c',
					'TMS__Override_Department__c', null, 'R320_TA.TXT', '32-37', 'Paychex', null),

			createPayrollSetting('Paychex Override Local', null, 'TMS__Payroll_Line_items__c',
					'TMS__Override_Local__c', null, 'R320_TA.TXT', '105-114', 'Paychex', null),

			createPayrollSetting('Paychex Override Rate', null, 'TMS__Payroll_Line_items__c',
					'TMS__Override_Rate__c', null, 'R320_TA.TXT', '54-62', 'Paychex', null),

			createPayrollSetting('Paychex Override State', null, 'TMS__Payroll_Line_items__c',
					'TMS__Override_State__c', null, 'R320_TA.TXT', '103-104', 'Paychex', null),

			createPayrollSetting('Paychex Sequence Number', null, 'TMS__Payroll_Line_items__c',
					'TMS__Sequence_Number__c', null, 'R320_TA.TXT', '90', 'Paychex', null),

			createPayrollSetting('Paychex Shift', null, 'TMS__Payroll_Line_items__c',
					'TMS__Shift__c', null, 'R320_TA.TXT', '50', 'Paychex', null),

			createPayrollSetting('Paychex Social Security Number', null, 'TMS__Payroll_Line_items__c',
					'TMS__Social_Security_Number__c', null, 'R320_TA.TXT', '117-127', 'Paychex', null),

			createPayrollSetting('Paychex Day', null, 'TMS__Payroll_Line_items__c',
					'TMS__Day__c', null, 'R320_TA.TXT', '75-76', 'Paychex', null),

			createPayrollSetting('Paychex Job', null, 'TMS__Payroll_Line_items__c',
					'TMS__Job__c', null, 'R320_TA.TXT', '38-49', 'Paychex', null),

			createPayrollSetting('Paychex Override Division', null, 'TMS__Payroll_Line_items__c',
					'TMS__Override_Division__c', null, 'R320_TA.TXT', '91-96', 'Paychex', null),

			createPayrollSetting('Paychex Month', null, 'TMS__Payroll_Line_items__c',
					'TMS__Month__c', null, 'R320_TA.TXT', '73-74', 'Paychex', null),

			createPayrollSetting('Paychex Year', null, 'TMS__Payroll_Line_items__c',
					'TMS__Year__c', null, 'R320_TA.TXT', '71-72', 'Paychex', null),

			createPayrollSetting('Paychex Rate Number', null, 'TMS__Payroll_Line_items__c',
					'TMS__Rate_Number__c', null, 'R320_TA.TXT', '116', 'Paychex', null),

			createPayrollSetting('Paychex State/Local Misc Field', null, 'TMS__Payroll_Line_items__c',
					'TMS__State_Local_Misc_Field__c', null, 'R320_TA.TXT', '115', 'Paychex', null),

			createPayrollSetting('Paychex Minute', null, 'TMS__Payroll_Line_items__c',
					'TMS__Minute__c', null, 'R320_TA.TXT', '79-80', 'Paychex', null),

			createPayrollSetting('Paychex Override Branch', null, 'TMS__Payroll_Line_items__c',
					'TMS__Override_Branch__c', null, 'R320_TA.TXT', '97-102', 'Paychex', null),

			createPayrollSetting('Paychex Hour', null, 'TMS__Payroll_Line_items__c',
					'TMS__Hour__c', null, 'R320_TA.TXT', '77-78', 'Paychex', null),


			createPayrollSetting('Batch Id', 'Batch Id',
					'TMS__Payroll_Line_items__c',
					'Batch_Id__c', '1', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Company code', 'Company code',
					'TMS__Payroll_Line_items__c',
					'TMS__Company_ID__c', '2', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('File #', 'File #',
					'TMS__Payroll_Line_items__c',
					'TMS__Employee_Number__c', '3', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Hours 3 amount', 'Hours 3 amount',
					'TMS__Payroll_Line_items__c',
					'TMS__Hours__c', '4', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Hours 3 code', 'Hours 3 code',
					'TMS__Payroll_Line_items__c',
					'Hours_3_code__c', '5', 'epidecaa.csv', null, 'ADP', '1'),


			createPayrollSetting('Co Code', 'Co Code',
					'Contact',
					'TMS__Co_Code__c', '1', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('File  #', 'File #',
					'Contact',
					'TMS__Employee_Id__c', '2', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Change Effective On', 'Change Effective On',
					'Contact',
					'TMS__Change_Effective_On__c', '3', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Is Paid By Wfn', 'Is Paid By Wfn',
					'Contact',
					'"Y"', '4', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Position Uses Time', 'Position Uses Time',
					'Contact',
					'"N"', '5', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('First Name', 'First Name',
					'Contact',
					'FirstName', '6', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Middle Name', 'Middle Name',
					'Contact',
					'TMS__Middle_Name__c', '7', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Last Name', 'Last Name',
					'Contact',
					'LastName', '8', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Reports to Position ID', 'Reports to Position ID',
					'Contact',
					'"no one"', '9', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Tax ID Type', 'Tax ID Type',
					'Contact',
					'"SSN"', '10', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Tax ID Number', 'Tax ID Number',
					'Contact',
					'TMS__SS__c', '11', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Worked in Country', 'Worked in Country',
					'Contact',
					'"USA"', '12', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Hire Date', 'Hire Date',
					'Contact',
					'TMS__Hire_Date__c', '13', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Gender', 'Gender',
					'Contact',
					'TMS__Gender__c', '14', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Address 1 Line 1', 'Address 1 Line 1',
					'Contact',
					'MailingStreet', '15', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Address 1 Line 2', 'Address 1 Line 2',
					'Contact',
					'""', '16', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Address 1 City', 'Address 1 City',
					'Contact',
					'MailingCity', '17', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Address 1 State Postal Code', 'Address 1 State Postal Code',
					'Contact',
					'MailingPostalCode', '18', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Address 1 Zip Code', 'Address 1 Zip Code',
					'Contact',
					'MailingPostalCode', '19', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Employee Status', 'Employee Status',
					'Contact',
					'"A"', '20', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Birth Date', 'Birth Date',
					'Contact',
					'Birthdate', '21', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Eeo Ethnic Code', 'Eeo Ethnic Code',
					'Contact',
					'TMS__Eeo_Ethnic_Code__c', '22', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Home Phone Number', 'Home Phone Number',
					'Contact',
					'HomePhone', '23', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Pay Frequency Code', 'Pay Frequency Code',
					'Contact',
					'"W"', '24', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Rate Type', 'Rate Type',
					'Contact',
					'"H"', '25', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Rate 1 Amount', 'Rate 1 Amount',
					'Contact',
					'TMS__Rate_1_Amount__c', '26', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Rate Currency', 'Rate Currency',
					'Contact',
					'"USD"', '27', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Standard Hours', 'Standard Hours',
					'Contact',
					'"No"', '28', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Home Department', 'Home Department',
					'Contact',
					'TMS__Home_Department__c', '29', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Pay Group', 'Pay Group',
					'Contact',
					'"1"', '30', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Employee Type', 'Employee Type',
					'Contact',
					'"hrly"', '31', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Naics Workers Comp Code', 'Naics Workers Comp Code',
					'Contact',
					'TMS__Naics_Workers_Comp_Code__c', '32', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Do Not Calculate Medicare', 'Do Not Calculate Medicare',
					'Contact',
					'"False"', '33', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Do Not Calculate Social Security', 'Do Not Calculate Social Security',
					'Contact',
					'"False"', '34', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Federal Marital Status', 'Federal Marital Status',
					'Contact',
					'TMS__Federal_Filing_Status__c', '35', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Federal Exemptions', 'Federal Exemptions',
					'Contact',
					'TMS__Federal_Exemptions__c', '36', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Do Not Calculate Federal Tax', 'Do Not Calculate Federal Tax',
					'Contact',
					'TMS__Do_Not_Calculate_Federal_Tax__c', '37', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Worked State Tax Code', 'Worked State Tax Code',
					'Contact',
					'TMS__Worked_State_Tax_Code__c', '38', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('State Marital Status', 'State Marital Status',
					'Contact',
					'TMS__State_Filing_Status__c', '39', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('State Exemptions', 'State Exemptions',
					'Contact',
					'TMS__State_Exemptions__c', '40', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Maryland County Tax Percentage', 'Maryland County Tax Percentage',
					'Contact',
					'"N/A"', '41', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Exemptions In Dollars', 'Exemptions In Dollars',
					'Contact',
					'"N/A"', '42', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('State Withholding Table', 'State Withholding Table',
					'Contact',
					'TMS__State_Withholding_Table__c', '43', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Do Not Calculate State Tax', 'Do Not Calculate State Tax',
					'Contact',
					'"N"', '44', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Worked Local Tax Code', 'Worked Local Tax Code',
					'Contact',
					'"N/A"', '45', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Lived Local Tax Code', 'Lived Local Tax Code',
					'Contact',
					'"N/A"', '46', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Sui/Sdi Tax Jurisdiction Code', 'Sui/Sdi Tax Jurisdiction Code',
					'Contact',
					'TMS__Sui_Sdi_Tax_Jurisdiction_Code__c', '47', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Custom Area 1', 'Custom Area 1',
					'Contact',
					'"N/A"', '48', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Custom Area 2', 'Custom Area 2',
					'Contact',
					'"N/A"', '49', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Business E-mail Address', 'Business E-mail Address',
					'Contact',
					'"N/A"', '50', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Personal E-mail Address', 'Personal E-mail Address',
					'Contact',
					'Email', '51', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Work Phone Number', 'Work Phone Number',
					'Contact',
					'"N/A"', '52', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Work Extension', 'Work Extension',
					'Contact',
					'"N/A"', '53', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Personal Cell Number', 'Personal Cell Number',
					'Contact',
					'MobilePhone', '54', 'epidecaa.csv', null, 'ADP', '1'),

			createPayrollSetting('Work Cell Number', 'Work Cell Number',
					'Contact',
					'"N/A"', '55', 'epidecaa.csv', null, 'ADP', '1'),


			createPayrollSetting('Co Code #1', 'Co Code',
					'Contact',
					'TMS__Co_Code__c', '1', 'directdeposit.csv', null, 'ADP', '1'),

			createPayrollSetting('Co Code #2', 'Co Code',
					'Contact',
					'TMS__Co_Code__c', '1', 'directdeposit.csv', null, 'ADP', '2'),

			createPayrollSetting('File # #1', 'File #',
					'Contact',
					'TMS__Employee_Id__c', '2', 'directdeposit.csv', null, 'ADP', '1'),

			createPayrollSetting('File # #2', 'File #',
					'Contact',
					'TMS__Employee_Id__c', '2', 'directdeposit.csv', null, 'ADP', '2'),

			createPayrollSetting('EE Name #1', 'EE Name',
					'Contact',
					'Name', '3', 'directdeposit.csv', null, 'ADP', '1'),

			createPayrollSetting('EE Name #2', 'EE Name',
					'Contact',
					'Name', '3', 'directdeposit.csv', null, 'ADP', '2'),

			createPayrollSetting('Change Effective On #1', 'Change Effective On',
					'Contact',
					'TMS__Change_Effective_On__c', '4', 'directdeposit.csv', null, 'ADP', '1'),

			createPayrollSetting('Change Effective On #2', 'Change Effective On',
					'Contact',
					'TMS__Change_Effective_On__c', '4', 'directdeposit.csv', null, 'ADP', '2'),

			createPayrollSetting('Bank Deposit Deduction Code #1', 'Bank Deposit Deduction Code',
					'Contact',
					'TMS__Bank_Account_Type__c', '5', 'directdeposit.csv', null, 'ADP', '1'),

			createPayrollSetting('Bank Deposit Deduction Code #2', 'Bank Deposit Deduction Code',
					'Contact',
					'TMS__Bank_Account_Type_other__c', '5', 'directdeposit.csv', null, 'ADP', '2'),

			createPayrollSetting('Bank Deposit Deduction Amount #1', 'Bank Deposit Deduction Amount',
					'Contact',
					'"N/A"', '6', 'directdeposit.csv', null, 'ADP', '1'),

			createPayrollSetting('Bank Deposit Deduction Amount #2', 'Bank Deposit Deduction Amount',
					'Contact',
					'TMS__Amount__c', '6', 'directdeposit.csv', null, 'ADP', '2'),

			createPayrollSetting('Bank Deposit Transit/ABA #1', 'Bank Deposit Transit/ABA',
					'Contact',
					'TMS__Routing__c', '7', 'directdeposit.csv', null, 'ADP', '1'),

			createPayrollSetting('Bank Deposit Transit/ABA #2', 'Bank Deposit Transit/ABA',
					'Contact',
					'TMS__Routing_other__c', '7', 'directdeposit.csv', null, 'ADP', '2'),

			createPayrollSetting('Bank Deposit Account Number #1', 'Bank Deposit Account Number',
					'Contact',
					'TMS__Accountnum__c', '8', 'directdeposit.csv', null, 'ADP', '1'),

			createPayrollSetting('Bank Deposit Account Number #2', 'Bank Deposit Account Number',
					'Contact',
					'TMS__Account_othernum__c', '8', 'directdeposit.csv', null, 'ADP', '2'),

			createPayrollSetting('Bank Full Deposit Flag #1', 'Bank Full Deposit Flag',
					'Contact',
					'TMS__Amount__c,"Yes"', '9', 'directdeposit.csv', null, 'ADP', '1'),

			createPayrollSetting('Bank Full Deposit Flag #2', 'Bank Full Deposit Flag',
					'Contact',
					'"No"', '9', 'directdeposit.csv', null, 'ADP', '2')
		};
	}


	/**
	* @author Aliaksandr Satskou
	* @name createPayrollFilenamesSettings
	* @date 04/10/2014
	* @description Creating Payroll Filenames Settings.
	* @return void
	*/
	public static void createPayrollFilenamesSettings() {
		insert new TMS__Payroll_Filenames__c(
			Name = 'Default',

			TMS__ADP_Data_File_Name__c = 'epidecaa.csv',

			TMS__Paychex_Data_File_Name__c = 'R320_TA.TXT',
			TMS__Paychex_Employee_Bank_Data_File_Name__c = 'CVT1B.TXT',
			TMS__Paychex_Employee_Data_File_Name__c = 'CVT1.TXT'
		);
	}
}