/**
* @author Aliaksandr Satskou
* @date 05/21/2014
* @description Generating notification for any additional Time record, which created for Week
		where Payroll already completed.
*/
public with sharing class PayrollNotify {


	/**
	* @author Aliaksandr Satskou
	* @date 05/21/2014
	* @name notifyOnAdditionalTime
	* @description Notify Payroll Email on any additional Time record created for Week where
			Payroll already completed.
	* @param timeList List of Time objects
	* @return void
	*/
	public static void notifyOnAdditionalTime(List<TMS__Time__c> timeList) {
		System.debug(LoggingLevel.ERROR, '::::timeList=' + timeList);

		if (timeList.size() == 0) { return; }


		Map<String, TMS__Payroll_Provider_Settings__c> payrollProviderSettingMap =
				CustomSettingsUtility.getPayrollProviderSettingMap();
		System.debug(LoggingLevel.ERROR, '::::payrollProviderSettingMap=' + payrollProviderSettingMap);

		Map<Contact, List<TMS__Time__c>> employeeTimeListMap = getEmployeeTimeListMap(timeList);
		System.debug(LoggingLevel.ERROR, '::::employeeTimeListMap=' + employeeTimeListMap);

		Map<Id, TMS__Week_Management__c> weekMap = getWeekMap(timeList);
		System.debug(LoggingLevel.ERROR, '::::weekMap=' + weekMap);

		List<TMS__Process_Payroll__c> payrollList = getPayrollList(timeList);
		System.debug(LoggingLevel.ERROR, '::::payrollList=' + payrollList);
		Map<TMS__Time__c, TMS__Process_Payroll__c> timePayrollMap = getTimePayrollMap(timeList, payrollList);
		System.debug(LoggingLevel.ERROR, '::::timePayrollMap=' + timePayrollMap);


		List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();
		String subject = Label.Payroll_Notification;


		Map<Contact, List<Id>> employeeWeekIdListMap = new Map<Contact, List<Id>>();

		for (Contact employee : employeeTimeListMap.keySet()) {
			Boolean isCreated = false;

			for (TMS__Time__c timeObj : employeeTimeListMap.get(employee)) {
				TMS__Process_Payroll__c payroll = timePayrollMap.get(timeObj);

				if (payroll != null) {
					String provider = payroll.TMS__Payroll_Provider_Name__c;

					if ((provider != null) && (payrollProviderSettingMap.get(provider) != null)) {
						String emailAddress = payrollProviderSettingMap.get(provider).TMS__Email__c;

						if (emailAddress != null) {
							if (timeObj.LastModifiedDate > payroll.CreatedDate) {
								String body = getBody(weekMap.get(timeObj.TMS__Week_Management__c), employee);


								List<Id> weekIdList = employeeWeekIdListMap.get(employee);

								if (weekIdList != null) {
									for (Id weekId : weekIdList) {
										if (timeObj.TMS__Week_Management__c == weekId) {
											isCreated = true;

											break;
										}
									}

									if (!isCreated) {
										emailList.add(createEmail(
												new List<String> { emailAddress }, subject, body));


										weekIdList.add(timeObj.TMS__Week_Management__c);
									}
								} else {
									emailList.add(createEmail(
											new List<String> { emailAddress }, subject, body));


									employeeWeekIdListMap.put(
											employee, new List<Id> { timeObj.TMS__Week_Management__c });
								}
							}
						}
					}
				}
			}
		}

		System.debug(LoggingLevel.ERROR, '::::::::emailList=' + emailList);
		System.debug(LoggingLevel.ERROR, '::::::::emailList.size=' + emailList.size());

		try {
			Messaging.sendEmail(emailList);
		} catch (Exception e) {  }
	}


	/**
	* @author Aliaksandr Satskou
	* @date 05/21/2014
	* @name getEmployeeTimeListMap
	* @description Getting map of employee to Time list.
	* @param timeList List of Time objects
	* @return Map<Contact, List<TMS__Time__c>>
	*/
	private static Map<Contact, List<TMS__Time__c>> getEmployeeTimeListMap(List<TMS__Time__c> timeList) {
		Set<Id> employeeIdSet = new Set<Id>();

		for (TMS__Time__c timeObj : timeList) {
			if (timeObj.TMS__Candidate__c != null) {
				employeeIdSet.add(timeObj.TMS__Candidate__c);
			}
		}

		List<Contact> employeeList = [
				SELECT Name
				FROM Contact
				WHERE Id IN :employeeIdSet];


		Map<Contact, List<TMS__Time__c>> employeeTimeListMap = new Map<Contact, List<TMS__Time__c>>();

		for (Contact employee : employeeList) {
			for (TMS__Time__c timeObj : timeList) {
				if (timeObj.TMS__Candidate__c == employee.Id) {
					List<TMS__Time__c> timeListFromMap = employeeTimeListMap.get(employee);

					if (timeListFromMap != null) {
						timeListFromMap.add(timeObj);
					} else {
						employeeTimeListMap.put(employee, new List<TMS__Time__c> { timeObj });
					}
				}
			}
		}


		return employeeTimeListMap;
	}


	/**
	* @author Aliaksandr Satskou
	* @date 05/21/2014
	* @name getWeekMap
	* @description Getting map of Week Managements.
	* @param timeList List of Time objects
	* @return Map<Id, TMS__Week_Management__c>
	*/
	private static Map<Id, TMS__Week_Management__c> getWeekMap(List<TMS__Time__c> timeList) {
		Set<Id> weekIdSet = new Set<Id>();

		for (TMS__Time__c timeObj : timeList) {
			if (timeObj.TMS__Week_Management__c != null) {
				weekIdSet.add(timeObj.TMS__Week_Management__c);
			}
		}


		return new Map<Id, TMS__Week_Management__c>([
				SELECT Name, TMS__Week__c
				FROM TMS__Week_Management__c
				WHERE Id IN :weekIdSet]);
	}


	/**
	* @author Aliaksandr Satskou
	* @date 05/21/2014
	* @name getPayrollList
	* @description Getting list of Payroll.
	* @param timeList List of Time objects
	* @return List<TMS__Process_Payroll__c>
	*/
	private static List<TMS__Process_Payroll__c> getPayrollList(List<TMS__Time__c> timeList) {
		String query =
				'SELECT TMS__Start_Date__c, TMS__End_Date__c, CreatedDate, TMS__Payroll_Provider_Name__c ' +
				'FROM TMS__Process_Payroll__c ';


		String whereClause = '';

		for (TMS__Time__c timeObj : timeList) {
			if (timeObj.TMS__Date__c != null) {
				System.debug(LoggingLevel.ERROR, ':::::TMS__Date__c=' + timeObj.TMS__Date__c);
				String timeDate = dateToSOQL(timeObj.TMS__Date__c);
				System.debug(LoggingLevel.ERROR, ':::::timeDate=' + timeDate);

				whereClause += '(TMS__Start_Date__c <= ' + timeDate + ' AND TMS__End_Date__c >= ' +
						timeDate + ') OR ';
			}
		}
		whereClause = (whereClause != '') ? whereClause.substring(0, whereClause.length() - 4) : '';


		List<String> pStatList = getPayrollStatusList();

		query += (whereClause != '')
				? 'WHERE ((' + whereClause + ') AND (TMS__Status__c IN :pStatList))'
				: 'WHERE (TMS__Status__c IN :pStatList)';


		return (List<TMS__Process_Payroll__c>)DataBase.query(query);
	}


	/**
	* @author Aliaksandr Satskou
	* @date 05/26/2014
	* @name dateToSOQL
	* @description Getting date compatible with SOQL query.
	* @param dTime Date Time
	* @return String
	*/
	private static String dateToSOQL(DateTime dTime) {
		String queryDate = dTime.year() + '-';

		queryDate += (dTime.month() < 10) ? '0' + dTime.month() + '-' : dTime.month() + '-';
		queryDate += (dTime.dayGmt() < 10) ? '0' + dTime.dayGmt() : '' + dTime.dayGmt();


		return queryDate;
	}


	/**
	* @author Aliaksandr Satskou
	* @date 05/21/2014
	* @name getTimePayrollMap
	* @description Getting map of Time to Payroll.
	* @param timeList List of Time
	* @param payrollList List of Payroll
	* @return Map<TMS__Time__c, TMS__Process_Payroll__c>
	*/
	private static Map<TMS__Time__c, TMS__Process_Payroll__c> getTimePayrollMap(
			List<TMS__Time__c> timeList, List<TMS__Process_Payroll__c> payrollList) {

		Map<TMS__Time__c, TMS__Process_Payroll__c> timePayrollMap =
				new Map<TMS__Time__c, TMS__Process_Payroll__c>();


		for (TMS__Time__c timeObj : timeList) {
			for (TMS__Process_Payroll__c payroll : payrollList) {
				if ((timeObj.TMS__Date__c >= payroll.TMS__Start_Date__c) &&
						(timeObj.TMS__Date__c <= payroll.TMS__End_Date__c)) {

					timePayrollMap.put(timeObj, payroll);

					break;
				}
			}

			if (timePayrollMap.get(timeObj) == null) {
				timePayrollMap.put(timeObj, null);
			}
		}


		return timePayrollMap;
	}


	/**
	* @author Aliaksandr Satskou
	* @date 05/21/2014
	* @name getPayrollStatusList
	* @description Getting Payroll statuses from Custom Setting.
	* @return List<String>
	*/
	private static List<String> getPayrollStatusList() {
		String notifyStat = (String)CustomSettingsUtility.getCustomSettingListTypeValue(
				null, 'TMS__Payroll_Notify_Statuses__c');

		if (notifyStat != null) {
			return notifyStat.split(';');
		}


		return new List<String>();
	}


	/**
	* @author Aliaksandr Satskou
	* @date 05/21/2014
	* @name getBody
	* @description Creating body of Email.
	* @param week Week Management
	* @param employee Employee
	* @return String
	*/
	private static String getBody(TMS__Week_Management__c week, Contact employee) {
		return Label.A_timesheet_has_been_submitted_for_Week_Management_No + week.Name + ' ' +
				Label.by + ' ' + employee.Name + '. ' +
				Label.This_week_has_a_completed_payroll_associated_with_it;
	}


	/**
	* @author Aliaksandr Satskou
	* @date 05/21/2014
	* @name createEmail
	* @description Creating Email message.
	* @param addressList List of addresses
	* @param subject Subject of Email
	* @param body Body of Email
	* @return Messaging.SingleEmailMessage
	*/
	public static Messaging.SingleEmailMessage createEmail(
			List<String> addressList, String subject, String body) {

		Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();

		email.setToAddresses(addressList);
		email.setSubject(subject);
		email.setPlainTextBody(body);


		return email;
	}
}