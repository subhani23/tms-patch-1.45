/**
* @author Aliaksandr Satskou
* @date 12/13/2013
* @description Project Management Tab Controller.
*/
public class ProjectManagementTabController {

	/* Added by Aliaksandr Satskou, 04/18/2014 (case #00023506) */
	public String locale {
		get { return UserInfo.getLocale(); }
		private set;
	}


	/**
	* @author Aliaksandr Satskou
	* @date 12/13/2013
	* @name getWeekManagementTab
	* @description Getting Week Management Tab URL.
	* @return PageReference
	*/
	public PageReference getWeekManagementTab() {
		Schema.DescribeSObjectResult sObjectResult = TMS__Week_Management__c.sObjectType.getDescribe();

		return new PageReference('/' + sObjectResult.getKeyPrefix());
	}


	/**
	* @author Aliaksandr Satskou
	* @date 12/13/2013
	* @name getProjectTab
	* @description Getting Project Tab URL.
	* @return PageReference
	*/
	public PageReference getProjectTab() {
		Schema.DescribeSObjectResult sObjectResult = TMS__Project__c.sObjectType.getDescribe();

		return new PageReference('/' + sObjectResult.getKeyPrefix());
	}


	/**
	* @author Aliaksandr Satskou
	* @date 12/13/2013
	* @name getProjectResourceTab
	* @description Getting Project Resource Tab URL.
	* @return PageReference
	*/
	public PageReference getProjectResourceTab() {
		Schema.DescribeSObjectResult sObjectResult = TMS__Project_Resource__c.sObjectType.getDescribe();

		return new PageReference('/' + sObjectResult.getKeyPrefix());
	}


	/**
	* @author Aliaksandr Satskou
	* @date 12/13/2013
	* @name getTaskTab
	* @description Getting Task Tab URL.
	* @return PageReference
	*/
	public PageReference getTaskTab() {
		Schema.DescribeSObjectResult sObjectResult = TMS__Task__c.sObjectType.getDescribe();

		return new PageReference('/' + sObjectResult.getKeyPrefix());
	}


	/**
	* @author Aliaksandr Satskou
	* @date 12/13/2013
	* @name getTimesheetTab
	* @description Getting Timesheet Tab URL.
	* @return PageReference
	*/
	public PageReference getTimesheetTab() {
		Schema.DescribeSObjectResult sObjectResult = TMS__Timesheet__c.sObjectType.getDescribe();

		return new PageReference('/' + sObjectResult.getKeyPrefix());
	}


	/**
	* @author Aliaksandr Satskou
	* @date 12/13/2013
	* @name getTimeTab
	* @description Getting Time Tab URL.
	* @return PageReference
	*/
	public PageReference getTimeTab() {
		Schema.DescribeSObjectResult sObjectResult = TMS__Time__c.sObjectType.getDescribe();

		return new PageReference('/' + sObjectResult.getKeyPrefix());
	}


	/**
	* @author Aliaksandr Satskou
	* @date 12/13/2013
	* @name getExpenseTab
	* @description Getting Expense Tab URL.
	* @return PageReference
	*/
	public PageReference getExpenseTab() {
		Schema.DescribeSObjectResult sObjectResult = TMS__Expense__c.sObjectType.getDescribe();

		return new PageReference('/' + sObjectResult.getKeyPrefix());
	}


	/**
	* @author Aliaksandr Satskou
	* @date 12/13/2013
	* @name getPayrollTab
	* @description Getting Payroll Tab URL.
	* @return PageReference
	*/
	public PageReference getPayrollTab() {
		Schema.DescribeSObjectResult sObjectResult = TMS__Process_Payroll__c.sObjectType.getDescribe();

		return new PageReference('/' + sObjectResult.getKeyPrefix());
	}
}