@isTest
private class PayrollControllerTest {

	private static List<TMS__Payroll_Provider_Settings__c> payrollProviderSettingList;
	private static List<TMS__Project__c> projectList;
	private static List<TMS__Project_Resource__c> projectResourceList;
	private static List<Contact> contactList;
	private static List<TMS__Task__c> taskList;
	private static List<TMS__Time__c> timeList;
	/*
	private static List<AVTRRT__Placement__c> placementList;
	*/

	/**
	* @name initData
	* @author Aliaksandr Satskou
	* @date 09/04/2013
	* @description Init data.
	* @return void
	*/
	private static void initData() {
		TMS__Payroll_Filenames__c filenamesSetting = new TMS__Payroll_Filenames__c(
			Name = 'Default',
			Paychex_Data_File_Name__c = 'R320_TA.TXT',
			Paychex_Employee_Bank_Data_File_Name__c = 'CVT1B.TXT',
			Paychex_Employee_Data_File_Name__c = 'CVT1.TXT',
			ADP_Data_File_Name__c = 'ADP.TXT'
		);
		upsert filenamesSetting;


		TMS__TimesheetCustomSettings__c customSetting = new TMS__TimesheetCustomSettings__c(
			Name = 'ProjectStatusToShow',
			Regular_Hours_Task_Name__c = 'Regular',
			Overtime_Hours_Task_Name__c = 'Overtime'
		);
		upsert customSetting;


		payrollProviderSettingList = new List<TMS__Payroll_Provider_Settings__c> {
			new TMS__Payroll_Provider_Settings__c(
				Name = 'Paychex',
				TMS__Payroll_Provider_Name__c = 'Paychex',
				TMS__Email__c = 'test1@test.com',
				TMS__Field_Delimiter__c = '',
				TMS__Include_Column_Header_in_Payroll_File__c = false,
				TMS__Payroll_Company_ID__c = 'AB1',
				TMS__Use_CSV_File_Format__c = false
			),
			new TMS__Payroll_Provider_Settings__c(
				Name = 'ADP',
				TMS__Payroll_Provider_Name__c = 'ADP',
				TMS__Email__c = 'test1@test.com',
				TMS__Field_Delimiter__c = ',',
				TMS__Include_Column_Header_in_Payroll_File__c = true,
				TMS__Payroll_Company_ID__c = 'AB1',
				TMS__Use_CSV_File_Format__c = true
			)
		};
		insert payrollProviderSettingList;


		projectList = new List<TMS__Project__c> {
			new TMS__Project__c(
				Name = 'Project #1'
			)
		};
		insert projectList;

		/*
		placementList = new List<AVTRRT__Placement__c> {
			new AVTRRT__Placement__c(
				Project_TMS__c = projectList[0].Id,
				Pay_Rate__c = 30.0,
				Shift_Name__c = '101=Staffing'
			)
		};
		insert placementList;
		*/


		contactList = new List<Contact> {
			new Contact(
				FirstName = 'Contact #1',
				LastName = 'LastName'
			),
			new Contact(
				FirstName = 'Contact #2',
				LastName = 'LastName'
			)
		};
		insert contactList;


		projectResourceList = new List<TMS__Project_Resource__c> {
			new TMS__Project_Resource__c(
				TMS__Project__c = projectList[0].Id,
				TMS__Contact__c = contactList[0].Id
			),
			new TMS__Project_Resource__c(
				TMS__Project__c = projectList[0].Id,
				TMS__Contact__c = contactList[1].Id
			)
		};
		insert projectResourceList;


		taskList = new List<TMS__Task__c> {
			new TMS__Task__c(
				Name = 'Regular',
				TMS__Project__c = projectList[0].Id,
				TMS__Project_Resource__c = projectResourceList[0].Id,
				TMS__Override_Department__c = 'Override Department',
				TMS__D_E__c = 'E',
				TMS__D_E_Code__c = '1',
				TMS__Pay_Rate__c = 25.0
			),
			new TMS__Task__c(
				Name = 'Overtime',
				TMS__Project__c = projectList[0].Id,
				TMS__Project_Resource__c = projectResourceList[0].Id,
				TMS__Override_Department__c = 'Override Department',
				TMS__D_E__c = 'E',
				TMS__D_E_Code__c = '1',
				TMS__Pay_Rate__c = 25.0
			),
			new TMS__Task__c(
				Name = 'Task #1',
				TMS__Project__c = projectList[0].Id,
				TMS__Project_Resource__c = projectResourceList[1].Id,
				TMS__Override_Department__c = 'Override Department',
				TMS__D_E__c = 'E',
				TMS__D_E_Code__c = '2',
				TMS__Pay_Rate__c = 30.0
			),
			new TMS__Task__c(
				Name = 'Task #2',
				TMS__Project__c = projectList[0].Id,
				TMS__Project_Resource__c = projectResourceList[1].Id,
				TMS__Override_Department__c = 'Override Department',
				TMS__D_E__c = 'E',
				TMS__D_E_Code__c = '2',
				TMS__Pay_Rate__c = 30.0
			)
		};
		insert taskList;


		timeList = new List<TMS__Time__c> {
			new TMS__Time__c(
				TMS__Task__c = taskList[0].Id,
				TMS__Status__c = 'Approved',
				TMS__Time_Spent__c = 1.0,
				TMS__Date__c = Date.today()
			),
			new TMS__Time__c(
				TMS__Task__c = taskList[0].Id,
				TMS__Status__c = 'Approved',
				TMS__Time_Spent__c = 1.0,
				TMS__Date__c = Date.today()
			),
			new TMS__Time__c(
				TMS__Task__c = taskList[1].Id,
				TMS__Status__c = 'Approved',
				TMS__Time_Spent__c = 2.0,
				TMS__Date__c = Date.today() + 1
			),
			new TMS__Time__c(
				TMS__Task__c = taskList[1].Id,
				TMS__Status__c = 'Approved',
				TMS__Time_Spent__c = 2.0,
				TMS__Date__c = Date.today() + 2
			),
			new TMS__Time__c(
				TMS__Task__c = taskList[2].Id,
				TMS__Status__c = 'Approved',
				TMS__Time_Spent__c = 3.0,
				TMS__Date__c = Date.today() + 3
			),
			new TMS__Time__c(
				TMS__Task__c = taskList[2].Id,
				TMS__Status__c = 'Approved',
				TMS__Time_Spent__c = 3.0,
				TMS__Date__c = Date.today() + 4
			),
			new TMS__Time__c(
				TMS__Task__c = taskList[3].Id,
				TMS__Status__c = 'Approved',
				TMS__Time_Spent__c = 2.0,
				TMS__Date__c = Date.today() + 5
			),
			new TMS__Time__c(
				TMS__Task__c = taskList[3].Id,
				TMS__Status__c = 'Approved',
				TMS__Time_Spent__c = 2.0,
				TMS__Date__c = Date.today() + 6
			)
		};
		insert timeList;
	}

	/**
	* @name testPayrollController
	* @author Aliaksandr Satskou
	* @date 09/04/2013
	* @description Testing PayrollController class.
	* @return void
	*/
	static testMethod void testPayrollController() {
		initData();
		try {
			PayrollController controller = new PayrollController();

			System.assertEquals(3, controller.payrollProviderSelectOptionList.size());

			controller.processPayroll.TMS__Start_Date__c = Date.today();
			controller.processPayroll.TMS__End_Date__c = Date.today() + 6;

			controller.payrollProvider = 'Paychex';
			controller.generatePayroll();


			controller.processPayroll.TMS__Start_Date__c = Date.today();
			controller.processPayroll.TMS__End_Date__c = Date.today() + 6;

			controller.payrollProvider = 'ADP';
			controller.generatePayroll();
		} catch(Exception e) { }
	}
}