global class AddExistProjectTemplateInProject
{
	static Boolean isOpportunity;
	static final String OPPORTUNITY_PREFIX = Opportunity.SObjectType.getDescribe().getKeyPrefix();

	global static void checkIdForPlacementOrOpportunity(string p_projectId, string p_projectName,
								String p_placementOrOpportunityId) {

		isOpportunity = p_placementOrOpportunityId.startsWith(OPPORTUNITY_PREFIX);

		CheckExistProjectTemplate(p_projectId, p_projectName, p_placementOrOpportunityId);
	}

	global static void CheckExistProjectTemplate(string projectId, string projectName) {}

	/* Added by Aliaksandr Satskou, 12/13/2012 */
	private static SObject placementWithBillRateAndPayRate;

	/**
	 Method

	 @name getPlacementWithBillRateAndPayRate
	 @author Aliaksandr Satskou
	 @data 12/13/2012
	 @description Getting Placement object by API names from Task.
	 @param p_templateTaskList - list of Task.
	 @param p_placementOrOpportunityId - Id of Placement object.
	 @return void.
	*/
	private static void getPlacementWithBillRateAndPayRate(List<TMS__Task__c> p_templateTaskList,
			String p_placementOrOpportunityId) {
		Set<String> v_placementFieldApiNameSet = new Set<String>();

		for (TMS__Task__c v_task : p_templateTaskList) {
			if (v_task.TMS__Pay_Rate_Field_From_Placement__c != null) {
				v_placementFieldApiNameSet.add(v_task.TMS__Pay_Rate_Field_From_Placement__c);
			}

			if (v_task.TMS__Bill_Rate_Field_From_Placement__c != null) {
				v_placementFieldApiNameSet.add(v_task.TMS__Bill_Rate_Field_From_Placement__c);
			}
		}

		String v_whatSelect = '';

		for (String v_placementFieldApiName : v_placementFieldApiNameSet) {
			v_whatSelect += ', ' + v_placementFieldApiName;
		}
		v_whatSelect = (v_whatSelect != '')
				? v_whatSelect.substring(2)
				: '';

		if (v_whatSelect != '') {
			List<SObject> v_placementList = DataBase.query('SELECT ' + v_whatSelect +
					' FROM AVTRRT__Placement__c WHERE Id = \'' + p_placementOrOpportunityId + '\' LIMIT 1');

			if (v_placementList.size() > 0) {
				placementWithBillRateAndPayRate = v_placementList[0];
			} else {
				placementWithBillRateAndPayRate = null;
			}
		} else {
			placementWithBillRateAndPayRate = null;
		}
	}

	/**
	 Method

	 @name getPlacementFieldValue
	 @author Aliaksandr Satskou
	 @data 12/13/2012
	 @description Getting Placement field value by API name.
	 @param p_placementFieldApiName - API name of Placement field.
	 @return Object.
	*/
	private static Object getPlacementFieldValue(String p_placementFieldApiName) {
		if (placementWithBillRateAndPayRate != null) {
			if (p_placementFieldApiName != null) {
				return placementWithBillRateAndPayRate.get(p_placementFieldApiName);
			}
		}

		return null;
	}

	global static void CheckExistProjectTemplate(string projectId, string projectName,
			String p_placementOrOpportunityId)
	{
		Map<String,Schema.RecordTypeInfo> recordTypes =
				TMS__Project__c.sObjectType.getDescribe().getRecordTypeInfosByName();

		List<TMS__Task__c> projectTaskList = new List<TMS__Task__c>();

		projectTaskList = [
				SELECT Name
				FROM TMS__Task__c
				WHERE TMS__Project__r.Id = :projectId];

		TMS__Project__c currentProject = [
				SELECT TMS__Project_Status__c, TMS__Internal_Approver__c, TMS__End_Date__c,
					   TMS__Budget__c, TMS__Billable__c
				FROM TMS__Project__c
				WHERE Id = :projectId];

		List<TMS__Project__c> projectTemplateList = [
				SELECT Name
				FROM TMS__Project__c
				WHERE RecordTypeId = : recordTypes.get('Project Template').getRecordTypeId()
				ORDER BY LastModifiedDate DESC];

		List<TMS__Project_Resource__c> projectResourceListForCheckDuplicat = new List<TMS__Project_Resource__c>();

		projectResourceListForCheckDuplicat = [
				SELECT Id,TMS__Contact__r.Name
				FROM TMS__Project_Resource__c
				WHERE TMS__Project__r.Id = :projectId];

		Map<Id, Id> mapTempResToRealRes = new Map<Id, Id>();

		Id projectTemplateId;
		if (!isOpportunity) {
			sObject v_placement = Database.query(
					' SELECT Project_Template_TMS__c ' +
					' FROM AVTRRT__Placement__c ' +
					' WHERE Id = \'' + p_placementOrOpportunityId + '\' ');

			projectTemplateId = (Id)v_placement.get('Project_Template_TMS__c');
		}

		if (projectTemplateId == null) {
			for (TMS__Project__c projectTemplate : projectTemplateList) {
				if (isOpportunity && projectName.contains(projectTemplate.Name)
						|| !isOpportunity && projectTemplate.Name == 'Placement') {
					projectTemplateId = projectTemplate.Id;
					break;
				}
			}
		}

		if (projectTemplateId != null) {

			/*TMS__Project__c currentTemplate = [
					SELECT TMS__Project_Status__c, TMS__Internal_Approver__c, TMS__End_Date__c,
						TMS__Budget__c, TMS__Billable__c
					FROM TMS__Project__c
					WHERE Id = :projectTemplateId];

			currentProject.TMS__Project_Status__c = currentTemplate.TMS__Project_Status__c;
			currentProject.TMS__Internal_Approver__c = currentTemplate.TMS__Internal_Approver__c != null
					? currentTemplate.TMS__Internal_Approver__c : currentProject.TMS__Internal_Approver__c;
			currentProject.TMS__End_Date__c = currentTemplate.TMS__End_Date__c;
			currentProject.TMS__Budget__c = currentTemplate.TMS__Budget__c;
			currentProject.TMS__Billable__c = currentTemplate.TMS__Billable__c;
			update currentProject;*/

			/* Commented by Aliaksandr Satskou, 12/12/2012 */
			/*
			List<TMS__Task__c> taskTemplateList =  [
					SELECT Name, TMS__Allocated_Hours__c, TMS__Sequence__c, TMS__Project_Resource__c
					FROM TMS__Task__c
					WHERE TMS__Project__c = :projectTemplateId];
			*/

			/* Added by Aliaksandr Satskou, 12/12/2012 */
			/* Note: Select all fields from TMS__Task__c. */
			Map<String, Schema.SObjectField> v_taskFieldApiNameSObjectFieldMap =
					Schema.SObjectType.TMS__Task__c.fields.getMap();

			String v_whatSelect = '';

			for (String v_taskFieldApiName : v_taskFieldApiNameSObjectFieldMap.keySet()) {
				v_whatSelect += ', ' + v_taskFieldApiName;
			}
			v_whatSelect = (v_whatSelect != '')
					? v_whatSelect.substring(2)
					: 'Id';

			String v_query = 'SELECT ' + v_whatSelect + ' FROM TMS__Task__c WHERE TMS__Project__c = \'' +
					projectTemplateId + '\'';

			List<TMS__Task__c> taskTemplateList = DataBase.query(v_query);
			
			
			/* Added by Aliaksandr Satskou, 08.30.2013 (case #00020089) */
			Map<Id, TMS__Task__c> templateTaskMap = new Map<Id, TMS__Task__c>();
			
			for (TMS__Task__c templateTask : taskTemplateList) {
				templateTaskMap.put(templateTask.Id, templateTask);
			}

			
			if (!isOpportunity) {
				getPlacementWithBillRateAndPayRate(taskTemplateList, p_placementOrOpportunityId);
			}
			/* --- */

			List<TMS__Project_Resource__c> projectResourceTemplateList = [
					SELECT Name, TMS__Bill_Rate__c, TMS__Project__c, TMS__Contact__r.Name
					FROM TMS__Project_Resource__c
					WHERE TMS__Project__c = :projectTemplateId];

			List<TMS__Project_Resource__c> projectResourceWithTasksList = [
					SELECT (SELECT Id FROM TMS__Tasks__r)
					FROM TMS__Project_Resource__c
					WHERE TMS__Project__c = :projectId];

			if (projectResourceTemplateList != null && projectResourceTemplateList.size() > 0)
			{
				Map<String, TMS__Project_Resource__c> projectResourceNameMap = new Map<String, TMS__Project_Resource__c>();

				for(TMS__Project_Resource__c projectResource : projectResourceListForCheckDuplicat)
					projectResourceNameMap.put(projectResource.TMS__Contact__r.Name, projectResource);

				List<TMS__Project_Resource__c> templateResourceListForProject = new List<TMS__Project_Resource__c>();
				List<TMS__Project_Resource__c> projectResourceListForProject = new List<TMS__Project_Resource__c>();

				for(TMS__Project_Resource__c  projectResourceTemplate :  projectResourceTemplateList)
				{
					templateResourceListForProject.add(projectResourceTemplate);
					if(projectResourceNameMap.get(projectResourceTemplate.TMS__Contact__r.Name) == null)
					{
						TMS__Project_Resource__c projectResourceNew = projectResourceTemplate.clone(false,false);
						projectResourceNew.TMS__Project__c = projectId;
						projectResourceListForProject.add(projectResourceNew);
					}
					else
					{
						projectResourceListForProject.add(projectResourceNameMap.get(projectResourceTemplate.TMS__Contact__r.Name));
					}
				}
				upsert projectResourceListForProject;

				for (Integer i=0; i<templateResourceListForProject.size(); i++)
					mapTempResToRealRes.put(templateResourceListForProject[i].Id, projectResourceListForProject[i].Id);

			}
			

			if (taskTemplateList != null && taskTemplateList.size() > 0)
			{
				Set<String> projectTaskNames = new Set<String>();
				for (TMS__Task__c projectTask : projectTaskList) {
					projectTaskNames.add(projectTask.Name);
				}

				List<TMS__Task__c> taskWithResourceList = new List<TMS__Task__c>();

				for (TMS__Project_Resource__c projectResourceWithTasks : projectResourceWithTasksList) {
					if (projectResourceWithTasks.TMS__Tasks__r.size() == 0) {
						for (TMS__Task__c templateTask : taskTemplateList) {
							if (templateTask.TMS__Project_Resource__c == null) {
								TMS__Task__c task = templateTask.clone(false, false);
								task.TMS__Project__c = projectId;
								task.TMS__Project_Resource__c = projectResourceWithTasks.Id;

								/* Added by Aliaksandr Satskou, 12/13/2012 */
								Object v_billRateFromPlacement = getPlacementFieldValue(
												templateTask.TMS__Bill_Rate_Field_From_Placement__c);
								task.TMS__Bill_Rate__c = (v_billRateFromPlacement != null)
										? decimal.valueOf('' + v_billRateFromPlacement)
										: 0;

								Object v_payRateFromPlacement = getPlacementFieldValue(
												templateTask.TMS__Pay_Rate_Field_From_Placement__c);
								task.TMS__Pay_Rate__c = (v_payRateFromPlacement != null)
										? decimal.valueOf('' + v_payRateFromPlacement)
										: 0;
								/* --- */

								taskWithResourceList.add(task);
							}
						}
					}
				}

				Boolean hasProjectResourcesWithoutTasks = taskWithResourceList.size() > 0;

				for (TMS__Task__c templateTask : taskTemplateList) {
					if (!projectTaskNames.contains(templateTask.Name)
							&& (
									templateTask.TMS__Project_Resource__c != null 
									|| !hasProjectResourcesWithoutTasks)) {
						TMS__Task__c task = templateTask.clone(false, false);
						task.TMS__Project__c = projectId;
						task.TMS__Project_Resource__c = mapTempResToRealRes.get(
								templateTask.TMS__Project_Resource__c);

						/* Added by Aliaksandr Satskou, 12/13/2012 */
						Object v_billRateFromPlacement = getPlacementFieldValue(
										templateTask.TMS__Bill_Rate_Field_From_Placement__c);
						task.TMS__Bill_Rate__c = (v_billRateFromPlacement != null)
								? decimal.valueOf('' + v_billRateFromPlacement)
								: 0;

						Object v_payRateFromPlacement = getPlacementFieldValue(
										templateTask.TMS__Pay_Rate_Field_From_Placement__c);
						task.TMS__Pay_Rate__c = (v_payRateFromPlacement != null)
								? decimal.valueOf('' + v_payRateFromPlacement)
								: 0;

						taskWithResourceList.add(task);
					}
				}

				insert taskWithResourceList;
				
				
				/* Added by Aliaksandr Satskou, 08.30.2013 (case #00020089) */
				Boolean isDisableAutomatingOvertimeLookup = 
						(Boolean)CustomSettingsUtility.getCustomSettingListTypeValue(
								null, 'TMS__Disable_Automating_Overtime_Lookup__c');
				
				if ((isDisableAutomatingOvertimeLookup != null) && (!isDisableAutomatingOvertimeLookup)) {
					Map<Id, List<TMS__Task__c>> projectResourceIdTaskListMap = new Map<Id, List<TMS__Task__c>>();
					
					for (TMS__Task__c task : taskWithResourceList) {
						List<TMS__Task__c> taskList = projectResourceIdTaskListMap.get(task.TMS__Project_Resource__c);
						
						if (taskList != null) {
							taskList.add(task);
						} else {
							projectResourceIdTaskListMap.put(task.TMS__Project_Resource__c, new List<TMS__Task__c> { task });
						}
					}
					
					
					List<TMS__Task__c> updateTaskList = new List<TMS__Task__c>();
					
					for (Id projectResourceId : projectResourceIdTaskListMap.keySet()) {
						for (Id templateTaskId : templateTaskMap.keySet()) {
							TMS__Task__c templateTask = templateTaskMap.get(templateTaskId);
							
							for (TMS__Task__c task : projectResourceIdTaskListMap.get(projectResourceId)) {
								if (task.Name == templateTask.Name) {
									Id owTaskId = templateTask.TMS__Overtime_overflow_task__c;
									
									if (owTaskId != null) {
										String owTaskName = templateTaskMap.get(owTaskId).Name;
										
										for (TMS__Task__c owTask : projectResourceIdTaskListMap.get(projectResourceId)) {
											if (owTask.Name == owTaskName) {
												task.TMS__Overtime_overflow_task__c = owTask.Id;
												updateTaskList.add(task);
												
												break;
											}
										}
										
										break;
									}
								}
							}
						}
					}
					
					update updateTaskList;
				}
			}
		}
	}

	private static testmethod void AddExistProjectTemplateInProjectTest(){

		Map<String,Schema.RecordTypeInfo> recordTypes =
				TMS__Project__c.sObjectType.getDescribe().getRecordTypeInfosByName();

		Opportunity testOpportunity = new Opportunity(
				Name = 'testTMSProjectTemplate',
				CloseDate = date.today(),
				StageName = 'Closed Won');
		insert testOpportunity;

		TMS__Project__c testTMSProjectTeplate = new TMS__Project__c(
				Name = 'testTMSProjectTemplate',
				RecordTypeId = recordTypes.get('Project Template').getRecordTypeId(),
				TMS__Opportunity__c = testOpportunity.Id);
		insert testTMSProjectTeplate;

		TMS__Project__c testTMSProject = new TMS__Project__c(
				Name = 'testTMSProjectTemplate',
				RecordTypeId = recordTypes.get('Project').getRecordTypeId(),
				TMS__Opportunity__c = testOpportunity.Id);
		insert testTMSProject;

		Contact testCon = new Contact(LastName = 'testCon', Email = 'test@test.test');
		insert testCon;

		TMS__Project_Resource__c testTMSProjRes = new TMS__Project_Resource__c(
				TMS__Bill_Rate__c = 10.00,
				TMS__Project__c = testTMSProjectTeplate.Id,
				TMS__Contact__c = testCon.Id);
		insert testTMSProjRes;

		TMS__Task__c testTMSTack = new TMS__Task__c(
				Name = 'testTMSTask',
				TMS__Allocated_Hours__c = 10.0,
				TMS__Sequence__c = '1',
				TMS__Project__c = testTMSProjectTeplate.Id,
				TMS__Project_Resource__c = testTMSProjRes.Id);
		insert testTMSTack;

		AddExistProjectTemplateInProject.checkIdForPlacementOrOpportunity(
				testTMSProject.Id, testTMSProject.Name,
				String.valueOf(testOpportunity.Id));
	}
}