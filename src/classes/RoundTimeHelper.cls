public without sharing class RoundTimeHelper {
	private static TMS__TimesheetCustomSettingsHierarchy__c cs = 
			TMS__TimesheetCustomSettingsHierarchy__c.getInstance();
	
	private static String WEEKEND_DIFFERENTIAL_TASK_NAME = Label.Weekend_Differential_Task_Suffix;

	private static Map<Id, TMS__Task__c> taskMap;
	private static Map<Object, List<TMS__Task__c>> projectIdToDifferentialTasksMap;
	private static Map<Object, sObject> parentTaskIdToDifferentialTaskMap;

	private static List<TMS__Time__c> timesToUpdate = new List<TMS__Time__c>();
	private static List<TMS__Time__c> timesToDelete = new List<TMS__Time__c>();
	private static List<TMS__Time__c> timesToInsert = new List<TMS__Time__c>();

	private static Boolean alreadyRunRound = false;
	private static Boolean alreadyRunSplit = false;

	private static Logger logger = new Logger('RoundTimeHelper');

	public static void before(List<TMS__Time__c> times) {
		logger.log('before', 'cs', cs);
		
		if (!alreadyRunRound) {
			alreadyRunRound = true;

			getTasksAndProjects(times);

			for (TMS__Time__c tm : times) {
				if (taskMap.get(tm.TMS__Task__c).TMS__Enable_Time_Tracking__c == true
						&& String.isNotBlank(tm.TMS__Start_Time__c)
						&& String.isNotBlank(tm.TMS__End_Time__c)) {
					RoundTimeHelper.setTimeSpent(tm, taskMap.get(tm.TMS__Task__c), true);
					
					if (cs.TMS__Rounding_Enabled__c == true) {
						RoundTimeHelper.roundIt(tm);
					}
					
					if (cs.TMS__Start_Week_Enabled__c == true) {
						RoundTimeHelper.setPreviousWeekTime(tm);
					}
					
					if (cs.TMS__Minimum_Hours_Enabled__c == true) {
						RoundTimeHelper.setMinimumHours(tm, taskMap.get(tm.TMS__Task__c));
					}
					
					RoundTimeHelper.setTimeSpent(tm, taskMap.get(tm.TMS__Task__c), false);
				}
			}
		}
	}

	public static void after(List<TMS__Time__c> times) {
		if (!alreadyRunSplit && cs.TMS__Weekend_Differential_Enabled__c == true) {
			alreadyRunSplit = true;

			for (TMS__Time__c tm : times) {
				if (taskMap.get(tm.TMS__Task__c).TMS__Enable_Time_Tracking__c == true
						&& String.isNotBlank(tm.TMS__Start_Time__c)
						&& String.isNotBlank(tm.TMS__End_Time__c)) {
					RoundTimeHelper.splitDifferentialForTime(tm);
				}
			}

			for (TMS__Time__c tm : timesToUpdate) {
				RoundTimeHelper.setTimeSpent(tm, taskMap.get(tm.TMS__Task__c), false);
			}

			for (TMS__Time__c tm : timesToInsert) {
				RoundTimeHelper.setTimeSpent(tm, taskMap.get(tm.TMS__Task__c), false);
			}

			update timesToUpdate;
			delete timesToDelete;
			insert timesToInsert;
		}
	}

	private static void getTasksAndProjects(List<TMS__Time__c> times) {
		taskMap = new Map<Id, TMS__Task__c>([
				SELECT Name, TMS__Task_data_type__c, TMS__Enable_Time_Tracking__c, 
						TMS__Project__c, TMS__Project_Resource__c, TMS__Minimum_Hours__c,
						TMS__Weekend_Differential_Parent__c,
						TMS__Weekend_Differential_Start_Weekday__c,
						TMS__Weekend_Differential_Start_Time_Hour__c,
						TMS__Weekend_Differential_Start_Time_Minute__c,
						TMS__Weekend_Differential_End_Weekday__c,
						TMS__Weekend_Differential_End_Time_Hour__c,
						TMS__Weekend_Differential_End_Time_Minute__c,
						TMS__Start_Week_Hours__c, TMS__Start_Week_Minutes__c,
						TMS__Rounding_Minutes__c, TMS__Rounding_Breakpoint__c
				FROM TMS__Task__c
				WHERE Id IN :GroupByHelper.getFieldValuesIds(times, 'TMS__Task__c')]);
				
		List<Date> dates = GroupByHelper.getFieldValuesDates(times, 'TMS__Date__c');

		List<TMS__Task__c> allDifferentialTasksOfProjects = [
				SELECT Name, TMS__Task_data_type__c, TMS__Enable_Time_Tracking__c, 
						TMS__Project__c, TMS__Project_Resource__c, TMS__Minimum_Hours__c,
						TMS__Weekend_Differential_Parent__c,
						TMS__Weekend_Differential_Start_Weekday__c,
						TMS__Weekend_Differential_Start_Time_Hour__c,
						TMS__Weekend_Differential_Start_Time_Minute__c,
						TMS__Weekend_Differential_End_Weekday__c,
						TMS__Weekend_Differential_End_Time_Hour__c,
						TMS__Weekend_Differential_End_Time_Minute__c,
						TMS__Start_Week_Hours__c, TMS__Start_Week_Minutes__c,
						TMS__Rounding_Minutes__c, TMS__Rounding_Breakpoint__c,
						(SELECT TMS__Date__c FROM TMS__Time__r WHERE TMS__Date__c IN :dates)
				FROM TMS__Task__c
				WHERE Name LIKE :'% - ' + WEEKEND_DIFFERENTIAL_TASK_NAME
					AND TMS__Project__c IN :GroupByHelper.getFieldValuesIds(
							taskMap.values(), 'TMS__Project__c')];

		projectIdToDifferentialTasksMap = 
				(Map<Object, List<TMS__Task__c>>)GroupByHelper.groupByField(
						allDifferentialTasksOfProjects, 'TMS__Project__c');
				
		parentTaskIdToDifferentialTaskMap = 
				(Map<Object, sObject>)GroupByHelper.groupByFieldReturnSingle(
						allDifferentialTasksOfProjects, 'TMS__Weekend_Differential_Parent__c');
	}

	private static void setPreviousWeekTime(TMS__Time__c tm) {
		if (isWeekendDifferential(tm)) { return; }
		
		Datetime startDateTime = getDateTime(tm.TMS__Date__c, tm.TMS__Start_Time__c);
		
		TMS__Task__c task = taskMap.get(tm.TMS__Task__c);
		
		Time START_WEEK_TIME = Time.newInstance(
				task.TMS__Start_Week_Hours__c != null 
						? Integer.valueOf(task.TMS__Start_Week_Hours__c) 
						: Integer.valueOf(cs.TMS__Start_Week_Hours__c), 
				task.TMS__Start_Week_Minutes__c != null 
						? Integer.valueOf(task.TMS__Start_Week_Minutes__c) 
						: Integer.valueOf(cs.TMS__Start_Week_Minutes__c), 
				0, 0);
		
		logger.log('setPreviousWeekTime', 'START_WEEK_TIME', START_WEEK_TIME);
		logger.log('setPreviousWeekTime', 'startDateTime.time()', startDateTime.time());
		logger.log('setPreviousWeekTime', 'startDateTime.format(u)', startDateTime.format('u'));
		
		tm.TMS__Previous_Week__c = 
				startDateTime.format('u') == '1' && startDateTime.time() < START_WEEK_TIME;
		
		logger.log('setPreviousWeekTime', 'tm.TMS__Previous_Week__c', tm.TMS__Previous_Week__c);
	}

	private static void setMinimumHours(TMS__Time__c tm, TMS__Task__c task) {
		TMS__Task__c differentialTask = 
				(TMS__Task__c)parentTaskIdToDifferentialTaskMap.get(task.Id);
		
		logger.log('setMinimumHours', 'differentialTask', differentialTask);
		
		TMS__Time__c differentialTime;
		if (differentialTask != null) {
			differentialTime = (TMS__Time__c)SearchForHelper.searchByFieldReturnSingle(
					differentialTask.TMS__Time__r, 'TMS__Date__c', tm.TMS__Date__c);
		}
		
		logger.log('setMinimumHours', 'differentialTime', differentialTime);
		
		if (task.TMS__Minimum_Hours__c != null 
				&& differentialTime == null) {
			
			Datetime startDateTime = getDateTime(tm.TMS__Date__c, tm.TMS__Start_Time__c);
			Datetime endDateTime = getDateTime(tm.TMS__Date__c, tm.TMS__End_Time__c);
			Integer breakMinutes = String.isNotBlank(tm.TMS__Break__c)
					? getTimeInMinutes(tm.TMS__Break__c) 
					: 0;

			logger.log('setMinimumHours', 'endDateTime', endDateTime);
			logger.log('setMinimumHours', 'startDateTime', startDateTime);
			
			logger.log('setMinimumHours', 'endDateTime.getTime()', endDateTime.getTime());
			logger.log('setMinimumHours', 'startDateTime.getTime()', startDateTime.getTime());
			logger.log('setMinimumHours', 'breakMinutes', breakMinutes);
			
			Long spentMinutes = 
					endDateTime.getTime() / 1000 / 60 - 
					startDateTime.getTime() / 1000 / 60 - 
					breakMinutes;
					
			logger.log('setMinimumHours', 'spentMinutes', spentMinutes);
					
			Long spentHours = spentMinutes / 60;

			logger.log('setMinimumHours', 'spentHours', spentHours);

			if (spentHours < task.TMS__Minimum_Hours__c) {
				endDateTime = startDateTime.addMinutes(
						Integer.valueOf(task.TMS__Minimum_Hours__c * 60 + breakMinutes));
				tm.TMS__End_Time__c = endDateTime.format('hh:mm a').toLowerCase();
			}
		}
	}

	private static void splitDifferentialForTime(TMS__Time__c tm) {
		tm = [
				SELECT TMS__Start_Time__c, TMS__End_Time__c, TMS__Break__c, 
						TMS__Task__c, TMS__Date__c 
				FROM TMS__Time__c 
				WHERE Id = :tm.Id];
		
		TMS__Task__c origTask = taskMap.get(tm.TMS__Task__c);
		
		if (isWeekendDifferential(tm)) { return; }
		
		logger.log('splitDifferentialForTime', 'tm.TMS__Date__c', tm.TMS__Date__c);

		Integer weekday = Integer.valueOf( 
				DateTime.newInstance(tm.TMS__Date__c, Time.newInstance(0, 0, 0, 0)).format('u'));

		logger.log('splitDifferentialForTime', 'weekday', weekday);
		
		Integer WEEKEND_DIFFERENTIAL_START_WEEKDAY = 
				origTask.TMS__Weekend_Differential_Start_Weekday__c != null 
						? Integer.valueOf(origTask.TMS__Weekend_Differential_Start_Weekday__c) 
						: Integer.valueOf(cs.TMS__Weekend_Differential_Start_Weekday__c);
		Time WEEKEND_DIFFERENTIAL_START_TIME = Time.newInstance(
				origTask.TMS__Weekend_Differential_Start_Time_Hour__c != null 
						? Integer.valueOf(origTask.TMS__Weekend_Differential_Start_Time_Hour__c) 
						: Integer.valueOf(cs.TMS__Weekend_Differential_Start_Time_Hour__c), 
				origTask.TMS__Weekend_Differential_Start_Time_Minute__c != null 
						? Integer.valueOf(origTask.TMS__Weekend_Differential_Start_Time_Minute__c) 
						: Integer.valueOf(cs.TMS__Weekend_Differential_Start_Time_Minute__c), 
				0, 0);
		Integer WEEKEND_DIFFERENTIAL_END_WEEKDAY = 
				origTask.TMS__Weekend_Differential_End_Weekday__c != null 
						? Integer.valueOf(origTask.TMS__Weekend_Differential_End_Weekday__c) 
						: Integer.valueOf(cs.TMS__Weekend_Differential_End_Weekday__c);
		Time WEEKEND_DIFFERENTIAL_END_TIME = Time.newInstance(
				origTask.TMS__Weekend_Differential_End_Time_Hour__c != null 
						? Integer.valueOf(origTask.TMS__Weekend_Differential_End_Time_Hour__c) 
						: Integer.valueOf(cs.TMS__Weekend_Differential_End_Time_Hour__c), 
				origTask.TMS__Weekend_Differential_End_Time_Minute__c != null 
						? Integer.valueOf(origTask.TMS__Weekend_Differential_End_Time_Minute__c) 
						: Integer.valueOf(cs.TMS__Weekend_Differential_End_Time_Minute__c), 
				0, 0);

		Datetime weekendDifferentialStart = DateTime.newInstance(
				tm.TMS__Date__c.addDays(WEEKEND_DIFFERENTIAL_START_WEEKDAY - weekday), 
				WEEKEND_DIFFERENTIAL_START_TIME);

		logger.log('splitDifferentialForTime', 'weekendDifferentialStart', weekendDifferentialStart);

		Datetime weekendDifferentialEnd = DateTime.newInstance(
				tm.TMS__Date__c.addDays(WEEKEND_DIFFERENTIAL_END_WEEKDAY - weekday), 
				WEEKEND_DIFFERENTIAL_END_TIME);

		logger.log('splitDifferentialForTime', 'weekendDifferentialEnd', weekendDifferentialEnd);
		
		Datetime startDateTime = getDateTime(tm.TMS__Date__c, tm.TMS__Start_Time__c);
		Datetime endDateTime = getDateTime(tm.TMS__Date__c, tm.TMS__End_Time__c);

		logger.log('splitDifferentialForTime', 'startDateTime', startDateTime);
		logger.log('splitDifferentialForTime', 'endDateTime', endDateTime);

		SplitResult splitResult = split(
				startDateTime, endDateTime, 
				weekendDifferentialStart, weekendDifferentialEnd);

		logger.log('splitDifferentialForTime', 
				'splitResult.startDateTimeBefore', splitResult.startDateTimeBefore);
		logger.log('splitDifferentialForTime', 
				'splitResult.endDateTimeBefore', splitResult.endDateTimeBefore);
		logger.log('splitDifferentialForTime', 
				'splitResult.startDateTimeAfter', splitResult.startDateTimeAfter);
		logger.log('splitDifferentialForTime', 
				'splitResult.endDateTimeAfter', splitResult.endDateTimeAfter);
		logger.log('splitDifferentialForTime', 
				'splitResult.startDateTimeBetween', splitResult.startDateTimeBetween);
		logger.log('splitDifferentialForTime', 
				'splitResult.endDateTimeBetween', splitResult.endDateTimeBetween);

		if (splitResult.startDateTimeBefore == null) { return; }

		if (splitResult.startDateTimeBefore.date() == splitResult.endDateTimeBefore.date()) {
			tm.TMS__End_Time__c = splitResult.endDateTimeBefore.format('hh:mm a').toLowerCase();
			timesToUpdate.add(tm);
		} else if (splitResult.startDateTimeAfter.date() == splitResult.endDateTimeAfter.date()) {
			tm.TMS__Start_Time__c = splitResult.startDateTimeAfter.format('hh:mm a').toLowerCase();
			timesToUpdate.add(tm);
		} else {
			timesToDelete.add(tm);
		}
		
		TMS__Task__c differentialTask;
		TMS__Time__c differentialTime;

		List<TMS__Task__c> differentialTasksOfCurrentProject = 
				projectIdToDifferentialTasksMap.get(origTask.TMS__Project__c);
		if (differentialTasksOfCurrentProject == null) {
			differentialTask = createDifferentialTask(origTask);
		} else {
			for (TMS__Task__c task : differentialTasksOfCurrentProject) {
				if (task.Name == origTask.Name + ' - ' + WEEKEND_DIFFERENTIAL_TASK_NAME) {
					differentialTask = task;

					Map<Object, List<TMS__Time__c>> dateToTime = 
							(Map<Object, List<TMS__Time__c>>)GroupByHelper.groupByField(
									differentialTask.TMS__Time__r, 'TMS__Date__c');

					logger.log('splitDifferentialForTime', 'differentialTask.TMS__Time__r', differentialTask.TMS__Time__r);
					logger.log('splitDifferentialForTime', 'dateToTime', dateToTime);

					if (dateToTime.get(tm.TMS__Date__c) != null) {
						differentialTime = dateToTime.get(tm.TMS__Date__c)[0];
					}

					break;
				}
			}

			if (differentialTask == null) {
				differentialTask = createDifferentialTask(origTask);
			}
		}

		taskMap.put(differentialTask.Id, differentialTask);

		if (differentialTime == null) {
			differentialTime = new TMS__Time__c();
			timesToInsert.add(differentialTime);
		} else {
			timesToUpdate.add(differentialTime);
		}

		differentialTime.TMS__Date__c = tm.TMS__Date__c;
		differentialTime.TMS__Start_Time__c = 
				splitResult.startDateTimeBetween.format('hh:mm a').toLowerCase();
		differentialTime.TMS__End_Time__c = 
				splitResult.endDateTimeBetween.format('hh:mm a').toLowerCase();
		differentialTime.TMS__Task__c = differentialTask.Id;
	}
	
	private static Boolean isWeekendDifferential(TMS__Time__c tm) {
		TMS__Task__c task = taskMap.get(tm.TMS__Task__c);
		
		logger.log('isWeekendDifferential', 'task', task);
		
		return task.TMS__Weekend_Differential_Parent__c != null;
	}

	private static TMS__Task__c createDifferentialTask(TMS__Task__c origTask) {
		TMS__Task__c differentialTask = new TMS__Task__c(
				Name = origTask.Name + ' - ' + WEEKEND_DIFFERENTIAL_TASK_NAME,
				TMS__Project__c = origTask.TMS__Project__c,
				TMS__Project_Resource__c = origTask.TMS__Project_Resource__c,
				TMS__Task_data_type__c = origTask.TMS__Task_data_type__c,
				TMS__Enable_Time_Tracking__c = origTask.TMS__Enable_Time_Tracking__c,
				TMS__Weekend_Differential_Parent__c = origTask.Id);

		insert differentialTask;

		List<TMS__Task__c> differentialTasksOfCurrentProject = 
				projectIdToDifferentialTasksMap.get(origTask.TMS__Project__c);
		if (differentialTasksOfCurrentProject == null) {
			projectIdToDifferentialTasksMap.put(
					origTask.TMS__Project__c, new List<TMS__Task__c> { differentialTask });
		} else {
			differentialTasksOfCurrentProject.add(differentialTask);
		}

		return differentialTask;
	}

	private static Datetime getDateTime(Date dt, String timeString) {
		logger.log('getDateTime', 'dt', dt);
		logger.log('getDateTime', 'timeString', timeString);
		
		Integer timeInMunites = getTimeInMinutes(timeString);
		
		logger.log('getDateTime', 'timeInMunites', timeInMunites);

		return DateTime.newInstance(
				dt, Time.newInstance(timeInMunites / 60, math.mod(timeInMunites, 60), 0, 0));
	}

	private static SplitResult split(
			Datetime startDateTime, Datetime endDateTime, 
			Datetime weekendDifferentialStart, Datetime weekendDifferentialEnd) {

		SplitResult splitResult = new SplitResult();

		if (weekendDifferentialStart <= startDateTime && weekendDifferentialEnd > startDateTime
			|| weekendDifferentialStart < endDateTime && weekendDifferentialEnd >= endDateTime 
			|| weekendDifferentialStart >= startDateTime && weekendDifferentialEnd <= endDateTime
			|| weekendDifferentialStart <= startDateTime && weekendDifferentialEnd >= endDateTime) {

			if (startDateTime >= weekendDifferentialStart) {
				splitResult.startDateTimeBetween = startDateTime;
			} else {
				splitResult.startDateTimeBetween = weekendDifferentialStart;
			}
			
			if (weekendDifferentialEnd >= endDateTime) {
				splitResult.endDateTimeBetween = endDateTime;
			} else {
				splitResult.endDateTimeBetween = weekendDifferentialEnd;
			}
			
			if (startDateTime >= weekendDifferentialStart) {
				splitResult.startDateTimeBefore = weekendDifferentialStart;
				splitResult.endDateTimeBefore = startDateTime;
			} else {
				splitResult.startDateTimeBefore = startDateTime;
				splitResult.endDateTimeBefore = weekendDifferentialStart;
			}
			
			if (weekendDifferentialEnd >= endDateTime) {
				splitResult.startDateTimeAfter = endDateTime;
				splitResult.endDateTimeAfter = weekendDifferentialEnd;
			} else {
				splitResult.startDateTimeAfter = weekendDifferentialEnd;
				splitResult.endDateTimeAfter = endDateTime;
			}
		}

		return splitResult;
	}

	private static void roundIt(TMS__Time__c tm) {
		if (isWeekendDifferential(tm)) { return; }
		
		tm.TMS__Start_Time__c = 
				roundTime(tm.TMS__Start_Time__c, true, taskMap.get(tm.TMS__Task__c));
		tm.TMS__End_Time__c = 
				roundTime(tm.TMS__End_Time__c, false, taskMap.get(tm.TMS__Task__c));
	}

	private static void setTimeSpent(TMS__Time__c tm, TMS__Task__c task, 
			Boolean forTypedTimeField) {

		logger.log('setTimeSpent', 'tm', tm);
		logger.log('setTimeSpent', 'task', task);

		Integer v_startTime = getTimeInMinutes(tm.TMS__Start_Time__c);
		Integer v_endTime = getTimeInMinutes(tm.TMS__End_Time__c);
		Integer v_breakTime = getTimeInMinutes(tm.TMS__Break__c);

		logger.log('setTimeSpent', 'tm.TMS__Start_Time__c', tm.TMS__Start_Time__c);
		logger.log('setTimeSpent', 'tm.TMS__End_Time__c', tm.TMS__End_Time__c);
		logger.log('setTimeSpent', 'tm.TMS__Break__c', tm.TMS__Break__c);

		logger.log('setTimeSpent', 'v_startTime', v_startTime);
		logger.log('setTimeSpent', 'v_endTime', v_endTime);

		Integer v_dayTimeSpent = null;

		if ((v_startTime != null) && (v_endTime != null)) {
			v_breakTime = (v_breakTime == null)
					? 0
					: v_breakTime;

			if (v_endTime > v_startTime) {
				v_dayTimeSpent = v_endTime - v_startTime - v_breakTime;
			} else {
				v_dayTimeSpent = 24*60 - (v_startTime - v_endTime) - v_breakTime;
			}
		}

		if (task.TMS__Task_data_type__c == 'Number') {
			if (forTypedTimeField) {
				tm.TMS__Typed_Time_Spent_Number_Format__c = 
						(Decimal)getTimeByMinutes(v_dayTimeSpent, false);
			} else {
				tm.TMS__Time_Spent__c = 
						(Decimal)getTimeByMinutes(v_dayTimeSpent, false);
			}
		} else if (task.TMS__Task_data_type__c == 'Time') {
			if (forTypedTimeField) {
				tm.TMS__Typed_Time_Spent_Time_Format__c = 
						(String)getTimeByMinutes(v_dayTimeSpent, true);
			} else {
				tm.TMS__Time_Spent_Time_Format__c = 
						(String)getTimeByMinutes(v_dayTimeSpent, true);
			}
		}

		logger.log('setTimeSpent', 'forTypedTimeField', forTypedTimeField);
		logger.log('setTimeSpent', 'tm', tm);
	}

	private static String roundTime(String p_timeString, Boolean isIn, TMS__Task__c task) {
		logger.log('roundTime', 'p_timeString', p_timeString);
		logger.log('roundTime', 'isIn', isIn);
		
		Integer v_hour = Integer.valueOf(p_timeString.substring(0, 2));
		Integer v_min = Integer.valueOf(p_timeString.substring(3, 5));

		String v_timePrefix = null;
		if (p_timeString.length() > 5) {
			v_timePrefix = p_timeString.substring(6, 8);
		}
		
		Integer ROUNDING_MINUTES = 
				task.TMS__Rounding_Minutes__c != null 
						? Integer.valueOf(task.TMS__Rounding_Minutes__c) 
						: Integer.valueOf(cs.TMS__Rounding_Minutes__c);
		Integer ROUNDING_BREAKPOINT = 
				task.TMS__Rounding_Breakpoint__c != null 
						? Integer.valueOf(task.TMS__Rounding_Breakpoint__c) 
						: Integer.valueOf(cs.TMS__Rounding_Breakpoint__c);

		Integer multiplier = v_min / ROUNDING_MINUTES;
		Integer intervalStart = multiplier * ROUNDING_MINUTES;
		Integer intervalEnd = (multiplier + 1) * ROUNDING_MINUTES;
		
		if (isIn) {
			if(v_min < (intervalStart + ROUNDING_BREAKPOINT)) {
				logger.log('roundTime', '1');
				v_min = intervalStart;
			} else {
				logger.log('roundTime', '2');
				v_min = intervalEnd;
			}
		}
		
		logger.log('roundTime', 'v_min', v_min);
		
		if (!isIn) { 
			if (v_min > (intervalEnd - ROUNDING_BREAKPOINT)) {
				logger.log('roundTime', '3');
				v_min = intervalEnd;
			} else {
				logger.log('roundTime', '4');
				v_min = intervalStart;
			}
		}
		
		logger.log('roundTime', 'v_min', v_min);
		
		if (v_min >= 60) {
			v_hour++;
			v_min = v_min - 60;
		}

		return padLeft(v_hour, 2, '0') + ':' + padLeft(v_min, 2, '0') + 
			(v_timePrefix != null ? (' ' + v_timePrefix) : '');
	}

	private static String padLeft(Integer num, Integer count, String padLetter) {
		String text = String.valueOf(num);
		while (text.length() < count) {
			text = padLetter + text; 
		}
		return text;
	}

	private static Integer getTimeInMinutes(String p_timeString) {
		Integer v_timeSpent = null;

		if (p_timeString != null) {
			Integer v_hour = Integer.valueOf(p_timeString.substring(0, 2));
			Integer v_min = Integer.valueOf(p_timeString.substring(3, 5));

			if (p_timeString.length() > 5) {
				String v_timePrefix = p_timeString.substring(6, 8);

				if (v_timePrefix.toLowerCase() == 'am') {
					if (v_hour != 12) {
						v_timeSpent = v_hour*60 + v_min;
					} else {
						v_timeSpent = 12*60 + v_hour*60 + v_min;
					}
				}

				if (v_timePrefix.toLowerCase() == 'pm') {
					if (v_hour != 12) {
						v_timeSpent = 12*60 + v_hour*60 + v_min;
					} else {
						v_timeSpent = v_hour*60 + v_min;
					}
				}
			} else {
				v_timeSpent = v_hour*60 + v_min;
			}
		}
		
		return v_timeSpent;
	}

	private static Object getTimeByMinutes(Integer p_timeSpent, Boolean p_timeFormat) {
		Object v_time = null;

		if (p_timeSpent != null) {
			Integer v_hour = 0;
			Integer v_min = 0;

			if (p_timeSpent >= 60) {
				while (p_timeSpent >= 60) {
					p_timeSpent -= 60;
					v_hour++;
				}
				v_min = p_timeSpent;
			} else {
				v_min = p_timeSpent;
			}

			if (p_timeFormat) {
				v_time = '';

				v_time += (v_hour > 9)
						? v_hour + ':'
						: '0' + v_hour + ':';
				v_time += (v_min > 9)
						? v_min + ''
						: '0' + v_min;
			} else {
				v_time = v_hour + Decimal.valueOf(v_min).divide(60, 2);
			}
		}

		return v_time;
	}

	private class SplitResult {
		public Datetime startDateTimeBefore;
		public Datetime endDateTimeBefore;
		public Datetime startDateTimeAfter;
		public Datetime endDateTimeAfter;
		public Datetime startDateTimeBetween;
		public Datetime endDateTimeBetween;
	}
}