global without sharing class Logger {
	private static String messages = '';

	private String className;
	
	public Boolean throwException = false;
	
	global Logger(String className) {
		this.className = className;
	}
	
	global void log(String methodName) {
		addMessage(className, methodName, '');
	}
	
	global void log(String methodName, String variable, Object value) {
		addMessage(className, methodName, variable + ' = ' + value);
	}
	
	global void log(String methodName, String message) {
		addMessage(className, methodName, message);
	}

	global static void addMessage(String className, String methodName, String variable, Object value) {
		addMessage(className, methodName, variable + '=' + value);
	}
	
	global static void addMessage(String className, String methodName, String message) {
		String logMessage =
				'::::::::::' + 
				'Class name: \'' + className +
			 '\' Method name: \'' + methodName +
			 '\' Message: \'' + message + '\'';
		
		System.debug(LoggingLevel.ERROR, logMessage);
		
		//messages += logMessage + '\r\n\r\n';
	}

	global static void catchBlockMethod(Exception e, String subject)
	{
		addErrorMessage(e.getMessage());

		sendLog(e.getMessage(), subject);

		if (Test.isRunningTest()) {
			throw e;
		}
	}
	
	global void catchBlockMethodNew(Exception e)
	{
		addErrorMessage(e.getMessage());

		sendLog(e.getMessage(), className);
		
		if (Test.isRunningTest() || throwException) {
			throw e;
		}
	}

	global static void addErrorMessage(String message)
	{
		// We can show the message if request was done from VF page only.
		// We can't call ApexPages.addMessage if we made request from webservice or trigger.
		if (ApexPages.currentPage() != null)
		{
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, message);
			ApexPages.addMessage(msg);
		}
	}

	global static void sendLog(String lastMessage, String subject)
	{
		messages =
				'Organization: \'' + UserInfo.getOrganizationName() + '\'\r\n' +
				((ApexPages.currentPage() != null) ? ('Page Url: \'' + ApexPages.currentPage().getUrl() + '\'\r\n') : '') +
				((Site.getName() != null) ? ('Site Name: \'' + Site.getName() + '\'\r\n') : '') +
				'\r\n' + messages;

		/*String fullLog = messages + lastMessage;

		Messaging.SingleEmailMessage emailMessage = new Messaging.SingleEmailMessage();
		emailMessage.setSubject(subject);
		emailMessage.setPlainTextBody(fullLog);
		emailMessage.setToAddresses(new List<String> {
				ConfigHelper.getConfigDataSet().AVTRRT__ErrorsReceiver__c });

		Messaging.sendEmail(new Messaging.SingleEmailMessage[] {emailMessage});*/
	}

	private static testmethod void testLogger() {
		addMessage('Test', 'Test', 'Test');
	}
}