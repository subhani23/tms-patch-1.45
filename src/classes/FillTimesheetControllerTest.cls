/**
* @author Aliaksandr Satskou
* @date 12/02/2013
* @description Test class for FillTimesheetController class.
*/
@isTest
private class FillTimesheetControllerTest {
	
	private static List<TMS__Project__c> projectList;
	private static List<TMS__Project_Resource__c> resourceList;
	private static List<Contact> candidateList;
	
	
	/**
	* @author Aliaksandr Satskou
	* @date 12/02/2013
	* @name initData
	* @description Init data.
	* @return void
	*/
	private static void initData() {
		insert new TMS__TimesheetCustomSettings__c(
			Name = 'ProjectStatusToShow',
			TMS__Fill_Timesheet_Column_Name_Order__c = 'TMS__Contact__r.Name',TMS__TMSProjectStatusToShow__c='open;'
		);
		
		
		candidateList = new List<Contact> {
			new Contact(
				LastName = 'Candidate #1'
			),
			new Contact(
				LastName = 'Candidate #2'
			),
			new Contact(
				LastName = 'Candidate #3'
			),
			new Contact(
				LastName = 'Candidate #4'
			)
		};
		insert candidateList;
		
		
		projectList = new List<TMS__Project__c> {
			new TMS__Project__c(
				Name = 'Project #1',
				TMS__Internal_Approvers__c = UserInfo.getName()
			),
			new TMS__Project__c(
				Name = 'Project #2',
				TMS__Internal_Approvers__c = 'User Name #1'
			)
		};
		insert projectList;
		
		
		resourceList = new List<TMS__Project_Resource__c> {
			new TMS__Project_Resource__c(
				TMS__Project__c = projectList[0].Id,
				TMS__Contact__c = candidateList[0].Id
			),
			new TMS__Project_Resource__c(
				TMS__Project__c = projectList[0].Id,
				TMS__Contact__c = candidateList[1].Id
			),
			new TMS__Project_Resource__c(
				TMS__Project__c = projectList[0].Id,
				TMS__Contact__c = candidateList[2].Id
			),
			new TMS__Project_Resource__c(
				TMS__Project__c = projectList[1].Id,
				TMS__Contact__c = candidateList[3].Id
			)
		};
		insert resourceList;
	}
	
	
	/**
	* @author Aliaksandr Satskou
	* @date 12/02/2013
	* @name testFillTimesheetController
	* @description Testing FillTimesheetController class.
	* @return void
	*/
    static testMethod void testFillTimesheetController() {
        initData();
        
        FillTimesheetController controller = new FillTimesheetController();
        controller.resourceId = resourceList[0].Id;
        //controller.login();
    }
}