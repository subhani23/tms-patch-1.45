/**
* @author Aliaksandr Satskou
* @date 03/21/2013
* @description Update Time objects, related to Overtime Task, based on Time objects, related to Regular Task.
*/
public with sharing class OvertimeHelper {
	
	/* Map of Regular Task and Overtime Task objects. */
	private static Map<Id, TMS__Task__c> regularAndOvertimeTaskMap;
	
	/* Map of Overtime Task Id to List of Time objects. */
	private static Map<Id, List<TMS__Time__c>> taskIdTimeListMap;
	
	private static Logger logger = new Logger('OvertimeHelper');
	
	/**
	* @name updateRegularAndOvertimeTaskMapIfNull
	* @author Aliaksandr Satskou
	* @data 04/22/2013
	* @description Updating map of Regular & Overtime Task objects.
	* @param p_taskIdSet Set of Id of Task objects
	* @param p_weekManagement Week Management object
	* @return void
	*/
	private static void updateRegularAndOvertimeTaskMapIfNull(
			Set<Id> p_taskIdSet, TMS__Week_Management__c p_weekManagement) {
		
		/* Getting Task objects with related list of Time objects. */       
		if (regularAndOvertimeTaskMap == null) {
			regularAndOvertimeTaskMap = new Map<Id, TMS__Task__c>([
					SELECT TMS__Overtime_overflow_task__c, TMS__Overtime_hours_threshold__c, 
						TMS__Overtime_calculation_schedule__c,
						TMS__Task_data_type__c, Name, TMS__Overtime_Billable__c, 
						(SELECT Id, TMS__Overtime_overflow_task__c  FROM TMS__OvertimeTasks__r),
						(SELECT TMS__Date__c, TMS__Time_Spent__c, TMS__Time_Spent_Time_Format__c,
							TMS__Status__c
						FROM TMS__Time__r
						WHERE TMS__Date__c >= :p_weekManagement.TMS__Start_Date__c 
						AND TMS__Date__c <= :p_weekManagement.TMS__End_Date__c)
					FROM TMS__Task__c
					WHERE Id IN :p_taskIdSet]);
			
			taskIdTimeListMap = new Map<Id, List<TMS__Time__c>>();
		}
	}
	
	
	/**
	* @name updateOvertime
	* @author Aliaksandr Satskou
	* @data 04/22/2013
	* @description Updating Time objects, related to Overtime Task, based on Time objects, 
	*       related to Regular Task.
	* @param p_taskIdSet Set of Id of Task objects
	* @param p_weekManagement Week Management object
	* @param p_timeStatus Status of Time objects before save
	* @return void
	*/
	public static void updateOvertime(
			Set<Id> p_taskIdSet, TMS__Week_Management__c p_weekManagement, String p_timeStatus) {
		
		/* Checking regularAndOvertimeTaskMap. */
		updateRegularAndOvertimeTaskMapIfNull(p_taskIdSet, p_weekManagement);
		
		List<TMS__Time__c> v_regularTimeList = new List<TMS__Time__c>();
		List<TMS__Time__c> v_overtimeTimeList = new List<TMS__Time__c>();
		//Set<Id> v_overtimeTaskIdSet = new Set<Id>();
		
		for (Id v_taskId : getSortedTaskIdList()) {
			/* Supposedly getting a Regular Task object. */
			TMS__Task__c v_regularTask = regularAndOvertimeTaskMap.get(v_taskId);
			
			/* Checking to see, if Task is Regular. */
			if (!isValidRegularTask(v_regularTask)) { 
				continue; 
			}
			
			/* Getting Overtime Task for our Regular Task. */
			TMS__Task__c v_overtimeTask = 
					regularAndOvertimeTaskMap.get(v_regularTask.TMS__Overtime_overflow_task__c);
			
			WrapperWeek v_regularWeek = getWrapperWeekByTask(v_regularTask, p_weekManagement);
			WrapperWeek v_overtimeWeek = getWrapperWeekByTask(v_overtimeTask, p_weekManagement);
			//v_overtimeWeek.clearWeekDayTimeList();
			
			/* Getting calculation scheduler of Regular Task. */
			String v_calculationScheduler = v_regularTask.TMS__Overtime_calculation_schedule__c;
			
			/* Calculating overtime for Daily scheduler. */
			if (v_calculationScheduler == 'Daily') {           
				v_overtimeWeek = updateOvertimeWrapperWeek(
						v_regularWeek, v_overtimeWeek, v_regularTask.TMS__Overtime_hours_threshold__c);
			}
			
			/* Calculating overtime for Weekly scheduler. */
			if (v_calculationScheduler == 'Weekly') {
				v_overtimeWeek = updateOvertimeWrapperWeek(v_regularWeek, v_overtimeWeek, null);
			}
			
			
			List<TMS__Time__c> v_timeList = new List<TMS__Time__c>();
			String v_overtimeBillable = v_regularWeek.task.TMS__Overtime_Billable__c;
						
			for (TMS__Time__c v_time : v_overtimeWeek.weekDayTimeList) {
				if (v_time.TMS__Date__c != null) {           	
					v_time.TMS__Task__c = v_overtimeWeek.task.Id;
					v_time.TMS__Week_Management__c = p_weekManagement.Id;
					v_time.TMS__Status__c = p_timeStatus;
					
					/* Added by Aliaksandr Satskou, 05/02/2013 (case #00016925) */
					if (v_overtimeBillable == 'Yes') {
						v_time.TMS__Billable__c = true;
					} else if (v_overtimeBillable == 'No') {
						v_time.TMS__Billable__c = false;
					}
					
					v_timeList.add(v_time);
					v_overtimeTimeList.add(v_time);
				}
			}
			
			if (v_timeList.size() > 0) {
				taskIdTimeListMap.put(v_overtimeTask.Id, v_timeList);
				//v_overtimeTaskIdSet.add(v_overtimeTask.Id);
				
				for (TMS__Time__c v_time : v_regularWeek.weekDayTimeList) {
					if (v_time.TMS__Date__c != null) {
						v_time.TMS__Status__c = p_timeStatus;
						
						v_regularTimeList.add(v_time);
					}
				}
			}
		}
		
		/* Deleting Time objects for Overtime Task objects. */
		//delete [SELECT Id FROM TMS__Time__c WHERE TMS__Task__c IN :v_overtimeTaskIdSet];
		
		/* Updating Time objects for Overtime Tasks. */
		//for (TMS__Time__c v_time : v_overtimeTimeList) {
		//    v_time.Id = null;
		//}
		
		taskIdTimeListMap = new Map<Id, List<TMS__Time__c>>();
		
		upsert v_regularTimeList;
		upsert v_overtimeTimeList;
		
		delete [
				SELECT Id
				FROM TMS__Time__c
				WHERE TMS__Task__c IN :regularAndOvertimeTaskMap.keySet()
				AND TMS__Time_Spent_Formula__c = 0];
	}
	
	
	/**
	* @name getSortedTaskIdList
	* @author Aliaksandr Satskou
	* @data 04/10/2013
	* @description Sorting Task objects.
	* @return List of Id of Task object
	*/
	private static List<Id> getSortedTaskIdList() {
		List<Id> v_taskIdList = new List<Id>();
		
		for (Id v_taskId : regularAndOvertimeTaskMap.keySet()) {
			TMS__Task__c v_task = regularAndOvertimeTaskMap.get(v_taskId);
			
			if (v_task.TMS__OvertimeTasks__r.size() != 0) {
				continue;
			} else {
				v_taskIdList.add(v_taskId);
				
				Id v_overtimeTaskId = v_task.TMS__Overtime_overflow_task__c;
				TMS__Task__c v_nextTask = (v_overtimeTaskId != null)
						? regularAndOvertimeTaskMap.get(v_overtimeTaskId)
						: null;
				
				while (v_nextTask != null) {
					v_taskIdList.add(v_nextTask.Id);
					
					Id v_nextTaskId = v_nextTask.TMS__Overtime_overflow_task__c;
					v_nextTask = (v_nextTaskId != null)
							? regularAndOvertimeTaskMap.get(v_nextTaskId)
							: null;
				}
			}
		}
		
		return v_taskIdList;
	}
	

	/**
	* @name updateOvertimeWrapperWeek
	* @author Aliaksandr Satskou
	* @data 04/01/2013
	* @description Calculating overtime for Time objects of Regular Task.
	* @param p_regularWrapperWeek WrapperWeek for Regular Task
	* @param p_overtimeWrapperWeek WrapperWeek for Overtime Task
	* @param p_limitTreshold Limit of regular hours per day
	* @return WrapperWeek
	*/
	private static WrapperWeek updateOvertimeWrapperWeek(
			WrapperWeek p_regularWrapperWeek, WrapperWeek p_overtimeWrapperWeek, Decimal p_limitTreshold) {
		
		String v_dataType = p_regularWrapperWeek.task.TMS__Task_data_type__c;
		Decimal v_treshold = p_regularWrapperWeek.task.TMS__Overtime_hours_threshold__c;
		
		String v_timeApiFieldName = null;

		if (v_dataType == 'Time') {
			v_timeApiFieldName = 'TMS__Time_Spent_Time_Format__c';
		} else {
			v_timeApiFieldName = 'TMS__Time_Spent__c';
		}
		
		logger.log('updateOvertimeWrapperWeek', 'v_timeApiFieldName', v_timeApiFieldName);
		
		WrapperWeek v_overtimeWrapperWeek = p_overtimeWrapperWeek;
		Decimal v_totalRegularHours = 0;
		Decimal v_totalOvertimeHours = 0;
		
		for (Integer i = 0; i < 7; i++) {
			Boolean v_isNeedUpdate = false;
			TMS__Time__c v_time = p_regularWrapperWeek.weekDayTimeList[i];
			TMS__Time__c v_overtime = v_overtimeWrapperWeek.weekDayTimeList[i];
			
			if (v_time.TMS__Date__c != null) {
				Decimal v_timeValue = convertTimeToNumber(v_time.get(v_timeApiFieldName));
				Decimal v_overtimeValue = convertTimeToNumber(v_overtime.get(v_timeApiFieldName));
				
				if (v_overtimeValue == null) { v_overtimeValue = 0; }
				
				if (v_timeValue != null) {
					Decimal v_dayOvertimeHours = 0;
					
					if (p_limitTreshold != null) {
						if (v_timeValue > p_limitTreshold) {
							v_isNeedUpdate = true;
						}
					} else {
						v_totalRegularHours += v_timeValue;
						
						if (v_totalRegularHours > v_treshold) {
							v_dayOvertimeHours = v_totalRegularHours - v_treshold - v_totalOvertimeHours;
							v_totalOvertimeHours += v_dayOvertimeHours;
							
							if (v_dayOvertimeHours > 0) {
								v_isNeedUpdate = true;
							}
						}
					}
					
					if (v_isNeedUpdate) {
						if (v_overtimeWrapperWeek.weekDayTimeList[i].Id == null) {
							v_overtimeWrapperWeek.weekDayTimeList[i] = 
									p_regularWrapperWeek.weekDayTimeList[i].clone(false, false);
						}
						
						
						if (v_dataType == 'Time') {
							if (p_limitTreshold != null) {  
								p_regularWrapperWeek.weekDayTimeList[i].put(v_timeApiFieldName,
										convertNumberToTime(v_timeValue - (v_timeValue - p_limitTreshold)));
								v_overtimeWrapperWeek.weekDayTimeList[i].put(v_timeApiFieldName, 
										convertNumberToTime(v_overtimeValue + v_timeValue - p_limitTreshold));
							} else {
								p_regularWrapperWeek.weekDayTimeList[i].put(v_timeApiFieldName,
										convertNumberToTime(v_timeValue - v_dayOvertimeHours));
								v_overtimeWrapperWeek.weekDayTimeList[i].put(
										v_timeApiFieldName, 
										convertNumberToTime(v_overtimeValue + v_dayOvertimeHours));
							}
						} else {
							if (p_limitTreshold != null) {
								p_regularWrapperWeek.weekDayTimeList[i].put(v_timeApiFieldName,
										v_timeValue - (v_timeValue - p_limitTreshold));
								v_overtimeWrapperWeek.weekDayTimeList[i].put(
										v_timeApiFieldName, v_overtimeValue + v_timeValue - p_limitTreshold);
							} else {
								p_regularWrapperWeek.weekDayTimeList[i].put(v_timeApiFieldName,
										v_timeValue - v_dayOvertimeHours);
								v_overtimeWrapperWeek.weekDayTimeList[i].put(
										v_timeApiFieldName, v_overtimeValue + v_dayOvertimeHours);
							}
						}
					} 
				}
			}
		}
		
		logger.log('updateOvertimeWrapperWeek', 'v_overtimeWrapperWeek', v_overtimeWrapperWeek);
		
		return v_overtimeWrapperWeek;
	}
	
	
	/**
	* @name convertTimeToNumber
	* @author Aliaksandr Satskou
	* @data 03/22/2013
	* @description Converting time to number.
	* @param p_time time in hh:mm format
	* @return Decimal
	*/
	public static Decimal convertTimeToNumber(Object p_time) {
		if (p_time == null) {
			return null;
		}
		
		String v_time = String.valueOf(p_time);
		
		if (v_time.indexOf('.') != -1) {
			return (Decimal)p_time;
		} else if (v_time.indexOf(':') != -1) {
			Integer v_colonIndex = v_time.indexOf(':');
			
			Decimal v_hours = Decimal.valueOf(v_time.substring(0, v_colonIndex));
			Decimal v_minutes = Decimal.valueOf(v_time.substring(v_colonIndex + 1));
			
			return v_hours + v_minutes.divide(60, 2);
		}
		
		return (Decimal)p_time;
	}
	
	
	/**
	* @name convertNumberToTime
	* @author Aliaksandr Satskou
	* @data 03/22/2013
	* @description Converting number to time.
	* @param p_time time in number format
	* @return String
	*/
	public static String convertNumberToTime(Decimal p_time) {
		if (p_time == null) {
			return null;
		}
		
		String v_time = String.valueOf(p_time);
		Integer v_dotIndex = v_time.indexOf('.');
		
		if (v_dotIndex != -1) {
			String v_hours = v_time.substring(0, v_dotIndex);
			Decimal v_minutes = Decimal.valueOf(v_time.substring(v_dotIndex + 1));
			
			v_minutes = Math.round((v_minutes * 60).divide(100, 2));
			
			return ((Decimal.valueOf(v_hours) <= 9)
					? '0' + v_hours
					: v_hours) + ':' + ((v_minutes <= 9)
							? '0' + String.valueOf(v_minutes)
							: String.valueOf(v_minutes));
		}
		
		return String.valueOf(p_time);
	}
	
	
	/**
	* @name isValidRegularTask
	* @author Aliaksandr Satskou
	* @data 03/26/2013
	* @description Checking to see, if Regular Task is valid for calculation overtime.
	* @param p_regularTask Regular Task object
	* @return Boolean
	*/
	private static Boolean isValidRegularTask(TMS__Task__c p_regularTask) {
		/* Checking to see, if Task is Regular. */
		if (p_regularTask.TMS__Overtime_overflow_task__c == null) { 
			return false; 
		}
		
		/* Checking to see, if threshold of Regular Task not null. */
		if (p_regularTask.TMS__Overtime_hours_threshold__c == null) {
			return false;
		}
		
		String v_calculationScheduler = p_regularTask.TMS__Overtime_calculation_schedule__c;
		
		/* Checking to see, if scheduler for Regular Task is correct. */
		if (v_calculationScheduler != null) {
			if ((v_calculationScheduler != 'Daily') && (v_calculationScheduler != 'Weekly')) { 
				return false; 
			}
		} else { return false; }
		
		return true;
	}
	
	
	/**
	* @name getWrapperWeekByTask
	* @author Aliaksandr Satskou
	* @data 04/11/2013
	* @description Creating WrapperWeek, based on Task & Time objects.
	* @param p_task Task object
	* @param p_weekManagement Week Management object
	* @return WrapperWeek
	*/
	private static WrapperWeek getWrapperWeekByTask(
			TMS__Task__c p_task, TMS__Week_Management__c p_weekManagement) {
				
		/* Creating new WrapperWeek. */
		WrapperWeek v_wrapperWeek = new WrapperWeek();
		v_wrapperWeek.task = p_task;
		v_wrapperWeek.weekDayTimeList = new List<TMS__Time__c> {
			new TMS__Time__c(), new TMS__Time__c(), new TMS__Time__c(),
			new TMS__Time__c(), new TMS__Time__c(), new TMS__Time__c(),
			new TMS__Time__c()
		};
		
		
		List<TMS__Time__c> v_timeList = (taskIdTimeListMap.get(p_task.Id) != null)
				? taskIdTimeListMap.get(p_task.Id)
				: p_task.TMS__Time__r;
		
		/* Populating weekDayTimeList of WrapperWeek, based on WeekManagement object. */
		for (TMS__Time__c v_time : v_timeList) {
			if (v_time.TMS__Date__c == p_weekManagement.TMS__Start_Date__c) {
				v_wrapperWeek.weekDayTimeList[0] = v_time;
			} else if (v_time.TMS__Date__c == p_weekManagement.TMS__Start_Date__c.addDays(1)) {
				v_wrapperWeek.weekDayTimeList[1] = v_time;
			} else if (v_time.TMS__Date__c == p_weekManagement.TMS__Start_Date__c.addDays(2)) {
				v_wrapperWeek.weekDayTimeList[2] = v_time;
			} else if (v_time.TMS__Date__c == p_weekManagement.TMS__Start_Date__c.addDays(3)) {
				v_wrapperWeek.weekDayTimeList[3] = v_time;
			} else if (v_time.TMS__Date__c == p_weekManagement.TMS__Start_Date__c.addDays(4)) {
				v_wrapperWeek.weekDayTimeList[4] = v_time;
			} else if (v_time.TMS__Date__c == p_weekManagement.TMS__Start_Date__c.addDays(5)) {
				v_wrapperWeek.weekDayTimeList[5] = v_time;
			} else if (v_time.TMS__Date__c == p_weekManagement.TMS__Start_Date__c.addDays(6)) {
				v_wrapperWeek.weekDayTimeList[6] = v_time;
			}
		}
		
		return v_wrapperWeek;
	}
	
	
	/**
	* @author Aliaksandr Satskou
	* @date 04/10/2013
	* @description Wrapper class, which contains Task object and list of related Time objects.
	*/
	private class WrapperWeek {
		public TMS__Task__c task;
		public List<TMS__Time__c> weekDayTimeList;
		
		public void clearWeekDayTimeList() {
			weekDayTimeList = new List<TMS__Time__c> {
				new TMS__Time__c(), new TMS__Time__c(), new TMS__Time__c(),
				new TMS__Time__c(), new TMS__Time__c(), new TMS__Time__c(),
				new TMS__Time__c()
			};
		}
	}
}