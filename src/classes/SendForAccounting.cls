public without sharing class SendForAccounting extends SendFor {
	
	/* Added by Aliaksandr Satskou, 02/13/2013 */
	private String SiteURL = (TMS__TimesheetCustomSettingsHierarchy__c.getInstance() != null)
			? TMS__TimesheetCustomSettingsHierarchy__c.getInstance().TMS__Site_URL__c
			: null;
			
	private Map<Id, Set<TMS__Project_Resource__c>> accountingIdToSetOfProjectResourceMap = 
			new Map<Id, Set<TMS__Project_Resource__c>>();
	
	protected override void getRecepientIdSet() {
		List<Time__c> v_approvedTimesForWeek = getApprovedTimes();
		
		recepientIdSet = getAccountingIdSetFromTimes(v_approvedTimesForWeek);
		
		initializeAccountingIdToSetOfProjectResourceMap(v_approvedTimesForWeek);
	}
	
	
	/**
	* @author Aliaksandr Satskou
	* @name getRecepientIdSet
	* @date 05/04/2013
	* @description Getting set of recepients.
	* @param p_resourceId Id of resource
	* @return void
	*/
	protected override void getRecepientIdSet(Id p_resourceId) {
		// NOT USED
	}
	
	private List<Time__c> getApprovedTimes() {
		String[] approvedStatuses = TMS__TimesheetCustomSettingsHierarchy__c.getInstance()
				.TMS__Statuses_for_Accounting_Manager__c.split(';');
				
		return [
				SELECT Task__r.Project__r.Accounting_Contact__c,
						Task__r.Project_Resource__r.Contact__r.Name,
						Task__r.Project_Resource__r.Project__r.Name,
						Task__r.Project_Resource__r.Project__r.Internal_Approver__r.Name
				FROM Time__c
				WHERE Week_Management__c = :weekManagement.Id 
					AND Status__c IN :approvedStatuses
					AND Task__r.IsBillable__c = 'true'];
	}
	
	private static Set<Id> getAccountingIdSetFromTimes(List<Time__c> p_timeList) {
		Set<Id> v_accountingIdSet = new Set<Id>();
		for (Time__c v_time : p_timeList) {
			v_accountingIdSet.add(v_time.Task__r.Project__r.Accounting_Contact__c);
		}
		return v_accountingIdSet;
	}
	
	private void initializeAccountingIdToSetOfProjectResourceMap(
			List<Time__c> v_approvedTimesForWeek) {
		
		// Initialize keys with empty value sets.
		for (Time__c v_time : v_approvedTimesForWeek) {
			accountingIdToSetOfProjectResourceMap.put(
					v_time.Task__r.Project__r.Accounting_Contact__c, 
					new Set<TMS__Project_Resource__c>());
		}
		
		// Set values.
		for (Time__c v_time : v_approvedTimesForWeek) {
			accountingIdToSetOfProjectResourceMap.get(v_time.Task__r.Project__r.Accounting_Contact__c)
					.add(v_time.Task__r.Project_Resource__r);
		}
	}
	
	protected override List<Messaging.SingleEmailMessage> generateEmails(Contact p_recepient) {
		Set<TMS__Project_Resource__c> v_projectResourceSet = 
				accountingIdToSetOfProjectResourceMap.get(p_recepient.Id);
		
		List<Messaging.SingleEmailMessage> v_mailList = new List<Messaging.SingleEmailMessage>();
		
		String v_weekManagementDates = 
				weekManagement.TMS__Start_Date__c.format() + 
				' - ' + 
				weekManagement.TMS__End_Date__c.format();
		
		for (TMS__Project_Resource__c v_projectResource : v_projectResourceSet) {
			Messaging.SingleEmailMessage v_email = new Messaging.SingleEmailMessage();
			
			v_email.setTargetObjectId(p_recepient.Id);
			v_email.setSubject(String.format(System.Label.Accounting_Email_Subject, 
					new String[] { 
							v_projectResource.Contact__r.Name,
							v_weekManagementDates,
							v_projectResource.Project__r.Name }));
			v_email.setPlainTextBody(String.format(System.Label.Accounting_Email_Body, 
					new String[] { 
							v_projectResource.Contact__r.Name,
							p_recepient.Name,
							v_projectResource.Project__r.Internal_Approver__r.Name,
							v_weekManagementDates,
							getApprovalPageURL(p_recepient, v_projectResource) }));
			
			v_mailList.add(v_email);
		}
		
		return v_mailList;
	}
	
	private String getApprovalPageURL(
			Contact p_recepient, 
			TMS__Project_Resource__c p_projectResource) {
		
		/* Edited by Aliaksandr Satskou, 03/20/2013, (case #00015810) */
		/* Commented by Aliaksandr Satskou, 02/13/2013 */
		return /*TMS__TimesheetCustomSettingsHierarchy__c.getInstance().TMS__Site_URL__c*/ SiteURL +
				'TMS__TimesheetApproval?sessionId=' + 
				p_recepient.FCMS__Sessions__r[0].FCMS__SessionId__c + 
				'&approval=accounting' + 
				'&week=' + EncodingUtil.urlEncode(weekManagement.TMS__Week__c, 'UTF-8') +
				'&p=' + EncodingUtil.urlEncode(p_recepient.CMS_profile_Name__c, 'UTF-8') +
				'&resourceId=' + p_projectResource.Id;
	}
}