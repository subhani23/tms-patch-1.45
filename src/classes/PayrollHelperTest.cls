@isTest
private class PayrollHelperTest {
     private static List<TMS__Payroll_Provider_Settings__c> payrollProviderSettingList;
    private static List<TMS__Project__c> projectList;
    private static List<TMS__Project_Resource__c> projectResourceList;
    private static List<Contact> contactList;
    private static List<TMS__Task__c> taskList;
    private static List<TMS__Time__c> timeList;
    private static TMS__TimesheetCustomSettingsHierarchy__c timeSheetSetting;
    private static List<TMS__Payroll_Line_items__c> payrollLineItemsList;
    private static List<TMS__Process_Payroll__c> processPayrollList;
    

    /**
    * @name initData
    * @author Aliaksandr Satskou
    * @date 09/04/2013
    * @description Init data.
    * @return void
    */
    private static void initData() {
        
        timeSheetSetting = TMS__TimesheetCustomSettingsHierarchy__c.getOrgDefaults();
        timeSheetSetting.Resource_Page_Selection_Columns__c='TMS__Resource__c;TMS__Date__c';
        timeSheetSetting.TMS__Consider_Payroll_Time_Statuses__c ='Approved';
        upsert timeSheetSetting;
        
        
        TMS__Payroll_Filenames__c filenamesSetting = new TMS__Payroll_Filenames__c(
            Name = 'Default',
            TMS__Paychex_Data_File_Name__c = 'R320_TA.TXT',
            TMS__Paychex_Employee_Bank_Data_File_Name__c = 'CVT1B.TXT',
            TMS__Paychex_Employee_Data_File_Name__c = 'CVT1.TXT',
            TMS__ADP_Data_File_Name__c = 'ADP.TXT'
        );
        upsert filenamesSetting;


        TMS__TimesheetCustomSettings__c customSetting = new TMS__TimesheetCustomSettings__c(
            Name = 'ProjectStatusToShow',
            TMS__Regular_Hours_Task_Name__c = 'Regular',
            TMS__Overtime_Hours_Task_Name__c = 'Overtime'
        );
        upsert customSetting;


        payrollProviderSettingList = new List<TMS__Payroll_Provider_Settings__c> {
            new TMS__Payroll_Provider_Settings__c(
                Name = 'Paychex',
                TMS__Payroll_Provider_Name__c = 'Paychex',
                TMS__Email__c = 'test1@test.com',
                TMS__Field_Delimiter__c = '',
                TMS__Include_Column_Header_in_Payroll_File__c = false,
                TMS__Payroll_Company_ID__c = 'AB1',
                TMS__Use_CSV_File_Format__c = false
            ),
            new TMS__Payroll_Provider_Settings__c(
                Name = 'ADP',
                TMS__Payroll_Provider_Name__c = 'ADP',
                TMS__Email__c = 'test1@test.com',
                TMS__Field_Delimiter__c = ',',
                TMS__Include_Column_Header_in_Payroll_File__c = true,
                TMS__Payroll_Company_ID__c = 'AB1',
                TMS__Use_CSV_File_Format__c = true
            )
        };
        insert payrollProviderSettingList;  

        
        Account account =new Account(Name='test',Company_Id__c ='15045091');
        insert account;
        
        projectList = new List<TMS__Project__c> {
            new TMS__Project__c(
                TMS__Client__c = account.id,
                Name = 'Project #1'
            )
        };
        insert projectList;

        contactList = new List<Contact> {
            new Contact(
                FirstName = 'Contact #1',
                LastName = 'LastName'
            ),
            new Contact(
                FirstName = 'Contact #2',
                LastName = 'LastName'
            )
        };
        insert contactList;


        projectResourceList = new List<TMS__Project_Resource__c> {
            new TMS__Project_Resource__c(
                TMS__Project__c = projectList[0].Id,
                TMS__Contact__c = contactList[0].Id
            ),
            new TMS__Project_Resource__c(
                TMS__Project__c = projectList[0].Id,
                TMS__Contact__c = contactList[1].Id
            )
        };
        insert projectResourceList;


        taskList = new List<TMS__Task__c> {
            new TMS__Task__c(
                Name = 'Regular',
                TMS__Project__c = projectList[0].Id,
                TMS__Project_Resource__c = projectResourceList[0].Id,
                TMS__Override_Department__c = 'Override Department',
                TMS__D_E__c = 'E',
                TMS__D_E_Code__c = '1',
                TMS__Pay_Rate__c = 25.0
            ),
            new TMS__Task__c(
                Name = 'Overtime',
                TMS__Project__c = projectList[0].Id,
                TMS__Project_Resource__c = projectResourceList[0].Id,
                TMS__Override_Department__c = 'Override Department',
                TMS__D_E__c = 'E',
                TMS__D_E_Code__c = '1',
                TMS__Pay_Rate__c = 25.0
            ),
            new TMS__Task__c(
                Name = 'Task #1',
                TMS__Project__c = projectList[0].Id,
                TMS__Project_Resource__c = projectResourceList[1].Id,
                TMS__Override_Department__c = 'Override Department',
                TMS__D_E__c = 'E',
                TMS__D_E_Code__c = '2',
                TMS__Pay_Rate__c = 30.0
            ),
            new TMS__Task__c(
                Name = 'Task #2',
                TMS__Project__c = projectList[0].Id,
                TMS__Project_Resource__c = projectResourceList[1].Id,
                TMS__Override_Department__c = 'Override Department',
                TMS__D_E__c = 'E',
                TMS__D_E_Code__c = '2',
                TMS__Pay_Rate__c = 30.0
            )
        };
        insert taskList;


        timeList = new List<TMS__Time__c> {
            new TMS__Time__c(
                TMS__Task__c = taskList[0].Id,
                TMS__Candidate__c = contactList[0].Id,
                TMS__Status__c = 'Approved',
                TMS__Time_Spent__c = 1.0,
                TMS__Date__c = Date.today()
            ),
            new TMS__Time__c(
                TMS__Task__c = taskList[0].Id,
                TMS__Candidate__c = contactList[0].Id,
                TMS__Status__c = 'Approved',
                TMS__Time_Spent__c = 1.0,
                TMS__Date__c = Date.today()
            ),
            new TMS__Time__c(
                TMS__Task__c = taskList[1].Id,
                TMS__Status__c = 'Approved',
                TMS__Time_Spent__c = 2.0,
                TMS__Date__c = Date.today() + 1
            ),
            new TMS__Time__c(
                TMS__Task__c = taskList[1].Id,
                TMS__Status__c = 'Approved',
                TMS__Time_Spent__c = 2.0,
                TMS__Date__c = Date.today() + 2
            ),
            new TMS__Time__c(
                TMS__Task__c = taskList[2].Id,
                TMS__Status__c = 'Approved',
                TMS__Time_Spent__c = 3.0,
                TMS__Date__c = Date.today() + 3
            ),
            new TMS__Time__c(
                TMS__Task__c = taskList[2].Id,
                TMS__Status__c = 'Approved',
                TMS__Candidate__c = contactList[0].Id,
                TMS__Time_Spent__c = 3.0,
                TMS__Date__c = Date.today() + 4
            ),
            new TMS__Time__c(
                TMS__Task__c = taskList[3].Id,
                TMS__Status__c = 'Approved',
                TMS__Candidate__c = contactList[0].Id,
                TMS__Time_Spent__c = 2.0,
                TMS__Date__c = Date.today() + 5
            ),
            new TMS__Time__c(
                TMS__Task__c = taskList[3].Id,
                TMS__Status__c = 'Approved',
                TMS__Candidate__c = contactList[0].Id,
                TMS__Time_Spent__c = 2.0,
                TMS__Date__c = Date.today() + 6
            )
        };
        insert timeList;
        
        processPayrollList = new List<TMS__Process_Payroll__c> {
            new TMS__Process_Payroll__c(
                TMS__Start_Date__c = Date.today(),
                TMS__End_Date__c = Date.today(),
                TMS__Payroll_Process_Date__c = Date.today(),
                TMS__Payroll_Provider_Name__c = 'Paychex',
                TMS__Comments__c = 'Comments #1'
            )
        };
        insert processPayrollList;
        
        payrollLineItemsList = new List<TMS__Payroll_Line_items__c> {
            new TMS__Payroll_Line_items__c(
                TMS__Payroll__c = processPayrollList[0].Id,
                TMS__Name__c = 'Name #1',
                TMS__Task__c = taskList[0].Id,
                TMS__Hours__c = 1.00,
                TMS__Employee_Number__c = 'Employee Number #1',
                TMS__Social_Security_Number__c = 'S.S. Number #1',
                TMS__Payroll_File_Name__c = 'R320_TA.TXT',
                TMS__Override_Department__c = 'Direct Hire',
                TMS__Override_Rate__c = 10.0000,
                TMS__Employee__c = contactList.get(0).Id
                
            ),
            new TMS__Payroll_Line_items__c(
                TMS__Payroll__c = processPayrollList[0].Id,
                TMS__Name__c = 'Name #1',
                TMS__Task__c = taskList[1].Id,
                TMS__Hours__c = 10.00,
                TMS__Employee_Number__c = 'Employee Number #1',
                TMS__Social_Security_Number__c = 'S.S. Number #1',
                TMS__Payroll_File_Name__c = 'R320_TA.TXT',
                TMS__Override_Rate__c = 10.0000,
                TMS__Override_Department__c = 'Direct Hire',
                TMS__Employee__c = contactList.get(0).Id
            )
        };
        insert payrollLineItemsList;
    }

    /**
    * @name testPayrollController
    * @author Aliaksandr Satskou
    * @date 09/04/2013
    * @description Testing PayrollController class.
    * @return void
    */
    static testMethod void testPayrollController() {
        initData();
        
       // try {
            String payrollAttributes ='{"startDate":"2016-08-07","endDate":"2017-10-07","payrollProvider":"Paychex","companyId":"15045091","hasCreateNewEmployeeFile":false}';
            String selectedRecords = '["'+contactList[0].id+'-'+taskList[0].id+'"]';
            /*PayrollresourceControllerNew controller = new PayrollresourceControllerNew();
            PayrollresourceControllerNew.getProviderNameList();
            PayrollresourceControllerNew.generateDataSet('','','','','','','',payrollAttributes);
            PayrollresourceControllerNew.generateResourcePayroll(payrollAttributes,selectedRecords); 
            */
            
            TMS__Process_Payroll__c payrollOne = new TMS__Process_Payroll__c();
            payrollOne.TMS__Start_Date__c = Date.today();
            payrollOne.TMS__End_Date__c = Date.today() + 6;
            payrollOne.TMS__Status__c = 'Completed';
            payrollOne.TMS__Payroll_Provider_Name__c = 'ADP';
            insert payrollOne;
            
            TMS__Payroll_Line_items__c pli = new TMS__Payroll_Line_items__c(
                TMS__Payroll__c = payrollOne.Id,
                TMS__Name__c = 'Name #2',
                TMS__Task__c = taskList[1].Id,
                TMS__Hours__c = 10.00,
                TMS__Employee_Number__c = 'Employee Number #1',
                TMS__Social_Security_Number__c = 'S.S. Number #1',
                TMS__Payroll_File_Name__c = 'R320_TA.TXT',
                TMS__Override_Rate__c = 10.0000,
                TMS__Override_Department__c = 'Direct Hire',
                TMS__Employee__c = contactList.get(0).Id);
                
                insert pli;
            
            List<String> taskContactIds = new List<String>{''+contactList[0].Id+'-'+taskList[0].Id};
            PayrollHelper payrollHelperController = new PayrollHelper();   
            PayrollHelper.initData(payrollOne,true,taskContactIds);
            PayrollHelper.insertData(); 
            PayrollHelper.paychex_createEmployeeData(payrollOne,'Paychex');
            PayrollHelper.paychex_createData(payrollOne);         
            PayrollHelper.paychex_createEmployeeBankData(payrollOne,'Paychex');
            
    }
}