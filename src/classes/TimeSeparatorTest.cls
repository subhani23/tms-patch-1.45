/**
* @author Aliaksandr Satskou
* @date 11/01/2013
* @description Test class for TimeSeparator class.
*/
@isTest
private class TimeSeparatorTest {
	
	private static List<TMS__Time__c> timeList;
	
	
	/**
	* @author Aliaksandr Satskou
	* @date 11/01/2013
	* @name initData
	* @description Init data.
	* @return void
	*/
	private static void initData() {
		timeList = new List<TMS__Time__c> {
			new TMS__Time__c(
				TMS__Date__c = Date.today()
			),
			new TMS__Time__c(
				TMS__Date__c = Date.today()
			),
			new TMS__Time__c(
				TMS__Date__c = Date.today() + 1
			)
		};
		insert timeList;
	}
	
	
	/**
	* @author Aliaksandr Satskou
	* @date 11/01/2013
	* @name testTimeSeparator
	* @description Testing TimeSeparator class.
	* @return void
	*/
	static testMethod void testTimeSeparator() {
		initData();
		
		TimeSeparator tSeparator = new TimeSeparator(timeList);
		System.assertEquals(true, tSeparator.isSeparated);
		System.assertEquals(true, tSeparator.isNext);
		System.assertEquals(false, tSeparator.isPrevious);
		
		tSeparator.getNextTimeList();
		System.assertEquals(true, tSeparator.isNext);
		System.assertEquals(false, tSeparator.isPrevious);
		
		tSeparator.getNextTimeList();
		System.assertEquals(false, tSeparator.isNext);
		System.assertEquals(true, tSeparator.isPrevious);
		
		tSeparator.getPreviousTimeList();
		System.assertEquals(true, tSeparator.isNext);
		System.assertEquals(false, tSeparator.isPrevious);
	}
}