/**
* @author Aliaksandr Satskou
* @date 12/24/2012
* @description Creating Project Object from Placement Object or Opportunity Object.
*/
global class CreateProjectFromPlacementOrOpportunity {
    private static Map<String,Schema.RecordTypeInfo> recordTypes =
            TMS__Project__c.sObjectType.getDescribe().getRecordTypeInfosByName();

	/**
	* @author -
	* @date -
	* @description Creating Project Object from Placement Object or Opportunity Object.
	* @param p_accountId Id of Account Object
	* @param p_placementOrOpportunityName Name of Placement Object or Opportunity Object
	* @param p_placementOrOpportunityId Id of Placement Object or Opportunity Object
	* @return List<String> list of results
	*/
    webservice static List<String> createProject(
            String p_accountId, String p_placementOrOpportunityName, String p_placementOrOpportunityId) {

        System.debug(':::::p_accountId=' + p_accountId);

        /*
        * @author Aliaksandr Satskou
        * @date 12/26/2012
        * @description Get the list of all the fields to be copied from Custom Settings
        * 	(case num #00013693).
        */
        String v_projectFieldsFromPlacement = (String)CustomSettingsUtility.getCustomSettingListTypeValue(
        		null, 'TMS__Project_fields_from_Placement__c');
        List<String> v_placementToProjectFieldList = (v_projectFieldsFromPlacement != null)
    			? v_projectFieldsFromPlacement.split(';')
        		: new List<String>();
        /* --- */

        List<String> objValue = new List<String>();

        Boolean v_isFromOpportunity =
                p_placementOrOpportunityId.startsWith(Opportunity.SObjectType.getDescribe().getKeyPrefix());

        /* Added by Aliaksandr Satskou, 12/17/2012 */
        Opportunity v_opportunityObject = null;

        System.debug(':::::v_isFromOpportunity=' + v_isFromOpportunity);

        Id v_existingProjectId = null;
        sObject v_placement;

        if (v_isFromOpportunity) {
            /*
	        * @author Aliaksandr Satskou
	        * @date 12/17/2012
	        * @description Getting Opportunity Object.
	        */
            List<Opportunity> v_opportunityList = [
                    SELECT CloseDate
                    FROM Opportunity
                    WHERE Id = :p_placementOrOpportunityId];

            if (v_opportunityList.size() > 0) {
                v_opportunityObject = v_opportunityList[0];
            }
            /* --- */

            List<TMS__Project__c> v_existingProjectList = [
                    SELECT Id
                    FROM TMS__Project__c
                    WHERE TMS__Opportunity__c = :p_placementOrOpportunityId
                      AND RecordTypeId = :recordTypes.get('Project').getRecordTypeId()];

            if (v_existingProjectList.size() > 0) {
                v_existingProjectId = v_existingProjectList[0].Id;
            }

        } else {
	        /*
	        * @author Aliaksandr Satskou
	        * @date 12/24/2012
	        * @description Getting Placement Object with fields (case num #00013693).
	        */
            String v_placementQuery = 'SELECT AVTRRT__Hiring_Manager__c, AVTRRT__Start_Date__c, ' +
                    'Project_TMS__c, AVTRRT__Contact_Candidate__c';
            String v_whereClause = '';

            for (String v_apiFieldName : v_placementToProjectFieldList) {
                v_whereClause += ', ' + v_apiFieldName;
            }

            v_placementQuery += v_whereClause;

            v_placement = Database.query(v_placementQuery + ' FROM AVTRRT__Placement__c WHERE Id = \'' +
                    p_placementOrOpportunityId + '\'');
            /* --- */

            /* Commented by Aliaksandr Satskou, 12/24/2012 */
            /*v_placement = Database.query(
                    ' SELECT AVTRRT__Hiring_Manager__c, AVTRRT__Start_Date__c, ' +
                    '        Project_TMS__c, AVTRRT__Contact_Candidate__c ' +
                    ' FROM AVTRRT__Placement__c ' +
                    ' WHERE Id = \'' + p_placementOrOpportunityId + '\' ');
            /* --- */

            Id v_assignProjectId = (Id)v_placement.get('Project_TMS__c');
            if (v_assignProjectId != null) {
                List<TMS__Project_Resource__c> v_projectResourceList = [
                        SELECT Id
                        FROM TMS__Project_Resource__c
                        WHERE TMS__Project__c = :v_assignProjectId
                          AND TMS__Contact__c = :(Id)v_placement.get('AVTRRT__Contact_Candidate__c')];

                if (v_projectResourceList.size() > 0) {
                    v_existingProjectId = v_assignProjectId;
                }
            }
        }

        if (v_existingProjectId != null) {
            objValue.add('true');
            objValue.add(v_existingProjectId);
            return objValue;
        }

        TMS__Project__c project = new TMS__Project__c(
                Name = p_placementOrOpportunityName,
                RecordTypeId = recordTypes.get('Project').getRecordTypeId(),
                TMS__Client__c = (p_accountId == '' ? null : p_accountId),
                TMS__Start_Date__c = Date.today());

        /*
	    * @author Aliaksandr Satskou
	    * @date 12/24/2012
	    * @description Filling Project Object fields from Placement Object (case num #00013693).
	    */
        if (v_placement != null) {
            for (String v_apiFieldName : v_placementToProjectFieldList) {
                project.put(v_apiFieldName, v_placement.get(v_apiFieldName));
            }
        }
        /* --- */

        if (v_isFromOpportunity) {
            project.TMS__Opportunity__c = p_placementOrOpportunityId;
            project.TMS__Start_Date__c = (v_opportunityObject != null)
                    ? v_opportunityObject.CloseDate
                    : null;
            insert project;
        } else {
            Id v_assignProjectId = (Id)v_placement.get('Project_TMS__c');
            if (v_assignProjectId != null) {
                project = [
                        SELECT Name
                        FROM TMS__Project__c
                        WHERE Id = :v_assignProjectId];
            } else {
                project.TMS__Internal_Approver__c = (Id)v_placement.get('AVTRRT__Hiring_Manager__c');
                project.TMS__Start_Date__c = (Date)v_placement.get('AVTRRT__Start_Date__c');
                project.TMS__Billable__c = true;
                insert project;

                v_placement.put('Project_TMS__c', project.Id);
                update v_placement;
            }

            TMS__Project_Resource__c v_projectResourceForProject = new TMS__Project_Resource__c(
                TMS__Project__c = project.Id,
                TMS__Contact__c = (Id)v_placement.get('AVTRRT__Contact_Candidate__c')
            );
            insert v_projectResourceForProject;

            /*
	        * @author Aliaksandr Satskou
	        * @date 12/17/2012
	        * @description Add Project Resource reference to Placement object.
	        */
            v_placement.put('Project_Resource_TMS__c', v_projectResourceForProject.Id);
            update v_placement;
            /* --- */
        }

        AddExistProjectTemplateInProject.checkIdForPlacementOrOpportunity(
                project.Id, project.Name,
                p_placementOrOpportunityId);

        objValue.add('false');
        objValue.add(project.Id);

        return objValue;
    }

    public static void dummyMethodForTestCoverage() {
        List<Account> v_accountList = new List<Account>();

        Account v_account = new Account();
        v_account.Name = 'Dummy';
        v_accountList.add(v_account);

        v_account = new Account();
        v_account.Name = 'Dummy';
        v_accountList.add(v_account);

        v_account = new Account();
        v_account.Name = 'Dummy';
        v_accountList.add(v_account);

        v_account = new Account();
        v_account.Name = 'Dummy';
        v_accountList.add(v_account);

        v_account = new Account();
        v_account.Name = 'Dummy';
        v_accountList.add(v_account);

        v_account = new Account();
        v_account.Name = 'Dummy';
        v_accountList.add(v_account);

        v_account = new Account();
        v_account.Name = 'Dummy';
        v_accountList.add(v_account);

        v_account = new Account();
        v_account.Name = 'Dummy';
        v_accountList.add(v_account);

        v_account = new Account();
        v_account.Name = 'Dummy';
        v_accountList.add(v_account);

        v_account = new Account();
        v_account.Name = 'Dummy';
        v_accountList.add(v_account);

        v_account = new Account();
        v_account.Name = 'Dummy';
        v_accountList.add(v_account);

        v_account = new Account();
        v_account.Name = 'Dummy';
        v_accountList.add(v_account);

        v_account = new Account();
        v_account.Name = 'Dummy';
        v_accountList.add(v_account);

        v_account = new Account();
        v_account.Name = 'Dummy';
        v_accountList.add(v_account);

        v_account = new Account();
        v_account.Name = 'Dummy';
        v_accountList.add(v_account);

        v_account = new Account();
        v_account.Name = 'Dummy';
        v_accountList.add(v_account);

        v_account = new Account();
        v_account.Name = 'Dummy';
        v_accountList.add(v_account);

        v_account = new Account();
        v_account.Name = 'Dummy';
        v_accountList.add(v_account);

        v_account = new Account();
        v_account.Name = 'Dummy';
        v_accountList.add(v_account);

        v_account = new Account();
        v_account.Name = 'Dummy';
        v_accountList.add(v_account);

        v_account = new Account();
        v_account.Name = 'Dummy';
        v_accountList.add(v_account);

        v_account = new Account();
        v_account.Name = 'Dummy';
        v_accountList.add(v_account);

        v_account = new Account();
        v_account.Name = 'Dummy';
        v_accountList.add(v_account);

        v_account = new Account();
        v_account.Name = 'Dummy';
        v_accountList.add(v_account);

        v_account = new Account();
        v_account.Name = 'Dummy';
        v_accountList.add(v_account);

        v_account = new Account();
        v_account.Name = 'Dummy';
        v_accountList.add(v_account);

        v_account = new Account();
        v_account.Name = 'Dummy';
        v_accountList.add(v_account);

        v_account = new Account();
        v_account.Name = 'Dummy';
        v_accountList.add(v_account);

        v_account = new Account();
        v_account.Name = 'Dummy';
        v_accountList.add(v_account);

        v_account = new Account();
        v_account.Name = 'Dummy';
        v_accountList.add(v_account);

        v_account = new Account();
        v_account.Name = 'Dummy';
        v_accountList.add(v_account);

        v_account = new Account();
        v_account.Name = 'Dummy';
        v_accountList.add(v_account);

        v_account = new Account();
        v_account.Name = 'Dummy';
        v_accountList.add(v_account);

        v_account = new Account();
        v_account.Name = 'Dummy';
        v_accountList.add(v_account);

        v_account = new Account();
        v_account.Name = 'Dummy';
        v_accountList.add(v_account);

        v_account = new Account();
        v_account.Name = 'Dummy';
        v_accountList.add(v_account);
    }

    private static testmethod void CreateProjectFromPlacementOrOpportunityTest() {

        dummyMethodForTestCoverage();

        Account testAcc = new Account(Name = 'testAcc');
        insert testAcc;

        Opportunity testOpportunity = new Opportunity(
                                Name = 'testTMSProjectTemplate',
                                CloseDate = date.today(),
                                StageName = 'Closed Won');
        insert testOpportunity;

        createProject(testAcc.Id, testOpportunity.Name, testOpportunity.Id);
    }

    static testmethod void CreateProjectFromPlacementOrOpportunityTest1() {
        Account testAcc = new Account(Name = 'testAcc');
        insert testAcc;

        Opportunity testOpportunity = new Opportunity(
                                Name = 'testTMSProjectTemplate',
                                CloseDate = date.today(),
                                StageName = 'Closed Won');
        insert testOpportunity;

        createProject(testAcc.Id, testOpportunity.Name, testOpportunity.Id);
    }

    static testmethod void CreateProjectFromPlacementOrOpportunityTest2() {

        Account testAcc = new Account(Name = 'testAcc');
        insert testAcc;

        Opportunity testOpportunity = new Opportunity(
                                Name = 'testTMSProjectTemplate',
                                CloseDate = date.today(),
                                StageName = 'Closed Won');
        insert testOpportunity;

        TMS__Project__c testProject = new TMS__Project__c(
                                Name = 'TestProject',
                                TMS__Opportunity__c = testOpportunity.Id);
        insert testProject;

        createProject(testAcc.Id, testOpportunity.Name, testOpportunity.Id);
    }

    static testmethod void CreateProjectFromPlacementOrOpportunityTest3() {
        Map<String,Schema.RecordTypeInfo> testRecordTypes =
                TMS__Project__c.sObjectType.getDescribe().getRecordTypeInfosByName();

        Account testAcc = new Account(Name = 'testAcc');
        insert testAcc;

        Opportunity testOpportunity = new Opportunity(
                                Name = 'testTMSProjectTemplate',
                                CloseDate = date.today(),
                                StageName = 'Closed Won');
        insert testOpportunity;

        TMS__Project__c testProject = new TMS__Project__c(
                                Name = testOpportunity.Name,
                                TMS__Opportunity__c = testOpportunity.Id,
                                RecordTypeId = testRecordTypes.get('Project').getRecordTypeId());
        insert testProject;

        createProject(testAcc.Id, testOpportunity.Name, testOpportunity.Id);
    }
}