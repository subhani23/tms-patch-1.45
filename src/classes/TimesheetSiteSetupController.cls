public with sharing class TimesheetSiteSetupController {

    /* Added by Aliaksandr Satskou, 04/18/2014 (case #00023506) */
    public String locale {
        get { return UserInfo.getLocale(); }
        private set;
    }


    public TMS__Week_Management__c week {get; set;}

    private Map<String, FCMS__Menu__c> mapMenus = new Map<String, FCMS__Menu__c>();
    private List<Account> accList = new List<Account>();
    private Map<String, FCMS__Page__c> mapPages = new Map<String, FCMS__Page__c>();
    private Map<String, FCMS__CMSProfile__c> mapProfiles = new Map<String, FCMS__CMSProfile__c>();


    public TimesheetSiteSetupController() {
        week = new TMS__Week_Management__c();
    }


    public Pagereference setSiteSetupForTimesheet() {
        setupCMSForTimesheet();

        setCustomSettings();

        ApexPages.addMessage(new ApexPages.Message(
                ApexPages.Severity.INFO,
                Label.Configuration_setup_has_been_successfully_completed));

        return null;
    }

    private void setupCMSForTimesheet() {
        List<FCMS__Page__c> pageList = [
                SELECT FCMS__Name__c,
                    (SELECT FCMS__CMSProfile__c
                     FROM FCMS__Permissions__r)
                FROM FCMS__Page__c
                WHERE FCMS__Name__c = 'Timesheet Page'
                OR FCMS__Name__c = 'Timesheet Approval'
                OR FCMS__Name__c = 'Update Profile TMS'];

        System.debug('::::pageList' + pageList);

        for (FCMS__Page__c page : pageList) {
            mapPages.put(page.FCMS__Name__c, page);
        }

        if (mapPages.get('Timesheet Page') == null) {
            insertPage('Timesheet Page', true);
        }

        accList = [
                SELECT Name
                FROM Account
                WHERE Name = 'Timesheet Pool'];

        if (accList.size() == 0) {
            accList.add(new Account(Name = 'Timesheet Pool'));
            insert accList;
        }

        List<FCMS__CMSProfile__c> cmsProfileList = [
                SELECT Name, FCMS__Profile_Manager__c, FCMS__Profile_Account__c,
                    FCMS__Name__c, FCMS__Default_Home_Page__c
                FROM FCMS__CMSProfile__c
                WHERE FCMS__Name__c = 'Timesheet Manager' OR FCMS__Name__c = 'Timesheet Employee'];

        for (FCMS__CMSProfile__c profile : cmsProfileList) {
            mapProfiles.put(profile.FCMS__Name__c, profile);
        }

        if (mapProfiles.get('Timesheet Manager') == null) {
            insertProfile('Timesheet Manager');
        }
        if (mapProfiles.get('Timesheet Employee') == null) {
            insertProfile('Timesheet Employee');
        }

        if (mapPages.get('Timesheet Approval') == null) {
            insertPageWithPermission(
                    'Timesheet Approval',
                    mapProfiles.get('Timesheet Manager').id,
                    null);
        }
        if (mapPages.get('Update Profile TMS') == null) {
            insertPageWithPermission(
                    'Update Profile TMS',
                    mapProfiles.get('Timesheet Manager').id,
                    mapProfiles.get('Timesheet Employee').id);
        }
        
        
        /* Edited by Aliaksandr Satskou, 02/03/2015 (case #00035429) */
        FCMS__Page__c timesheetPage = mapPages.get('Timesheet Page');
        
        FCMS__CMSProfile__c employeeProfile = mapProfiles.get('Timesheet Employee');
        FCMS__CMSProfile__c managerProfile = mapProfiles.get('Timesheet Manager');
        
        if (timesheetPage != null) {
            List<FCMS__Permission__c> permissionList = timesheetPage.FCMS__Permissions__r;
            
            
            Boolean isEmployeePermission = false;
            Boolean isManagerPermission = false;
            
            if (permissionList != null) {
                for (FCMS__Permission__c permission : permissionList) {
                    if (permission.FCMS__CMSProfile__c == employeeProfile.Id) {
                        isEmployeePermission = true;
                    }
                    
                    if (permission.FCMS__CMSProfile__c == managerProfile.Id) {
                        isManagerPermission = true;
                    }
                }        
            }
            
            
            if (!isEmployeePermission) {
                insertPagePermission(timesheetPage.Id, employeeProfile.Id);
            }
            
            if (!isManagerPermission) {
                insertPagePermission(timesheetPage.Id, managerProfile.Id);
            }
        }


        List<FCMS__Menu__c> menuList = [
                SELECT FCMS__Name__c
                FROM FCMS__Menu__c
                WHERE FCMS__Name__c = 'Timesheet Approval' OR
                    FCMS__Name__c = 'Timesheet Entry' OR
                    FCMS__Name__c = 'Update Profile'];

        for (FCMS__Menu__c menu : menuList) {
            mapMenus.put(menu.FCMS__Name__c, menu);
        }

        if (mapMenus.get('Timesheet Approval') == null) {
            insertMenuWithPermission(
                    'Timesheet Approval',
                    1,
                    'Timesheet Approval',
                    true,
                    mapProfiles.get('Timesheet Manager').id,
                    null);
        }
        if (mapMenus.get('Timesheet Entry') == null) {
            insertMenuWithPermission(
                    'Timesheet Entry',
                    2,
                    'Timesheet Page',
                    true,
                    mapProfiles.get('Timesheet Manager').id,
                    mapProfiles.get('Timesheet Employee').id);
        }
        if (mapMenus.get('Update Profile') == null) {
            insertMenuWithPermission(
                    'Update Profile',
                    3,
                    'Update Profile TMS',
                    true,
                    mapProfiles.get('Timesheet Manager').id,
                    mapProfiles.get('Timesheet Employee').id);
        }

        List<FCMS__Menu_Component__c> menuComponentList = [
                SELECT FCMS__Name__c
                FROM FCMS__Menu_Component__c
                WHERE FCMS__Name__c = 'TimesheetMenu'
                LIMIT 1];

        if (menuComponentList.size() == 0) {

            String menusString = '';
            for (String name : mapMenus.keySet()) {
                menusString += ',' + mapMenus.get(name).FCMS__Name__c;
            }

            FCMS__Menu_Component__c menuComponent = new FCMS__Menu_Component__c();
            menuComponent.FCMS__Name__c = 'TimesheetMenu';
            menuComponent.Name = 'TimesheetMenu';
            menuComponent.FCMS__Active__c = true;
            menuComponent.FCMS__Component_Menus__c = menusString != ''
                    ? menusString.substring(1)
                    : '';
            insert menuComponent;

            menuComponentList.add(menuComponent);
        }

        List<FCMS__Block__c> blockList = [
                SELECT Id
                FROM FCMS__Block__c
                WHERE FCMS__Menu_Component_Name__c = :menuComponentList[0].FCMS__Name__c
                AND FCMS__Type__c = 'Menu Component'
                LIMIT 1];

        if (blockList.size() == 0) {
            insertBlockWithPermission(
                    'Header Menus TMS',
                    'header',
                    1,
                    'Menu Component',
                    menuComponentList[0].FCMS__Name__c,
                    null,
                    null,
                    null,
                    null,
                    null,
                    mapProfiles.get('Timesheet Manager').id,
                    mapProfiles.get('Timesheet Employee').id);
        }

        insertPageBlockWithPermission(
                'Timesheet Block',
                'Timesheet Page',
                'TMS__Timesheet',
                mapProfiles.get('Timesheet Manager').id,
                mapProfiles.get('Timesheet Employee').id);

        insertPageBlockWithPermission(
                'TimesheetApprovalBlock',
                'Timesheet Approval',
                'TMS__TimesheetApproval',
                mapProfiles.get('Timesheet Manager').id,
                null);

        insertPageBlockModuleWithPermission(
                'UpdateProfileBlock TMS',
                'Update Profile TMS',
                'FCMS__Updateuserprofile',
                mapProfiles.get('Timesheet Manager').id,
                mapProfiles.get('Timesheet Employee').id);
    }

    private FCMS__Page__c insertPage(String name, Boolean isAuthRequired) {
        FCMS__Page__c page = new FCMS__Page__c(
                Name = name,
                FCMS__Name__c = name,
                FCMS__Active__c = true,
                FCMS__Authentication_Required__c = isAuthRequired);
        insert page;

        mapPages.put(name, page);

        return page;
    }

    private void insertProfile(String profileName) {
        FCMS__CMSProfile__c cmsProfile = new FCMS__CMSProfile__c (
                Name = profileName,
                FCMS__Name__c = profileName,
                FCMS__Profile_Account__c = accList[0].id,
                FCMS__Default_Home_Page__c = mapPages.get('Timesheet Page').Id,
                FCMS__Profile_Manager__c = UserInfo.getUserId(),
                FCMS__Portal_Enabled__c = true,
                FCMS__User_Registration__c = true);

        cmsProfile = FCMS.CMSProfileExtension.setupProfile(cmsProfile);

        mapProfiles.put(cmsProfile.FCMS__Name__c, cmsProfile);
    }

    private FCMS__Page__c insertPageWithPermission(String name, Id firstProfileId, Id secondProfileId) {

        FCMS__Page__c page = insertPage(name, true);
        insertPagePermission(page.id, firstProfileId);

        if(secondProfileId != null) {
            insertPagePermission(page.id, secondProfileId);
        }

        return page;
    }

    private void insertPagePermission(Id pageId, Id profileId) {
        FCMS__Permission__c permission = new FCMS__Permission__c();
                permission.FCMS__CMSProfile__c = profileId;
                permission.FCMS__Page__c = pageId;
        insert permission;
    }

    private FCMS__Menu__c insertMenuWithPermission(String name, Integer orderNumber,
            String pageName, Boolean visAuth, Id firstProfileId, Id secondProfileId) {

        FCMS__Menu__c menu = new FCMS__Menu__c(
                Name = name,
                FCMS__Name__c = name,
                FCMS__Order__c = orderNumber,
                FCMS__Active__c = true,
                FCMS__Page__c = mapPages.get(pageName).Id,
                FCMS__Visiblity_Authentication__c = visAuth);
        insert menu;

        mapMenus.put(name, menu);

        FCMS__Permission__c permission = new FCMS__Permission__c();
        permission.FCMS__CMSProfile__c = firstProfileId;
        permission.FCMS__Menu__c = menu.id;
        insert permission;

        if (secondProfileId != null) {
            FCMS__Permission__c newPermission = new FCMS__Permission__c();
            permission.FCMS__CMSProfile__c = secondProfileId;
            permission.FCMS__Menu__c = menu.id;
            insert newPermission;
        }

        return menu;
    }

    private FCMS__Block__c insertBlockWithPermission(String name, String section,
            Decimal weight, String blockType, String menuComponentName, String pageName,
            String iFrameURL, String addParameters, String heigth, String width,
            Id firstProfileId, Id secondProfileId) {

        FCMS__Block__c block = new FCMS__Block__c(
                Name = name,
                FCMS__Name__c = name,
                FCMS__Section__c = section,
                FCMS__Weight__c = weight,
                FCMS__Type__c = blockType,
                FCMS__Menu_Component_Name__c = menuComponentName,
                FCMS__IFrame_URL__c = iFrameURL,
                FCMS__Add_Parameters_From_Parent_Window__c = addParameters,
                FCMS__Height__c = heigth,
                FCMS__Width__c = width,
                FCMS__Active__c = true);

        if (pageName != null)
            block.FCMS__Page__c = mapPages.get(pageName).Id;

        insert block;

        FCMS__Permission__c permission = new FCMS__Permission__c();
        permission.FCMS__CMSProfile__c = firstProfileId;
        permission.FCMS__Block__c = block.id;
        insert permission;

        if (secondProfileId != null) {
            FCMS__Permission__c newPermission = new FCMS__Permission__c();
            newPermission.FCMS__CMSProfile__c = secondProfileId;
            newPermission.FCMS__Block__c = block.id;
            insert newPermission;
        }

        return block;
    }

    private void insertPageBlockWithPermission(
            String name, String pageName,
            String iFrameURL, Id firstProfileId, Id secondprofileId) {

        List<FCMS__Block__c> blockList = [
                SELECT Id
                FROM FCMS__Block__c
                WHERE FCMS__Name__c = :name
                AND FCMS__Type__c = 'IFRAME'
                LIMIT 1];

        if (blockList.size() == 0) {
            insertBlockWithPermission(
                    name,
                    'content',
                    0.0,
                    'IFRAME',
                    null,
                    pageName,
                    iFrameURL,
                    'sessionId=,page=,p=,jobsite=,',
                    '800px',
                    '100%',
                    firstProfileId,
                    secondprofileId);
        }
    }

    private void insertPageBlockModuleWithPermission(String name, String pageName, String iFrameURL,
            Id firstProfileId, Id secondprofileId) {

        List<FCMS__Block__c> blockList = [
                SELECT Id
                FROM FCMS__Block__c
                WHERE FCMS__Page__r.FCMS__Name__c = :pageName
                AND FCMS__Type__c = 'MODULE'
                LIMIT 1];

        if (blockList.size() == 0) {
            insertBlockModuleWithPermission(
                    name,
                    pageName,
                    'content',
                    'MODULE',
                    'FCMS__UpdateUserProfile',
                    firstProfileId,
                    secondprofileId);
        }
    }

    private void insertBlockModuleWithPermission(String blockName, String pageName,
            String sectionType, String blockType, string moduleName,
            Id firstProfileId, Id secondprofileId) {

        FCMS__Block__c block = new FCMS__Block__c(
                Name = blockName,
                FCMS__Name__c = blockName,
                FCMS__Section__c = sectionType,
                FCMS__Type__c = blockType,
                FCMS__Active__c = true,
                FCMS__Module_Name__c = moduleName,
                FCMS__Authentication_Required__c = true);

        if (pageName != null) {
            block.FCMS__Page__c = mapPages.get(pageName).Id;
        }
        insert block;

        FCMS__Permission__c permission = new FCMS__Permission__c();
        permission.FCMS__CMSProfile__c = firstProfileId;
        permission.FCMS__Block__c = block.id;
        insert permission;

        FCMS__Permission__c newPermission = new FCMS__Permission__c();
        newPermission.FCMS__CMSProfile__c = secondProfileId;
        newPermission.FCMS__Block__c = block.id;
        insert newPermission;
    }

    /**
    @name customizePayrollSendStatus
    @author Aliaksandr Satskou
    @data 11/08/2012
    */
    private static void setCustomSettings() {
        TMS__TimesheetCustomSettingsHierarchy__c v_timeSheetCustomSettingsHierarchy =
                TMS__TimesheetCustomSettingsHierarchy__c.getOrgDefaults();

        setCustomSettingIfNull(v_timeSheetCustomSettingsHierarchy,
                'TMS__Payroll_send_status__c', 'Sent to Payroll');

        setCustomSettingIfNull(v_timeSheetCustomSettingsHierarchy,
                'TMS__Statuses_for_Accounting_Manager__c', 'Approved');

        upsert v_timeSheetCustomSettingsHierarchy;
    }

    private static void setCustomSettingIfNull(
            TMS__TimesheetCustomSettingsHierarchy__c customSetting,
            String name, Object value) {

        if (customSetting.get(name) == null) {
            customSetting.put(name, value);
        }
    }


    /**
    * @author Aliaksandr Satskou
    * @name generateWeekManagements
    * @date 04/24/2014
    * @description Generating Week Managements based on Start Date and End Date.
    * @return void
    */
    public void generateWeekManagements() {
        Date startDate = week.TMS__Start_Date__c;
        Date endDate = week.TMS__End_Date__c;

        if (startDate > endDate) {
            ApexPages.addMessage(new ApexPages.Message(
                    ApexPages.Severity.ERROR,
                    Label.Start_Date_cannot_be_greater_than_End_Date));

            return;
        }
        
        
        /* Added by Aliaksandr Satskou, 09/15/2014 (case #00031841) */
        List<TMS__Week_Management__c> existingWeekList = [
                SELECT TMS__Start_Date__c, TMS__End_Date__c
                FROM TMS__Week_Management__c];


        Date currentDate = startDate;
        List<TMS__Week_Management__c> weekManagementList = new List<TMS__Week_Management__c>();

        while (currentDate <= endDate) {
            /* Edited by Aliaksandr Satskou, 09/15/2014 (case #00031841) */
            if (!isWeekExist(existingWeekList, currentDate, currentDate + 6)) {
                weekManagementList.add(new TMS__Week_Management__c(
                    TMS__Start_Date__c = currentDate,
                    TMS__End_Date__c = currentDate + 6,
                    TMS__Active__c = week.TMS__Active__c,
                    TMS__Generate__c = week.TMS__Generate__c
                ));
            } else {
                ApexPages.addMessage(new ApexPages.Message(
                    ApexPages.Severity.INFO,
                    Label.Week_Management + ' ' + currentDate.format() + ' - ' + 
                            (currentDate + 6).format() + ' ' + Label.already_exist));
            }
    
            currentDate = currentDate.addDays(7);
        }


        insert weekManagementList;

        ApexPages.addMessage(new ApexPages.Message(
                    ApexPages.Severity.INFO,
                    weekManagementList.size() + ' ' + Label.Week_Managements_was_successfully_created));
                    
        
        /* Added by Aliaksandr Satskou, 09/29/2014 (case #00032182) */
        week = new TMS__Week_Management__c();
    }
    
    
    /**
    * @author Aliaksandr Satskou
    * @date 09/15/2014
    * @description Checking, if Week Management with Start Date & End Date already in list.
    * @param weekList Week list
    * @param startDate Start Date
    * @param endDate End Date
    * @return Boolean
    */
    private static Boolean isWeekExist(List<TMS__Week_Management__c> weekList, Date startDate, Date endDate) {
        for (TMS__Week_Management__c week : weekList) {
            if ((week.TMS__Start_Date__c == startDate) && (week.TMS__End_Date__c == endDate)) {
                return true;
            }
        }
        
        return false;
    }
}