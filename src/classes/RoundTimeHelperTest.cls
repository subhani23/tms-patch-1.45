@isTest
private class RoundTimeHelperTest {
	private static TMS__Task__c taskNumber;
	private static TMS__Task__c taskTime;
	private static TMS__Week_Management__c weekManagement;
	
	@isTest
	private static void testCustomSettingFields() {
		initData();
		
		Test.startTest();
		
		insertTimes();
		
		Test.stopTest();
	}
	
	@isTest
	private static void testTaskFields() {
		initData();
		
		taskNumber.TMS__Start_Week_Hours__c = 6;
		taskNumber.TMS__Start_Week_Minutes__c = 59;
		
		taskNumber.TMS__Rounding_Minutes__c = 6;
		taskNumber.TMS__Rounding_Breakpoint__c = 3;
		
		taskNumber.TMS__Weekend_Differential_Start_Weekday__c = 5;
		taskNumber.TMS__Weekend_Differential_Start_Time_Hour__c = 22;
		taskNumber.TMS__Weekend_Differential_Start_Time_Minute__c = 0;
		taskNumber.TMS__Weekend_Differential_End_Weekday__c = 7;
		taskNumber.TMS__Weekend_Differential_End_Time_Hour__c = 22;
		taskNumber.TMS__Weekend_Differential_End_Time_Minute__c = 0;
		
		update taskNumber;
		
		Test.startTest();
		
		insertTimes();
		
		Test.stopTest();
	}
	
	private static void insertTimes() {
		List<TMS__Time__c> timeList = new List<TMS__Time__c>();
		
		timeList.add(new TMS__Time__c(
				TMS__Date__c = Date.today(),
				TMS__Start_Time__c = '05:00 pm',
				TMS__End_Time__c = '11:00 pm',
				TMS__Status__c = 'Open',
				TMS__Task__c = taskNumber.Id,
				TMS__Week_Management__c = weekManagement.Id));
				
		timeList.add(new TMS__Time__c(
				TMS__Date__c = Date.today() + 1,
				TMS__Start_Time__c = '05:00 pm',
				TMS__End_Time__c = '06:00 pm',
				TMS__Status__c = 'Open',
				TMS__Task__c = taskNumber.Id,
				TMS__Week_Management__c = weekManagement.Id));
				
		timeList.add(new TMS__Time__c(
				TMS__Date__c = Date.today() + 2,
				TMS__Start_Time__c = '05:00 pm',
				TMS__End_Time__c = '11:00 pm',
				TMS__Status__c = 'Open',
				TMS__Task__c = taskNumber.Id,
				TMS__Week_Management__c = weekManagement.Id,
				TMS__Break__c = '01:00'));
				
		timeList.add(new TMS__Time__c(
				TMS__Date__c = Date.today() + 3,
				TMS__Start_Time__c = '05:00 pm',
				TMS__End_Time__c = '11:00 pm',
				TMS__Status__c = 'Open',
				TMS__Task__c = taskNumber.Id,
				TMS__Week_Management__c = weekManagement.Id));
				
		timeList.add(new TMS__Time__c(
				TMS__Date__c = Date.today() + 4,
				TMS__Start_Time__c = '05:00 pm',
				TMS__End_Time__c = '11:00 pm',
				TMS__Status__c = 'Open',
				TMS__Task__c = taskNumber.Id,
				TMS__Week_Management__c = weekManagement.Id));
				
		timeList.add(new TMS__Time__c(
				TMS__Date__c = Date.today() + 5,
				TMS__Start_Time__c = '05:00 pm',
				TMS__End_Time__c = '11:00 pm',
				TMS__Status__c = 'Open',
				TMS__Task__c = taskNumber.Id,
				TMS__Week_Management__c = weekManagement.Id));
		
		insert timeList;
		
		TMS__Time__c anotherTime = new TMS__Time__c(
				TMS__Date__c = Date.today() + 6,
				TMS__Start_Time__c = '05:00 pm',
				TMS__End_Time__c = '11:00 pm',
				TMS__Status__c = 'Open',
				TMS__Task__c = taskNumber.Id,
				TMS__Week_Management__c = weekManagement.Id);
				
		insert anotherTime;
		
		update anotherTime;
		
		TMS__Time__c timeTypeTime = new TMS__Time__c(
				TMS__Date__c = Date.today() + 6,
				TMS__Start_Time__c = '05:00 pm',
				TMS__End_Time__c = '11:00 pm',
				TMS__Status__c = 'Open',
				TMS__Task__c = taskTime.Id,
				TMS__Week_Management__c = weekManagement.Id);
				
		insert timeTypeTime;
	}
	
	private static void initData() {
		TMS__TimesheetCustomSettingsHierarchy__c cs =
				TMS__TimesheetCustomSettingsHierarchy__c.getOrgDefaults();
				
		cs.TMS__Start_Week_Enabled__c = true;
		cs.TMS__Start_Week_Hours__c = 6;
		cs.TMS__Start_Week_Minutes__c = 59;
		
		cs.TMS__Rounding_Enabled__c = true;
		cs.TMS__Rounding_Minutes__c = 6;
		cs.TMS__Rounding_Breakpoint__c = 3;
		
		cs.TMS__Weekend_Differential_Enabled__c = true;
		cs.TMS__Weekend_Differential_Start_Weekday__c = 5;
		cs.TMS__Weekend_Differential_Start_Time_Hour__c = 22;
		cs.TMS__Weekend_Differential_Start_Time_Minute__c = 0;
		cs.TMS__Weekend_Differential_End_Weekday__c = 7;
		cs.TMS__Weekend_Differential_End_Time_Hour__c = 22;
		cs.TMS__Weekend_Differential_End_Time_Minute__c = 0;
		
		cs.TMS__Minimum_Hours_Enabled__c = true;
		
		insert cs;

		Contact contact = new Contact(LastName = 'Test Last Name');
		insert contact;

		RecordType projectRecordType = [
				SELECT Id
				FROM RecordType
				WHERE SObjectType = 'TMS__Project__c' AND Name = 'Project'
				LIMIT 1];

		TMS__Project__c project = new TMS__Project__c(Name = 'Test Project');
		insert project;

		TMS__Project_Resource__c projectResource = new TMS__Project_Resource__c(
				TMS__Project__c = project.Id,
				TMS__Contact__c = contact.Id);
		insert projectResource;

		taskNumber = new TMS__Task__c(
				Name = 'Test taskNumber',
				TMS__Project__c = project.Id,
				TMS__Project_Resource__c = projectResource.Id,
				TMS__Enable_Time_Tracking__c = true,
				TMS__Minimum_Hours__c = 2,
				TMS__Task_data_type__c = 'Number');
		insert taskNumber;
		
		taskTime = new TMS__Task__c(
				Name = 'Test taskTime',
				TMS__Project__c = project.Id,
				TMS__Project_Resource__c = projectResource.Id,
				TMS__Enable_Time_Tracking__c = true,
				TMS__Minimum_Hours__c = 2,
				TMS__Task_data_type__c = 'Time');
		insert taskTime;

		weekManagement = new TMS__Week_Management__c(
				TMS__Start_Date__c = Date.today(),
				TMS__End_Date__c = Date.today().addDays(6),
				TMS__Active__c = true);
		insert weekManagement;
	}
}