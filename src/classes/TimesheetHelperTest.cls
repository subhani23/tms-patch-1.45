/**
* @author Aliaksandr Satskou
* @date 10/17/2013
* @description Test class for TimesheetHelper class.
*/
@isTest
private class TimesheetHelperTest {
	
	private static List<Contact> candidateList;
	private static List<TMS__Week_Management__c> weekManagementList;
	private static List<TMS__Project_Resource__c> resourceList;
	
	
	/**
	* @author Aliaksandr Satskou
	* @name initData
	* @date 10/17/2013
	* @description Init data.
	* @return void
	*/
	private static void initData() {
		TMS__TimesheetCustomSettings__c tmsSetting = new TMS__TimesheetCustomSettings__c(
			Name = 'ProjectStatusToShow',
			TMS__Enable_Timesheet_Object__c = true
		);
		insert tmsSetting;
		
		
		candidateList = new List<Contact> {
			new Contact(
				LastName = 'Contact #1'
			)
		};
		insert candidateList;
		
		
		weekManagementList = new List<TMS__Week_Management__c> {
			new TMS__Week_Management__c(
				TMS__Start_Date__c = Date.today(),
				TMS__End_Date__c = Date.today() + 6,
				TMS__Active__c = true
 			)
		};
		insert weekManagementList;
		
		
		resourceList = new List<TMS__Project_Resource__c> {
			new TMS__Project_Resource__c(
				TMS__Contact__c = candidateList[0].Id
			)
		};
		insert resourceList;
	}
	
	
	/**
	* @author Aliaksandr Satskou
	* @name testTimesheetHelper
	* @date 10/17/2013
	* @description Testing TimesheetHelper class.
	* @return void
	*/
	static testMethod void testTimesheetHelper() {
		System.assertEquals(false, TimesheetHelper.isObjectEnabled());
		
		initData();
		//System.assertEquals(true, TimesheetHelper.isObjectEnabled());
		
		
		TimesheetHelper timesheet = new TimesheetHelper(candidateList[0].Id, weekManagementList[0].Id);
		System.assertEquals(false, timesheet.isFound);
		System.assertEquals(false, timesheet.isCreated);
		System.assertEquals(null, timesheet.getId());
		
		timesheet.create();
		timesheet.save();
		
		List<TMS__Timesheet__c> timesheetList = [
				SELECT TMS__Candidate__c, TMS__Week_Management__c
				FROM TMS__Timesheet__c
				WHERE Id = :timesheet.getId()];
		
		System.assertEquals(candidateList[0].Id, timesheetList[0].TMS__Candidate__c);
		System.assertEquals(weekManagementList[0].Id, timesheetList[0].TMS__Week_Management__c);
		
		
		timesheet = new TimesheetHelper(candidateList[0].Id, weekManagementList[0].Id);
		System.assertEquals(true, timesheet.isFound);
		System.assertEquals(timesheetList[0].Id, timesheet.getId());
		
		timesheet.setStatus('Open');
		timesheet.save();
		
		timesheetList = [
				SELECT TMS__Status__c FROM TMS__Timesheet__c];
		System.assertEquals('Open', timesheetList[0].TMS__Status__c);
	}
}