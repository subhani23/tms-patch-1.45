/**
* @author Aliaksandr Satskou
* @date 09/04/2013
* @description Creating Payroll object with Line Items.
*/
public with sharing class PayrollController {

	/* Added by Aliaksandr Satskou, 04/18/2014 (case #00023506) */
	public String locale {
		get { return UserInfo.getLocale(); }
		private set;
	}
	
	public Map<String, TMS__Payroll_Provider_Settings__c> payrollProviderSettingMap;
	public List<TMS__Payroll_Line_items__c> outputLineList = new List<TMS__Payroll_Line_items__c>();

	public TMS__Process_Payroll__c processPayroll {
		get {
			if ((processPayroll.TMS__End_Date__c != null) &&
				(processPayroll.TMS__Payroll_Process_Date__c == null)) {
					processPayroll.TMS__Payroll_Process_Date__c = processPayroll.TMS__End_Date__c + 2;
			}

			return processPayroll;
		}
		set;
	}

	public List<SelectOption> payrollProviderSelectOptionList {get; set;}
	public String payrollProvider {get; set;}


	public Boolean createEmployeeFile {get; set;}


	/**
	* @name PayrollController
	* @author Aliaksandr Satskou
	* @date 09/04/2013
	* @description Initialize data.
	*/
	public PayrollController() {
		processPayroll = new TMS__Process_Payroll__c();

		payrollProviderSelectOptionList = new List<SelectOption>{ new SelectOption('', '-None-') };
		payrollProviderSelectOptionList.addAll(PayrollHelper.getProviderNameList());

		if (payrollProviderSelectOptionList.size() == 2) {
			payrollProvider = payrollProviderSelectOptionList[1].getValue();
		}
	}


	/**
	* @name generatePayroll
	* @author Aliaksandr Satskou
	* @date 09/04/2013
	* @description Creating Payroll with Line Items.
	* @return void
	*/
	public void generatePayroll() {
		if (payrollProvider == null) {
			ApexPages.addMessage(new ApexPages.Message(
					ApexPages.Severity.ERROR, System.Label.Please_select_Payroll_Provider));
			return;
		}

		if (processPayroll.TMS__End_Date__c < processPayroll.TMS__Start_Date__c) {
			ApexPages.addMessage(new ApexPages.Message(
					ApexPages.Severity.ERROR, System.Label.End_Date_cant_be_less_than_Start_Date));
			return;
		}

		processPayroll.TMS__Status__c = Label.In_Progress;
		processPayroll.TMS__Payroll_Provider_Name__c = payrollProvider;
		insert processPayroll;

		PayrollHelper.initData(processPayroll,false,null);

		try {
			if (payrollProvider.toLowerCase() == 'paychex') {
				PayrollHelper.paychex_createData(processPayroll);
				PayrollHelper.paychex_createEmployeeBankData(processPayroll, payrollProvider);
				PayrollHelper.paychex_createEmployeeData(processPayroll, payrollProvider);
			}

			if (payrollProvider.toLowerCase() == 'adp') {
				PayrollHelper.adp_createData(processPayroll, payrollProvider);

				if ((createEmployeeFile != null) && (createEmployeeFile)) {
					PayrollHelper.adp_createEmployeeFile(processPayroll, payrollProvider);
				}
			}

			processPayroll.TMS__Status__c = Label.Completed;
			processPayroll.TMS__Comments__c = System.Label.Payroll_was_successfully_created;

			ApexPages.addMessage(new ApexPages.Message(
					ApexPages.Severity.INFO, System.Label.Payroll_was_successfully_created));
		} catch (Exception v_exception) {
			processPayroll.TMS__Status__c = Label.Error;
			processPayroll.TMS__Comments__c = v_exception.getMessage();

			ApexPages.addMessage(new ApexPages.Message(
					ApexPages.Severity.ERROR, v_exception.getMessage()));
		}


		PayrollHelper.insertData();

		try {
			upsert processPayroll;
		} catch (Exception e) {  }

		processPayroll = new TMS__Process_Payroll__c();
		
	}
	
	
	
}