/**
* @author Aliaksandr Satskou
* @date 10/01/2013
* @description Test class for MissingTimesheets class.
*/
public with sharing class MissingTimesheetController {

	/* Added by Aliaksandr Satskou, 04/18/2014 (case #00023506) */
	public String locale {
		get { return UserInfo.getLocale(); }
		private set;
	}


	public Map<TMS__Project__c, Set<TMS__Project_Resource__c>> projectProjectResourceSetMap {get; set;}
	public Map<Account, List<TMS__Project__c>> accountProjectListMap {get; set;}
	public TMS__Week_Management__c weekManagement {get; set;}


	private static String tmsSubmittedStatus = CustomSettingsUtility.getTimesheetSubmittedStatus();
	private static String tmsApprovedStatus = CustomSettingsUtility.getTimesheetApprovedStatus();

	/**
	* @author Aliaksandr Satskou
	* @name isTimeListWithSameStatus
	* @date 12/12/2013
	* @description Checking Time list on Status.
	* @param timeList List of Time objects
	* @param status Status of Time object
	* @return Boolean
	*/
	private static Boolean isTimeListWithSameStatus(List<TMS__Time__c> timeList, String status) {
		Decimal timeSpent = 0;
		Boolean isSameStatus = true;

		for (TMS__Time__c timeObj : timeList) {
			timeSpent += timeObj.TMS__Time_Spent_Formula__c;

			if (timeObj.TMS__Status__c != status) {
				isSameStatus = false;

				break;
			}
		}

		if ((isSameStatus) && (timeSpent > 0)) {
			return true;
		}


		return false;
	}

	/**
	* @author Aliaksandr Satskou
	* @name MissingTimesheetController
	* @date 14/01/2013
	* @description Constructor. Contains all business logic.
	*/
	public MissingTimesheetController() {
		String v_weekManagementId = ApexPages.currentPage().getParameters().get('WeekManagementId');

		if (v_weekManagementId != null) {
			projectProjectResourceSetMap = new Map<TMS__Project__c, Set<TMS__Project_Resource__c>>();
			accountProjectListMap = new Map<Account, List<TMS__Project__c>>();

			weekManagement = [
					SELECT Id, Name, TMS__Week__c, TMS__End_Date__c
					FROM TMS__Week_Management__c
					WHERE Id = :v_weekManagementId
					LIMIT 1];

			/* Getting 'Open Project Criteria' from Custom Settings */
			List<TMS__TimesheetCustomSettings__c> v_timesheetCustomSettingList =
					CustomSettingsUtility.getCustomSettingListTypeList();

			String v_openProjectCriteria = ((v_timesheetCustomSettingList != null) &&
					(v_timesheetCustomSettingList.size() > 0))
							? v_timesheetCustomSettingList[0].TMS__Open_Project_Criteria__c
							: null;

			Date v_weekManagementEndDate = weekManagement.TMS__End_Date__c;

			/* Creating query for Project */
			String v_query = 'SELECT Name, TMS__Client__c FROM TMS__Project__c ' +
					'WHERE TMS__End_Date__c >= :v_weekManagementEndDate' +
					(((v_openProjectCriteria != null) && (v_openProjectCriteria != ''))
							? (' AND ' + v_openProjectCriteria)
							: '');

			/* Getting Project objects */
			Map<Id, SObject> v_projectMap = new Map<Id, SObject>(DataBase.query(v_query));

			/* Getting Time objects for specified Week Management */
			List<TMS__Time__c> v_timeList = [
					SELECT TMS__Task__c, TMS__Status__c, TMS__Time_Spent_Formula__c
					FROM TMS__Time__c
					WHERE TMS__Week_Management__c = :v_weekManagementId
					AND TMS__Task__c != null];

			/* Map of Task Id to Time object list */
			Map<Id, List<TMS__Time__c>> v_taskIdTimeListMap = new Map<Id, List<TMS__Time__c>>();

			for (TMS__Time__c v_time : v_timeList) {
				List<TMS__Time__c> v_timeListFromMap = v_taskIdTimeListMap.get(v_time.TMS__Task__c);

				if (v_timeListFromMap != null) {
					v_timeListFromMap.add(v_time);
				} else {
					v_taskIdTimeListMap.put(v_time.TMS__Task__c, new List<TMS__Time__c> { v_time });
				}
			}

			/* List of Task Id, for which Time objects not submitted or submitted with 0 time spent */
			List<Id> v_missingTimesheetTaskIdList = new List<Id>();

			for (Id v_taskId : v_taskIdTimeListMap.keySet()) {
				List<TMS__Time__c> v_timeListFromMap = v_taskIdTimeListMap.get(v_taskId);

				if ((!isTimeListWithSameStatus(v_timeListFromMap, tmsSubmittedStatus)) &&
						(!isTimeListWithSameStatus(v_timeListFromMap, tmsApprovedStatus))) {

					v_missingTimesheetTaskIdList.add(v_taskId);
				}
			}

			/* Getting list of Task objects */
			List<TMS__Task__c> v_taskList = [
					SELECT TMS__Project__c, TMS__Project_Resource__c, TMS__Project__r.TMS__Client__c
					FROM TMS__Task__c
					WHERE Id IN :v_missingTimesheetTaskIdList
					AND TMS__Project__c IN :v_projectMap.keySet()
					AND TMS__Project_Resource__c != null];

			Set<Id> v_projectResourceIdSet = new Set<Id>();

			for (TMS__Task__c v_task : v_taskList) {
				v_projectResourceIdSet.add(v_task.TMS__Project_Resource__c);
			}

			/* Getting map of Project Resource objects */
			Map<Id, TMS__Project_Resource__c> v_projectResourceMap = new Map<Id, TMS__Project_Resource__c>([
					SELECT Name, TMS__Contact__c, TMS__Contact__r.Name
					FROM TMS__Project_Resource__c
					WHERE Id IN :v_projectResourceIdSet]);

			Set<Id> v_accountIdSet = new Set<Id>();

			/* Populating map of Project to Task objects list */
			for (TMS__Task__c v_task : v_taskList) {
				TMS__Project__c v_project = (TMS__Project__c)v_projectMap.get(v_task.TMS__Project__c);

				Set<TMS__Project_Resource__c> v_projectResourceSetFromMap =
						projectProjectResourceSetMap.get(v_project);

				if (v_projectResourceSetFromMap != null) {
					v_projectResourceSetFromMap.add(
							v_projectResourceMap.get(v_task.TMS__Project_Resource__c));
				} else {
					projectProjectResourceSetMap.put(v_project, new Set<TMS__Project_Resource__c> {
						v_projectResourceMap.get(v_task.TMS__Project_Resource__c) });
				}

				v_accountIdSet.add(v_task.TMS__Project__r.TMS__Client__c);
			}

			/* Getting map of Account objects */
			Map<Id, Account> v_accountMap = new Map<Id, Account>([
					SELECT Name
					FROM Account
					WHERE Id IN :v_accountIdSet]);

			/* Populating map of Account object to Project objects list */
			for (TMS__Project__c v_project : projectProjectResourceSetMap.keySet()) {
				List<TMS__Project__c> v_projectListFromMap =
						accountProjectListMap.get(v_accountMap.get(v_project.TMS__Client__c));

				if (v_projectListFromMap != null) {
					v_projectListFromMap.add(v_project);
				} else {
					accountProjectListMap.put(v_accountMap.get(v_project.TMS__Client__c),
							new List<TMS__Project__c> { v_project });
				}
			}
		}
	}
}