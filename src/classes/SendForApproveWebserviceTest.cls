@isTest
public class SendForApproveWebserviceTest {
	private static TestHelperClasses.WeekManagementWrapper weekManagementWrapper;
	
	public static testMethod void testSuccess() {
		initData();
		
		Test.startTest();
		
		SendForApproveWebservice.send(weekManagementWrapper.weekManagement.Id);
		
		Test.stopTest();
		
		//System.assertEquals(,);
	}
	
	private static void initData() {
		TestHelperClasses.ProjectWrapper v_projectWrapper1 = 
				TestHelper.createProjectWith2Resources2TasksAndInternalApprover();
		
		TestHelperClasses.ProjectWrapper v_projectWrapper2 = 
				TestHelper.createProjectWith2Resources2TasksAndInternalApprover();
		
		weekManagementWrapper = TestHelper.createWeekManagement();
		
		TestHelper.createTimesForWeekManagementAndProjectResource(
				weekManagementWrapper, v_projectWrapper1.projectResourceList[0]);
		TestHelper.createTimesForWeekManagementAndProjectResource(
				weekManagementWrapper, v_projectWrapper2.projectResourceList[0]);
		
		System.debug(LoggingLevel.ERROR, ':::::::weekManagementWrapper.weekManagement.Id=' + 
				weekManagementWrapper.weekManagement.Id);
				
		System.debug(LoggingLevel.ERROR, ':::::::weekManagementWrapper.timeList=' + 
				weekManagementWrapper.timeList);
				
		for (TMS__Time__c v_time : weekManagementWrapper.timeList) {
			System.debug(LoggingLevel.ERROR, ':::::::v_time=' + v_time);
			v_time.TMS__Status__c = 'Submitted';
		}
		
		update weekManagementWrapper.timeList;
	}
}