/**
* @author Aliaksandr Satskou
* @date 06/06/2013
* @description Test class for AccountingURL class.
*/
@isTest
private class AccountingURLTest {
	
	private static List<TMS__Project__c> projectList;
	private static List<Contact> candidateList;
	private static List<TMS__Project_Resource__c> projectResourceList;
	private static List<TMS__Task__c> taskList;
	private static List<TMS__Time__c> timeList;
	private static List<TMS__Week_Management__c> weekManagementList;
	
	
	/**
	* @name initData
	* @author Aliaksandr Satskou
	* @data 06/06/2013
	* @description Initialize all needed test data.
	* @return void
	*/
	private static void initData() {
		TMS__TimesheetCustomSettingsHierarchy__c customSetting = 
					TMS__TimesheetCustomSettingsHierarchy__c.getInstance();
					
		customSetting.TMS__Statuses_for_Accounting_Manager__c = 'Approved';		
		upsert customSetting;
		
		
		candidateList = new List<Contact> {
			new Contact(
				LastName = 'Contact #1',
				Email = 'test@test.com'
			),
			new Contact(
				LastName = 'Accounting #2',
				Email = 'test@test.com'
			),
			new Contact(
				LastName = 'Manager #3',
				Email = 'test@test.com'
			)
		};
		insert candidateList;

		projectList = new List<TMS__Project__c> {
			new TMS__Project__c(
				RecordTypeId = RecordTypeUtility.getProjectRecordTypeId(),
				Name = 'Project #1',
				TMS__Project_Status__c = 'Open',
				TMS__Accounting_Contact__c = candidateList[1].Id,
				TMS__Internal_Approver__c = candidateList[2].Id
			)
		};
		insert projectList;

		projectResourceList = new List<TMS__Project_Resource__c> {
			new TMS__Project_Resource__c(
				TMS__Project__c = projectList[0].Id,
				TMS__Contact__c = candidateList[0].Id
			)
		};
		insert projectResourceList;
		
		weekManagementList = new List<TMS__Week_Management__c> {
			new TMS__Week_Management__c(
				TMS__Start_Date__c = Date.today(),
				TMS__End_Date__c = Date.today() + 6,
				TMS__Active__c = true
			)
		};
		insert weekManagementList;
		
		taskList = new List<TMS__Task__c> {
			new TMS__Task__c(
				Name = 'Task #1',
				TMS__Project__c = projectList[0].Id,
				TMS__Project_Resource__c = projectResourceList[0].Id,
				TMS__Billable__c = 'Yes'
			),
			new TMS__Task__c(
				Name = 'Task #2',
				TMS__Project__c = projectList[0].Id,
				TMS__Project_Resource__c = projectResourceList[0].Id,
				TMS__Billable__c = 'Yes'
			)
		};
		insert taskList;
		
		timeList = new List<TMS__Time__c> {
			new TMS__Time__c(
				TMS__Task__c = taskList[0].Id,
				TMS__Date__c = Date.today(),
				TMS__Time_Spent__c = 10.0,
				TMS__Status__c = 'Approved',
				TMS__Week_Management__c = weekManagementList[0].Id
			),
			new TMS__Time__c(
				TMS__Task__c = taskList[0].Id,
				TMS__Date__c = Date.today() + 1,
				TMS__Time_Spent__c = 10.0,
				TMS__Status__c = 'Approved',
				TMS__Week_Management__c = weekManagementList[0].Id
			),
			new TMS__Time__c(
				TMS__Task__c = taskList[0].Id,
				TMS__Date__c = Date.today() + 2,
				TMS__Time_Spent__c = 8.0,
				TMS__Status__c = 'Approved',
				TMS__Week_Management__c = weekManagementList[0].Id
			),
			new TMS__Time__c(
				TMS__Task__c = taskList[0].Id,
				TMS__Date__c = Date.today() + 3,
				TMS__Time_Spent__c = 0.0,
				TMS__Status__c = 'Approved',
				TMS__Week_Management__c = weekManagementList[0].Id
			),
			new TMS__Time__c(
				TMS__Task__c = taskList[0].Id,
				TMS__Date__c = Date.today() + 4,
				TMS__Time_Spent__c = 8.0,
				TMS__Status__c = 'Approved',
				TMS__Week_Management__c = weekManagementList[0].Id
			)
		};
		insert timeList;
	}
	
	
	/**
	* @name testAccountingURL
	* @author Aliaksandr Satskou
	* @data 06/06/2013
	* @description Testing AccountingURL class.
	* @return void
	*/
    static testMethod void testAccountingURL() {
        initData();
        
        List<TMS__Project_Resource__c> v_projectResourceList = 
        		AccountingURL.getUpdatedResource(weekManagementList[0].Id, candidateList[0].Id);
        
        update v_projectResourceList;
        
        
        projectResourceList = [
        		SELECT TMS__View_Timesheet__c 
        		FROM TMS__Project_Resource__c
        		WHERE Id IN :projectResourceList];
        
        System.assertNotEquals(null, projectResourceList[0].TMS__View_Timesheet__c);
    }
}