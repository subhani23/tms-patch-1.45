@isTest
public class TimeSheetControllerTest {
	
	private static TestDataGenerator gen = new TestDataGenerator();
	
	private static TimeSheetController controller;

	private static Logger logger = new Logger('TimeSheetControllerTest');

	@isTest
	private static void testSave() {
		gen.initData(new TestDataGenerator.InitOptions());

		Test.startTest();

		controller = new TimeSheetController();

		controller.init();
		controller.save();

		Test.stopTest();
	}
	
	@isTest
	private static void testSubmit() {
		gen.initData(new TestDataGenerator.InitOptions());

		Test.startTest();

		controller = new TimeSheetController();

		controller.init();
		controller.submitForApproval();
		
		Test.stopTest();
	}
	
	@isTest
	private static void testOtherActions() {
		gen.initData(new TestDataGenerator.InitOptions());

		Test.startTest();

		controller = new TimeSheetController();

		controller.init();

		controller.wrapperClassList[0].getProjectItems();
		controller.wrapperClassList[0].getTaskItems();

		controller.getTempInteger();
		controller.setTempInteger(3);
		controller.getWeek();
		controller.getWeekItems();
		controller.previousWeek();
		controller.nextWeek();
		controller.createNewTimeSheet();
		controller.addNewtimesheetRow();
		controller.changeTask();
		controller.changePercentage();
		
		String buttonsMessage = controller.buttonsMessage;

		Test.stopTest();
	}

	@isTest
	private static void testExpenses() {
		gen.initData(new TestDataGenerator.InitOptions());

		Test.startTest();

		controller = new TimeSheetController();
		controller.init();

		userSelectsExpenseFileForUploadAndClicksUploadButton();
		assertExpensesAfterUpload();

		userClicksDeleteLinkForUploadedExpense();
		assertExpensesAfterDelete();

		Test.stopTest();
	}

	private static void userSelectsExpenseFileForUploadAndClicksUploadButton() {
		controller.expenseWrapperList[0].attachment.Name = 'Test Expense File.xls';
		controller.expenseWrapperList[0].attachment.Body = Blob.valueOf('Test File Body');
		controller.expenseWrapperList[0].expense.TMS__Comments__c = 'Test Comment';
		controller.expenseWrapperList[0].expense.TMS__Project__c = gen.project.Id;
		controller.expenseWrapperList[0].isNeedSave = true;

		controller.saveExpenses();
	}

	private static void assertExpensesAfterUpload() {
		List<TMS__Expense__c> expenseList = [
				SELECT TMS__Status__c, TMS__Comments__c, (SELECT Name FROM Attachments)
				FROM TMS__Expense__c
				WHERE TMS__Contact__c = :gen.employee.Id];

		//System.assertEquals(1, expenseList.size());
		//System.assertEquals('Test Comment', expenseList[0].TMS__Comments__c);
	}

	private static void userClicksDeleteLinkForUploadedExpense() {
		controller.deleteExpenseId = controller.expenseList[0].Id;
		controller.deleteExpense();
	}

	private static void assertExpensesAfterDelete() {
		assertExpensesDeletedInMemory();
		assertExpensesDeletedInDatabase();
	}

	private static void assertExpensesDeletedInMemory() {
		//System.assertEquals(0, controller.expenseList.size());
	}

	private static void assertExpensesDeletedInDatabase() {
		List<TMS__Expense__c> expenseList = [
				SELECT Id
				FROM TMS__Expense__c
				WHERE TMS__Contact__c = :gen.employee.Id];

		//System.assertEquals(0, expenseList.size());
	}

	@isTest
	private static void testCasesOneWithEmptySubject() {
		gen.initData(new TestDataGenerator.InitOptions());

		insertCasesOneWithEmptySubject();

		Test.startTest();

		controller = new TimeSheetController();

		controller.init();

		Test.stopTest();

		TimeSheetController.WrapperClass timesheetRow = controller.wrapperClassList[0];

		System.assertEquals(4, timesheetRow.caseList.size());

		/*
		System.assertEquals(
				'- None -', timesheetRow.caseList[0].getLabel());
		System.assertEquals(
				gen.cases[0].CaseNumber + ' - Test Case', timesheetRow.caseList[1].getLabel());
		System.assertEquals(
				gen.cases[1].CaseNumber + ' - Test Case ...', timesheetRow.caseList[2].getLabel());
		System.assertEquals(
				gen.cases[2].CaseNumber + ' - ', timesheetRow.caseList[3].getLabel());
		*/
	}

	private static void insertCasesOneWithEmptySubject() {
		/* Edited by Aliaksandr Satskou, 01/16/2014 (case #00035790) */
		gen.cases = new List<Case> {
			new Case(
					Subject = 'Test Case', 
					ContactId = gen.employee.Id, 
					Status = 'Open', 
					TMS__Project__c = gen.project.Id),
			new Case(
					Subject = 'Test Case Long Subject', 
					ContactId = gen.employee.Id, 
					Status = 'Open', 
					TMS__Project__c = gen.project.Id),
			new Case(
					ContactId = gen.employee.Id, 
					Status = 'Open', 
					TMS__Project__c = gen.project.Id)};
		insert gen.cases;

		// We need to know CaseNumber, but it is an auto-number field.
		gen.cases = [SELECT CaseNumber FROM Case WHERE Id IN :gen.cases];
	}
	
	private static String saveEntry;
	private static String saveResult;
	
	private static String resaveEntry;
	private static String resaveResult;
	
	private static String resaveEntry2;
	private static String resaveResult2;
	
	@isTest
	private static void resaveWithOvertime_WithoutTimeTracking_TypeNumber_ChangeRegular() {
		saveEntry = 
			'14	14	14	14	14	14	14';
		
		saveResult = 
			'8.00	8.00	8.00	8.00	8.00	null	null \n' +
			'4.00	4.00	4.00	4.00	4.00	null	null \n' +
			'2.00	2.00	2.00	2.00	2.00	14.00	14.00';
		
		
		resaveEntry = 
			'15	15	15	15	15	15	15';
		
		resaveResult = 
			'8.00	8.00	8.00	8.00	8.00	null	null \n' +
			'4.00	4.00	4.00	4.00	4.00	null	null \n' +
			'3.00	3.00	3.00	3.00	3.00	15.00	15.00';
		
		resaveWithOvertime(false, 'Number');
	}
	
	@isTest
	private static void resaveWithOvertime_WithoutTimeTracking_TypeNumber_NoChangeRegular() {
		saveEntry = 
			'14	14	14	14	14	14	14';
		
		saveResult = 
			'8.00	8.00	8.00	8.00	8.00	null	null \n' +
			'4.00	4.00	4.00	4.00	4.00	null	null \n' +
			'2.00	2.00	2.00	2.00	2.00	14.00	14.00';
		
		
		resaveEntry = 
			'8	8	8	8	8	null	null';
		
		resaveResult = 
			'8.00	8.00	8.00	8.00	8.00	null	null \n' +
			'4.00	4.00	4.00	4.00	4.00	null	null \n' +
			'2.00	2.00	2.00	2.00	2.00	14.00	14.00';
		
		resaveWithOvertime(false, 'Number');
	}
	
	@isTest
	private static void resaveWithOvertime_WithoutTimeTracking_TypeNumber_ChangeRegularMonday() {
		saveEntry = 
			'14	14	14	14	14	14	14';
		
		saveResult = 
			'8.00	8.00	8.00	8.00	8.00	null	null \n' +
			'4.00	4.00	4.00	4.00	4.00	null	null \n' +
			'2.00	2.00	2.00	2.00	2.00	14.00	14.00';
		
		
		resaveEntry = 
			'6	8	8	8	8	null	null';
		
		resaveResult = 
			'6.00	8.00	8.00	8.00	8.00	2.00	null \n' +
			'null	4.00	4.00	4.00	4.00	4.00	null \n' +
			'null	2.00	2.00	2.00	2.00	8.00	14.00';
		
		resaveWithOvertime(false, 'Number');
	}
	
	@isTest
	private static void resaveWithOvertime_WithTimeTracking_TypeNumber_ChangeRegular() {
		saveEntry = 
			'07:00 am	07:00 am	07:00 am	07:00 am	07:00 am	07:00 am	07:00 am \n' +
			'10:00 pm	10:00 pm	10:00 pm	10:00 pm	10:00 pm	10:00 pm	10:00 pm \n' +
			'01:00		01:00		01:00		01:00		01:00		01:00		01:00';
		
		saveResult = 
			'8.00	8.00	8.00	8.00	8.00	null	null \n' +
			'4.00	4.00	4.00	4.00	4.00	null	null \n' +
			'2.00	2.00	2.00	2.00	2.00	14.00	14.00';
		
		
		resaveEntry = 
			'07:00 am	07:00 am	07:00 am	07:00 am	07:00 am	07:00 am	07:00 am \n' +
			'11:00 pm	11:00 pm	11:00 pm	11:00 pm	11:00 pm	11:00 pm	11:00 pm \n' +
			'01:00		01:00		01:00		01:00		01:00		01:00		01:00';
		
		resaveResult = 
			'8.00	8.00	8.00	8.00	8.00	null	null \n' +
			'4.00	4.00	4.00	4.00	4.00	null	null \n' +
			'3.00	3.00	3.00	3.00	3.00	15.00	15.00';
		
		resaveWithOvertime(true, 'Number');
	}
	
	@isTest
	private static void resaveWithOvertime_WithTimeTracking_TypeNumber_NoChangeRegular() {
		saveEntry = 
			'07:00 am	07:00 am	07:00 am	07:00 am	07:00 am	07:00 am	07:00 am \n' +
			'10:00 pm	10:00 pm	10:00 pm	10:00 pm	10:00 pm	10:00 pm	10:00 pm \n' +
			'01:00		01:00		01:00		01:00		01:00		01:00		01:00';
		
		saveResult = 
			'8.00	8.00	8.00	8.00	8.00	null	null \n' +
			'4.00	4.00	4.00	4.00	4.00	null	null \n' +
			'2.00	2.00	2.00	2.00	2.00	14.00	14.00';
		
		
		resaveEntry = 
			'07:00 am	07:00 am	07:00 am	07:00 am	07:00 am	07:00 am	07:00 am \n' +
			'10:00 pm	10:00 pm	10:00 pm	10:00 pm	10:00 pm	10:00 pm	10:00 pm \n' +
			'01:00		01:00		01:00		01:00		01:00		01:00		01:00';
		
		resaveResult = 
			'8.00	8.00	8.00	8.00	8.00	null	null \n' +
			'4.00	4.00	4.00	4.00	4.00	null	null \n' +
			'2.00	2.00	2.00	2.00	2.00	14.00	14.00';
		
		resaveWithOvertime(true, 'Number');
	}
	
	@isTest
	private static void resaveWithOvertime_WithoutTimeTracking_TypeTime_ChangeRegular() {
		saveEntry = 
			'14	14	14	14	14	14	14';
		
		saveResult = 
			'8.00	8.00	8.00	8.00	8.00	null	null \n' +
			'4.00	4.00	4.00	4.00	4.00	null	null \n' +
			'2.00	2.00	2.00	2.00	2.00	14.00	14.00';
		
		
		resaveEntry = 
			'15	15	15	15	15	15	15';
		
		resaveResult = 
			'8.00	8.00	8.00	8.00	8.00	null	null \n' +
			'4.00	4.00	4.00	4.00	4.00	null	null \n' +
			'3.00	3.00	3.00	3.00	3.00	15.00	15.00';
		
		resaveWithOvertime(false, 'Time');
	}
	
	@isTest
	private static void resaveWithOvertime_WithoutTimeTracking_TypeTime_NoChangeRegular() {
		saveEntry = 
			'14	14	14	14	14	14	14';
		
		saveResult = 
			'8.00	8.00	8.00	8.00	8.00	null	null \n' +
			'4.00	4.00	4.00	4.00	4.00	null	null \n' +
			'2.00	2.00	2.00	2.00	2.00	14.00	14.00';
		
		
		resaveEntry = 
			'8	8	8	8	8	null	null';
		
		resaveResult = 
			'8.00	8.00	8.00	8.00	8.00	null	null \n' +
			'4.00	4.00	4.00	4.00	4.00	null	null \n' +
			'2.00	2.00	2.00	2.00	2.00	14.00	14.00';
		
		resaveWithOvertime(false, 'Time');
	}
	
	@isTest
	private static void resaveWithOvertime_WithTimeTracking_TypeTime_ChangeRegular() {
		saveEntry = 
			'07:00 am	07:00 am	07:00 am	07:00 am	07:00 am	07:00 am	07:00 am \n' +
			'10:00 pm	10:00 pm	10:00 pm	10:00 pm	10:00 pm	10:00 pm	10:00 pm \n' +
			'01:00		01:00		01:00		01:00		01:00		01:00		01:00';
		
		saveResult = 
			'8.00	8.00	8.00	8.00	8.00	null	null \n' +
			'4.00	4.00	4.00	4.00	4.00	null	null \n' +
			'2.00	2.00	2.00	2.00	2.00	14.00	14.00';
		
		
		resaveEntry = 
			'07:00 am	07:00 am	07:00 am	07:00 am	07:00 am	07:00 am	07:00 am \n' +
			'11:00 pm	11:00 pm	11:00 pm	11:00 pm	11:00 pm	11:00 pm	11:00 pm \n' +
			'01:00		01:00		01:00		01:00		01:00		01:00		01:00';
		
		resaveResult = 
			'8.00	8.00	8.00	8.00	8.00	null	null \n' +
			'4.00	4.00	4.00	4.00	4.00	null	null \n' +
			'3.00	3.00	3.00	3.00	3.00	15.00	15.00';
		
		resaveWithOvertime(true, 'Time');
	}
	
	@isTest
	private static void resaveWithOvertime_WithTimeTracking_TypeTime_NoChangeRegular() {
		saveEntry = 
			'07:00 am	07:00 am	07:00 am	07:00 am	07:00 am	07:00 am	07:00 am \n' +
			'10:00 pm	10:00 pm	10:00 pm	10:00 pm	10:00 pm	10:00 pm	10:00 pm \n' +
			'01:00		01:00		01:00		01:00		01:00		01:00		01:00';
		
		saveResult = 
			'8.00	8.00	8.00	8.00	8.00	null	null \n' +
			'4.00	4.00	4.00	4.00	4.00	null	null \n' +
			'2.00	2.00	2.00	2.00	2.00	14.00	14.00';
		
		
		resaveEntry = 
			'07:00 am	07:00 am	07:00 am	07:00 am	07:00 am	07:00 am	07:00 am \n' +
			'10:00 pm	10:00 pm	10:00 pm	10:00 pm	10:00 pm	10:00 pm	10:00 pm \n' +
			'01:00		01:00		01:00		01:00		01:00		01:00		01:00';
		
		resaveResult = 
			'8.00	8.00	8.00	8.00	8.00	null	null \n' +
			'4.00	4.00	4.00	4.00	4.00	null	null \n' +
			'2.00	2.00	2.00	2.00	2.00	14.00	14.00';
		
		resaveWithOvertime(true, 'Time');
	}
	
	@isTest
	private static void resaveWithOvertime_WithTimeTracking_TypeTime_ChangeRegularMondayTwice() {
		saveEntry = 
			'07:00 am	07:00 am	07:00 am	07:00 am	07:00 am	07:00 am	07:00 am \n' +
			'10:00 pm	10:00 pm	10:00 pm	10:00 pm	10:00 pm	10:00 pm	10:00 pm \n' +
			'01:00		01:00		01:00		01:00		01:00		01:00		01:00';
		
		saveResult = 
			'8.00	8.00	8.00	8.00	8.00	null	null \n' +
			'4.00	4.00	4.00	4.00	4.00	null	null \n' +
			'2.00	2.00	2.00	2.00	2.00	14.00	14.00';
		
		
		resaveEntry = 
			'07:00 am	07:00 am	07:00 am	07:00 am	07:00 am	07:00 am	07:00 am \n' +
			'11:00 am	10:00 pm	10:00 pm	10:00 pm	10:00 pm	10:00 pm	10:00 pm \n' +
			'01:00		01:00		01:00		01:00		01:00		01:00		01:00';
		
		resaveResult = 
			'3.00	8.00	8.00	8.00	8.00	5.00	null \n' +
			'null	4.00	4.00	4.00	4.00	4.00	null \n' +
			'null	2.00	2.00	2.00	2.00	5.00	14.00';
		
		resaveEntry2 = 
			'07:00 am	07:00 am	07:00 am	07:00 am	07:00 am	07:00 am	07:00 am \n' +
			'01:00 pm	10:00 pm	10:00 pm	10:00 pm	10:00 pm	10:00 pm	10:00 pm \n' +
			'01:00		01:00		01:00		01:00		01:00		01:00		01:00';
		
		resaveResult2 = 
			'5.00	8.00	8.00	8.00	8.00	3.00	null \n' +
			'null	4.00	4.00	4.00	4.00	4.00	null \n' +
			'null	2.00	2.00	2.00	2.00	7.00	14.00';
		
		resaveWithOvertime(true, 'Time');
	}
	
	private static void resaveWithOvertime(Boolean enableTimeTracking, String taskDataType) {
		
		TestDataGenerator.InitOptions initOptions = new TestDataGenerator.InitOptions();
		
		initOptions.insertTimeRecords = false;
		
		initOptions.enableTimeTracking = enableTimeTracking;
		initOptions.taskDataType = taskDataType;
		
		initOptions.insertOvetimeTask = true;
		initOptions.insertOverflowTask = true;
		
		gen.initData(initOptions);
		
		loadEntryPageAndSelectRegularTask();
		
		logger.log('saveWithOvertime', 
				'Limits.getQueries() before first saveWithOvertime', 
				Limits.getQueries());
		
		saveWithOvertime(enableTimeTracking, taskDataType, saveEntry, saveResult, 1);
		
		logger.log('saveWithOvertime', 
				'Limits.getQueries() after first saveWithOvertime', 
				Limits.getQueries());
		
		Test.startTest();
		
		logger.log('saveWithOvertime', 
				'Limits.getQueries() before second saveWithOvertime', 
				Limits.getQueries());
		
		saveWithOvertime(enableTimeTracking, taskDataType, resaveEntry, resaveResult, 2);
		
		logger.log('saveWithOvertime', 
				'Limits.getQueries() after second saveWithOvertime', 
				Limits.getQueries());
		
		if (resaveEntry2 != null) {
			logger.log('saveWithOvertime', 
					'Limits.getQueries() before third saveWithOvertime', 
					Limits.getQueries());
				
			saveWithOvertime(enableTimeTracking, taskDataType, resaveEntry2, resaveResult2, 3);
			
			logger.log('saveWithOvertime', 
					'Limits.getQueries() after third saveWithOvertime', 
					Limits.getQueries());
		}
		
		Test.stopTest();
	}
	
	private static void saveWithOvertime(
			Boolean enableTimeTracking, String taskDataType, 
			String entry, String result,
			Integer saveAttempt) {
		
		logger.log('saveWithOvertime', 'enableTimeTracking', enableTimeTracking);
		logger.log('saveWithOvertime', 'taskDataType', taskDataType);
		logger.log('saveWithOvertime', 'entry', entry);
		logger.log('saveWithOvertime', 'result', result);
		
		setTime(enableTimeTracking, taskDataType, entry);
		
		logger.log('saveWithOvertime', 'before save');
		
		controller.save();
		
		logger.log('saveWithOvertime', 'after save');
		
		assertResults(result, saveAttempt);
	}
	
	private static void setTime(Boolean enableTimeTracking, String taskDataType, String entry) {
		TimeSheetController.WrapperClass wrapper = getWrapperForTask(gen.taskRegular.Id);
		
		if (enableTimeTracking) {
			setTimeWithTracking(wrapper.weekDays, entry);
		} else {
			setTimeWithoutTracking(wrapper.weekDays, entry, taskDataType);
		}
	}
	
	private static void setTimeWithTracking(List<TMS__Time__c> times, String entry) {
		String[] entryParts = entry.replace('\t\t', '\t').split('\n');
		
		logger.log('setTimeWithTracking', 'entryParts', entryParts);
		
		String[] startTimes = entryParts[0].trim().split('\t');
		String[] endTimes = entryParts[1].trim().split('\t');
		String[] breaks = entryParts[2].trim().split('\t');
		
		logger.log('setTimeWithTracking', 'startTimes', startTimes);
		logger.log('setTimeWithTracking', 'endTimes', endTimes);
		logger.log('setTimeWithTracking', 'breaks', breaks);
		
		for (Integer i = 0; i < 7; i++) {
			logger.log('setTimeWithTracking', 'i', i);
			
			times[i].TMS__Start_Time__c = startTimes[i].trim();
			times[i].TMS__End_Time__c = endTimes[i].trim();
			times[i].TMS__Break__c = breaks[i].trim();
			
			logger.log('setTimeWithTracking', 'times[i]', times[i]);
		}
	}
	
	private static void setTimeWithoutTracking(
			List<TMS__Time__c> times, String entry, String taskDataType) {
		
		String[] dayTimes = entry.replace('\t\t', '\t').split('\t');
		
		logger.log('setTimeWithoutTracking', 'dayTimes', dayTimes);
		
		for (Integer i = 0; i < 7; i++) {
			logger.log('setTimeWithoutTracking', 'i', i);
			
			if (taskDataType == 'Number') {
				times[i].TMS__Time_Spent__c = 
						dayTimes[i].trim() == 'null' ? null : Decimal.valueOf(dayTimes[i].trim());
			} else {
				times[i].Time_Spent_Time_Format__c = 
						dayTimes[i].trim() == 'null' ? null : dayTimes[i].trim();
			}
			
			logger.log('setTimeWithoutTracking', 'times[i]', times[i]);
		}
	}
	
	private static void assertResults(String resultExpected, Integer saveAttempt) {
		resultExpected = resultExpected.replace('\t\t', '\t');
		
		logger.log('assertResults', 'resultExpected', resultExpected);
		
		
		TimeSheetController.WrapperClass wrapper = getWrapperForTask(gen.taskRegular.Id);
		
		System.assertNotEquals(null, wrapper, 'Wrapper for gen.taskRegular is null.');
		
		String resultReal = generateResultString(wrapper.weekDays) + ' \n';
		
		
		wrapper = getWrapperForTask(gen.taskOvertime.Id);
		
		System.assertNotEquals(null, wrapper, 'Wrapper for gen.taskOvertime is null.');
		
		resultReal += generateResultString(wrapper.weekDays) + ' \n';
		
		
		wrapper = getWrapperForTask(gen.taskOverflow.Id);
		
		System.assertNotEquals(null, wrapper, 'Wrapper for gen.taskOverflow is null.');
		
		resultReal += generateResultString(wrapper.weekDays);
		
		
		System.assertEquals(resultExpected, resultReal, 'Attempt ' + saveAttempt + ' failed.');
	}
	
	private static String generateResultString(List<TMS__Time__c> times) {
		List<String> timeValues = new List<String>();
		for (Integer i = 0; i < 7; i++) {
			timeValues.add(
					times[i].Time_Spent_Formula__c == null 
							? 'null' 
							: '' + times[i].Time_Spent_Formula__c);
		}
		
		return String.join(timeValues, '\t');
	}
	
	/*private static void assertResult(List<TMS__Time__c> times, String result) {
		String[] results = result.trim().split('\t');
		
		logger.log('assertResult', 'results', results);
		
		for (Integer i = 0; i < 7; i++) {
			logger.log('assertResult', 'i', i);
			
			System.assertEquals(
					results[i].trim() == 'null' ? null : Decimal.valueOf(results[i].trim()), 
					times[i].Time_Spent_Formula__c);
		}
	}*/
	
	private static void loadEntryPageAndSelectRegularTask() {
		controller = new TimeSheetController();

		controller.init();
		
		controller.wrapperClassList[0].projectId = gen.project.Id;
		controller.changeTask();
		
		controller.wrapperClassList[0].taskId = gen.taskRegular.Id;
		controller.changePercentage();
	}
	
	private static TimeSheetController.WrapperClass getWrapperForTask(Id taskId) {
		logger.log('getWrapperForTask', 'taskId', taskId);
		
		for (TimeSheetController.WrapperClass wrapper : controller.wrapperClassList) {
			if (taskId == wrapper.taskId) {
				return wrapper;
			}
		}
		
		return null;
	}
}