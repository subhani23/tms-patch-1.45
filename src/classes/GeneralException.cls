/**
* @description  Class for thowing the General custom exception, implments standard 'Exception' class.
*/
global with sharing class GeneralException extends Exception {}