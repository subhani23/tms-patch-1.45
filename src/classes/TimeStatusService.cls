/**
* @author Aliaksandr Satskou
* @date 10/21/2013
* @description Updating status of Time objects.
*/
global class TimeStatusService {

	private static final Integer STATUS_APPROVED = 1;
	private static final Integer STATUS_REJECTED = 2;
	private static final Integer STATUS_SAVE = 3;

	private static final Integer OBJECT_TYPE_TIME = 1;
	private static final Integer OBJECT_TYPE_EXPENSE = 2;

	private static String submitStatus = CustomSettingsUtility.getTimesheetSubmittedStatus();
	private static String approvedStatus = CustomSettingsUtility.getTimesheetApprovedStatus();
	private static String rejectedStatus = CustomSettingsUtility.getTimesheetRejectedStatus();
	private static String saveStatus = CustomSettingsUtility.getTimesheetSaveStatus();
	private static String createdStatus = CustomSettingsUtility.getTimesheetCreatedStatus();
	private static String expenseSubmitStatus = CustomSettingsUtility.getExpenseSubmittedStatus();
	private static String expenseApprovedStatus = CustomSettingsUtility.getExpenseApprovedStatus();
	private static String expenseRejectedStatus = CustomSettingsUtility.getExpenseRejectedStatus();
	private static String expenseSaveStatus = CustomSettingsUtility.getExpenseSaveStatus();

	private static Logger logger = new Logger('TimeStatusService');

	/**
	* @author Aliaksandr Satskou
	* @name updateTimeListStatus
	* @date 10/21/2013
	* @description Updating status of Time objects.
	* @param timeIdList List of Time Id's
	* @param status New status flag
	* @return void
	*/
	WebService static Boolean updateTimeListStatusNew(List<Id> timeIdList, Integer status) {
		List<TMS__Time__c> timeList = [
				SELECT TMS__Status__c
				FROM TMS__Time__c
				WHERE Id IN :timeIdList AND TMS__Status__c = :submitStatus];

		if (timeList.size() > 0) {
			update (List<TMS__Time__c>)changeStatus(timeList, 'TMS__Status__c', 1, status);
			return true;
		} else {
			return false;
		}
	}

	WebService static void updateTimeListStatus(List<Id> timeIdList, Integer status) {}


	/**
	* @author Aliaksandr Satskou
	* @name updateExpenseListStatus
	* @date 10/21/2013
	* @description Updating status of Expense objects.
	* @param expenseIdList List of Expense Id's
	* @param status New status flag
	* @return void
	*/
	WebService static Boolean updateExpenseListStatusNew(List<Id> expenseIdList, Integer status) {
		List<TMS__Expense__c> expenseList = [
				SELECT TMS__Status__c
				FROM TMS__Expense__c
				WHERE Id IN :expenseIdList AND TMS__Status__c = :expenseSubmitStatus];

		if (expenseList.size() > 0) {
			update (List<TMS__Expense__c>)changeStatus(expenseList, 'TMS__Status__c', 2, status);
			return true;
		} else {
			return false;
		}
	}

	WebService static void updateExpenseListStatus(List<Id> expenseIdList, Integer status) {}

	WebService static void updateTimesheetTimeListStatus(List<Id> timesheetIdList, Integer status) { }

	WebService static Boolean updateTimesheetTimeListStatusNew(
			List<Id> timesheetIdList, Integer status) {

		return updateTimesheetTimeListStatusNewOpen(timesheetIdList, status, false);
	}

	/**
	* @author Aliaksandr Satskou
	* @name updateTimesheetTimeListStatus
	* @date 10/21/2013
	* @description Updating status of Time objects for Timesheet object.
	* @param timesheetIdList List of Time Id's
	* @param status New status flag
	* @return void
	*/
	WebService static Boolean updateTimesheetTimeListStatusNewOpen(
			List<Id> timesheetIdList, Integer status, Boolean allTimes) {

		logger.log('updateTimesheetTimeListStatusNewOpen', 'timesheetIdList', timesheetIdList);
		logger.log('updateTimesheetTimeListStatusNewOpen', 'status', status);
		logger.log('updateTimesheetTimeListStatusNewOpen', 'allTimes', allTimes);
		logger.log('updateTimesheetTimeListStatusNewOpen', 'submitStatus', submitStatus);
		List<TMS__Timesheet__c> timesheetList = Database.query(
				' SELECT TMS__Status__c,TMS__Expense_Status__c, ' + 
					' (SELECT TMS__Status__c ' + 
					'  FROM TMS__Time__r), ' + 
					' (SELECT TMS__Status__c ' + 
					'  FROM TMS__Expenses__r) ' + 
				' FROM TMS__Timesheet__c ' + 
				' WHERE Id IN :timesheetIdList ' + 
					(allTimes ? '' : ' AND (TMS__Status__c = :submitStatus OR TMS__Status__c = :createdStatus) ' +
					' AND (TMS__Expense_Status__c =: expenseSubmitStatus OR TMS__Expense_Status__c = null)'));


		if (timesheetList.size() > 0) {
			List<TMS__Time__c> updateTimeList = new List<TMS__Time__c>();
			List<TMS__Expense__c> updateExpenseList = new List<TMS__Expense__c>();

			for (TMS__Timesheet__c timesheet : timesheetList) {
				if (status == STATUS_APPROVED) {
					if(timesheet.TMS__Status__c != createdStatus) {
						timesheet.TMS__Status__c = approvedStatus;
					}
					if(timesheet.TMS__Expense_Status__c != null) {
						timesheet.TMS__Expense_Status__c = approvedStatus;
					}
				}

				if (status == STATUS_REJECTED) {
					if(timesheet.TMS__Status__c != null) {
						timesheet.TMS__Status__c = rejectedStatus;
					}
					if(timesheet.TMS__Expense_Status__c != null) {
						timesheet.TMS__Expense_Status__c = rejectedStatus;
					}
				}

				if (status == STATUS_SAVE) {
					if(timesheet.TMS__Status__c != null) {
						timesheet.TMS__Status__c = saveStatus;
					}
					if(timesheet.TMS__Expense_Status__c != null) {
						timesheet.TMS__Expense_Status__c = saveStatus;
					}
				}

				updateTimeList.addAll((List<TMS__Time__c>)changeStatus(
						timesheet.TMS__Time__r, 'TMS__Status__c', 1, status));

				updateExpenseList.addAll((List<TMS__Expense__c>)changeStatus(
						timesheet.TMS__Expenses__r, 'TMS__Status__c', 2, status));
			}


			update updateTimeList;
			update updateExpenseList;
			update timesheetList;

			return true;
		} else {
			return false;
		}
	}


	/**
	* @author Aliaksandr Satskou
	* @name changeStatus
	* @date 10/21/2013
	* @description Changing status of list of sObject.
	* @param objectList List of sObject
	* @param fieldName Field name
	* @param objectType Object type - Time(1) or Expense(2)
	* @param status New status flag
	* @return List<sObject>
	*/
	private static List<sObject> changeStatus(
			List<sObject> objectList, String fieldName, Integer objectType, Integer status) {

		List<sObject> updateList = new List<sObject>();

		String statusString;
		if (status == STATUS_APPROVED) {
			statusString = (objectType == OBJECT_TYPE_TIME) ? approvedStatus : expenseApprovedStatus;
		} else if (status == STATUS_REJECTED) {
			statusString = (objectType == OBJECT_TYPE_TIME) ? rejectedStatus : expenseRejectedStatus;
		} else if (status == STATUS_SAVE) {
			statusString = (objectType == OBJECT_TYPE_TIME) ? saveStatus : expenseSaveStatus;
		}

		for (sObject obj : objectList) {
			obj.put(fieldName, statusString);
			updateList.add(obj);
		}

		return updateList;
	}

	/**
	* @author Aliaksandr Satskou
	* @name updateTimesheetRejectionReason
	* @date 10/17/2014
	* @description Updating rejection reason of Timesheet objects.
	* @param timesheetId Timesheet Id
	* @param reason Rejection reason
	* @return Boolean
	*/
	WebService static Boolean updateTimesheetRejectionReason(Id timesheetId, String reason) {
		
		if ((reason != null) && (reason != '')) {
			List<TMS__Timesheet__c> timesheetList = [
					SELECT TMS__Status__c,TMS__Expense_Status__c, TMS__Reject_Reason__c,
						(SELECT TMS__Comments__c, TMS__Status__c
						FROM TMS__Time__r),
						(SELECT TMS__Comments__c, TMS__Status__c
						FROM TMS__Expenses__r)
					FROM TMS__Timesheet__c
					WHERE Id = :timesheetId AND (TMS__Status__c = :submitStatus OR TMS__Status__c =:createdStatus)
					AND (TMS__Expense_Status__c =: expenseSubmitStatus
					OR TMS__Expense_Status__c = null)
					LIMIT 1];
			
			if (timesheetList.size() > 0) {
				timesheetList[0].TMS__Reject_Reason__c = reason;
				if(timesheetList[0].TMS__Status__c != createdStatus) {
					timesheetList[0].TMS__Status__c = rejectedStatus;
				}
				if(timesheetList[0].TMS__Expense_Status__c != null) {
					timesheetList[0].TMS__Expense_Status__c = rejectedStatus;
				}

				List<TMS__Time__c> timeList = new List<TMS__Time__c>();
				List<TMS__Expense__c> expenseList = new List<TMS__Expense__c>();

				for (TMS__Time__c timeObj : timesheetList[0].TMS__Time__r) {
					timeObj.TMS__Comments__c = Label.Rejection_Reason + ': ' + reason;
					timeObj.TMS__Status__c = rejectedStatus;

					timeList.add(timeObj);
				}

				for (TMS__Expense__c expense : timesheetList[0].TMS__Expenses__r) {
					expense.TMS__Comments__c = Label.Rejection_Reason + ': ' + reason;
					expense.TMS__Status__c = expenseRejectedStatus;

					expenseList.add(expense);
				}

				update timeList;
				update expenseList;
				update timesheetList;


				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
}