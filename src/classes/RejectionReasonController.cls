/**
* @author Aliaksandr Satskou
* @date 07/12/2013
* @description Controller for rejection reason.
*/
global without sharing class RejectionReasonController {

	/* Added by Aliaksandr Satskou, 04/18/2014 (case #00023506) */
	public String locale {
		get { return UserInfo.getLocale(); }
		private set;
	}


	private String week;
	private String sessionId;

	private String timeIds;
	private String projectResourceIds;
	private String page;

	private String baseString;
	private String projectManager = '';

	private String currentDate = String.valueOf(Date.today());
	private List<FCMS__Session__c> sessionList = new List<FCMS__Session__c>();

	/* Added by Aliaksandr Satskou, 05/27/2013 */
	private static String timesheetRejectedStatus = CustomSettingsUtility.getTimesheetRejectedStatus();
	/* Added by Pandeiswari, 02/08/2018 case #:00068170 */
	private static String rejectionEmailTemplate  = CustomSettingsUtility.getRejectionEmailTemplate();

	private static Boolean isNotifyResource = CustomSettingsUtility.isNotifyResourceOnRejection();
	private static Boolean isNotifyManager = CustomSettingsUtility.isNotifyManagerOnRejection();


	/**
	* @name RejectionReasonController
	* @author Aliaksandr Satskou
	* @data 07/12/2013
	* @description Getting URL parameters and loading session.
	*/
	global RejectionReasonController() {
		System.debug(LoggingLevel.ERROR, ':::::::::::isNotifyResource=' + isNotifyResource);
		System.debug(LoggingLevel.ERROR, ':::::::::::isNotifyManager=' + isNotifyManager);

		week = ApexPages.currentPage().getParameters().get('week');
		sessionId = ApexPages.currentPage().getParameters().get('sessionId');
		page = ApexPages.currentPage().getParameters().get('p');
		timeIds = ApexPages.currentPage().getParameters().get('timeIds');
		projectResourceIds = ApexPages.currentPage().getParameters().get('projectResourceIds');


		if ((sessionId != null) && (sessionId.length() > 0)) {
			sessionList = [
					SELECT Id, FCMS__Session_For__c, FCMS__Session_For__r.Id, FCMS__Session_For__r.Name,
						FCMS__Session_For__r.Email, FCMS__Is_Valid__c, FCMS__SessionId__c
					FROM FCMS__Session__c
					WHERE FCMS__SessionId__c = :sessionId
						AND FCMS__Is_Valid__c = true];


			if ((sessionList != null) && (sessionList.size() > 0)) {
				projectManager = sessionList[0].FCMS__Session_For__r.Name;
			}
		}
	}


	/**
	* @name getBaseString
	* @author Aliaksandr Satskou
	* @data 07/12/2013
	* @description Getting base string.
	* @return String
	*/
	global String getBaseString() {
		baseString = 'Date: ' + currentDate + '\n' + 'Project Manager: ' +
				projectManager + '\n' + 'Rejection Reason: ';

		return baseString;
	}


	/**
	* @name setBaseString
	* @author Aliaksandr Satskou
	* @data 07/12/2013
	* @description Setting base string.
	* @return String
	*/
	public void setBaseString(String baseString) {
		this.baseString = baseString;
	}


	/**
	* @name submit
	* @author Aliaksandr Satskou
	* @data 07/12/2013
	* @description Updating Time objects and sending email.
	* @return PageReference
	*/
	global PageReference submit()
	{
		if ((timeIds != null) && (timeIds.length() > 0))
		{
			if ((projectResourceIds != null) && (projectResourceIds.length() > 0))
			{
				List<String> projectResourceIdList = projectResourceIds.split(',');
				rejectTimesheets(projectResourceIdList, week,baseString);

				List<String> timeIdList = timeIds.split(',');
				rejectTimeEntries(timeIdList, baseString);

				EmailTemplate emailTemplate = getEmailTemplate();

				List<Contact> contactList = [
						SELECT Name, Email
						FROM Contact
						WHERE Id IN :projectResourceIdList
						AND Email != null];

				sendEmail(contactList,timeIdList, emailTemplate, week, baseString, sessionList[0].FCMS__Session_For__r.Email);
			}
		}

		return new PageReference('/apex/TimesheetApproval?sessionId=' + sessionId+'&week='+week+'&approval=manager&p='+page);
	}

	public static void sendEmail(List<Contact> contactList,List<String> timeIdList,
			 EmailTemplate emailtemplate, String week, String baseString, String managerEmailAddress)
	{
		MetadataUtilNew mdTime =
			new MetadataUtilNew(new TMS__Time__c());
			
		/* Added by Pandeiswari, 02/08/2018 case #:00068170 */
		List<TMS__Time__c> timeList = Database.query(
			'SELECT '+ mdTime.getAllFieldsString() + 
			' FROM TMS__Time__c '+  
			' WHERE ID IN: timeIdList');
			
		/* Commented by Pandeiswari, 02/08/2018 case #:00068170 */	
		
		/*List<TMS__Week_Management__c> weekList = [
				SELECT TMS__Week__c
				FROM TMS__Week_Management__c
				WHERE TMS__Week__c = :week];*/

		List<Messaging.SingleEmailMessage> messageList =
				new List<Messaging.SingleEmailMessage>();

		for (Contact contact : contactList)
		{
			Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();

			if (emailTemplate != null)
			{
				message.setTemplateId(emailTemplate.Id);
				message.setTargetObjectId(contact.Id);

				if (timeList.size() > 0)
				{
					message.setWhatId(timeList[0].Id);
				}

				message.setSaveAsActivity(false);
			}
			else
			{
				message.setPlainTextBody(baseString);
			}

			if (isNotifyManager)
			{
				message.setCcAddresses(new List<String> {managerEmailAddress});
			}

			messageList.add(message);
		}

		Messaging.sendEmail(messageList);
	}

	public static EmailTemplate getEmailTemplate()
	{
		EmailTemplate emailTemplate;

		try
		{
			FCMS__CMSSiteSetup__c cmsSiteSetup = [
					SELECT FCMS__Value__c
					FROM FCMS__CMSSiteSetup__c
					WHERE FCMS__Name__c = 'Reject Timesheet Template Name'
					LIMIT 1];

			if ((cmsSiteSetup != null) && (cmsSiteSetup.FCMS__Value__c != null)
					&& (cmsSiteSetup.FCMS__Value__c != ''))
			{
				emailTemplate = getEmailTemplateByName(cmsSiteSetup.FCMS__Value__c);
			}

			if (emailTemplate == null)
			{
				emailTemplate = getEmailTemplateByName(rejectionEmailTemplate);
			}
		}
		catch(Exception e)
		{
			emailTemplate = getEmailTemplateByName(rejectionEmailTemplate);
		}

		return emailTemplate;
	}

	/**
	* @name getEmailTemplateByName
	* @author Aliaksandr Satskou
	* @data 07/12/2013
	* @description Getting email template by name.
	* @param name Name of email template
	* @return EmailTemplate
	*/
	private static EmailTemplate getEmailTemplateByName(String name)
	{
		List<EmailTemplate> templateList = [SELECT Name, Subject FROM EmailTemplate WHERE Name = :name];

		if (templateList.size() > 0)
		{
			return templateList[0];
		}

		return null;
	}

	public static void rejectTimeEntries(List<Id> timeIdList, String baseString)
	{
		List<TMS__Time__c> timeList = [
				SELECT TMS__Time_Spent__c, TMS__Status__c, TMS__Comments__c
				FROM TMS__Time__c
				WHERE Id IN :timeIdList];

		for (TMS__Time__c timeObj : timeList)
		{
			timeObj.TMS__Status__c = timesheetRejectedStatus;

			timeObj.TMS__Comments__c = (timeObj.TMS__Comments__c != null)
					? timeObj.TMS__Comments__c + '\n' + baseString
					: baseString;
		}

		update timeList;
	}

	public static void rejectTimesheets(List<Id> projectResourceIdList, String week, String reason)
	{
		Boolean isTimesheetEnabled = TimesheetHelper.isObjectEnabled();

		if (isTimesheetEnabled)
		{
			List<TMS__Timesheet__c> timesheetList = [
					SELECT TMS__Status__c,TMS__Reject_Reason__c
					FROM TMS__Timesheet__c
					WHERE TMS__Project_Resource__r.TMS__Contact__c IN :projectResourceIdList
					AND TMS__Week_Management__r.TMS__Week__c = :week];

			for (TMS__Timesheet__c timesheet : timesheetList)
			{
				timesheet.TMS__Status__c = timesheetRejectedStatus;
				timesheet.TMS__Reject_Reason__c = reason;
			}

			update timesheetList;
		}
	}
}