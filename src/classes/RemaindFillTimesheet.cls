public class RemaindFillTimesheet
{
	/* Added by Aliaksandr Satskou, 04/18/2014 (case #00023506) */
	public String locale {
		get { return UserInfo.getLocale(); }
		private set;
	}


	public RemaindFillTimesheet(ApexPages.StandardController controller)
	{
		this();
	}

	public RemaindFillTimesheet()
	{
		List<TMS__Week_Management__c> weekMgmt = [
				SELECT  TMS__Week__c, TMS__End_Date__c
				FROM TMS__Week_Management__c
				WHERE TMS__Active__c = true
				ORDER BY TMS__End_Date__c DESC
				LIMIT 1];
		System.debug('*****Week mgmt = ' + weekMgmt);

		if(weekMgmt.size() > 0)
		{
			List<Id> filledContacts = GetFilledContacts(weekMgmt[0].TMS__End_Date__c);
			System.debug('*******Amrender contact Ids Map = ' + filledContacts.size());

			List<AggregateResult> uniqueContacts = [
					SELECT TMS__Contact__c
					FROM TMS__Project_Resource__c
					WHERE TMS__Contact__r.FCMS__UserName__c != null AND TMS__Contact__c NOT IN :filledContacts
					GROUP BY TMS__Contact__c];
			System.debug('*******Amrender uniqueContactIdsList = ' + uniqueContacts.size() + '*******' + uniqueContacts);

			List<Id> uniqueContactIds = GetIdsFromAggregateResults(uniqueContacts, 'TMS__Contact__c');
			List<Contact> conList = [
					SELECT Id, Name, Email
					FROM Contact
					WHERE Id IN :uniqueContactIds AND Email != null AND Email != ''];

			if(conList != null && conList.size() > 0)
			{
				List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
				for(Contact contact : conList)
				{
					Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
					mail.setToAddresses(new List<String>{contact.Email});
					mail.setSubject('Your TimeSheet is due');
					mail.setPlainTextBody('Dear ' + (string.valueOf(contact.Name)).toUpperCase() + ',' + '\nYour timesheet is due for the period ' + weekMgmt[0].TMS__Week__c);
					mail.setSaveAsActivity(false);
					mailList.add(mail);
				}
				System.debug('*********Amrender mailList =' + mailList.size() + '*****' + mailList);

				if(mailList.size() > 0)
				{
					try
					{
						Messaging.SendEmailResult [] r = Messaging.sendEmail(mailList);
						System.debug('**********Done= ' + r);
					}
					catch(Exception e)
					{
						System.debug('**********error = ' + e);
					}
				}
			}
		}
	}

	private List<Id> GetFilledContacts(Date dt)
	{
		System.debug('*********in fill timesheet date = ' + dt);

		List<AggregateResult> timeList = [
				SELECT TMS__Task__r.TMS__Project_Resource__r.TMS__Contact__c
				FROM TMS__Time__c
				WHERE TMS__Date__c = :dt.addDays(1) OR
					  TMS__Date__c = :dt.addDays(2) OR
					  TMS__Date__c = :dt.addDays(3) OR
					  TMS__Date__c = :dt.addDays(4) OR
					  TMS__Date__c = :dt.addDays(5)
				GROUP BY TMS__Task__r.TMS__Project_Resource__r.TMS__Contact__c];

		return GetIdsFromAggregateResults(timeList, 'TMS__Task__r.TMS__Project_Resource__r.TMS__Contact__c');
	}

	private List<Id> GetIdsFromAggregateResults(List<AggregateResult> aggregateResults, string idFieldName)
	{
		List<Id> result = new List<Id>();
		for (AggregateResult aggregateResult : aggregateResults)
			result.add((Id)aggregateResult.get(idFieldName));
		return result;
	}

	public static testMethod void tstRemaindFillTimesheet()
	{
		RemaindFillTimesheet obj = new RemaindFillTimesheet();
	}
}