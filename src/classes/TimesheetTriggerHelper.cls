/**
* @author Aliaksandr Satskou
* @date 12/04/2013
* @description Helper class for TimesheetTrigger.
*/
public without sharing class TimesheetTriggerHelper {
	
	private static Logger logger = new Logger('TimesheetTriggerHelper');
	
	/**
	* @author Aliaksandr Satskou
	* @date 12/04/2013
	* @name updateTimesheetFieldsFromPlacement
	* @description Updating fields from Placement.
	* @param timesheetList List of Timesheet objects
	* @return void
	*/
	public static void updateTimesheetFieldsFromPlacement(List<TMS__Timesheet__c> timesheetList) {
		Set<Id> projectIdSet = new Set<Id>();
	
		for (TMS__Timesheet__c timesheet : timesheetList) {
			if (timesheet.TMS__Project__c != null) {
				projectIdSet.add(timesheet.TMS__Project__c);
			}
		}
		
		
		try {
			String query = 'SELECT AVTRRT__Account_Manager__c, AVTRRT__Employer__c, Project_TMS__c FROM ' + 
					'AVTRRT__Placement__c WHERE Project_TMS__c IN :projectIdSet ORDER BY CreatedDate DESC';
			
			List<sObject> placementList = DataBase.query(query);
			
			
			if (placementList.size() > 0) {
				Map<Id, sObject> projectIdPlacementMap = new Map<Id, sObject>();
				
				for (sObject placement : placementList) {
					sObject placementFromMap = projectIdPlacementMap.get((Id)placement.get('Project_TMS__c'));
					
					if (placementFromMap == null) {
						projectIdPlacementMap.put((Id)placement.get('Project_TMS__c'), placement);
					}
				}
				
				
				for (TMS__Timesheet__c timesheet : timesheetList) {
					if (timesheet.TMS__Project__c != null) {
						sObject placement = projectIdPlacementMap.get(timesheet.TMS__Project__c);
						
						timesheet.TMS__Account__c = (Id)placement.get('AVTRRT__Employer__c');
						timesheet.TMS__Account_Manager__c = (Id)placement.get('AVTRRT__Account_Manager__c');
						timesheet.TMS__Placement_ID__c = (String)placement.get('Id');
					}
				}
			}
		} catch (Exception e) { }
	}
	
	
	/**
	* @author Aliaksandr Satskou
	* @date 12/04/2013
	* @name updateTimesheetHours
	* @description Updating Timesheet Hours field.
	* @param timeList List of Time objects
	* @return void
	*/
	public static void updateTimesheet(List<TMS__Time__c> times) {
		List<TMS__Timesheet__c> timesheets = [
				SELECT TMS__Candidate__c, (SELECT TMS__Status__c FROM TMS__Time__r)
				FROM TMS__Timesheet__c
				WHERE Id IN :GroupByHelper.getFieldValuesIds(times, 'TMS__Timesheet__c')];
		
		logger.log('updateTimesheet', 'timesheets', timesheets);
		
		setTimesheetStatuses(timesheets);
		
		Map<String, TMS__TimesheetCustomSettings__c> tmsSettingMap =
				TMS__TimesheetCustomSettings__c.getAll();
		if (tmsSettingMap != null) {
			TMS__TimesheetCustomSettings__c tmsSetting = tmsSettingMap.get('ProjectStatusToShow');
			if (tmsSetting != null) {
				Boolean isUpdateTimesheetHours = tmsSetting.TMS__Update_Timesheet_Hours__c;
				if (isUpdateTimesheetHours == true) {
					updateTimesheetHours(timesheets);
				}
			}
		}
		
		update timesheets;
	}
	
	private static void updateTimesheetHours(List<TMS__Timesheet__c> timesheets) {
		List<AggregateResult> groupTimeList = [
				SELECT TMS__Timesheet__c, SUM(TMS__Time_Spent_Formula__c) deGroupTime
				FROM TMS__Time__c
				WHERE TMS__Timesheet__c IN :timesheets
				GROUP BY TMS__Timesheet__c];
		
		for (TMS__Timesheet__c timesheet : timesheets) {
			for (AggregateResult groupTime : groupTimeList) {
				if (groupTime.get('TMS__Timesheet__c') == timesheet.Id) {
					timesheet.TMS__Timesheet_Hours__c = (Decimal)groupTime.get('deGroupTime');
				}
			}
		}
	}
	
	private static void setTimesheetStatuses(List<TMS__Timesheet__c> timesheets) {
		for (TMS__Timesheet__c timesheet : timesheets) {
			logger.log('setTimesheetStatuses', 'timesheet', timesheet);
			
			Set<String> timeStatuses = 
					GroupByHelper.getFieldValuesSetStrings(timesheet.TMS__Time__r, 'TMS__Status__c');
			
			logger.log('setTimesheetStatuses', 'timeStatuses', timeStatuses);
			
			List<String> timeStatusesList = new List<String>();
			timeStatusesList.addAll(timeStatuses);
			 
			if (timeStatuses.size() > 1) {
				
				String timesheetMultipleStatus = CustomSettingsUtility.getTimesheetMultipleStatus();
				
				timesheet.TMS__Status__c = timesheetMultipleStatus != null 
											? timesheetMultipleStatus 
											: 'Partial';
			} else {
				timesheet.TMS__Status__c = timeStatusesList[0];
			}
		}
	}
}