/**
* @author Aliaksandr Satskou
* @date 12/13/2013
* @description Fill Timesheet functionality.
*/
public with sharing class FillTimesheetController {

	private String aes128KeyBase64 = 'VEZ27Xlqw+mKLblpeAl93g==';
	
	/* Added by Aliaksandr Satskou, 04/18/2014 (case #00023506) */
	public String locale {
		get { return UserInfo.getLocale(); }
		private set;
	}


	private static String sortOrder = '';

	private Map<Id, TMS__Project_Resource__c> resourceMap;
	public List<TMS__Project_Resource__c> resourceList {get; set;}
	public String resourceId {get; set;}


	/**
	* @author Aliaksandr Satskou
	* @date 12/13/2013
	* @name FillTimesheetController
	* @description Getting and forming data.
	*/
	public FillTimesheetController() {
		String weekId = ApexPages.currentPage().getParameters().get('weekId');


		Map<String, TMS__TimesheetCustomSettings__c> tmsSettingMap = TMS__TimesheetCustomSettings__c.getAll();

		if (tmsSettingMap != null) {
			TMS__TimesheetCustomSettings__c tmsSetting = tmsSettingMap.get('ProjectStatusToShow');

			if (tmsSetting != null) {
				sortOrder = (String)tmsSetting.TMS__Fill_Timesheet_Column_Name_Order__c;
			}
		}

		List <String> statusesToShow = new List<String>();
				String TMSProjectStatusesToShow =
						TMS__TimesheetCustomSettings__c.getInstance('ProjectStatusToShow')
								.TMS__TMSProjectStatusToShow__c;

				if(TMSProjectStatusesToShow != null) {
					statusesToShow = TMSProjectStatusesToShow.split(';');
					for(Integer i=0; i < statusesToShow.size(); i++) {
						statusesToShow[i] = '\''+  statusesToShow[i].trim() +'\'';
					}
		}


		String query =
				'SELECT TMS__Contact__c, TMS__Contact__r.Name, TMS__Project__c, TMS__Project__r.Name, ' +
				'TMS__Project__r.TMS__Client__c, TMS__Project__r.TMS__Client__r.Name, ' +
				'TMS__Project__r.TMS__Start_Date__c, TMS__Project__r.TMS__End_Date__c, ' +
				'TMS__Contact__r.FCMS__Username__c, TMS__Contact__r.FCMS__Password__c, ' +
				'(SELECT Name, TMS__Status__c FROM TMS__Timesheets__r WHERE ' +
				'TMS__Week_Management__c = :weekId) FROM TMS__Project_Resource__c WHERE ' +
				'TMS__Project__r.TMS__Internal_Approvers__c includes ' +
				'(\'' + UserInfo.getName() + '\') and TMS__Project__r.TMS__Project_Status__c IN '+ statusesToShow + 'ORDER BY ' + sortOrder;

		//system.assert(false,query);
		resourceList = DataBase.query(query);


		resourceMap = new Map<Id, TMS__Project_Resource__c>();

		for (TMS__Project_Resource__c resource : resourceList) {
			resourceMap.put(resource.Id, resource);
		}
	}


	/**
	* @author Aliaksandr Satskou
	* @date 12/02/2013
	* @name login
	* @description Login to Timesheet for Employee.
	* @return void
	*/
	public PageReference login() {
		TMS__Project_Resource__c resource = resourceMap.get((Id)resourceId);
		
		String encryptedUsername = encryptToBase64(resource.TMS__Contact__r.FCMS__UserName__c);
		
		System.debug(LoggingLevel.ERROR, '::::::encryptedUsername = ' + encryptedUsername);
		
		String encryptedPassword = encryptToBase64(resource.TMS__Contact__r.FCMS__Password__c);

		return new PageReference(
				TMS__TimesheetCustomSettingsHierarchy__c.getInstance().TMS__Site_URL__c + 
				'FCMS__DoLogin?username=' + encryptedUsername + '&password=' + encryptedPassword);
	}
	
	private String encryptToBase64(String textToEncrypt) {
		return encryptToBase64(Blob.valueOf(textToEncrypt));
	}
	
	private String encryptToBase64(Blob blobToEncrypt) {
		Blob aes128Key = EncodingUtil.base64Decode(aes128KeyBase64);
		Blob encrypted = Crypto.encryptWithManagedIV('AES128', aes128Key, blobToEncrypt);
		String encryptedBase64 = EncodingUtil.base64Encode(encrypted);
		
		System.debug(LoggingLevel.ERROR, '::::::encryptedBase64 = ' + encryptedBase64);
		
		return EncodingUtil.urlEncode(encryptedBase64, 'UTF-8');
	}
}