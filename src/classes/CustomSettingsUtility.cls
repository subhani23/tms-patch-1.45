/**
* @author Aliaksandr Satskou
* @date 12/24/2012
* @description Working with data from Custom Settings.
*/
public with sharing class CustomSettingsUtility {
	private static Map<String, TMS__TimesheetCustomSettings__c> dataSetNameCustomSettingListTypeMap;
	private static Map<String, TMS__Payroll_Provider_Settings__c> dataSetNamePayrollProviderSettingMap;
	private static Map<String, TMS__Payroll_Settings__c> dataSetNamePayrollSettingMap;


	/**
	* @author Aliaksandr Satskou
	* @date 12/24/2012
	* @description Checking 'dataSetNameCustomSettingListTypeMap' map.
	* @return void
	*/
	private static void retrieveCustomSettingsListTypeData() {
		if (dataSetNameCustomSettingListTypeMap == null) {
			dataSetNameCustomSettingListTypeMap = TMS__TimesheetCustomSettings__c.getAll();
		}
	}


	/**
	* @author Aliaksandr Satskou
	* @date 12/26/2012
	* @description Checking 'dataSetNamePayrollProviderSettingMap' map.
	* @return void
	*/
	private static void retrievePayrollProviderSettingsData() {
		if (dataSetNamePayrollProviderSettingMap == null) {
			dataSetNamePayrollProviderSettingMap = TMS__Payroll_Provider_Settings__c.getAll();
		}
	}


	/**
	* @author Aliaksandr Satskou
	* @date 12/26/2012
	* @description Checking 'dataSetNamePayrollSettingMap' map.
	* @return void
	*/
	private static void retrievePayrollSettingsData() {
		if (dataSetNamePayrollSettingMap == null) {
			dataSetNamePayrollSettingMap = TMS__Payroll_Settings__c.getAll();
		}
	}


	/**
	* @author Aliaksandr Satskou
	* @date 12/24/2012
	* @description Getting list of all Custom Settings with 'List' type.
	* @return List<TMS__TimesheetCustomSettings__c> List of all Custom Settings with 'List' type
	*/
	public static List<TMS__TimesheetCustomSettings__c> getCustomSettingListTypeList() {
		retrieveCustomSettingsListTypeData();

		return (dataSetNameCustomSettingListTypeMap != null)
				? dataSetNameCustomSettingListTypeMap.values()
				: null;
	}


	/**
	* @author Aliaksandr Satskou
	* @date 12/24/2012
	* @description Getting Custom Setting with 'List' type by dataset name.
	* @param p_dataSetName Name of dataset
	* @return TMS__TimesheetCustomSettings__c Custom Setting with 'List' type
	*/
	public static TMS__TimesheetCustomSettings__c getCustomSettingListTypeByDataSetName(
			String p_dataSetName) {
		retrieveCustomSettingsListTypeData();

		return (dataSetNameCustomSettingListTypeMap != null)
				? dataSetNameCustomSettingListTypeMap.get(p_dataSetName)
				: null;
	}


	/**
	* @author Aliaksandr Satskou
	* @date 12/24/2012
	* @description Getting value of Custom Setting with 'List' type by dataset name and setting name.
	* @param p_dataSetName Name of dataset
	* @param p_settingName Name of setting
	* @return Object Value of setting
	*/
	public static Object getCustomSettingListTypeValue(String p_dataSetName, String p_settingName) {
		retrieveCustomSettingsListTypeData();
		TMS__TimesheetCustomSettings__c v_customSettingListType = null;

		if (p_dataSetName != null) {
			v_customSettingListType = dataSetNameCustomSettingListTypeMap.get(p_dataSetName);
		} else {
			List<TMS__TimesheetCustomSettings__c> v_customSettingListTypeList =
					getCustomSettingListTypeList();

			if ((v_customSettingListTypeList != null) && (v_customSettingListTypeList.size() > 0)) {
				v_customSettingListType = v_customSettingListTypeList[0];
			}
		}

		return (v_customSettingListType != null)
				? v_customSettingListType.get(p_settingName)
				: null;
	}


	/**
	* @author Aliaksandr Satskou
	* @date 12/24/2012
	* @description Getting Custom Setting with 'Hierarchy' type for the current User.
	* @return TMS__TimesheetCustomSettingsHierarchy__c Custom Setting with 'Hierarchy' type
	*/
	public static TMS__TimesheetCustomSettingsHierarchy__c getCustomSettingHierarchyType() {
		return TMS__TimesheetCustomSettingsHierarchy__c.getInstance();
	}


	/**
	* @author Aliaksandr Satskou
	* @date 12/24/2012
	* @description Getting Custom Setting with 'Hierarchy' type by User or Profile Id.
	* @param p_userOrProfileId User or Profile Id
	* @return TMS__TimesheetCustomSettingsHierarchy__c Custom Setting with 'Hierarchy' type
	*/
	public static TMS__TimesheetCustomSettingsHierarchy__c getCustomSettingHierarchyTypeByUserOrProfileId(
			String p_userOrProfileId) {
		return TMS__TimesheetCustomSettingsHierarchy__c.getInstance(p_userOrProfileId);
	}


	/**
	* @author Aliaksandr Satskou
	* @date 12/24/2012
	* @description Getting Custom Setting with 'Hierarchy' type for the organization.
	* @return TMS__TimesheetCustomSettingsHierarchy__c Custom Setting with 'Hierarchy' type
	*/
	public static TMS__TimesheetCustomSettingsHierarchy__c getCustomSettingHierarchyTypeOrgDefaults() {
		return TMS__TimesheetCustomSettingsHierarchy__c.getOrgDefaults();
	}


	/**
	* @author Aliaksandr Satskou
	* @date 12/26/2012
	* @description Getting list of all Payroll Provider Custom Settings.
	* @return List<TMS__Payroll_Provider_Settings__c> List of all Payroll Provider Custom Settings
	*/
	public static List<TMS__Payroll_Provider_Settings__c> getPayrollProviderSettingList() {
		retrievePayrollProviderSettingsData();

		return (dataSetNamePayrollProviderSettingMap != null)
				? dataSetNamePayrollProviderSettingMap.values()
				: null;
	}


	/**
	* @author Aliaksandr Satskou
	* @date 12/26/2012
	* @description Getting Payroll Provider Custom Setting by dataset name.
	* @param p_dataSetName Name of dataset
	* @return TMS__Payroll_Provider_Settings__c Payroll Provider Custom Setting
	*/
	public static TMS__Payroll_Provider_Settings__c getPayrollProviderSettingByDataSetName(
			String p_dataSetName) {
		retrievePayrollProviderSettingsData();

		return (dataSetNamePayrollProviderSettingMap != null)
				? dataSetNamePayrollProviderSettingMap.get(p_dataSetName)
				: null;
	}


	/**
	* @author Aliaksandr Satskou
	* @date 12/26/2012
	* @description Getting value of Payroll Provider Custom Setting by dataset name and setting name.
	* @param p_dataSetName Name of dataset
	* @param p_settingName Name of setting
	* @return Object Value of setting
	*/
	public static Object getPayrollProviderSettingValue(String p_dataSetName, String p_settingName) {
		retrievePayrollProviderSettingsData();
		TMS__Payroll_Provider_Settings__c v_payrollProviderSetting = null;

		if (p_dataSetName != null) {
			v_payrollProviderSetting = dataSetNamePayrollProviderSettingMap.get(p_dataSetName);
		} else {
			List<TMS__Payroll_Provider_Settings__c> v_payrollProviderSettingList =
					getPayrollProviderSettingList();

			if ((v_payrollProviderSettingList != null) && (v_payrollProviderSettingList.size() > 0)) {
				v_payrollProviderSetting = v_payrollProviderSettingList[0];
			}
		}

		return (v_payrollProviderSetting != null)
				? v_payrollProviderSetting.get(p_settingName)
				: null;
	}


	/**
	* @author Aliaksandr Satskou
	* @date 12/26/2012
	* @description Getting list of all Payroll Providers.
	* @return List<String> List of Providers
	*/
	public static List<String> getPayrollProviderNameList() {
		retrievePayrollProviderSettingsData();

		Set<String> v_payrollProviderNameSet = new Set<String>();
		List<TMS__Payroll_Provider_Settings__c> v_payrollProviderSettingList =
				getPayrollProviderSettingList();

		if (v_payrollProviderSettingList != null) {
			for (TMS__Payroll_Provider_Settings__c v_payrollProviderSetting : v_payrollProviderSettingList) {
				if (v_payrollProviderSetting.TMS__Payroll_Provider_Name__c != null) {
					v_payrollProviderNameSet.add(v_payrollProviderSetting.TMS__Payroll_Provider_Name__c);
				}
			}
		}

		return new List<String>(v_payrollProviderNameSet);
	}


	/**
	* @author Aliaksandr Satskou
	* @date 12/26/2012
	* @description Getting list of all Payroll Custom Settings.
	* @return List<TMS__Payroll_Settings__c> List of all Payroll Custom Settings
	*/
	public static List<TMS__Payroll_Settings__c> getPayrollSettingList() {
		retrievePayrollSettingsData();

		return (dataSetNamePayrollSettingMap != null)
				? dataSetNamePayrollSettingMap.values()
				: null;
	}


	/**
	* @author Aliaksandr Satskou
	* @date 12/26/2012
	* @description Getting Payroll Custom Setting by dataset name.
	* @param p_dataSetName Name of dataset
	* @return TMS__Payroll_Settings__c Payroll Custom Setting
	*/
	public static TMS__Payroll_Settings__c getPayrollSettingByDataSetName(String p_dataSetName) {
		retrievePayrollSettingsData();

		return (dataSetNamePayrollSettingMap != null)
				? dataSetNamePayrollSettingMap.get(p_dataSetName)
				: null;
	}


	/**
	* @author Aliaksandr Satskou
	* @date 12/26/2012
	* @description Getting value of Payroll Custom Setting by dataset name and setting name.
	* @param p_dataSetName Name of dataset
	* @param p_settingName Name of setting
	* @return Object Value of setting
	*/
	public static Object getPayrollSettingValue(String p_dataSetName, String p_settingName) {
		retrievePayrollSettingsData();
		TMS__Payroll_Settings__c v_payrollSetting = null;

		if (p_dataSetName != null) {
			v_payrollSetting = dataSetNamePayrollSettingMap.get(p_dataSetName);
		} else {
			List<TMS__Payroll_Settings__c> v_payrollSettingList = getPayrollSettingList();

			if ((v_payrollSettingList != null) && (v_payrollSettingList.size() > 0)) {
				v_payrollSetting = v_payrollSettingList[0];
			}
		}

		return (v_payrollSetting != null)
				? v_payrollSetting.get(p_settingName)
				: null;
	}


	/**
	* @author Aliaksandr Satskou
	* @date 12/26/2012
	* @description Getting set of Payroll file names.
	* @return Set<String> Set of Payroll file names
	*/
	public static Set<String> getPayrollFileNameSet() {
		retrievePayrollSettingsData();

		Set<String> v_payrollFileNameSet = new Set<String>();
		List<TMS__Payroll_Settings__c> v_payrollSettingList = getPayrollSettingList();

		if (v_payrollSettingList != null) {
			for (TMS__Payroll_Settings__c v_payrollSetting : v_payrollSettingList) {
				v_payrollFileNameSet.add(v_payrollSetting.TMS__File_Name__c);
			}
		}

		return v_payrollFileNameSet;
	}

	/**
	* @author Aliaksandr Satskou
	* @name getTimesheetSaveStatus
	* @date 04/29/2013
	* @description Getting save status for timesheet.
	* @return String
	*/
	public static String getTimesheetCreatedStatus() {
		return (String)getCustomSettingListTypeValue(null, 'TMS__Timesheet_Created_Status__c');
	}
	
	/**
	* @author Aliaksandr Satskou
	* @name getTimesheetSaveStatus
	* @date 04/29/2013
	* @description Getting save status for timesheet.
	* @return String
	*/
	public static String getTimesheetSaveStatus() {
		return (String)getCustomSettingListTypeValue(null, 'TMS__Timesheet_Save_Status__c');
	}


	/**
	* @author Aliaksandr Satskou
	* @name getTimesheetSubmittedStatus
	* @date 04/29/2013
	* @description Getting submitted status for timesheet.
	* @return String
	*/
	public static String getTimesheetSubmittedStatus() {
		return (String)getCustomSettingListTypeValue(null, 'TMS__Timesheet_Submitted_Status__c');
	}


	/**
	* @author Aliaksandr Satskou
	* @name getTimesheetApprovedStatus
	* @date 04/29/2013
	* @description Getting approved status for timesheet.
	* @return String
	*/
	public static String getTimesheetApprovedStatus() {
		return (String)getCustomSettingListTypeValue(null, 'TMS__Timesheet_Approved_Status__c');
	}


	/**
	* @author Aliaksandr Satskou
	* @name getTimesheetApprovedStatus
	* @date 04/29/2013
	* @description Getting rejected status for timesheet.
	* @return String
	*/
	public static String getTimesheetRejectedStatus() {
		return (String)getCustomSettingListTypeValue(null, 'TMS__Timesheet_Rejected_Status__c');
	}
	
	/**
	* @author Pandeiswari
	* @name getRejectionEmailTemplate
	* @date 02/08/2018	
	* @description Getting email template for timesheet rejection.
	* @return String
	*/
	public static String getRejectionEmailTemplate() {
		return (String)getCustomSettingListTypeValue(null, 'TMS__Email_Template_for_Timesheet_Rejection__c');
	}
	
	
	/**
	* @author Pandeiswari
	* @name getFirstReminderTemplate
	* @date 02/08/2018	
	* @description Getting email template for Missing timesheet.
	* @return String
	*/
	public static String getFirstReminderTemplate() {
		return (String)getCustomSettingListTypeValue(null, 'TMS__Missing_Timesheet_1st_Reminder_Template__c');
	}
	
	
	/**
	* @author Pandeiswari
	* @name getFinalEmailTemplate
	* @date 02/08/2018	
	* @description Getting email template for Missing timesheet
	* @return String
	*/
	public static String getFinalEmailTemplate() {
		return (String)getCustomSettingListTypeValue(null, 'TMS__Missing_Timeshet_Final_Reminder_Template__c');
	}

	/**
	* @author Pandeiswari
	* @name getMissingTimesheetDisplayColumns
	* @date 02/13/2018	
	* @description Getting display columns list for missing timesheet.
	* @return String
	*/
	public static String getMissingTimesheetDisplayColumns() {
		return (String)getCustomSettingListTypeValue(null, 'TMS__Missing_Timesheet_Display_Columns__c');
	}
	
	/**
	* @author Pandeiswari
	* @name getMissingTimesheetCriteria
	* @date 02/13/2018	
	* @description Getting candidate criteria for missing timesheet.
	* @return String
	*/
	public static String getMissingTimesheetCriteria() {
		return (String)getCustomSettingListTypeValue(null, 'TMS__Missing_Timesheet_Candidate_Criteria__c');
	}
	
	/**
	* @author Pandeiswari
	* @name getMissingTimesheetCriteria
	* @date 02/13/2018	
	* @description Getting Missing Timesheet Status
	* @return String
	*/
	public static String getMissingTimesheetStatus() {
		return (String)getCustomSettingListTypeValue(null, 'TMS__Missing_Timesheet_Status_to_Consider__c');
	}
	
	/**
	* @author Aliaksandr Satskou
	* @name getTimesheetSaveStatus
	* @date 05/27/2013
	* @description Getting save status for expense.
	* @return String
	*/
	public static String getExpenseSaveStatus() {
		return (String)getCustomSettingListTypeValue(null, 'TMS__Expense_Save_Status__c');
	}


	/**
	* @author Aliaksandr Satskou
	* @name getTimesheetSaveStatus
	* @date 05/27/2013
	* @description Getting submitted status for expense.
	* @return String
	*/
	public static String getExpenseSubmittedStatus() {
		return (String)getCustomSettingListTypeValue(null, 'TMS__Expense_Submitted_Status__c');
	}


	/**
	* @author Aliaksandr Satskou
	* @name getTimesheetSaveStatus
	* @date 05/27/2013
	* @description Getting approved status for expense.
	* @return String
	*/
	public static String getExpenseApprovedStatus() {
		return (String)getCustomSettingListTypeValue(null, 'TMS__Expense_Approved_Status__c');
	}


	/**
	* @author Aliaksandr Satskou
	* @name getTimesheetSaveStatus
	* @date 05/27/2013
	* @description Getting rejected status for expense.
	* @return String
	*/
	public static String getExpenseRejectedStatus() {
		return (String)getCustomSettingListTypeValue(null, 'TMS__Expense_Rejected_Status__c');
	}

	
	/**
	* @author Pandeiswari
	* @name getTimesheetMultipleStatus
	* @date 05/27/2013
	* @description Getting Multiple timesheet status.
	* @return String
	*/
	public static String getTimesheetMultipleStatus() {
		return (String)getCustomSettingListTypeValue(null, 'TMS__Timesheet_Multiple_Status__c');
	}
	
	/**
	* @author Pandeiswari
	* @name getExpenseMultipleStatus
	* @date 05/27/2013
	* @description Getting Multiple Expense status.
	* @return String
	*/
	public static String getExpenseMultipleStatus() {
		return (String)getCustomSettingListTypeValue(null, 'TMS__Expense_Multiple_Status__c');
	}
	
	/**
	* @author Pandeiswari
	* @name getExpenseEditableStatus
	* @date 05/27/2013
	* @description Getting Expense EditableStatus
	* @return String
	*/
	public static String getExpenseEditableStatus() {
		return (String)getCustomSettingListTypeValue(null, 'TMS__Expense_Editable_Status__c');
	}
	
	/**
	* @author Aliaksandr Satskou
	* @name isNotifyResourceOnRejection
	* @date 07/12/2013
	* @description Notify resource on timesheet rejection.
	* @return Boolean
	*/
	public static Boolean isNotifyResourceOnRejection() {
		return (Boolean)getCustomSettingListTypeValue(null, 'TMS__Notify_Resource_on_Rejection__c');
	}

	/**
	* @author Pandeiswari
	* @name disableExpenseStatus
	* @date 07/12/2013
	* @description Dispaly expense status trigger.
	* @return Boolean
	*/
	public static Boolean disableExpenseStatus() {
		return (Boolean)getCustomSettingListTypeValue(null, 'TMS__Disable_Expense_Status_update_in_Timeshe__c');
	}
	

	/**
	* @author Aliaksandr Satskou
	* @name isNotifyResourceOnRejection
	* @date 07/12/2013
	* @description Notify manager on timesheet rejection.
	* @return Boolean
	*/
	public static Boolean isNotifyManagerOnRejection() {
		return (Boolean)getCustomSettingListTypeValue(null, 'TMS__Notify_Manager_on_Rejection__c');
	}


	/**
	* @author Aliaksandr Satskou
	* @name getPayrollProviderSettingMap
	* @date 05/27/2014
	* @description Getting Map of dataset to Payroll Provider Setting.
	* @return Map<String, TMS__Payroll_Provider_Settings__c>
	*/
	public static Map<String, TMS__Payroll_Provider_Settings__c> getPayrollProviderSettingMap() {
		retrievePayrollProviderSettingsData();


		return dataSetNamePayrollProviderSettingMap;
	}


	public class StatusContainer {
		public String tmsOpen;
		public String tmsSubmitted;
		public String tmsApproved;
		public String tmsRejected;
		public String tmsCreated;

		public String expOpen;
		public String expSubmitted;
		public String expApproved;
		public String expRejected;


		public StatusContainer() {
			tmsOpen = getTimesheetSaveStatus();
			tmsSubmitted = getTimesheetSubmittedStatus();
			tmsApproved = getTimesheetApprovedStatus();
			tmsRejected = getTimesheetRejectedStatus();
			tmsCreated = getTimesheetCreatedStatus();
			expOpen = getExpenseSaveStatus();
			expSubmitted = getExpenseSubmittedStatus();
			expApproved = getExpenseApprovedStatus();
			expRejected = getExpenseRejectedStatus();
		}
	}
}