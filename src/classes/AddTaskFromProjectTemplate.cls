public class AddTaskFromProjectTemplate {

	/* Added by Aliaksandr Satskou, 04/18/2014 (case #00023506) */
	public String locale {
		get { return UserInfo.getLocale(); }
		private set;
	}


	List<TMS__Project__c> projectTemplateList;
	List<TMS__Task__c> templateTaskList;
	List<TMS__Task__c> projectTaskList = new List<TMS__Task__c>();
	List<TMS__Project_Resource__c> projectResourceListForCheckDuplicat = new List<TMS__Project_Resource__c>();
	//public List<Boolean> radioButtonList {get; set;}
	String projectTemplate;
	String pId;

	public String getProjectTemplate() {
		return projectTemplate;
	}

	public void setProjectTemplate(String projectTemplate) {
		this.projectTemplate = projectTemplate;
		//radioButtonList.add(radioButton);
	}

	public AddTaskFromProjectTemplate() {
		Map<String,Schema.RecordTypeInfo> recordTypes =
			TMS__Project__c.sObjectType.getDescribe().getRecordTypeInfosByName();

		pId = ApexPages.currentPage().getParameters().get('pId');
		System.debug('****pId = '+pId);
		projectTemplateList= [
								SELECT Id, Name
								FROM TMS__Project__c
								WHERE RecordTypeId = : recordTypes.get('Project Template').getRecordTypeId()];
	}

	public List<SelectOption> getRadioButtonItems() {
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('None','None'));
		for(TMS__Project__c projectTemplate : projectTemplateList) {
			options.add(new SelectOption(projectTemplate.Id, projectTemplate.Name));
		}
		return options;
	}

	public PageReference addTask() {
		projectTaskList = [
								SELECT Id, Name
								FROM TMS__Task__c
								WHERE TMS__Project__r.Id = :pId];

		projectResourceListForCheckDuplicat = [
								SELECT Id,TMS__Contact__r.Name
								FROM TMS__Project_Resource__c
								WHERE TMS__Project__r.Id = :pId];


		List<TMS__Project_Resource__c> projectResourceList = new List<TMS__Project_Resource__c>();
		List<TMS__Task__c> taskListForProject = new List<TMS__Task__c>();
		List<String> taskNameList = new List<String>();

		System.debug('*********amrender projectTemplate ='+projectTemplate);

		PageReference pageRef = new PageReference('/apex/TaskListInProject?id='+pId);
		//PageReference pageRef = new PageReference('/'+pId);

		if(projectTemplate != null && projectTemplate.length() > 0)
		{
			templateTaskList = [
								SELECT Id, Name, TMS__Allocated_Hours__c,
									TMS__Sequence__c, TMS__Project_Resource__c
								FROM TMS__Task__c
								WHERE TMS__Project__c = :projectTemplate];



			projectResourceList = [
								SELECT Id,Name,TMS__Bill_Rate__c,TMS__Project__c,TMS__Contact__r.Name
								FROM TMS__Project_Resource__c
								WHERE TMS__Project__c = : projectTemplate];
		}

		System.debug('*********amrender task list ='+ templateTaskList);
		System.debug('*********amrender resource list ='+ projectResourceList);

		Map<Id, Id> mapTempResToRealRes = new Map<Id, Id>();

		if (projectResourceList != null && projectResourceList.size() > 0)
		{
			Map<String, TMS__Project_Resource__c> projectResourceNameMap =
								new Map<String, TMS__Project_Resource__c>();

			for(TMS__Project_Resource__c projectResource : projectResourceListForCheckDuplicat)
				projectResourceNameMap.put(projectResource.TMS__Contact__r.Name, projectResource);

			List<TMS__Project_Resource__c> templateResourceListForProject =
								new List<TMS__Project_Resource__c>();
			List<TMS__Project_Resource__c> projectResourceListForProject =
								new List<TMS__Project_Resource__c>();
			for(TMS__Project_Resource__c  projectResourceTemplate :  projectResourceList)
			{
				templateResourceListForProject.add(projectResourceTemplate);
				if(projectResourceNameMap.get(projectResourceTemplate.TMS__Contact__r.Name) == null)
				{
					TMS__Project_Resource__c projectResourceNew = projectResourceTemplate.clone(false,false);
					projectResourceNew.TMS__Project__c = pId;
					projectResourceListForProject.add(projectResourceNew);
				}
				else
				{
					projectResourceListForProject.add(projectResourceNameMap.get(projectResourceTemplate.TMS__Contact__r.Name));
				}
			}
			upsert projectResourceListForProject;

			for (Integer i=0; i<templateResourceListForProject.size(); i++)
				mapTempResToRealRes.put(templateResourceListForProject[i].Id, projectResourceListForProject[i].Id);
		}

		if(templateTaskList != null && templateTaskList.size() > 0)
		{
			Set<String> projectTaskNames = new Set<String>();
			for (TMS__Task__c projectTask : projectTaskList)
				projectTaskNames.add(projectTask.Name);

			for(TMS__Task__c templateTask : templateTaskList)
			{
				if(!projectTaskNames.contains(templateTask.Name))
				{
					TMS__Task__c task = templateTask.clone(false, false);
					task.TMS__Project__c = pId;
					task.TMS__Project_Resource__c = mapTempResToRealRes.get(templateTask.TMS__Project_Resource__c);
					taskListForProject.add(task);
				}
			}

			insert taskListForProject;
		}

		System.debug('*********amrender task list new='+templateTaskList);

		System.debug('*********amrender  projectResourceList ='+ projectResourceList);

		return pageRef;
	}

	private static testMethod void tstAddTaskFromProjectTemplate() {

		Id projectTemplateRecordTypeId =
								Schema.SObjectType.TMS__Project__c.getRecordTypeInfosByName()
								.get('Project Template').getRecordTypeId();

		Id projectRecordTypeId =
								Schema.SObjectType.TMS__Project__c.getRecordTypeInfosByName()
								.get('Project').getRecordTypeId();

		TMS__Project__c testTMSProjectTeplate = new TMS__Project__c(Name = 'testTMSProjectTemplate',
								RecordTypeId = projectTemplateRecordTypeId);
		insert testTMSProjectTeplate;

		TMS__Project__c testTMSProject = new TMS__Project__c(Name = 'testTMSProject',
								RecordTypeId = projectRecordTypeId);
		insert testTMSProject;

		Contact testCon = new Contact(LastName = 'testCon', Email = 'test@test.test');
		insert testCon;

		TMS__Project_Resource__c testTMSProjRes = new TMS__Project_Resource__c(TMS__Bill_Rate__c = 10.00,
								TMS__Project__c = testTMSProjectTeplate.Id, TMS__Contact__c = testCon.Id);
		insert testTMSProjRes;

		TMS__Task__c testTMSTack = new TMS__Task__c(Name = 'testTMSTask', TMS__Allocated_Hours__c = 10.0,
								TMS__Sequence__c = '1', TMS__Project__c = testTMSProjectTeplate.Id,
								TMS__Project_Resource__c = testTMSProjRes.Id);
		insert testTMSTack;

		AddTaskFromProjectTemplate controller = new AddTaskFromProjectTemplate();

		controller.getProjectTemplate();
		controller.getRadioButtonItems();
		controller.setProjectTemplate(testTMSProjectTeplate.Id);
		controller.addTask();
	}
}