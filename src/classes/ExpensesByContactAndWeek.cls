public class ExpensesByContactAndWeek {
	private Id contactId;
	private Id weekId;
	private List<String> expenseRejectedStatus;
	
	public ExpensesByContactAndWeek(Id contactId, Id weekId) {
		this.contactId = contactId;
		this.weekId = weekId;
		String expenseEditableStaus = CustomSettingsUtility.getExpenseEditableStatus();
		expenseRejectedStatus = expenseEditableStaus != null 
								? expenseEditableStaus.split(';')
								: new List<String>();
	}

	public List<TMS__Expense__c> getAllExpenses() {
		/* Edited by Aliaksandr Satskou, 04/25/2013 (case #00016617) */
		List<TMS__Expense__c> allExpensesList = [
				SELECT Status__c, Comments__c, TMS__Project__c, TMS__Amount__c, TMS__Category__c,
					CreatedDate, TMS__Timesheet__c,TMS__Manager_Comments__c,
					(SELECT Name FROM Attachments)
				FROM TMS__Expense__c
				WHERE TMS__Week_Management__c = :weekId
				AND Contact__c = :contactId
				AND TMS__Status__c NOT IN: expenseRejectedStatus];

		Logger.addMessage('ExpensesByContactAndWeek', 'getAllExpenses', 'allExpensesList=' + allExpensesList);

		return allExpensesList;
	}
	
	public List<TMS__Expense__c> getRejectedExpenses() {
		List<TMS__Expense__c> allExpensesList = [
				SELECT Status__c, Comments__c, TMS__Project__c, TMS__Amount__c, TMS__Category__c,
					CreatedDate, TMS__Timesheet__c,TMS__Manager_Comments__c,
					(SELECT Name FROM Attachments)
				FROM TMS__Expense__c
				WHERE TMS__Week_Management__c = :weekId
				AND Contact__c = :contactId
				AND TMS__Status__c IN: expenseRejectedStatus];

		Logger.addMessage('ExpensesByContactAndWeek', 'getRejectedExpenses', 'allExpensesList=' + allExpensesList);

		return allExpensesList;
	}
}