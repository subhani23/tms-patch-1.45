public class SearchForHelper {
	private static Logger logger = new Logger('SearchForHelper');

	public static Object searchByFieldReturnSingle(
			List<sObject> recordsToSearch, String fieldName, Object fieldValue) {

		if (recordsToSearch != null) {
			for (sObject record : recordsToSearch) {
				Object key = getValue(record, fieldName);

				logger.log('searchByFieldReturnSingle', 'key', key);
				logger.log('searchByFieldReturnSingle', 'fieldValue', fieldValue);

				if (key == fieldValue) {
					return record;
				}
			}
		}

		return null;
	}
	
	private static Object getValue(sObject record, String keyField) {
		Object value;
		
		String[] keyLevels = keyField.split('\\.');
		
		for (Integer i = 0; i < keyLevels.size(); i++) {
			String keyLevel = keyLevels[i];
			
			if (i < keyLevels.size() - 1) {
				record = (sObject)record.getSObject(keyLevel);
			} else {
				value = record.get(keyLevel);
			}
		}
		
		return value;
	}
}