/**
* @author Aliaksandr Satskou
* @date 07/12/2013
* @description Test class for RejectionReasonController class.
*/
@isTest
private class RejectionReasonControllerTest {

	private static List<Contact> contactList;
	private static List<FCMS__Session__c> sessionList;
	private static List<TMS__Time__c> timeList;
	private static List<String> timeIdList;
	private static List<TMS__Week_Management__c> weekManagementList;


	/**
	* @name initData
	* @author Aliaksandr Satskou
	* @data 07/12/2013
	* @description Init all needed data.
	* @return void
	*/
	private static void initData()
	{
		/*
		EmailTemplate template = new EmailTemplate();
		template.name = 'Notify Timesheet Rejection';
		template.subject = 'Timesheet rejected';
		template.developerName = 'NotifyTimesheetRejection';
		template.folderId = UserInfo.getUserId();
		template.templateType = 'Text';
		insert template;

		template = new EmailTemplate();
		template.name = 'Notify Timesheet Rejection 2';
		template.subject = 'Timesheet rejected 2';
		template.developerName = 'NotifyTimesheetRejection2';
		template.folderId = UserInfo.getUserId();
		template.templateType = 'Text';
		insert template;
		*/


		TMS__TimesheetCustomSettings__c settings = new TMS__TimesheetCustomSettings__c();
		settings.TMS__Notify_Resource_on_Rejection__c = true;
		settings.TMS__Notify_Manager_on_Rejection__c = true;
		settings.TMS__Enable_Timesheet_Object__c = true;
		settings.name = 'Default';
		insert settings;

		contactList = new List<Contact> {
			new Contact(
				LastName = 'Contact #1',
				Email = 'test@test.com'
			)
		};
		insert contactList;

		sessionList = new List<FCMS__Session__c> {
			new FCMS__Session__c(
				FCMS__Session_For__c = contactList[0].Id,
				FCMS__Is_Valid__c = true,
				FCMS__SessionId__c = '86319643624873'
			)
		};
		insert sessionList;

		timeList = new List<TMS__Time__c> {
			new TMS__Time__c(
				TMS__Time_Spent__c = 2.4,
				TMS__Status__c = 'Status #1',
				TMS__Comments__c = 'Comments #1'
			),
			new TMS__Time__c(
				TMS__Time_Spent__c = 4.2,
				TMS__Status__c = 'Status #2',
				TMS__Comments__c = 'Comments #2'
			)
		};
		insert timeList;

		weekManagementList = new List<TMS__Week_Management__c> {
			new TMS__Week_Management__c(
				TMS__Start_Date__c = Date.today(),
				TMS__End_Date__c = Date.today() + 6,
				TMS__Active__c = true
			)
		};
		insert weekManagementList;
	}


	static testmethod void testRejectTimeEntries()
	{
		initData();
		RejectionReasonController.rejectTimesheets(new List<Id>{contactList[0].id}, weekManagementList[0].Id,'test');
	}

	static testmethod void testRejectTimesheets()
	{
		initData();
		RejectionReasonController.rejectTimeEntries(new List<Id>{timeList[0].id}, 'base string');
	}

	static testmethod void testGetEmailTemplate2()
	{
		initData();

		FCMS__CMSSiteSetup__c cmsSiteSetup = new FCMS__CMSSiteSetup__c();
		cmsSiteSetup.FCMS__Name__c = 'Reject Timesheet Template Name';
		cmsSiteSetup.FCMS__Value__c = 'Notify Timesheet Rejection';
		insert cmsSiteSetup;

		EmailTemplate template = RejectionReasonController.getEmailTemplate();
		System.assertEquals(template.Name, 'Notify Timesheet Rejection');
	}

	static testmethod void testGetEmailTemplate1()
	{
		initData();

		EmailTemplate template = RejectionReasonController.getEmailTemplate();
		System.assertEquals(template.Name, 'Notify Timesheet Rejection');
	}

	static testmethod void testSendEmail()
	{
		initData();

		EmailTemplate template = RejectionReasonController.getEmailTemplate();
		timeIdList = new List<String>();
		timeIdList.add(''+ timeList[0].Id);
		RejectionReasonController.sendEmail(contactList, timeIdList, template, weekManagementList[0].Id, 'base string', 'test@gmail.com');
	}

	static testmethod void testSubmitAndSend()
	{
		initData();

		PageReference pageReference = Page.RejectionReason;
		pageReference.getParameters().put('wId', weekManagementList[0].Id);
		pageReference.getParameters().put('timeIds', timeList[0].Id);
		pageReference.getParameters().put('sessionId', sessionList[0].FCMS__SessionId__c);
		pageReference.getParameters().put('projectResourceIds', contactList[0].Id);

		Test.setCurrentPageReference(pageReference);
		RejectionReasonController controller = new RejectionReasonController();
		controller.getBaseString();
		controller.setBaseString('Base String');
		controller.submit();
	}


	static testmethod void testSubmitWithNoNotify()
	{
		initData();

		contactList[0].email = null;
		update contactList;

		PageReference pageReference = Page.RejectionReason;
		pageReference.getParameters().put('wId', weekManagementList[0].Id);
		pageReference.getParameters().put('timeIds', timeList[0].Id);
		pageReference.getParameters().put('sessionId', sessionList[0].FCMS__SessionId__c);
		pageReference.getParameters().put('projectResourceIds', contactList[0].Id);

		Test.setCurrentPageReference(pageReference);
		RejectionReasonController controller = new RejectionReasonController();
		controller.getBaseString();
		controller.setBaseString('Base String');
		controller.submit();
	}
}